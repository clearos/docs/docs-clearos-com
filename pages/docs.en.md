---
title: Introduction to ClearOS
published: true
taxonomy:
    category: docs
---

Welcome to the ClearOS Documentation.

ClearOS is an Open-Source, Linux-based OS platform that is designed from the ground up to be extensible. ClearOS provides any combination of network, gateway and server functionality to small/medium sized business, non-profit organizations, education/government and distributed enterprise.

Supporting best-in-class open source and 3rd party applications through one secure, intuitive web-based interface, ClearOS is about choice - giving administrators the ability to run services on-premise or in the cloud, or both - "Hybrid Technology".

* [Introduction to ClearOS](introduction)
 * What is ClearOS and how is it useful.
* [ClearOS Development](developer-documentation)
 * See something missing or something that needs fixing. Here are the steps to contributing to ClearOS open source or to making your own paid applications.
* [Installing ClearOS](installing-clearos)
 * How to install ClearOS and get it working for you.
* [ClearCenter Marketplace Applications](applications)
 * Installing the applications and services that you need.
* [Howtos and Best Practices](howtos)
 * Guides, walkthroughs, howtos, and best practices for ClearOS.

### UPDATES
**NOTE**: This documentation is currently under heavy modifications. To learn more or to help with this migration please see the [Migration Plan document](introduction/migration).
