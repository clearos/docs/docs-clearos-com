---
title: Migration Plan for Documentation
published: true
taxonomy:
    category: docs
---

The ClearOS documentation on www.clearos.com is based on an wiki standard that is peculiar to Dokuwiki. When 'git' was chosen as the new, default standard for source change control, the advantages of Markdown became apparent.

This site is based on Markdown and Grav. The scope of the Migration Plan is to move documentation from Dokuwiki format to Grav.

The core of the existing documentation is contained in a backup on gitlab:

https://gitlab.com/clearos/docs/old-docs

Review the current steps for migrating documents here:

https://gitlab.com/clearos/docs/old-docs/blob/master/README.md
