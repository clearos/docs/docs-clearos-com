---
title: Resources
media_order: 'gitlab_edit.png,gitlab_commit.png,gitlab_clone.png'
taxonomy:
    category:
        - docs
visible: true
---

ClearOS resources are organized into 5 main sections:
1. User and Administrator Documentation
2. Developer Documentation
3. Knowledge Base / How-to (Wiki) Guides
4. Community Forums
5. Bug Tracking

## User and Administrator Documentation

The sections below provide links to documentation specific for the ClearOS version.  If you would like to contribute to the User and Administrator Documentation, you will need to create a ClearFoundation account [here](https://www.clearos.com/community/my-profile/my-community-dashboard/registration/steps/1).  Send your account username to developer@clearfoundation.com requesting access to make edits.  Once done, you can head over to the documentation/Wiki editor [here](https://www.clearos.com/dokuwiki2/doku.php?id=start).

### ClearOS 7
User and Administration documentation for ClearOS 7 can be found [here](https://www.clearos.com/resources/documentation/clearos/index:userguide7).

### ClearOS 6
User and Administration documentation for ClearOS 6 can be found [here](https://www.clearos.com/resources/documentation/clearos/index:userguide6).

## Developer Documentation
Unlike the user/admin docs, the developer documentation is managed by a Git repository in [GitLabs](https://about.gitlab.com/).
### Getting Started
Regardless of how small or big a contribution you are making, you'll first need to create (if you don't have one already) a GitLab account [here](https://gitlab.com/users/sign_in).

You will also need to become somewhat familar with [Markdown](https://www.markdownguide.org/).  Don't worry...Markdown makes writing documentation easy and there's even a WYSIWYG editor that makes writing documenation as easy as writing an email in Gmail.

### Branches
There are two main branches in the developer documentation:
1. Master - the master branch is what is shown publically at [https://docs.clearos.com](https://docs.clearos.com).  When a change or merge request is accepted to the master branch, the documentation will automatically sync to where we host the website and everyone will see the change(s) made.  Not everyone can commit to the master branch.  If you start to make contributions and have been 'vetted' by the project's lead, we will happily add your GitLab account to become a member so that you may commit directly to the master branch.  Once a member, you won't have to wait around for commits to be accepted.
2. Public - the public branch is open commits from anyone with a GitLab account.  Commits made to this branch need to be approved by a developer who is a member of the developer docs project.  Once accepted, your pull request will be merged with the master and appear on the documentation website for everyone to see.
### Pull Requests
If you're making a relatively minor change like fixing a grammatical error or typo, the simplest thing to do is make the change in GitLab directly. The documentation project lives here.

You can make a pull request right on the GitLab site if you like.
1. Login to your GitLab account
2. Navigate to the [base developer docs](https://gitlab.com/clearos/docs/Developer-Markdown/tree/public/pages)
3. Browser through the folder structure to find the page you want to edit

![Editing in GitLab](gitlab_edit.png)

4. Provide a comment and click on **Commit changes**.

![Committing in GitLab](gitlab_commit.png)

### Cloning the Repo

If you'll be making more than a simple change or like working locally, best pratice would be to clone the repo.  Navigate in your browser to the project base for the Developer Markdown project found [here](https://gitlab.com/clearos/docs/Developer-Markdown).

Select SSH or HTTPS (if you have setup public/private keys, use SSH - otherwise use HTTPS) and copy the URL to your clipboard using the clipboard icon

![Cloning a Project](gitlab_clone.png)

On your desktop, ensure you have git installed, and run (example provided is using SSH keys):

    git clone git@gitlab.com:clearos/docs/Developer-Markdown.git
    
Change directory into the Developer-Markdown newly created folder and make sure you have the appropriate branch (master or public) selected.  Edit the markdown text as required and commit your changes:

    git commit -a
    
Finally, push your commits to the remote branch:

    git push

### Hosting a Clone of the Developer Docs
If you work on the docs all the time and would to use the markdown editor provided by the Grav framework, you should checkout the devoloper docs framework.

! You'll need either a ClearOS or CentOS install to use the installation as decribed below.

Make sure git is installed on your server:

    yum -y install git
    
Change directory to a location where you'd like to host the document root.  It could be /var/www/html or simply a user's home directory.

!!! If you are using a user's home directory (eg. /home/<username>) you will find the permissions are too restrictive for hosting Apache docs without running: chmod 755 /home/<username>

Checkout the Developer documentation framework:

    git clone https://gitlab.com/clearos/docs/clearos-developer-docs.git

Change to the folder of the project you just checked out:

    cd clearos-developer-docs
    
Run the bootstrap/setup file and answer the questions (or accept defaults).  The script must be run as root, so either change to the root user or prefix the command with 'sudo':

    ./bin/setup

Configure a local IP to map docs.clearos.lan to the server's IP.  On a ClearOS installed, add (or modify) an entry in the <code>/etc/hosts</code> file:

    192.168.11.1  docs.clearos.lan
    systemctl restart dnsmasq.service
    nscd -i hosts

!!! If you don't feel like mapping a domain, you can access the resource on alternate port 8095

In your browser, navigate to:

    https://docs.clearos.lan
    
or

    https://<IP>:8095
    
Where IP is your server's IP address and you have not configured/setup the docs.clearos.lan alias.
    
On first hitting the page, it will redirect you to the admin route to configure a user account.

Once logged into the Grav admin portal, navigate to Plugins -> Git Sync.  Run through the wizard, adding your GitLab credentials and using the Git Repository:

    https://gitlab.com/clearos/docs/Developer-Markdown.git
    
! If you do not yet have write access to the master branch, you should put **public** as the name in both local and remote branch names.

Do not worry about the last step for the webhook...it is already configured on the GitLab project.

## Knowledge Base / Howto (Wiki) Guides
## Community Forums
## Bug Tracking