---
title: Raid Support Overview
visible: true
taxonomy:
    category: docs
---

Software, BIOS-based, and hardware RAID (redundant array of independent disks) have supported methods in ClearOS. It is important to understand the benefits and deficiencies of different types of RAID if you are going to implement RAID with ClearOS.

The following table reviews RAID categories and their relative strengths and weaknesses compared to other RAID types on the list. If a formula is given, N is the number of drives that you are using. **Tolerance** is a relative measure of the number or fraction or percentages of the disks in the array that can fail before the dataset is lost (the higher, the better.) **Utilization** is a measure or percentage of drives that are utilized for real data. **Support** indicates whether Hardware, Software, or BIOS-based RAID is supported for the RAID level listed. **Perf** (Performance) lists a relative speed of performance in comparison to other RAID sets.  **Bootable** demonstrates if the configuration is bootable. **Comments** lists some helpful hits.

| RAID Level | Tolerance | Utilization | Support | Perf | Bootable | Comments |
| ----------- | ----------- | ----------- | ----------- | ----------- | ----------- | ----------- |
| 0 | 0 | 100% | HW, SW, BIOS | Fastest | HW-yes, SW-no | Good if you don't care about your data (ie. caches) |
| 1 | 1 | 50% or less | HW, SW, BIOS | Fast | yes | As fast as a single disk, can use multiple disks for extra tolerance. Good but expensive solution |
| 5 | 1 | N-1/N | HW, SW, BIOS(rare) | Slow | yes † | Maximizes utilization in exchange for performance |
| 6 | 2 | N-2/N | HW,SW | Slower | yes † | Maximizes utilization in exchange for performance. More tolerant than RAID 5. |
| 10 | 1 to N/2 | N/2 * | HW, SW, BIOS | Fast | yes | Performance of RAID 0 with redundancy of RAID 1. Often the same as RAID 1 or represented as RAID 1+0. Expensive. |
| 50 | 1 to N/2 | (N/2)-1/N  * | HW | Slow | yes † | Faster and more expensive than RAID 5 |

| RAID Level | Tolerance |
| ----------- | ----------- |
| Hybrid | Hybrid levels include support for things like mirroring a RAID 6 array (RAID 60) or wacky things like a mirror of a RAID 5 that are comprised of RAID 6 |

*Requires an even number of drives

†Requires GRUB2 for SW boot

‡There may be ways to overcome this limitation but it is technically difficult

## Selecting RAID Technology
There are advantages and disadvantages to each of the technologies that you can select for RAID. You should choose the one that best fits your needs.

### Advantages
* **Hardware RAID** The RAID array is maintained independent of running OS. Hardware RAID typically has options that allow for various features upgrades including network interfaces for outage notification, battery backup for better resiliency to pending write operations during a power failure, and more. Hardware RAID cards are generally flexible in the RAID levels they support. Performance is generally very fast.
* **BIOS RAID** Cheap. Many desktop board include BIOS RAID as a built-in functionality.
* **Software RAID** Flexible and Fast. It also has the ability to be controller-agnostic. This is beneficial if your disk controller for your hard drives fails or your server goes south altogether. Software RAID doesn't care about how the drive is served up, only that it can see the data. Software RAID is also capable of setting up RAID at the partition level which means you can have different types of RAID all on the same disk. For example you can have RAID 5 for a bulk of your disk and RAID 0 for your caching all on the same spindles.

### Disadvantages
* **Hardware RAID** Costly. Some hardware RAID solutions require that you match the controller and/or the firmware level if there is ever a controller failure.
* **BIOS RAID** Traditionally, support for BIOS RAID has been problematic (dmraid). While these issues have largely been solved, some older and exotic devices may not work at all. In the case of HPE DSA BIOS-RAID, it is not supported at all.
* **Software RAID** Configuration and recovery of software RAID can have a high learning curve.

## Hardware RAID Support
True hardware RAID cards can be a really good thing and save your butt or it can be a nightmare. It is essential that if you are using hardware RAID, you are well-trained on how to support your hardware. Support for the various hardware RAID cards under ClearOS can be spotty and limited. Generally speaking, if your hardware RAID card manufacturer has support for RedHat®, Fedora, or CentOS® then you will increase the odds that it will work well with ClearOS.

It is recommended that if you use hardware RAID, you familiarize yourself with the methods and procedures of recovering your RAID in event that your RAID controller fails. This is critical because if you are not prepared beforehand, you can waste a lot of time searching for answers, some of which may be destructive to your data. Things like mismatched firmware with replacement RAID controllers can lead to extensive downtime and even loss of data. You don't want your RAID card to increase the likelihood that you lose your data so be prepared and put together the notes and information on how to recover from such a potential in a safe place.

You will want to install the Linux tools for your RAID card after the completion of your ClearOS installation in order to get the management and notification abilities that come with your RAID card. As mentioned previously, you are likely on your own for the implementation and you should seek guidance from your hardware vendor for support under ClearOS. If you are using HPE RAID cards approved on the compatibility matrix for ClearOS (not DSA, BIOS RAID) then you can contact ClearOS Support professionals for assistance if your subscription entitles you to support.

Some hardware RAID controller cards are not true hardware controller cards. If your configuration for RAID is in your regular computer BIOS, please refer to the BIOS RAID section. From the 'Managing RAID on Linux' book from O'Reilly.

> "The low-end (RAID) controllers are, in essence, software RAID support controllers because they rely on the operating system to handle RAID support operations and because they store array configuration information on individual component disk.  The real value of the controller is in the extra ATA channels."

If you have a hardware RAID controller and still would like to use software RAID, configure your RAID card for JBOD or set up individual disks as stand-alone RAID 0 devices (JBOD is better). Not only will you get better support, but potentially more performance and ability to transition to another controller without worrying about firmware matching.

### AVOID THESE CARDS
Unsupported and not recommended

* Some Promise hardware, notably FastTrak100 TX and FastTrak TX2000
* HighPoint RocketRAID cards

As a rule of thumb, if a hardware card is under USD $150, then it is probably not true hardware RAID and either not recommended or not supported.

## BIOS RAID Support
BIOS RAID support is accomplished by configuring a RAID set in the BIOS. ClearOS will see the disks using the 'dmraid' drivers. Our recommendation is to use Software RAID instead of BIOS RAID since the success rate is much higher for implementation and recovery for software RAID from a ClearOS support model.

## Software RAID Support
You can implement software RAID in ClearOS by selecting the **I will configure partitioning** option in the installation wizard.

![7_install_select_custom_partitioning.png](./7_install_select_custom_partitioning.png "Custom Partitioning")

You can find tips and tricks about partitioning in the section [Configuring Partitions and RAID](../../installer/full-install-iso/configuring-partitions-and-raid)

## RAID Support Links
* [ATA and SATA Technical Guide For RAID Support](https://ata.wiki.kernel.org/index.php/SATA_hardware_features)
