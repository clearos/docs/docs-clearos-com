---
title: Preparation
visible: true
taxonomy:
    category: docs
---

## Overview of Preparation Tasks
This section will help you prepare for an installation of ClearOS. The primary topics are:

* [What is ClearOS?](./what-is-clearos)
 * Describes what ClearOS can provide to your environments and what problems it solves.
* [System Requirements](./system-requirements)
 * Reviews what hardware you will need to get started with ClearOS.
* [RAID Support Overview](./raid-support-overview)
 * Talks about disk and hardware options that will make your storage more resilient to damage or failure using a Redundant Array of Independent Disks (RAID).
* [Compatibility](compatibility)
 * Provides resources to understand if your hardware is compatible with ClearOS.
* [Downloading ClearOS](downloading-clearos)
 * Where to get ClearOS or ClearOS Installer options.
