---
title: 'Converting CentOS 7 to ClearOS 7'
published: true
taxonomy:
    category:
        - docs
---

# Prerequisites
You will need to start with a sparse install of CentOS available. For virtualization providers, find a very minimal instance type to use.

There are two different conditions where it should be possible to concert Centos to ClearOS:
1. If the Centos version is the same as the ClearOS version in the os repo. You can check the clearos-release file in http://mirror1-newyork.clearos.com/clearos/7/os/x86_64/Packages/. If the first two digits of the version are the same or greater than the version of Centos `cat \etc\centos-release` then use the first update method.
1. If the version of ClearOS in the os repo is behind the Centos version, then if the version in the clearos-updates repo is the same as the Centos version then use the second method (a slight tweak to the first). You can check the clearos-release file in https://mirror1-newyork.clearos.com/clearos/7/updates/x86_64/RPMS/.
1. If the Centos version is ahead of the ClearOS version in clearos-updates then you cannot convert Centos to ClearOS

## ClearOS Version in os is the same as Centos
```
sudo su - # unnecessary if you are already running as root
yum -y remove NetworkManager
# Set SELinux to disabled
sed -i 's/=enforcing/=disabled/' /etc/selinux/config
setenforce 0
```
### ClearOS Home and Business
It can be difficult to get business installed on a CentOS system that comes via a virtual platform like AWS because many of them run updates which will give you a very advanced version and depending on the update cycle, you may not be able to update unless you enable repos. When stuck, find the equivalent step in ClearOS community and run yum with --enablerepo=clearos-updates,clearos-centos and possibly, clearos-centos-updates.

### ClearOS Community
```
mkdir -p /tmp/rpms
cd /tmp/rpms/
curl -LO http://mirror1-newyork.clearos.com/clearos/7/os/x86_64/Packages/$(curl -s http://mirror1-newyork.clearos.com/clearos/7/os/x86_64/Packages/|grep -oh system-base-[0-9]+*.*.rpm\"|grep -oh system-base-[0-9]+*.*.rpm)
curl -LO http://mirror1-newyork.clearos.com/clearos/7/os/x86_64/Packages/$(curl -s http://mirror1-newyork.clearos.com/clearos/7/os/x86_64/Packages/|grep -oh clearos-release-[0-9]+*.*.rpm\"|grep -oh clearos-release-[0-9]+*.*.rpm)
curl -LO http://mirror1-newyork.clearos.com/clearos/7/os/x86_64/Packages/$(curl -s http://mirror1-newyork.clearos.com/clearos/7/os/x86_64/Packages/|grep -oh app-base-core-[0-9]+*.*.rpm\"|grep -oh app-base-core-[0-9]+*.*.rpm)
curl -LO http://mirror1-newyork.clearos.com/clearos/7/os/x86_64/Packages/$(curl -s http://mirror1-newyork.clearos.com/clearos/7/os/x86_64/Packages/|grep -oh app-base-[0-9]+*.*.rpm\"|grep -oh app-base-[0-9]+*.*.rpm)
rpm -ivh --nodeps app-base-*
rm -rf /var/cache/yum/
rpm -ivh --nodeps system-base-*.rpm
yum install clearos-release-*.rpm
yum reinstall app-base-*
rm -rf /var/cache/yum/
yum reinstall system-base
yum reinstall clearos-release
rm -rf /var/cache/yum/
rm -f *.rpm
systemctl stop webconfig
```

### Set the root password (should be unnecessary if it is already set)
```
passwd
```

### Run updates to basics and fixup nameserver then finish updates
```
# Run updates to basics and fixup nameserver then finish updates
yum -y update app-base
rm -rf /var/cache/yum/ && yum -y update
rm -rf /var/cache/yum/
echo "nameserver 8.8.8.8" > /etc/resolv.conf
yum -y install app-accounts app-configuration-backup app-dashboard app-date app-dns app-edition app-events app-incoming-firewall app-groups app-language app-log-viewer app-mail app-marketplace app-process-viewer app-software-updates app-ssh-server app-support app-user-profile app-users --enablerepo=clearos-epel
systemctl restart syswatch
allow-port -p TCP -d 22 -n SSH
allow-port -p TCP -d 81 -n Webconfig
systemctl start webconfig
systemctl enable webconfig

# Needed on Linode or possibly other installations, but not when converting from a centos iso install in a vm
# In the Linode Console > Disks/Configs edit the current configuration profile and turn off "autoconfigure networking" then:
#### Begin Linode bit
echo "DEVICE=eth0" > /etc/sysconfig/network-scripts/ifcfg-eth0
echo "TYPE=\"Ethernet\"" >> /etc/sysconfig/network-scripts/ifcfg-eth0
echo "ONBOOT=\"yes\"" >> /etc/sysconfig/network-scripts/ifcfg-eth0
echo "USERCTL=\"no\"" >> /etc/sysconfig/network-scripts/ifcfg-eth0
echo "BOOTPROTO=\"dhcp\"" >> /etc/sysconfig/network-scripts/ifcfg-eth0
echo "PEERDNS=\"no\"" >> /etc/sysconfig/network-scripts/ifcfg-eth0

echo "nameserver 8.8.8.8" > /etc/resolv-peerdns.conf
echo "nameserver 8.8.4.4" >> /etc/resolv-peerdns.confreboot
#### End Linode bit

reboot
```

## ClearOS version in os is behind Centos but version in clearos-updates is the same
```
sudo su - # unnecessary if you are already running as root
yum -y remove NetworkManager
sed -i 's/=enforcing/=disabled/' /etc/selinux/config
setenforce 0

mkdir -p /tmp/rpms
cd /tmp/rpms/
curl -LO https://mirror1-newyork.clearos.com/clearos/7/updates/x86_64/RPMS/$(curl -s https://mirror1-newyork.clearos.com/clearos/7/updates/x86_64/RPMS/|grep -oh system-base-[0-9]+*.*.rpm\"|grep -oh system-base-[0-9]+*.*.rpm)
curl -LO https://mirror1-newyork.clearos.com/clearos/7/updates/x86_64/RPMS/$(curl -s https://mirror1-newyork.clearos.com/clearos/7/updates/x86_64/RPMS/|grep -oh clearos-release-[0-9]+*.*.rpm\"|grep -oh clearos-release-[0-9]+*.*.rpm)
curl -LO https://mirror1-newyork.clearos.com/clearos/7/updates/x86_64/RPMS/$(curl -s https://mirror1-newyork.clearos.com/clearos/7/updates/x86_64/RPMS/|grep -oh app-base-core-[0-9]+*.*.rpm\"|grep -oh app-base-core-[0-9]+*.*.rpm)
curl -LO https://mirror1-newyork.clearos.com/clearos/7/updates/x86_64/RPMS/$(curl -s https://mirror1-newyork.clearos.com/clearos/7/updates/x86_64/RPMS/|grep -oh app-base-[0-9]+*.*.rpm\"|grep -oh app-base-[0-9]+*.*.rpm)
rpm -ivh --nodeps app-base-*
rm -rf /var/cache/yum/
rpm -ivh --nodeps system-base-*.rpm
yum install clearos-release-*.rpm
yum reinstall app-base-* --enablerepo=clearos-centos,clearos-centos-updates
rm -rf /var/cache/yum/
yum reinstall system-base
yum reinstall clearos-release
rm -rf /var/cache/yum/
rm -f *.rpm
systemctl stop webconfig

# Set a root password so that you can login to Webconfig. may already be done so should be unnecessary
passwd

# Run updates to basics and fixup nameserver then finish updates
yum -y update app-base # may find nothing to do
rm -rf /var/cache/yum/ && yum -y update --enablerepo=clearos-centos,clearos-centos-updates
rm -rf /var/cache/yum/
echo "nameserver 8.8.8.8" > /etc/resolv.conf
yum -y install app-accounts app-configuration-backup app-dashboard app-date app-dns app-edition app-events app-incoming-firewall app-groups app-language app-log-viewer app-mail app-marketplace app-process-viewer app-software-updates app-ssh-server app-support app-user-profile app-users --enablerepo=clearos-epel
systemctl restart syswatch
allow-port -p TCP -d 22 -n SSH
allow-port -p TCP -d 81 -n Webconfig
systemctl start webconfig
systemctl enable webconfig

# Needed on Linode or possibly other installations, but not when converting from a centos iso install in a vm
# In the Linode Console > Disks/Configs you may want to edit the current configuration profile and turn off "autoconfigure networking". Then:
#### Begin Linode bit
echo "DEVICE=eth0" > /etc/sysconfig/network-scripts/ifcfg-eth0
echo "TYPE=\"Ethernet\"" >> /etc/sysconfig/network-scripts/ifcfg-eth0
echo "ONBOOT=\"yes\"" >> /etc/sysconfig/network-scripts/ifcfg-eth0
echo "USERCTL=\"no\"" >> /etc/sysconfig/network-scripts/ifcfg-eth0
echo "BOOTPROTO=\"dhcp\"" >> /etc/sysconfig/network-scripts/ifcfg-eth0
echo "PEERDNS=\"no\"" >> /etc/sysconfig/network-scripts/ifcfg-eth0

echo "nameserver 8.8.8.8" > /etc/resolv-peerdns.conf
echo "nameserver 8.8.4.4" >> /etc/resolv-peerdns.confreboot
#### End Linode bit

reboot
```


## Appendix: Original StackScript from Linode
```
#!/bin/bash -x

(
ARCH=`arch`

# Prep release and repos
rpm -Uvh http://download2.clearsdn.com/marketplace/cloud/7/noarch/clearos-release-7-current.noarch.rpm
rpm --import /etc/pki/rpm-gpg/RPM-GPG-KEY-ClearOS-7

# Install and upgrade
yum --enablerepo=* clean all
yum --enablerepo=clearos-centos -y install app-base
yum --enablerepo=* clean all
service webconfig stop
yum --enablerepo=clearos-centos -y install app-accounts app-configuration-backup app-dashboard app-date app-dns app-edition app-events app-incoming-firewall app-groups app-language app-log-viewer app-mail app-marketplace app-process-viewer app-software-updates app-ssh-server app-support app-user-profile app-users

# Default networking
yum -y remove NetworkManager
echo "DEVICE=eth0" > /etc/sysconfig/network-scripts/ifcfg-eth0
echo "TYPE=\"Ethernet\"" >> /etc/sysconfig/network-scripts/ifcfg-eth0
echo "ONBOOT=\"yes\"" >> /etc/sysconfig/network-scripts/ifcfg-eth0
echo "USERCTL=\"no\"" >> /etc/sysconfig/network-scripts/ifcfg-eth0
echo "BOOTPROTO=\"dhcp\"" >> /etc/sysconfig/network-scripts/ifcfg-eth0
echo "PEERDNS=\"no\"" >> /etc/sysconfig/network-scripts/ifcfg-eth0

echo "nameserver 8.8.8.8" > /etc/resolv-peerdns.conf
echo "nameserver 8.8.4.4" >> /etc/resolv-peerdns.conf

sed -i -e 's/^GATEWAYDEV=.*/GATEWAYDEV="eth0"/' /etc/sysconfig/network
sed -i -e 's/^EXTIF=.*/EXTIF="eth0"/' /etc/clearos/network.conf

service syswatch restart

# Enable firewall
allow-port -p TCP -d 22 -n SSH
allow-port -p TCP -d 81 -n Webconfig
sed -i -e 's/^MODE=.*/MODE="standalone"/' /etc/clearos/network.conf

# Start webconfig
service webconfig start

) 2>&1 | tee /var/log/clearos-installer.log

# Reboot
reboot
```
