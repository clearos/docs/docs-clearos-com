---
title: 'Upgrading ClearOS6.x to ClearOS 7.x'
published: true
taxonomy:
    category:
        - docs
---

## 1. Upgrading from ClearOS 6.x to ClearOS 7.x

This guide contains an overview of the requirements and tasks associated with migrating from ClearOS 6.x to ClearOS 7.x. There are a number of changes which make ClearOS difficult to go from 6 to 7.

## 2. Inline Upgrade Not Possible

There is no supported method for upgrading directly from ClearOS 6 to ClearOS 7.  A fresh install of ClearOS 7 is required.  Configuration data can be migrated, but all other data will need to be manually moved to ClearOS 7.  It is imperative that you make a backup of your data to ensure no data is lost in the transition.

The steps for backing up configuration data are found below.

## Configuration Backup 

To upgrade the configuration files from ClearOS 6 to ClearOS 7 use the following steps: 

### ClearOS 6 
 
  - Login to the ClearOS web-based interface, 
  - Go to 'System' > 'Configuration Backup' in the menu 
  - Create backup file 
  - Download backup file 

### ClearOS 7 

<note warning>At the moment there is a problem restoring an Active Directory Connector configuration into a fresh installation ClearOS7. In order for it to work correctly you **must** install the Windows Networking (Samba) app from the Marketplace **before** you do the configuration restore. If you do not do this you will need to reinstall from scratch. Installing the Windows Networking (Samba) app after a config restore then doing another config restore does not fix the problem.  
You may then need to start winbind and set it to restart automatically on boot:  
```
systemctl start winbind.service
systemctl enable winbind.service
```

Remember to rename the machine in IP settings while both the old and new machine are on the network as Active Directory will not like two machines of the same name joined to the domain at the same time.

If you still have problems please consult the [Active Directory Connector Troubleshooting](https://documentation.clearos.com/content:en_us:kb_troubleshooting_the_ad_connector) guide, especially the "wbinfo -t" section.</note>

  - Install a ClearOS 7 system
  - Go through the install wizard after first boot
  - Do not select any Marketplace apps during the install wizard (but see warning above)
  - Go to 'System' > 'Backup' > 'Configuration Backup' in the menu and restore the ClearOS 6 backup on the ClearOS 7 system.

All configuration settings will get migrated except the following:
  * Network settings
  * DHCP server settings (leases are preserved)
  * 1-to-1 NAT firewall rules
  * RADIUS settings
  * ibVPN

## 3. Upgrade by Technology 
<note warning>This document is still in progress; if your section does NOT yet exist, please contact support. Additionally, the information here is provided As Is without any warranty. Be sure to have a solid backup of your data or even better, use a different drive or system for your new ClearOS 7 system.</note>

### System 

### Server 
#### Mail 
There are 3 different mail systems currently on ClearOS6.

  * Zarafa ZCP (Zarafa Collaboration Platform)
  * Google Apps
  * Cyrus IMAP and POP server

In ClearOS7 there are 4 different mail systems available:

  * Zarafa ZCP (Zarafa Collaboration Platform), Community support only.
  * Google Apps
  * Cyrus IMAP and POP server
  * Kopano (a full Groupware package), Commercial support available

The migration of mail services is can be a difficult transition if you don't have a plan. If you just use ClearOS for POP3 or IMAP, then this can be an easy transition for your users. The method of the transition depends on what you want to transition to:

##### Zarafa 
[Here is an extensive guide for Zarafa migrations](https://documentation.clearos.com/content:en_us:kb_howtos_upgrading_zarafa_6_to_7). Please use this guide to migrate to Zarafa. You can also use the imapsync method detailed in the "Cyrus IMAP and POP server" section.

##### Zarafa to Kopano 
It is possible to upgrade from ClearOS7/Zarafa to ClearOS7/Kopano following the [Zarafa to Kopano Upgrade HowTo](https://documentation.clearos.com/content:en_us:7_kb_zarafa_to_kopano_upgrade) once you have upgraded from ClearOS6/Zarafa to ClearOS7/Zarafa. Alternatively you can use the imapsync method detailed in the "Cyrus IMAP and POP server" section to upgrade directly from ClearOS6/Zarafa to ClearOS7/Kopano.

##### Google Apps 
If you are migrating to Google Apps, you will want to use a 2 machine approach so that you can stub in and create all of your users and structure on Google Apps before moving any user data. The key here is to not pull the trigger on the MX record change until the Google Apps integration is all functioning and test. Then switch the MX records. You will need to enable IMAP functionality under each user to move the data. Next, using their client's existing mail client, add the Google apps IMAP account.

[This guide from Google](http://support.google.com/a/bin/answer.py?hl=en&answer=105694) covers how to do this.

##### Cyrus IMAP and POP server 
With Cyrus, you have two options. Since Cyrus is what is running on 7.x, migrating the data to 7.x is a possibility. There are two approaches, imapsync and the whole database.

**imapsync:**
This can be run incrementally so you can do the bulk of the transfer some time before you shut down the old server and run it again during the run up to shutting down the server. For this you need all the users passwords. To run the command from the new server while it is connected to your LAN but not active, run the following for each user:
```
imapsync --host1 {your_6.9_server_IP} --user1 {username} --password1 {username's_password} \
 --host2 localhost --user2 {username} --password2 {username's_password} \
 --useheader 'Message-ID' --skipsize  --delete2 --expunge2
```

If you want to script it, create two files, e.g sync-mail and sync-mail-sub. In sync-mail put:
```
sync-mail-sub {user1} {user1_password}
sync-mail-sub {user2} {user2_password}
etc
```
and sync-mail-sub:
```
imapsync --host1 172.17.2.1 --user1 $1 --password1 $2 --host2 localhost --user2 $1 --password2 $2 \
 --useheader 'Message-ID' --skipsize  --delete2 --expunge2
```
Optionally add the --nolog flag to sync-mail-sub.

Make the files executable and run them:
```
chmod 700 sync-mail syncmail-sub
./sync-mail
```

The first time it is run it will be slow as it brings all the emails across. Subsequent runs will be much quicker as it only brings the changes across.

<note>This technique will also work if the user has a different username and/or password on the old and new server, but the script will need to be adjusted accordingly.</note>

**The whole database:**
This is a one off operation which has to be done with cyrus-imapd shutdown on both systems. You need to copy across in their entirety `/var/spool/imap/` and `/var/lib/imap/` excluding the `/var/lib/imap/socket/*`. File ownerships also need to come across. You can either create  a tar file with something like:

```
tar -cvPf mail.tar /var/spool/imap /var/lib/imap --exclude=/var/lib/imap/socket/*
```
Then untar it at the receiving end with:
```
tar -xvPf mail.tar
```
or, easier, would be to use the Data Transfer method with rsync at the bottom of the howto.

Once you've done this, restart cyrus-imapd on the new server and run the reconstruct command:
```
systemctl start cyrus-imapd.service
/usr/lib/cyrus-imapd/reconstruct
```
Then check your mail is there.
#### MySQL 
Migrating MySQL data is pretty straight forward. For MySQL, use the mysqldump command from command line. [This howto](http://webcheatsheet.com/SQL/mysql_backup_restore.php) covers both the backup and restore procedure.

#### Directory 
For Directory services, you have three options with ClearOS 7:

  * OpenLDAP
  * Active Directory Connector
  * Samba Directory (Beta)

Settings should have migrated successfully.

#### Print Server 
#### FTP Server 
#### Windows Networking 
Settings should have migrated successfully, but check `/etc/samba/smb.conf` for any old interfaces in the `interfaces =` line. Remove if necessary.

#### Flexshares 
Settings should have migrated successfully. However the folders under /var/flexshare/shares will not have been created. If you rsync your flexshares across (see further on) then you will be OK. If not, you need to either manually created the folders or delete and re-add the flexshares.

#### Web Server 
Settings should have migrated successfully but there are a lot more settings available in ClearOS 7. Review in the Webconfig. If you rsync the flexshares, var/www.html and /var/www/virtual across then you will almost be OK. You also need to add the bind mounts from the old /etc/fstab to the new and and make them active with a `mount -a`. You can see the old bind mounts with: `grep \/var\/www\/.*bind /etc/fstab`. Alternatively you can delete and recreate your websites in the webconfig.  
See the section below about Data Transfer for moving web sites across.

### Network 
#### Bandwidth Manager 
The bandwidth manager is app in ClearOS 7 is completely different from the one in ClearOS 6 and no rule conversion is possible.

#### Firewall 
Firewall rules should have migrated but they need to be sanitised to change any references to the old interfaces to the new interfaces. Files to review are:

  * /etc/clearos/firewall.conf
  * /etc/clearos/network.conf
  * /etc/clearos/multiwan.conf
  * /etc/clearos/firewall.d/custom
  * /etc/clearos/firewall.d/local

Then do a:
```
service firewall restart
```
<note warning>Mistakes in firewall configuration can cause you to lose connectivity to your server. Ensure that you have a monitor and keyboard attached in case you need to stop the firewall (service firewall stop)</note>

#### DHCP Server 
This needs to be reconfigured as the interfaces have changed. You can look in `/etc/dnsmasq.d/dhcp.conf` and delete any lines referring to the old interfaces.

#### DNS Server
 
#### NTP Server
#### IP Settings 
There is no real migration path for IP settings other than to ensure that the network interface name are the same between the old and new system. As earlier, check and remove any orphaned interface names from `/etc/clearos/network.conf`.

#### RADIUS Server 
#### OpenVPN 
All user certificates should have migrated across successfully so no change should be needed to any of your workstations
#### PPTP Server 
#### MultiWAN 
See the Firewall subsection above for migration details.

### Gateway 
#### Anti-Spam 
#### Anti-Malware 
#### Content Filter 
#### Intrusion Prevention/Detection 
## Data, User files, Flexshares, Websites, Home Folders etc 
Possibly the most effective tool for data migration is rsync. It will preserve permissions and file ownership and can run incrementally. This means you can run the script in advance of shutting the old server down and each time you run it, it will only pick up the changes from the previous time. The most basic way of using rsync tunnels over ssh and is relatively slow. It also cannot be scripted easily unless you set up ssh keys between the servers as there is no good way of scripting ssh passwords. There are plenty of references on the internet for how to set up SSH keys such as [here](http://sshkeychain.sourceforge.net/mirrors/SSH-with-Keys-HOWTO/SSH-with-Keys-HOWTO-4.html). An alternative is to run rsync in daemon mode on the old server and data transfers will run much faster:

On the old server, create a file, /etc/rsync.conf and in it put:
```
lock file = /var/run/rsync.lock
log file = /var/log/rsyncd.log
pid file = /var/run/rsyncd.pid

[oldserver]
    path = /
    comment = Old Server root file system
    uid = root
    gid = root
    read only = yes
    list = yes
    hosts allow = your_new_server's_ip_address
    use chroot = false
```
Note this is being set up without any password security. You can add it ("secrets file = /etc/rsyncd.secrets") but then you will need to adjust the scripts as well. The daemon has been secured to allow access from just one machine by the `hosts allow` line.  
Start the rsync daemon on the old server:
```
rsync --daemon
```

On your new server you can run manually or set up a script file:
```
# Flexshares
rsync -rav --progress --exclude=.trash root@your_old_server_IP::oldserver/var/flexshare/shares/ \
    /var/flexshare/shares/ --delete
# /root folder
rsync -rav --progress root@your_old_server_IP::oldserver/root/ /root/ --exclude=".*" --delete
# User Home folders
rsync -rav --progress root@your_old_server_IP::oldserver/home/ /home/ --exclude=".*" --delete
# Default Web site - note this may come across with the Flexshare transfer if you created your
# Website in the Webconfig Web Server
rsync -rav --progress root@your_old_server_IP::oldserver/var/www/html/ /var/www/html/ --delete
# Other Web sites - note these may come across with the Flexshare transfer if you created your
# Websites in the Webconfig Web Server
rsync -rav --progress root@your_old_server_IP::oldserver/var/www/virtual/ /var/www/virtual/ --delete
```
Replace your_old_server_IP with the old server's IP address.   
You can expand or reduce this list as much as you want.   
In the flexshares this excludes the recycled bin from being transferred. Remove the "--exclude=.trash" if you want to transfer the recycled bin as well.   
In the /home and /root folders, consider if you want the "." files. 
