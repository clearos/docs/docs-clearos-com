---
title: 'RPM Basics'
visible: true
taxonomy:
    category: docs
---

## Building An RPM - The Basics

If you have never built an RPM or need to know about the ClearOS package build environment, then this document is for you. The information provided here gives you an overview of what is needed to manually build your own RPMs. Though ClearOS uses a build system for creating RPMs, it is still worthwhile learning the manual procedure described here.

## Prepare Your Development Environment

Before getting underway, the first thing you need to do create a development environment. Follow [these instructions](getting-started/developer-environment) and come back to this document when you're done.

## Prepare the Source Code

With your development environment ready to go, it is now time to prepare the source code. To get the ball rolling, we are going to start with a source RPM. In our example, we are using the dnsmasq software since it is small and straightforward. 

    rpm -Uvh http://mirror1-sanfrancisco.clearos.com/clearos/7/updates/SRPMS/dnsmasq-2.76-2.v7.2.src.rpm
    
You can ignore those warnings about user/groups not existing - you will see that most of the time when install source RPMs. Installing the source RPM added three files to the RPM build directory:

* rpmbuild/SPECS/dnsmasq.spec
* rpmbuild/SOURCES/dnsmasq.init
* rpmbuild/SOURCES/dnsmasq-2.49.tar.gz

In more complex packages, you will see patches and other helper files in the SOURCES directory. In our example, we only have the single dnsmasq tar-zip along with a ClearOS compatible start/stop script (/etc/rc.d/init.d/dnsmasq). You don't need a source RPM to put these files in the SPECS and SOURCES directories, you can place those files manually. We simply used a source RPM for convenience.

## Build the RPM
Now it is time to actually build the rpm!

    rpmbuild -ba rpmbuild/SPECS/dnsmasq.spec
    
Uh-oh. Something went wrong:

    error: Failed build dependencies:
	dbus-devel is needed by dnsmasq-2.76-2.v7.2.x86_64
	libidn-devel is needed by dnsmasq-2.76-2.v7.2.x86_64
    
The dnsmasq build requires the dbus-devel package and libidn-devel. You will find that you may need to install other devel packages to build an RPM, so get used to that process when building RPMs the old fashioned way. As root, go ahead and install dbus-devel and libidn-devel:

    su -c 'yum install dbus-devel libidn-devel' -
    
Then re-rerun the rpmbuild command:

    rpmbuild -ba rpmbuild/SPECS/dnsmasq.spec
    
If all goes well, you should get both your RPM and source RPM files built:

    ./rpmbuild/RPMS/x86_64/dnsmasq-2.76-2.v7.2.x86_64.rpm
    ./rpmbuild/RPMS/x86_64/dnsmasq-utils-2.76-2.v7.2.x86_64.rpm
    ./rpmbuild/RPMS/x86_64/dnsmasq-debuginfo-2.76-2.v7.2.x86_64.rpm

## Next Steps

If this is your first time building an RPM, then you have just started a journey of learning and discovery! The [Fedora RPM Guide](https://docs-old.fedoraproject.org/en-US/Fedora_Draft_Documentation/0.1/html/RPM_Guide/index.html) is a good place to go for a lot more detail. Also take a look at the links listed at the end of this document. From spec files, to good 64-bit habits, to cross-distribution support – there is quite a bit of ground to cover.

## Additional Information

[Fedora RPM Guide](https://docs-old.fedoraproject.org/en-US/Fedora_Draft_Documentation/0.1/html/RPM_Guide/index.html)
[Gurl Labs Creating RPMS](http://www.gurulabs.com/GURULABS-RPM-LAB/GURULABS-RPM-GUIDE-v1.0.PDF)
[rpm.org](http://rpm.org)