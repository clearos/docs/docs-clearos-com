---
title: Authentication
visible: true
---

## API Key
For ClearOS 7, a simple shared API key has been implemented to provide authentication as well as full authorization.  This API key has been automatically generated and stored in /etc/api.conf:

    api_key = abc123key

You can authenticate you API requests by using this key in the request header, e.g.

    curl https://192.168.1.100:83/api/v1/network/interfaces -k -s -H "x-api-key: abc123key"

You should see output similar to below:

```json
{
    "status_code": "0",
    "status_message": "Success",
    "data": {
        "enp0s3": {
            "address": "192.168.55.7",
            "netmask": "255.255.255.0",
            "broadcast": "192.168.55.255",
            "hwaddress": "08:00:27:30:27:36",
            "mtu": 1500,
            "speed": 1000,
            "type": "Ethernet",
            "type_text": "Ethernet",
            "vendor": "Intel Corporation",
            "device": "82540EM Gigabit Ethernet Controller",
            "sub_device": "PRO/1000 MT Desktop Adapter",
            "role": "EXTIF",
            "role_text": "External"
        },
    }
}
 ```
 
## SSH Digital Signatures
In the future, authentication and authorization will be provided through SSH digital signatures in a similar way to [AWS API Signatures](https://docs.aws.amazon.com/general/latest/gr/signing_aws_api_requests.html).  