---
title: 'Data Options'
media_order: api_data_options_dropdown.png
visible: true
---

When retrieving data from the ClearOS REST API, a list of available options for some data items is available.  For example, the Dynamic DNS user interface provides a list of available domains that can be used by the end user as shown in this screenshot:

![API Data Options](api_data_options_dropdown.png)

This list of domains is sent in the **data_options** parameter of the JSON payload.  Here’s an example of the data options for a device request:

```json
"data_options": {
        "state": {
            "1000": "Active",
            "2000": "Pending",
            "3000": "Deleted",
            "3010": "Licensing Issue"
        },
        "software": {
            "20700": "ClearOS Community Edition 7.0",
            "20710": "ClearOS Community Edition 7.1",
            "20720": "ClearOS Community Edition 7.2",
            "20730": "ClearOS Community Edition 7.3",
            "20740": "ClearOS Community Edition 7.4",
            "110710": "ClearOS Business Edition 7.1",
            "110720": "ClearOS Business Edition 7.2",
            "110730": "ClearOS Business Edition 7.3",
            "110740": "ClearOS Business Edition 7.4",
            "120710": "ClearOS Home Edition 7.1",
            "120720": "ClearOS Home Edition 7.2",
            "120730": "ClearOS Home Edition 7.3",
            "120740": "ClearOS Home Edition 7.4"
        }
    },
}
```