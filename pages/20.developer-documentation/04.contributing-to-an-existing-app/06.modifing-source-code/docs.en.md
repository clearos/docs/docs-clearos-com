---
title: 'Modifying Source Code'
media_order: 'gitlab_users.png,users_sub_check.png'
taxonomy:
    category:
        - docs
visible: true
---

## Making Changes

With the source code checked out and confirmation that Webconfig (on the developer port 1501) is reflecting your changes in real-time, it's time to get do some coding.

Login to your devel server.

! If your git key resides of your laptop/workstation keychain, be sure to SSH to your ClearOS with the "-A" flag to enable authentication forwarding.

Using your favorite editor (vim or nano are the usual "go to" tools, however an IDE is also a possibility), navigate to the apps's developer base:

    cd ~/apps/<basename>
    
In our example, the basename is **_users_**.

## View

The first thing we need to do is add a button to the warning table in the view.  Open view/summary.php and you'll see:

    if (!empty($warning_items)) {
        // TODO: a nice warning icon would be nice
        $headers = array(
            lang('base_app'),
            lang('base_subscription')
        );

        $sub_options['default_rows'] = 50;
        $sub_options['sort'] = FALSE;
        $sub_options['no_action'] = TRUE;

        echo summary_table(
            lang('base_subscription_warnings'),
            array(),
            $headers,
            $warning_items,
            $sub_options
        );
    }

The empty array provided as the secondary parameter of the function **summary_table** is where the anchor will goes.  Let's add the button with a link to /app/users/check_subscriptions like this:

    if (!empty($warning_items)) {
        $anchors = array(
            anchor_custom('/app/users/check_subscriptions', lang('base_check_for_updates'))
        );
        // TODO: a nice warning icon would be nice
        $headers = array(
            lang('base_app'),
            lang('base_subscription')
        );

        $sub_options['default_rows'] = 50;
        $sub_options['sort'] = FALSE;
        $sub_options['no_action'] = TRUE;

        echo summary_table(
            lang('base_subscription_warnings'),
            $anchors,
            $headers,
            $warning_items,
            $sub_options
        );
    }


Refreshing Webconfig and ensuring we are generating an alert to display that table, we get:

![Added Button](users_sub_check.png)

That's only half of it though...Now that we have added our anchor to refresh the subscription cache, we need to tie it to a controller.

Open up controllers/users.php and add the function/route as follows:

    /**
     * Check for subscription updates.
     *
     * @return view
     */

    function check_subscriptions()
    {
        if (clearos_library_installed('clearcenter/Subscription_Manager')) {
            $this->load->library('clearcenter/Subscription_Engine');
            $this->subscription_engine->get_subscription_updates(TRUE);
            sleep(3);
        }
        redirect('/users');
    }

!!! Yes...the use of _sleep_ is a hack.  The function get_subscription_updates actually forks a process and does not wait around for the return so our page will quite likely load before the new subscription data is pulled down.  Adding a sleep gives us a pretty good chance the users will see the updated data from the ClearSDN portal on the first update.

All done.  If we click on that button and watch the cached subscription files with:

    watch ls -l /var/clearos/clearcenter/subscriptions/

We'll see any files in that folder update with the new create timestamps.  We can be happy knowing that we are looking at the latest data.

## Updating the App Metadata

Now that we're done our changes, we need to bump the version information since we'll want to build a new RPM to push to the updates mirrors.  To do that, edit:

    deploy/info.php

There, you will find something like:

    $app['version'] = '2.5.0';

For small changes, updating the least significant integer is fine...so the above would become:

    $app['version'] = '2.5.1';

Now, use the **_clearos_** CLI tool to automatically write out the spec file to the packaging folder:

    clearos spec
    
At this point, you can even built your package locally:

    clearos local
    
That will compile new packages and you'll find them in:

    ~/rpmbuild/RPMS/noarch/

This is quite handy for testing your updates prior to pushing to the Koji build system.  For example, you may want to copy the RPMs over to a 'clean install' and test.  On the devel server (and assuming your clean test server is located at 10.0.0.10):

   scp ~/rpmbuild/RPMS/noarch/app-users* root@10.0.0.10:/tmp/
   ssh root@10.0.0.10 
   cd /tmp/
   yum localinstall app-users*
   
You'll now have the new updates applied to a test server where you can test to see if your development environment was masking any issues with your update on a clean server.
