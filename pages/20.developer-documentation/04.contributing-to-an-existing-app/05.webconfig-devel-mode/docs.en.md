---
title: 'Webconfig Devel Mode'
media_order: gitlab_users.png
taxonomy:
    category:
        - docs
visible: true
---

## Accessing the Source Code using Webconfig

If all has gone well, you should be able to open the following URL in your browser (being sure to change devel.lan to your server's hostname or IP address):

[https://devel.lan:1501/app/users](https://devel.lan:1501/app/users)

! Note the use of port 1501 instead of 81.

If you want to do a quick proof that the users page you're looking at is coming from the Git source in your developer account's home directory, you can edit **_controller/users.php_** and find the default function (index) and add a redirect:

    function index()
    {
        redirect('https://www.google.ca');
        return;
    ...
    ..
    .
    }

Reload the page and you should be off to Google.  Good confirmation...don't forget to remove those two lines.

If you had any difficulties reaching this point, go back and check all of the steps in the [Getting Started](/getting-started) section.