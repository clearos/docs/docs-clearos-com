---
title: 'Create a Bug/Issuer Tracker'
media_order: 'gitlab_filter.png,gitlab_issues.png'
taxonomy:
    category:
        - docs
visible: true
---

## Creating a Bug/Issue Tracker
Creating a bug/issue tracker is a critical step in contributing to source code.  Other developers (and users) need to know:
* What issues exist
* What features are planned
* Who is working on any particular issue or feature

For these reasons, _before_ you start coding, you'll need to search the bug/feature trackers to see if it's already been reported, or is new.  If it exists and unassigned, feel free to assign yourself to the entry.  If it does not exist, create a new entry for transparency and collaboration.

### GitLab Issue Tracker

This is the direction we're heading... maintaining all bugs/feature requests in the GitLab source code repository.

If you haven't already done so, create a GitLab account [here](https://gitlab.com/users/sign_in) (separate from your [ClearCenter account](https://secure.clearcenter.com/portal) that you've certainly used to register ClearOS systems to).

Find the project you plan on working on under the main "ClearOS" project located here:

[https://gitlab.com/clearos](https://gitlab.com/clearos)

Use the handy filter to quickly find the app if you know the package name (eg. app-**_basename_**)

![GitLab Filter](gitlab_filter.png)

Navigate to the app's main source code repository and click on the plus (+) sign next to the URL and select "New Issue".

![New GitLab Issue](gitlab_issues.png)

You can take a look at our [real-world example](/contributing-to-an-existing-app#example) the issue tracker [here](https://gitlab.com/clearos/clearfoundation/app-users/issues/1).

For more on using GitLab for creating issues, you can watch [this video](https://www.youtube.com/watch?v=k1phfdVBuUg&t=90s).

### Mantis Bug/Feature Tracker (deprecated)

The Mantis Bug and Feature Tracking has been deprecated. You can view the old tracker here: [bug/feature tracker](https://tracker.clearos.com/view_all_bug_page.php)
