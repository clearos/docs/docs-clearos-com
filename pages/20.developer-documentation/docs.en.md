---
title: ClearOS Development
published: true
taxonomy:
    category: docs
---

Welcome to the ClearOS Developer Documentation.

! This section is for developers.

The developer documentation provided here is continually kept up-to-date based on the latest code.  If a feature or change has been made in a more recent version, you will see a version tag to indicate what version it was introduced.

Before you get scared away by the word developer you should know that you don't need to code software to contribute! You can:

* Help with translations.
* Submit, diagnose and verify bugs.
* Create new themes.
* Develop new apps.
* Contribute with packaging RPMs and releases
* Test code produced by developers and make suggestions
* Help design core parts of the architecture
