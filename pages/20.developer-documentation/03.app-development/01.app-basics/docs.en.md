---
title: Overview
visible: true
taxonomy:
    category: docs
---

## Anatomy of an App
This section provides a technical review of a ClearOS app:

* Where files are installed
* Packaging information
* Naming conventions

## Naming Conventions
Every app has a simple basename that is used as a keyword across various systems. For example, the Base app uses the basename base.

| System                     | Example                      |
| -------------------------- | -----------------------------|
| Packaging                  | app-certificate-manager-2.4.24-1.v7.noarch   |
| Marketplace                | [Certificate Manager](https://www.clearos.com/products/purchase/clearos-marketplace-apps/system/certificate_manager) |
| Documentation              | [Certificate Manager Docs](https://www.clearos.com/resources/documentation/clearos/content:en_us:app_certificate_manager)
| Bug Tracker                | List of issues               |
| GIT                        | [https://gitlab.com/clearos/clearfoundation/app-certificate-manager](https://gitlab.com/clearos/clearfoundation/app-certificate-manager) |
| Translations               | Certificate Manager app [in Dutch](https://gitlab.com/clearos/clearfoundation/app-certificate-manager/tree/master/language/nl)           |

## File and Directory Locations
An app uses the traditional Linux file system layout, so you can find an app installed in the locations defined in the following table.

| Location                             | Type      | Purpose |
| ------------------------------------ | --------- | ------- |
| /etc/clearos/**_basename_**          | File      | Simple configuration file (optional) |
| /etc/clearos/**_basename_**.d        | Directory | Configlet files (optional) |
| /usr/clearos/apps/**_basename_**/    | Directory | Core software, libraries and GUI (required) |
| /var/clearos/**_basename_**          | Directory | State files (optional) |

## Two Software Packages: Core Libraries and GUI
In many circumstances, a ClearOS app may need to use a library from another app. For example, the PPTP Server requires bits and pieces from the Samba library. For this reason, every app is packaged into two distinct RPMs:

1. app-**_basename_**-core: core libraries
2. app-**_basename_**: GUI frontend

When the PPTP Server is installed, it will pull in the app-samba-core library, but not the whole Samba GUI frontend.