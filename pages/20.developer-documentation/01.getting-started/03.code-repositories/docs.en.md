---
title: 'Code Repositories'
media_order: date_app.png
visible: true
taxonomy:
    category: docs
---

ClearOS software development uses the Git version control system, on [GitLab](https://gitlab.com).

## Source Code Repository

The main project is located at [https://gitlab.com/clearos](https://gitlab.com/clearos).

Sub-projects fork off the main project, as desribed below:

| Sub Project         | Description                                                                |
| ------------------- | -------------------------------------------------------------------------- | 
| ClearCenter         | Both Open Source and Proprietary (private repos) apps from ClearCenter     |
| ClearFoundation     | Open Source packages from ClearFoundation                                  |
| Upstream            | Packages maintained in upstream repositories with minor patches by ClearOS |
| Docs                | Documentation (currently developer docs only)                              |

Software repositories have branches.  Every project has a master branch.

!!! New to Git?  Read up on branches [here](https://docs.gitlab.com/ee/user/project/repository/branches/).

In addition to the master branch, ClearOS uses branches to build packages and perform version control on the RPM's build from the Git software repositories.

The clear7 branch is a reserved branch for building packages for ClearOS 7.  Similarly, if you see a clear6 branch, you can infer that this branch is for maintaining older ClearOS 6 code.

There may be any number of other branches, depending on whether another developer is working out of a custom branch to maintain their code prior to merging back into the master branch.

!!! For a good introduction to Git commit flow, have a look at [this GitHub document](https://guides.github.com/introduction/flow/).

## Creating a GitLab Account

In order to get started, create a GitLab account [here](https://gitlab.com/users/sign_in).

## Adding an SSH Key

One of the first tasks you'll want to do is add your public key to your GitLab account.  This will facilitate cloning and making pull requests to the various code repositories.

After you have logged in, at the top right hand corner of the site, click on your profile avatar and select "Settings".

Under "User Settings" in the vertical navigation bar on the left, select "SSH Keys".  Copy your public key that you created in the [Getting Started > SSH Keys](/getting-started/ssh-keys) section of this documentation and paste it in the textarea box labled "Keys".

! Ensure you are pasting the public key portion (file ending in .pub) of your key pair!

Add a Title (just something to easily remember the keypair you're using and click "Add Key".

## Cloning a Project

Back in your development environment, navigate to your home directory and then into the app folder:

    cd ~/apps

Navigate the GitLab website until you find the project you wish to checkout.  In the example below, we're going to checkout the "Date" app located [here](https://gitlab.com/clearos/clearfoundation/app-date).

![](date_app.png)

Select SSH (instead of HTTPS) for the URL and click on the clipboard icon to copy the URL.  In the apps folder paste the url as follows:

    git clone git@gitlab.com:clearos/app-date.git date
    
! Take note above that we clone the software repository the app basename (eg. "date") in place of the repository name (app-date).  This is to be compatible with the ClearOS Webconfig framework.

## Creating a Pull Request
TODO

## Merging
TODO

## Triggering a Build
TODO
