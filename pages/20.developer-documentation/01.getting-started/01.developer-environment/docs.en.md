---
title: 'Development Environment'
visible: true
taxonomy:
    category: docs
---

Whether you are translating, creating or patching apps, or building RPM packages, you will want to create a ClearOS development environment!

## Installing the Development Tools
Before getting underway, the first thing you need to do is install a ClearOS system. Thanks to the age of virtual machines, you no longer need dedicated hardware. Go ahead and install ClearOS on your favorite virtual machine (or dedicated hardware) and get it up and running.

Once you have the system up and running, login as 'root' through SSH instead of at the console. This will give you lots of advantages including the ability to scroll back easier and copy and paste information from this guide.

If you're running a Linux workstation/laptop, well, we'll assume you know how to access a shell.

For Windows workstations accessing the 'dev' environment, you can use [PuTTY](https://www.chiark.greenend.org.uk/~sgtatham/putty/latest.html). For Mac, use the Terminal program located in the Utilities folder of your Applications. We will use root for now but will use a named user account later. Since you are maintaining a development environment, we recommend you keep the ClearOS Developer and EPEL software repositories enabled since most of the development tools live there:

    yum-config-manager --enable clearos-developer
    
ClearOS Home/Business should already have clearos-centos-verified and clearos-epel-verified enabled and ClearOS Community should have clearos-centos, clearos centos-updates and clearos-epel enabled.

You can then install a basic set of development tools with the following commands:

```
yum -y upgrade
yum -y install clearos-devel app-devel

```

Don't be alarmed at the number of packages that get pulled down.  The development environment covers a broad range of activities - from building and packaging ClearOS apps to compiling code in the C programming language.

Unless you plan on using an IDE, you should also install the text editor of your choice, for example:

    yum -y install vim-enhanced

All of the examples provided in this documenation are done through direct editing of the files using a text editor like vi or nano.  KISS principle applies.

## ClearOS CLI for Developers
Now that you have the developer tools installed, you have access to the **clearos CLI** script.  It's a simple, command-line interface included with the app-devel package you installed in the section above and placed in the /usr/bin folder.

It provides a number of helpful commands for your use while developing your ClearOS app.

Run:

    clearos -h

for a complete list of supported actions.  Throughout this documentation, we'll use the clearos CLI tool to make life easier on ourselves.

## Create the Environment
Now that all the development packages are installed, it is time to prep the following:
* User Account Creation
* Environment Variables
* RPM Packaging Directories
* Permissions and Sudoers
* Development/Sandboxed Webconfig

Fortunately, all of this is abstracted away from the developer by using the clearos CLI tool.  You can find the technical details of each of the above steps in the [Section Appendix](/getting-started/section-appendix) - the following command performs all the necessary steps:

    clearos setup
    
You will need to specify a username, password and default text editor.

!!! In the clearos setup command, please specify a new user account instead of using an existing account in LDAP. In development mode, LDAP may not always be available so it is not advisable to use an LDAP user.

So exactly what did happen by running the <code>clearos setup</code> command?  As it turns out, quite a few things to make your life easier.

### User Created

As you may have guessed, a user with the username you provided was created, along with the password and a home directory.  That user is defined only in Unix-land - <code>/etc/passwd</code>, while your user password is found in <code>/etc/shadow</code>.

Go ahead and try out your new user:

    su - <username>
    
Where <username> is the username you entered in the <code>clearos setup</code> script.  You'll now find yourself logged into the non-super user account in /home/<username>.  There won't be anything in your home directory, but that will change shortly once we start checking out source code repositories.
    
### Webconfig Devel Configlet

A development Apache configlet was added to our sandboxed Apache server.  You'll find it here:

    /usr/clearos/sandbox/etc/httpd/conf.d/devel.conf

Let's take a look at a sample one:

    Listen 1501

    <VirtualHost _default_:1501>
        # Document root
        DocumentRoot /usr/clearos/framework/htdocs
        SetEnv CLEAROS_CONFIG /home/devel/.clearos

        # For framework development only
        # DocumentRoot /home/devel/clearos/webconfig/framework/htdocs
        # SetEnv CLEAROS_BOOTSTRAP /home/devel/clearos/webconfig/framework/shared

        # Enable SSL
        SSLEngine on
        SSLProtocol all -SSLv2 -SSLv3 -TLSv1
        SSLHonorCipherOrder On
        SSLCipherSuite ECDH+AESGCM:DH+AESGCM:ECDH+AES256:DH+AES256:ECDH+AES128:DH+AES:ECDH+3DES:DH+3DES:RSA+AESGCM:RSA+AES:RSA+3DES:!aNULL:!MD5

        # Cert files are in a configlet
        Include /usr/clearos/sandbox/etc/httpd/conf.d/certs.conf

        # Rewrites
        RewriteEngine on
        RewriteCond %{REQUEST_METHOD} ^(TRACE|TRACK)
        RewriteRule .* - [F]
        RewriteRule ^/app/(.*)$ /app/index.php/$1 [L]
        RewriteRule ^/api/v1/(.*)$ /app/index.php/$1 [L]

        # Aliases
        Alias /cache/ /var/clearos/framework/cache/
        Alias /approot/ /usr/clearos/apps/
        Alias /themes/ /usr/clearos/themes/

        # Old SVN roots
        Alias /clearos/themes/ /home/devel/clearos/themes/
    </VirtualHost>
!!! If you have the firewall enabled on your ClearOS box, you'll need to add an allow rule for port 1501.  You can do this via Webconfig (https://ClearOS_LAN_IP:81/app/incoming_firewall) or with the ClearOS API port allow convenience script: <code>/usr/sbin/allow-port</code>.
    
The first and most obvious thing to extract is that it has Apache listening on a different port than the usual port 81, 1501. When you're working in your developer environment and want to see instananous changes to the project you're working on (or even the last commits to the git software code repositories - master, production or other branches), using port 1501 will ensure you're using the code in your developer home directory, and not code installed from the yum package manager.

To be clear on the point, if Webconfig app package is installed via RPM, it ends up here:

    /usr/clearos/apps/<basename>

But if you checkout code for your development environment, it will end up here:
    
    /home/<username>/apps</basename>

Where basename is the package name, less the "app-" prefix, and dashes converted to underscore.  Examples:

|RPM Package Name      | Basename           |
| -------------------- | ------------------ |
|app-mail-smtp         | mail_smtp          |
|app-plex              | plex               |
|app-incoming_firewall | incoming_firewall |

If you navigate in your browser to <code>https://ClearOS_LAN_IP:81/app/dropbox</code> you will always be getting the stable code from the RPM package.

If you navigate in your browser to <code>https://ClearOS_LAN_IP:1501/app/dropbox</code> you will get the stable code from the RPM package only if you have **not** checked out the [app-dropbox source code](https://gitlab.com/clearos/clearfoundation/app-dropbox) from the git repository.  If that package has been cloned to your /home/<username>/apps/dropbox folder, port 1501 will pull from that code instead.

The only other thing of interest here is in regards to framework development.  Most app development won't need to touch the ClearOS framework, but if you did, remember to uncomment this section and checkout the [framework repository](https://gitlab.com/clearos/clearfoundation/clearos-framework).

## App Development Folder

The final step in configuring your development environment is to tell the framework where you'd like to put your apps in development...both any new apps you create or cloned copies of existing apps that you wish to contribute to.

To specify this, edit the **~.clearos__** file located in your home directory.

    nano ~/.clearos

Or if you prefer vim,

    vi ~/.clearos

Add the following configuration option:

    <?php

    \clearos\framework\Config::$apps_paths[] = 'apps';

After making the change, the webconfig engine needs to be restarted with the new configuration:

    clearos reload
