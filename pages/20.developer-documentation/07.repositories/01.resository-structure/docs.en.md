---
title: 'Repository Structure'
visible: true
taxonomy:
    category: docs
---

For ClearOS 7, the repository structure looks like this:

```
clearos
│
└─── 7
     │
     └─── contribs-testing
     │
     └─── contribs
     │
     └─── infra-testing
     │
     └─── infra
     │
     └─── iso
     │
     └─── os
     │
     └─── updates-testing
     │
     └─── updates
```