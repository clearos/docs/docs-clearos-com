===== Restore Configuration Backup from Command Line =====

Restoring your Configuration Backup can be easily done from Webconfig.  This guide, however, assists in a situation where you only have SSH or Console access to ClearOS.  This guide is compatible with versions 6.x and 7.x of ClearOS.


==== Locate the Configuration Backup file =====

First you will need to locate the Configuration Backup file you wish to restore from.
They are found in this directory: '/var/clearos/configuration_backup/'.  You can list all Configuration Backup files found in that directory by executing the following command:

<code>
ls /var/clearos/configuration_backup/
</code>

==== Command to restore from Configuration Backup file ====

<note important>
You must run the following commands as 'root'.
</note>
Define your Configuration Backup file as a variable named 'restorefile'.  Replace 'nameoffile.tgz' with the actual name of the file.
<code>
restorefile=nameoffile.tgz
</code>

Then you can execute the following command:

<code>
configuration-restore -f /var/clearos/configuration_backup/${restorefile}
</code>

This can take up to five minutes to complete.

==== Check Progress of Restoration ====

You can check the progress by executing the following command:

<code>
tail -f /var/clearos/framework/tmp/configuration_backup.json
</code>