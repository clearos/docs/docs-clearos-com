===== ClearOS Release Information =====
/**
 * This guide is dynamically updated. This is built from the releases.sh script which processes the :contnet:en_us directory for file
 * Contact DaveLoper for help in maintaining this file or for any changes
 * Typically, no changes are needed to this file. Simply update the keywords in the associated files and re-run the 'releases.sh' script.
 */
This menu contains links to release notes and other information related to the release various versions of ClearOS. Typically, users will use the Current Production Release of ClearOS and developers will use the Current Testing Versions. The older release information is given for historical purposes and to aid individuals in updating from older version.

===== Current Releases =====
==== Production ====
  * [[:content:en_us:announcements_releases_clearos_community_6.5.0_final_release_information|ClearOS Community 6.5.0 Final Release Information]]
  * [[:content:en_us:announcements_releases_clearos_professional_6-5-0_release_notes|ClearOS Professional 6.5.0 Final Release Notes]]
  * [[:content:en_us:announcements_releases_clearos_professional_6.5.0_release_notes|ClearOS Professional 6.5.0 Final Release Notes]]
==== Current Testing Versions ====
  * [[:content:en_us:announcements_releases_clearos_community_6.6.0_beta_1_release_information|ClearOS Community 6.6.0 Beta1 Release Information]]

===== General Information =====
  * [[:content:en_us:announcements_releases_test_releases|Information on Test Releases of ClearOS]]
  * [[:content:en_us:announcements_releases_version_numbering|Releases Version Numbering]]
==== ClearOS 6.x Features ====
  * [[:content:en_us:announcements_releases_clearos_professional_new_features_in_clearos_6|ClearOS 6.x Features]]

===== Prior Releases =====
==== Version 6.5 ====
=== Final ===
=== Testing ===
  * [[:content:en_us:announcements_releases_clearos_community_6.5.0_beta_1_release_information|ClearOS Community 6.5.0 Beta 1 Release Information]]
  * [[:content:en_us:announcements_releases_clearos_community_6.5.0_beta_2_release_information|ClearOS Community 6.5.0 Beta 2 Release Information]]
  * [[:content:en_us:announcements_releases_clearos_community_6.5.0_beta_3_release_information|ClearOS Community 6.5.0 Beta 3 Release Information ]]
  * [[:content:en_us:announcements_releases_clearos_professional_6.5.0_beta_2_release_information|ClearOS Professional 6.5.0 Beta 2 Release Notes]]

==== Version 6.4 ====
=== Final ===
  * [[:content:en_us:announcements_releases_clearos_community_6.4.0_final_release_information|ClearOS Community 6.4.0 Final Release Information]]
  * [[:content:en_us:announcements_releases_clearos_professional_6.4.0_release_notes|ClearOS Professional 6.4.0 Final Release Notes]]
=== Testing ===
  * [[:content:en_us:announcements_releases_clearos_community_6.4.0_alpha_1_release_information|ClearOS community 6.4.0 Alpha 1 Release Information]]
  * [[:content:en_us:announcements_releases_clearos_community_6.4.0_beta_1_release_information|ClearOS Community 6.4.0 Beta 1 Release Information]]
  * [[:content:en_us:announcements_releases_clearos_community_6.4.0_beta_2_release_information|ClearOS Community 6.4.0 Beta 2 Release Information]]
  * [[:content:en_us:announcements_releases_clearos_professional_6.4.0_beta_1_release_notes|ClearOS Professional 6.4.0 Beta 1 Release Notes]]

==== Version 6.3 ====
=== Final ===
  * [[:content:en_us:announcements_releases_clearos_community_6.3.0_final_release_information|ClearOS Community 6.3.0 Final Release Information]]
  * [[:content:en_us:announcements_releases_clearos_professional_6.3.0_release_notes|ClearOS Professional 6.3.0 Final Release Notes]]
=== Testing ===
  * [[:content:en_us:announcements_releases_clearos_community_6.3.0_beta_1_release_information|ClearOS Community 6.3.0 Beta 1 Release Information]]

==== Version 6.2 ====
=== Final ===
  * [[:content:en_us:announcements_releases_clearos_community_6.2.0_final_release_information|ClearOS Community 6.2.0 Final Release Information]]
  * [[:content:en_us:announcements_releases_clearos_professional_6.2.0_release_notes|Clearos Professional 6.2.0 Final Release Notes]]
=== Testing ===
  * [[:content:en_us:announcements_releases_clearos_6_mail_stack_testing|Mail Stack Testing for ClearOS 6.2]]
  * [[:content:en_us:announcements_releases_clearos_community_6.2.0_beta_3_release_information|ClearOS Community 6.2.0 Beta 3 Release Information]]
  * [[:content:en_us:announcements_releases_clearos_community_6.2.0_rc_1_release_information|ClearOS Community 6.2.0 RC 1 Release Information]]
  * [[:content:en_us:announcements_releases_clearos_professional_6.2.0_beta_2_release_information|ClearOS Profesional 6.2.0 Beta 2 Release Information]]
  * [[:content:en_us:announcements_releases_clearos_professional_6.2.0_beta_3_release_notes|Clearos Professional 6.2.0 Beta 3 Release Notes]]
  * [[:content:en_us:announcements_releases_clearos_professional_6.2.0_rc_1_release_notes|Clearos Professional 6.2.0 Release Candidate 1 Release Notes]]

==== Version 6.1 ====
=== Final ===
=== Testing ===
  * [[:content:en_us:announcements_releases_clearos_professional_6.1.0_alpha_1_release_information|ClearOS Professional 6.1.0 Alpha 1 Release Information]]
  * [[:content:en_us:announcements_releases_clearos_professional_6.1.0_alpha_2_release_information|ClearOS Professional 6.1.0 alpha 2 Release Information]]
  * [[:content:en_us:announcements_releases_clearos_professional_6.1.0_beta_1_release_information|ClearOS Professional 6.1.0 Beta 1 Release Information]]

==== Version 6.0 ====
=== Final ===
  * [[:content:en_us:announcements_releases_clearos_professional_6.0.0_developer_release_information|ClearOS Professional 6.0.0 Developer Release Information]]
=== Testing ===
  * [[:content:en_us:announcements_releases_clearos_professional_6.0.0_alpha_1_release_information|ClearOS Professional 6.0.0 Alpha 1 Release Information]]
  * [[:content:en_us:announcements_releases_clearos_professional_6.0.0_developer_release_2_information|ClearOS Professional 6.0.0 Developer Release 2 Information]]
  * [[:content:en_us:announcements_releases_clearos_professional_6.0.0_status_update|ClearOS Professional 6.0.0 Status Update]]

==== Version 5.2 and Legacy/Unsupported releases ====
=== Final ===
  * [[:content:en_us:announcements_releases_clearos_enterprise_5.1_release_notes|ClearOS Enterprise 5.1 Release Notes]]
  * [[:content:en_us:announcements_releases_clearos_enterprise_5.1_service_pack_1|ClearOS Enterprise 5.1 Service Pack 1]]
  * [[:content:en_us:announcements_releases_clearos_enterprise_5.2_release_notes|ClearOS Enterprise 5.2 Release Notes]]
  * [[:content:en_us:announcements_releases_clearos_enterprise_5.2_service_pack_1|ClearOS Enterprise 5.2 Service Pack 1]]
=== Testing ===
  * [[:content:en_us:announcements_releases_clearos_enterprise_5.1_beta_1_release_information|ClearOS Enterprise 5.1 Beta 1 Release Information]]
  * [[:content:en_us:announcements_releases_clearos_enterprise_5.1_beta_2_release_information|ClearOS Enterprise 5.1 Beta 2 Release Information]]
  * [[:content:en_us:announcements_releases_clearos_enterprise_5.1_release_candidate_1_release_information|ClearOS Enterprise 5.1 Release Candidate 1 Release Information]]
  * [[:content:en_us:announcements_releases_clearos_enterprise_5.1_release_candidate_2_release_information|ClearOS Enterprise 5.1 Release Candidate 2 Release Candidate Information]]
  * [[:content:en_us:announcements_releases_clearos_enterprise_5.2_alpha_1_release_information|ClearOS Enterprise 5.2 Alpha 1 Release Information]]
  * [[:content:en_us:announcements_releases_clearos_enterprise_5.2_beta_1_release_information|ClearOS Enterprise 5.2 Beta 1 Release Information]]
  * [[:content:en_us:announcements_releases_clearos_enterprise_5.2_beta_2_release_information|ClearOS Enterprise 5.2 Beta 2 Release Information]]
  * [[:content:en_us:announcements_releases_clearos_enterprise_5.2_rc_1_release_information|ClearOS Enterprise 5.2 RC 1 Release Information]]

===== Other Testing Releases =====
  * [[:content:en_us:announcements_releases_clearos_professional_samba_directory_-_alpha_1|Samba Directory - Alpha 1]]
  * [[:content:en_us:announcements_releases_clearos_professional_samba_directory_-_beta_1|Samba Directory - Beta 1]]

===== Licenses =====
  * [[:content:en_us:announcements_releases_gpl_2|GNU GENERAL PUBLIC LICENSE v2=====]]

