===== Best Practices, Whitepapers and Implementation Guides =====
/**
 * This guide is dynamically updated. the following tags are used in the following sections:
 * kb bestpractices
 */
ClearOS and the apps associated with ClearOS provide a platform for great IT solutions. Some solutions are more typical than another and this section is a collection of useful articles that describe some typical scenarios and the best ways to implement them, some whitepapers and studies into ClearOS and related apps.

==== Cloud Best Practices ====

==== Server Best Practices ====
  * [[:content:en_us:kb_adding_workstation_to_a_domain|Adding a Workstation to a Windows domain]]
  * [[:content:en_us:kb_howto_testing|Howto and Knowledgebase Template]]

==== Network Best Practices ====

==== Gateway Best Practices ====
  * [[:content:en_us:kb_howtos_antimalware_and_antispam_gateway|Antimalware and Antispam Gateway]]

==== Settings Best Practices ====

===== All Best Practices Guides =====
  * [[:content:en_us:kb_adding_workstation_to_a_domain|Adding a Workstation to a Windows domain]]
  * [[:content:en_us:kb_bestpractices_account_synchronization_-_deployment_walkthrough|Account Synchronization - Deployment Walkthrough]]
  * [[:content:en_us:kb_bestpractices_account_synchronization_-_headquarters|Account Synchronization - Headquarters]]
  * [[:content:en_us:kb_bestpractices_account_synchronization_-_remote_office|Account Synchronization - Remote Office]]
  * [[:content:en_us:kb_bestpractices_content_filtering_with_active_directory|Content Filtering with Active Directory]]
  * [[:content:en_us:kb_bestpractices_custom_partitioning_guide_for_clearos_6.x|Custom Partitioning Guide for ClearOS 6.x]]
  * [[:content:en_us:kb_bestpractices_live_monitoring_of_web_traffic_in_proxy_and_content_filter|Live Monitoring of Web Traffic in Proxy and Content Filter]]
  * [[:content:en_us:kb_bestpractices_managing_static_routes|Managing Static Routes]]
  * [[:content:en_us:kb_bestpractices_multiple_servers_for_optimum_filtration|Multiple Servers for Optimum Filtration]]
  * [[:content:en_us:kb_bestpractices_synchronizing_zarafa_mail_contacts_and_calendars_-_mobile_devices|Synchronizing Zarafa Mail Contacts and Calenders - Mobile Devices]]
  * [[:content:en_us:kb_bestpractices_vulnerabilities_overview|Vulnerabilities Overview]]
  * [[:content:en_us:kb_howtos_antimalware_and_antispam_gateway|Antimalware and Antispam Gateway]]
  * [[:content:en_us:kb_howto_testing|Howto and Knowledgebase Template]]

