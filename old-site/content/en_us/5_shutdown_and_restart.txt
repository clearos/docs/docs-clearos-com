===== Shutdown and Restart =====
A tool to shutdown or restart your system.

===== Installation =====
This feature is part of the core system and installed by default.

===== Menu =====
You can find this feature in the menu system at the following location:
 
<navigation>System|Settings|Shutdown - Restart</navigation>

===== Configuration =====
{{keywords>clearos, clearos content, Shutdown and Restart, app-restart, clearos5, userguide, categorysystem, subcategorysystemsettings, maintainer_dloper}}
