===== Apr-util =====
The Apache Portable Runtime (APR) provides additional utility interfaces for the Apache web server.

===== Changes =====
  * Postgres database support has been removed.  This was done to remove the additional Postgres library dependency.
  * This package also requires a rebuild for the newer LDAP library.

===== Future =====
The modifications for apr-util will not be required in 6.0:
  * Postgres support is now built as a module (apr-util-pgsql)
  * LDAP rebuild is not required



{{keywords>clearos, clearos content, Apr-util, version5, dev, packaging, maintainer_dloper, maintainerreview_x, keywordfix}}
