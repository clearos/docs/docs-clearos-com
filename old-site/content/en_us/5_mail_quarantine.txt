===== Mail Quarantine =====
The **Mail Quarantine** allows administrators quarantine mail messages that have:

  * Exceeded a spam threshold
  * A detected virus
  * Failed policy checks (for example, a disallowed file type)

===== Installation =====
If you did not select this module to be included during the installation process, you must first [[:content:en_us:5_software_modules|install the module]].

===== Menu =====
You can find this feature in the menu system at the following location:
 
{{keywords>clearos, clearos content, Mail Quarantine, app-mail_quarantine, clearos5, userguide, categoryserver, subcategorymailscanning, maintainer_dloper}}
