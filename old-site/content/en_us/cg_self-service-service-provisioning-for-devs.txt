===== Self-Service Provisioning =====
This article explains how to use ClearGLASS to set up self-service provisioning for multi and hybrid cloud environments.

Any business that builds software and uses public and private clouds for development, testing, staging, and production can benefit from this type of setup.

You will learn how to:

  * Use RBAC to organize teams, set access policies, and create end-user accounts
  * Use tags to track environments/infrastructure
  * Tips for controlling costs
  * Automate developer workflows
==== Role-Based Access Control (RBAC) ====

To get started, go to the Team section and create a team.

[image]

Give your team a name and click Add. Your Team will appear in the Team list.

[image]

Next, you will invite members to join the team. Each member will receive an email and be asked to create a ClearGLASS account. When the member logs in to ClearGLASS, they will be able to see the clouds and perform the actions you have granted them. Let's discuss permissions.

[image]

Permissions are managed by the ClearGLASS administrator via the policy engine. The policy engine will let you control every function in the ClearGLASS console. The first step is to always give Read access to a cloud.  Once this is done, you can start to create your policy. For example, if you want allow Members to be able to provision machines on GCE and AWS only, you would Allow Read access to both GCE and AWS. 

[image]

A few details about RBAC: first, you can have many organizations, but note that a service plan is tied to an organization. An organization can have many teams, and teams can have many members; members can belong to more than one team. The  RBAC feature is pretty straight forward and is easy to use. You should be able to create your first team and invite users in a few minutes. 

Now lets discuss tagging.

==== Setting Tags ====
The  tagging feature lets you add tags to a machine; a machine can be a VM, bare-metal server, or container. The tag feature is located in the Machine section.

[image]

You can have more than one tag per machine.

[image]

As the ClearGLASS administrator, you will want to create a  tagging policy for your users. The tags will help you identify machines, see when they were created, and determine if they are needed.

[image]

Now, let's discuss how you can use RBAC and Tagging to control costs.

==== Managing Costs ====
ClearGLASS provides a few different tools for controlling cost. Let's start with the Price Widget.

[image]

This widget displays all your Clouds, machine inventory, and the estimated monthly cost. This is great for seeing all your clouds, machines and the costs in one view.

Another useful feature is the Created column in the Machine page. You can sort to see when a machine was created and the related cost. This is great to see newly provisioned machines and the cost. If there is an XL AWS machine created, you can quickly spot that and check with the owner to determine if the machine is needed.

[image]

The tagging feature can be used to see the total machines a user has and there related costs. You can also use this feature to see the total machines a Team has and their costs.

[image]

==== Automating Workflows ====
Once you have created a team and invited members, you can provide the Members with scripts and templates that they can use to provision new infrastructure or on existing machines. ClearGLASS supports executable scripts, Ansible playbooks, and Cloudify blueprints. 