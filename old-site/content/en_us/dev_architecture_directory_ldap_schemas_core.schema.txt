===== Directory LDAP Schemas Core,Schema =====
Provides Core LDAP functionality


===== Schema Properties ===== 
=== Attribute Key ===

  * SUP = SUP: 
    * N indicates that the value is set and equals 'name'
    * P indicates that the value is set and equals 'postaladdress'
    * DN indicates that the value is set and equals 'distinguishedName'
  * EQ = EQUALITY: 
    * CIM indicates caseIgnoreMatch, 
    * CILM indicates caseIgnoreListMatch, 
    * TNM indicates telephoneNumberMatch, 
    * NSM indicates numericStringMatch,
    * PAM indicates presentationAddressMatch,
    * OIM indicates objectIdentifierMatch,
    * CEM indicates certificateExactMatch,
    * BSM indicates bitStringMatch,
    * PIM indicates protocolInformationMatch,
    * UMM indicates uniqueMemberMatch
  * ORD = ORDERING:
    * CIOM indicates caseIgnoreOrderingMatch
  * SUB = SUBSTRING: 
    * CISM indicates caseIgnoreSubstringMatch, 
    * CILSM indicates caseIgnoreListSubstringsMatch, 
    * TSNM indicates telephoneNumberSubstringsMatch, 
    * NSSM indicates numericStringSubstringsMatch
  * SV = SINGLE-VALUE: Y indicates that this value is set

=== Attribute Type Table ===

^  Value  ^  Name  ^  Description  ^  SUP  ^  EQ  ^ ORD ^  SUB  ^  Syntax  ^  SV  ^
| 2.5.4.2 | knowledgeInformation | RFC2256: knowledge information |  | CIM |  |  | 1.3.6.1.4.1.1466.115.121.1.15{32768} |
^ 2.5.4.4 ^ sn, surname ^ RFC2256: last (family) name(s) for which the entity is known by ^ N ^  ^  ^  ^  ^  ^
| 2.5.4.5 | serialNumber | RFC2256: serial number of the entity |  | CIM |  | CISM | 1.3.6.1.4.1.1466.115.121.1.44{64} |  |
^ 2.5.4.6 ^ c, countryName ^ RFC4519: two-letter ISO-3166 country code ^ N ^  ^  ^  ^ 1.3.6.1.4.1.1466.115.121.1.11 ^ Y ^
| 2.5.4.7 | l, localityName | RFC2256: locality which this object resides in | N |  |  |  |  |  |
^ 2.5.4.8 ^ st, stateOrProvinceName ^ RFC2256: state or province which this object resides in ^ N ^  ^  ^  ^  ^  ^
| 2.5.4.9 | street, streetAddress | RFC2256: street address of this object |  | CIM |  | CISM | 1.3.6.1.4.1.1466.115.121.1.15{128} |  |
^ 2.5.4.10 ^ o, organizationName ^ RFC2256: organization this object belongs to ^ N ^  ^  ^  ^  ^  ^
| 2.5.4.11 | ou, organizationalUnitName | RFC2256: organizational unit this object belongs to | N |  |  |  |  |  |
^  Value  ^  Name  ^  Description  ^  SUP  ^  EQ  ^ ORD ^  SUB  ^  Syntax  ^  SV  ^
| 2.5.4.12 | title |RFC2256: title associated with the entity | N |  |  |  |  |  |
^ 2.5.4.14 ^ searchGuide ^ RFC2256: search guide, deprecated by enhancedSearchGuide ^  ^  ^  ^  ^ 1.3.6.1.4.1.1466.115.121.1.25 ^  ^
| 2.5.4.15 | businessCategory | RFC2256: business category |  | CIM |  | CISM | 1.3.6.1.4.1.1466.115.121.1.15{128} |  |
^ 2.5.4.16 ^ postalAddress ^ RFC2256: postal address ^  ^ CILM ^  ^ CILSM ^ 1.3.6.1.4.1.1466.115.121.1.41 ^  ^
| 2.5.4.17 | postalCode | RFC2256: postal code |  | CIM |  | CISM | 1.3.6.1.4.1.1466.115.121.1.15{40} |  |
^ 2.5.4.18 ^ postOfficeBox ^ RFC2256: Post Office Box ^  ^ CIM ^  ^ CISM ^ 1.3.6.1.4.1.1466.115.121.1.15{40} ^  ^
| 2.5.4.19 | physicalDeliveryOfficeName | RFC2256: Physical Delivery Office Name |  | CIM |  | CISM | 1.3.6.1.4.1.1466.115.121.1.15{128} |  |
^ 2.5.4.20 ^ telephoneNumber ^ RFC2256: Telephone Number ^  ^ TNM ^  ^ TNSM ^ 1.3.6.1.4.1.1466.115.121.1.50{32} ^  ^
| 2.5.4.21 | telexNumber | RFC2256: Telex Number |  |  |  |  | 1.3.6.1.4.1.1466.115.121.1.52 |  |
^  Value  ^  Name  ^  Description  ^  SUP  ^  EQ  ^ ORD ^  SUB  ^  Syntax  ^  SV  ^
| 2.5.4.22 | teletexTerminalIdentifier | RFC2256: Teletex Terminal Identifier |  |  |  |  | 1.3.6.1.4.1.1466.115.121.1.51 |  |
^ 2.5.4.23 ^ facsimileTelephoneNumber, fax ^ RFC2256: Facsimile (Fax) Telephone Number ^  ^  ^  ^  ^ 1.3.6.1.4.1.1466.115.121.1.22 ^  ^
| 2.5.4.24 | x121Address | RFC2256: X.121 Address |  | NSM |  | NSSM | 1.3.6.1.4.1.1466.115.121.1.36{15} |  |
^ 2.5.4.25 ^ internationaliSDNNumber ^ RFC2256: international ISDN number ^  ^ NSM ^  ^ NSSM ^ 1.3.6.1.4.1.1466.115.121.1.36{16} ^  ^
| 2.5.4.26 | registeredAddress | RFC2256: registered postal address | P |  |  |  | 1.3.6.1.4.1.1466.115.121.1.41 |  |
^ 2.5.4.27 ^ destinationIndicator ^ RFC2256: destination indicator ^  ^ CIM ^  ^  CISM ^ 1.3.6.1.4.1.1466.115.121.1.44{128} ^  ^
| 2.5.4.28 | preferredDeliveryMethod | RFC2256: preferred delivery method |  |  |  |  | 1.3.6.1.4.1.1466.115.121.1.14 | Y |
^ 2.5.4.29 ^ presentationAddress ^ RFC2256: presentation address ^  ^ PAM ^  ^  ^ 1.3.6.1.4.1.1466.115.121.1.43 ^ Y ^
| 2.5.4.30 | supportedApplicationContext | RFC2256: supported application context |  | OIM |  |  | 1.3.6.1.4.1.1466.115.121.1.38 |  |
^  Value  ^  Name  ^  Description  ^  SUP  ^  EQ  ^ ORD ^  SUB  ^  Syntax  ^  SV  ^
| 2.5.4.31 | member | RFC2256: member of a group | DN |  |  |  |  |  |
^ 2.5.4.32 ^ owner ^ RFC2256: owner (of the object) ^ DN ^  ^  ^  ^  ^  ^
| 2.5.4.33 | roleOccupant' | RFC2256: occupant of role | DN |  |  |  |  |  |
^ 2.5.4.36 ^ userCertificate ^ RFC2256: X.509 user certificate, use ;binary ^  ^ CEM ^  ^  ^ 1.3.6.1.4.1.1466.115.121.1.8 ^  ^
| 2.5.4.37 | cACertificate | RFC2256: X.509 CA certificate, use ;binary |  | CEM |  |  | 1.3.6.1.4.1.1466.115.121.1.8 |  |
^ 2.5.4.38 ^ authorityRevocationList ^ RFC2256: X.509 authority revocation list, use ;binary ^  ^  ^  ^  ^ 1.3.6.1.4.1.1466.115.121.1.9 ^  ^
| 2.5.4.39 | certificateRevocationList | RFC2256: X.509 certificate revocation list, use ;binary |  |  |  |  | 1.3.6.1.4.1.1466.115.121.1.9 |  |
^ 2.5.4.40 ^ crossCertificatePair ^ RFC2256: X.509 cross certificate pair, use ;binary ^  ^  ^  ^  ^ 1.3.6.1.4.1.1466.115.121.1.10 ^  ^
| 2.5.4.42 | givenName, gn | RFC2256: first name(s) for which the entity is known by | N |  |  |  |  |  |
^  Value  ^  Name  ^  Description  ^  SUP  ^  EQ  ^ ORD ^  SUB  ^  Syntax  ^  SV  ^
| 2.5.4.43 | initials | RFC2256: initials of some or all of names, but not the surname(s). | N |  |  |  |  |  |
^ 2.5.4.44 ^ generationQualifier ^ RFC2256: name qualifier indicating a generation ^ N ^  ^  ^  ^  ^  ^
| 2.5.4.45 | x500UniqueIdentifier | RFC2256: X.500 unique identifier |  | BSM |  |  | 1.3.6.1.4.1.1466.115.121.1.6 |  |
^ 2.5.4.46 ^ dnQualifier ^ RFC2256: DN qualifier ^  ^ CIM ^ CIOM ^ CISM^ 1.3.6.1.4.1.1466.115.121.1.44 ^  ^
| 2.5.4.47 | enhancedSearchGuide | RFC2256: enhanced search guide |  |  |  |  | 1.3.6.1.4.1.1466.115.121.1.21 |  |
^ 2.5.4.48 ^ protocolInformation ^ RFC2256: protocol information ^  ^ PIM ^  ^  ^ 1.3.6.1.4.1.1466.115.121.1.42 ^  ^
| 2.5.4.50 | uniqueMember | RFC2256: unique member of a group |  | UMM |  |  | 1.3.6.1.4.1.1466.115.121.1.34 |  |
^ 2.5.4.51 ^ houseIdentifier ^ RFC2256: house identifier ^  ^ CIM ^  ^ CISM ^ 1.3.6.1.4.1.1466.115.121.1.15{32768} ^  ^
| 2.5.4.52 | supportedAlgorithms | RFC2256: supported algorithms |  |  |  |  | 1.3.6.1.4.1.1466.115.121.1.49 |  |
^  Value  ^  Name  ^  Description  ^  SUP  ^  EQ  ^ ORD ^  SUB  ^  Syntax  ^  SV  ^
| 2.5.4.53 | deltaRevocationList | RFC2256: delta revocation list; use ;binary |  |  |  |  | 1.3.6.1.4.1.1466.115.121.1.9 |  |
^ 2.5.4.54 ^ dmdName ^ RFC2256: name of DMD ^ N ^  ^  ^  ^  ^  ^
| 2.5.4.65 | pseudonym | X.520(4th): pseudonym for the object | N |  |  |  |  |  |




*** Object Classes ***

^  Value  ^  Name  ^  Description  ^  

# Standard object classes from RFC2256

# system schema
#objectclass ( 2.5.6.0 NAME 'top'
#	DESC 'RFC2256: top of the superclass chain'
#	ABSTRACT
#	MUST objectClass )

# system schema
#objectclass ( 2.5.6.1 NAME 'alias'
#	DESC 'RFC2256: an alias'
#	SUP top STRUCTURAL
#	MUST aliasedObjectName )

objectclass ( 2.5.6.2 NAME 'country'
	DESC 'RFC2256: a country'
	SUP top STRUCTURAL
	MUST c
	MAY ( searchGuide $ description ) )

objectclass ( 2.5.6.3 NAME 'locality'
	DESC 'RFC2256: a locality'
	SUP top STRUCTURAL
	MAY ( street $ seeAlso $ searchGuide $ st $ l $ description ) )

objectclass ( 2.5.6.4 NAME 'organization'
	DESC 'RFC2256: an organization'
	SUP top STRUCTURAL
	MUST o
	MAY ( userPassword $ searchGuide $ seeAlso $ businessCategory $
		x121Address $ registeredAddress $ destinationIndicator $
		preferredDeliveryMethod $ telexNumber $ teletexTerminalIdentifier $
		telephoneNumber $ internationaliSDNNumber $ 
		facsimileTelephoneNumber $ street $ postOfficeBox $ postalCode $
		postalAddress $ physicalDeliveryOfficeName $ st $ l $ description ) )

objectclass ( 2.5.6.5 NAME 'organizationalUnit'
	DESC 'RFC2256: an organizational unit'
	SUP top STRUCTURAL
	MUST ou
	MAY ( userPassword $ searchGuide $ seeAlso $ businessCategory $
		x121Address $ registeredAddress $ destinationIndicator $
		preferredDeliveryMethod $ telexNumber $ teletexTerminalIdentifier $
		telephoneNumber $ internationaliSDNNumber $
		facsimileTelephoneNumber $ street $ postOfficeBox $ postalCode $
		postalAddress $ physicalDeliveryOfficeName $ st $ l $ description ) )

objectclass ( 2.5.6.6 NAME 'person'
	DESC 'RFC2256: a person'
	SUP top STRUCTURAL
	MUST ( sn $ cn )
	MAY ( userPassword $ telephoneNumber $ seeAlso $ description ) )

objectclass ( 2.5.6.7 NAME 'organizationalPerson'
	DESC 'RFC2256: an organizational person'
	SUP person STRUCTURAL
	MAY ( title $ x121Address $ registeredAddress $ destinationIndicator $
		preferredDeliveryMethod $ telexNumber $ teletexTerminalIdentifier $
		telephoneNumber $ internationaliSDNNumber $ 
		facsimileTelephoneNumber $ street $ postOfficeBox $ postalCode $
		postalAddress $ physicalDeliveryOfficeName $ ou $ st $ l ) )

objectclass ( 2.5.6.8 NAME 'organizationalRole'
	DESC 'RFC2256: an organizational role'
	SUP top STRUCTURAL
	MUST cn
	MAY ( x121Address $ registeredAddress $ destinationIndicator $
		preferredDeliveryMethod $ telexNumber $ teletexTerminalIdentifier $
		telephoneNumber $ internationaliSDNNumber $ facsimileTelephoneNumber $
		seeAlso $ roleOccupant $ preferredDeliveryMethod $ street $
		postOfficeBox $ postalCode $ postalAddress $
		physicalDeliveryOfficeName $ ou $ st $ l $ description ) )

objectclass ( 2.5.6.9 NAME 'groupOfNames'
	DESC 'RFC2256: a group of names (DNs)'
	SUP top STRUCTURAL
	MUST ( member $ cn )
	MAY ( businessCategory $ seeAlso $ owner $ ou $ o $ description ) )

objectclass ( 2.5.6.10 NAME 'residentialPerson'
	DESC 'RFC2256: an residential person'
	SUP person STRUCTURAL
	MUST l
	MAY ( businessCategory $ x121Address $ registeredAddress $
		destinationIndicator $ preferredDeliveryMethod $ telexNumber $
		teletexTerminalIdentifier $ telephoneNumber $ internationaliSDNNumber $
		facsimileTelephoneNumber $ preferredDeliveryMethod $ street $
		postOfficeBox $ postalCode $ postalAddress $
		physicalDeliveryOfficeName $ st $ l ) )

objectclass ( 2.5.6.11 NAME 'applicationProcess'
	DESC 'RFC2256: an application process'
	SUP top STRUCTURAL
	MUST cn
	MAY ( seeAlso $ ou $ l $ description ) )

objectclass ( 2.5.6.12 NAME 'applicationEntity'
	DESC 'RFC2256: an application entity'
	SUP top STRUCTURAL
	MUST ( presentationAddress $ cn )
	MAY ( supportedApplicationContext $ seeAlso $ ou $ o $ l $
	description ) )

objectclass ( 2.5.6.13 NAME 'dSA'
	DESC 'RFC2256: a directory system agent (a server)'
	SUP applicationEntity STRUCTURAL
	MAY knowledgeInformation )

objectclass ( 2.5.6.14 NAME 'device'
	DESC 'RFC2256: a device'
	SUP top STRUCTURAL
	MUST cn
	MAY ( serialNumber $ seeAlso $ owner $ ou $ o $ l $ description ) )

objectclass ( 2.5.6.15 NAME 'strongAuthenticationUser'
	DESC 'RFC2256: a strong authentication user'
	SUP top AUXILIARY
	MUST userCertificate )

objectclass ( 2.5.6.16 NAME 'certificationAuthority'
	DESC 'RFC2256: a certificate authority'
	SUP top AUXILIARY
	MUST ( authorityRevocationList $ certificateRevocationList $
		cACertificate ) MAY crossCertificatePair )

objectclass ( 2.5.6.17 NAME 'groupOfUniqueNames'
	DESC 'RFC2256: a group of unique names (DN and Unique Identifier)'
	SUP top STRUCTURAL
	MUST ( uniqueMember $ cn )
	MAY ( businessCategory $ seeAlso $ owner $ ou $ o $ description ) )

objectclass ( 2.5.6.18 NAME 'userSecurityInformation'
	DESC 'RFC2256: a user security information'
	SUP top AUXILIARY
	MAY ( supportedAlgorithms ) )

objectclass ( 2.5.6.16.2 NAME 'certificationAuthority-V2'
	SUP certificationAuthority
	AUXILIARY MAY ( deltaRevocationList ) )

objectclass ( 2.5.6.19 NAME 'cRLDistributionPoint'
	SUP top STRUCTURAL
	MUST ( cn )
	MAY ( certificateRevocationList $ authorityRevocationList $
		deltaRevocationList ) )

objectclass ( 2.5.6.20 NAME 'dmd'
	SUP top STRUCTURAL
	MUST ( dmdName )
	MAY ( userPassword $ searchGuide $ seeAlso $ businessCategory $
		x121Address $ registeredAddress $ destinationIndicator $
		preferredDeliveryMethod $ telexNumber $ teletexTerminalIdentifier $
		telephoneNumber $ internationaliSDNNumber $ facsimileTelephoneNumber $
		street $ postOfficeBox $ postalCode $ postalAddress $
		physicalDeliveryOfficeName $ st $ l $ description ) )

#
# Object Classes from RFC 2587
#
objectclass ( 2.5.6.21 NAME 'pkiUser'
	DESC 'RFC2587: a PKI user'
	SUP top AUXILIARY
	MAY userCertificate )

objectclass ( 2.5.6.22 NAME 'pkiCA'
	DESC 'RFC2587: PKI certificate authority'
	SUP top AUXILIARY
	MAY ( authorityRevocationList $ certificateRevocationList $
		cACertificate $ crossCertificatePair ) )

objectclass ( 2.5.6.23 NAME 'deltaCRL'
	DESC 'RFC2587: PKI user'
	SUP top AUXILIARY
	MAY deltaRevocationList )

#
# Standard Track URI label schema from RFC 2079
# system schema
#attributetype ( 1.3.6.1.4.1.250.1.57 NAME 'labeledURI'
#	DESC 'RFC2079: Uniform Resource Identifier with optional label'
#	EQUALITY caseExactMatch
#	SYNTAX 1.3.6.1.4.1.1466.115.121.1.15 )

objectclass ( 1.3.6.1.4.1.250.3.15 NAME 'labeledURIObject'
	DESC 'RFC2079: object that contains the URI attribute type'
	SUP top AUXILIARY
	MAY ( labeledURI ) )

#
# Derived from RFC 1274, but with new "short names"
#
#attributetype ( 0.9.2342.19200300.100.1.1
#	NAME ( 'uid' 'userid' )
#	DESC 'RFC1274: user identifier'
#	EQUALITY caseIgnoreMatch
#	SUBSTR caseIgnoreSubstringsMatch
#	SYNTAX 1.3.6.1.4.1.1466.115.121.1.15{256} )

attributetype ( 0.9.2342.19200300.100.1.3
	NAME ( 'mail' 'rfc822Mailbox' )
	DESC 'RFC1274: RFC822 Mailbox'
    EQUALITY caseIgnoreIA5Match
    SUBSTR caseIgnoreIA5SubstringsMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.26{256} )

objectclass ( 0.9.2342.19200300.100.4.19 NAME 'simpleSecurityObject'
	DESC 'RFC1274: simple security object'
	SUP top AUXILIARY
	MUST userPassword )

# RFC 1274 + RFC 2247
attributetype ( 0.9.2342.19200300.100.1.25
	NAME ( 'dc' 'domainComponent' )
	DESC 'RFC1274/2247: domain component'
	EQUALITY caseIgnoreIA5Match
	SUBSTR caseIgnoreIA5SubstringsMatch
	SYNTAX 1.3.6.1.4.1.1466.115.121.1.26 SINGLE-VALUE )

# RFC 2247
objectclass ( 1.3.6.1.4.1.1466.344 NAME 'dcObject'
	DESC 'RFC2247: domain component object'
	SUP top AUXILIARY MUST dc )

# RFC 2377
objectclass ( 1.3.6.1.1.3.1 NAME 'uidObject'
	DESC 'RFC2377: uid object'
	SUP top AUXILIARY MUST uid )

# RFC 4524
#   The 'associatedDomain' attribute specifies DNS [RFC1034][RFC2181]
#   host names [RFC1123] that are associated with an object.   That is,
#   values of this attribute should conform to the following ABNF:
#
#    domain = root / label *( DOT label )
#    root   = SPACE
#    label  = LETDIG [ *61( LETDIG / HYPHEN ) LETDIG ]
#    LETDIG = %x30-39 / %x41-5A / %x61-7A ; "0" - "9" / "A"-"Z" / "a"-"z"
#    SPACE  = %x20                        ; space (" ")
#    HYPHEN = %x2D                        ; hyphen ("-")
#    DOT    = %x2E                        ; period (".")
attributetype ( 0.9.2342.19200300.100.1.37
	NAME 'associatedDomain'
	DESC 'RFC1274: domain associated with object'
	EQUALITY caseIgnoreIA5Match
	SUBSTR caseIgnoreIA5SubstringsMatch
	SYNTAX 1.3.6.1.4.1.1466.115.121.1.26 )

# RFC 2459 -- deprecated in favor of 'mail' (in cosine.schema)
attributetype ( 1.2.840.113549.1.9.1
	NAME ( 'email' 'emailAddress' 'pkcs9email' )
	DESC 'RFC3280: legacy attribute for email addresses in DNs'
	EQUALITY caseIgnoreIA5Match
	SUBSTR caseIgnoreIA5SubstringsMatch
	SYNTAX 1.3.6.1.4.1.1466.115.121.1.26{128} )


===== Links =====


Includes LDAPv3 schema items from:
RFC 2252/2256 (LDAPv3)

Select standard track schema items:
RFC 1274 (uid/dc)
RFC 2079 (URI)
RFC 2247 (dc/dcObject)
RFC 2587 (PKI)
RFC 2589 (Dynamic Directory Services)
RFC 4524 (associatedDomain)

Select informational schema items:
RFC 2377 (uidObject)


Standard attribute types from RFC 2256

{{keywords>clearos, clearos content, dev, architecture, maintainer_dloper}}
