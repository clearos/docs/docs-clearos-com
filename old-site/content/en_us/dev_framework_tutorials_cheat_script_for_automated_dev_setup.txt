===== Dev Framework Tutorials Cheat Script for Automated Dev Setup =====
This script automates the devuser creation and also much of the development environment. You will still need to do some things after the script is run:

== setupdevenv.sh ==
<code>
#!/bin/bash

if [ -z $1 ] ; then
	echo USAGE: setupdevenv.sh [your packages nametag] {optional:devuser password}
	exit 1
else
	dist=$1
fi

if [ -z $2 ] ; then
	password=`tr -dc A-Za-z0-9_ < /dev/urandom | head -c 16 | xargs` 
else
	password=$2
fi

#echo Distribution nametag: $dist
#echo Password: $password


yum -y upgrade
yum -y --enablerepo=clearos-developer,clearos-epel install clearos-devel
yum -y install vim-enhanced
useradd -u 500 devuser
echo $password | passwd --stdin devuser 
chown -R devuser /home/devuser
su - devuser
chmod og+rx /home/devuser

echo "%dist .$dist" >> ~/.rpmmacros
echo -e "%_topdir\t$HOME/rpmbuild" >> ~/.rpmmacros
mkdir -p ~/rpmbuild/{BUILD,BUILDROOT,RPMS,SOURCES,SPECS,SRPMS}
logout

cp ./mydevel.conf /usr/clearos/sandbox/etc/httpd/conf.d/mydevel.conf
service webconfig restart

echo Please modify local environment according to: http://www.clearfoundation.com/docs/developer/framework/development_environment


if [ "$2" = "$password" ] ; then
	echo Password that was given was used.
else
	echo Password for devuser is $password
fi
exit 0
</code>

== mydevel.conf ==
<code>
#----------------------------------------------------------------
# App development
#----------------------------------------------------------------

Listen 1501

<VirtualHost _default_:1501>
        # Document root
        DocumentRoot /home/devuser/clearos/webconfig/framework/trunk/htdocs
        SetEnv CLEAROS_BOOTSTRAP /home/devuser/clearos/webconfig/framework/trunk/shared
        SetEnv CLEAROS_CONFIG /home/devuser/.clearos

        # Enable SSL
        SSLEngine on
        SSLProtocol all -SSLv2
        SSLCipherSuite ALL:!ADH:!EXPORT:!SSLv2:RC4+RSA:+HIGH:+MEDIUM:+LOW
        SSLCertificateFile /usr/clearos/sandbox/etc/httpd/conf/server.crt
        SSLCertificateKeyFile /usr/clearos/sandbox/etc/httpd/conf/server.key

        # Rewrites
        RewriteEngine on
        RewriteCond %{REQUEST_METHOD} ^(TRACE|TRACK)
        RewriteRule .* - [F]
        RewriteRule ^/app/(.*)$ /app/index.php/$1 [L]

        # Developer aliases
        Alias /cache/ /var/clearos/framework/cache/
        Alias /themes/ /home/devuser/clearos/webconfig/themes/
        Alias /clearos/approot/ /home/devuser/clearos/webconfig/apps/

        # Personal or 3rd party SVN trees
        # Alias /example/approot /home/devuser/example/webconfig/apps
        # Alias /example2/approot /home/devuser/example2/webconfig/apps
</VirtualHost>
</code>

{{keywords>clearos, clearos content, maintainer_dloper, maintainerreview_x, keywordfix}}
