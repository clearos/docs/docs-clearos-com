===== Zarafa to Kopano Upgrade for ClearOS 7 =====

In 2016, Zarafa re-branded to Kopano.  As of ClearOS 7.3, Kopano Basic is now the supported mail and messaging collaboration suite in place of Zarafa.

If you are running ClearOS 7 and are using Zarafa and wish to upgrade to Kopano Basic, this article is for you.

<note warning>For Zarafa Community users, if you would like to upgrade to Kopano Basic, you will need to purchase a Kopano Basic subscription for sufficient users using the platform.</note>

==== Backup ====
As with any upgrade, please take precautions and backup your Zarafa instance in the event you need to roll-back.

==== Command Line Upgrade ====

Stop all Zarafa services.  You will also want to stop Postfix MTA or firewall inbound port 25 to prevent new mail from coming in.

<code>
service zarafa-gateway stop
service zarafa-server stop
service zarafa-ical stop
service zarafa-presence stop
service zarafa-licensed stop
service zarafa-dagent stop
service zarafa-spooler stop
service zarafa-search-plus stop
service zarafa-monitor stop
</code>

Remove Zarafa apps...this does not delete/destroy your mailboxes.

<code>
yum remove zarafa-* app-zarafa-extension-core
</code>

Zarafa packages have some issues with scripts on un-install.  You'll need to manually remove some.

<code>rpm -e --noscripts zarafa-search-plus zarafa-gateway zarafa-server zarafa-dagent zarafa-spooler zarafa-ical zarafa-presence zarafa-monitor
</code>

Install Kopano.  You will only be able to do this step if you have purchased Kopano basic and assigned it to your server or have contacted the ClearCenter team and requested a Zarafa Small Business to Kopano Basic upgrade.

<code>yum --disablerepo=private-clearcenter-zarafa-small-business,private-clearcenter-zarafa-community install app-kopano-basic</code>

Your users will now require you to enable the Kopano extension.  In Webconfig, navigate to 'System -> Accounts' and edit each user to enable the Kopano extension.

<note info>If you have many users, you may want to user the Account Import utility available from the Marketplace.</note>

Time to fix database properties.  Get the system MySQL password for your old Zarafa install.

<code>cat /var/clearos/system_database/zarafa</code>

Next, edit /etc/kopano/server.cfg

Find these three settings that will be unique or refer to Kopano, and change them to the values provided below:

<code>
mysql_user = zarafa
mysql_password = YOUR ZARAFA MYSQL PW
mysql_database = zarafa

</code>

Next, sync the attachments directory.  Use rsync tool.

<code>
yum install rsync
rsync -avz /var/lib/zarafa/attachments/ /var/lib/kopano/attachments/
chown -R kopano.kopano /var/lib/kopano/attachments/
</code>

If you don't want to use rsync because of space requirements, you can simply move the files:

<code>
mv /var/lib/zarafa/attachments/* /var/lib/kopano/attachments/
chown -R kopano.kopano /var/lib/kopano/attachments/
</code>

Fix Zarafa Z-push (aka ActiveSync) config in /usr/share/zarafa-z-push/config.php, setting the full email option to false:

<code>
 define('USE_FULLEMAIL_FOR_LOGIN', false);
</code>

Restart the Kopano services.

<code>kopano-condrestart</code>

You should now do comprehensive testing before allowing new mail to flow in.

If everything checks out, restart any services (like Postfix) you may have stopped and/or open firewall to allow mail to begin flowing in again.


{{keywords>clearos, clearos content, clearos7, kb, categorysystem, maintainer_bchambers}}