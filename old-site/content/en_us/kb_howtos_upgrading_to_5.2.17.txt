===== Upgrading to PHP 5.2.17 in ClearOS 5.2 =====

<note important>You are required to install Tim's Repo first - you can find that here:

[[http://www.clearfoundation.com/docs/howtos/adding_tim_s_repo]]
</note>
Please note these are not the official packages, and have been put together by http://rpms.famillecollet.com/ for RHEL5 distributions (thank you!). I have rebuilt these so they install for ClearOS with the addition of two RPM's (libedit and sqlite2). 

==== Options ====

You can also install many other PHP features through YUM. 
<code>
yum install php-* 

yum list php-* </code>for all the available extensions you can add.

==== Download ====
<code>
yum --enablerepo=timb upgrade php
</code>

==== Removal ====

<code>
yum remove php-*
</code>


==== Forum Page Link ====

[[http://www.clearfoundation.com/component/option,com_kunena/Itemid,232/catid,17/func,view/id,19846/]]

Please post support issues there. 
{{keywords>clearos, clearos content, howto, php, clearos5, maintainer_dloper, maintainer_tburgess}}
