===== OpenEMR =====
This howto will guide you through some settings to make [[http://www.oemr.org|OpenEMR]] run under ClearOS 5.2. OpenEMR is an open source [[http://en.wikipedia.org/wiki/LAMP_%28software_bundle%29|LAMP]] software designed for medical records. "OpenEMR is an [[http://onc-chpl.force.com/ehrcert/EHRProductDetail?id=a0X30000003jFmdEAE&retURL=%2Fehrcert%2FEHRProductSearch&setting=Ambulatory|ONC modular “Meaningful Use” certified]] electronic health records and medical practice management application. It features fully integrated electronic health, records, practice management, scheduling, electronic billing. After an 18 month development effort from our "Meaningful Use" team we have been Meaningful Use Certified for use inside the United States. As of August 11, 2011 we passed at the 100% level."

===== Getting Started =====
To prepare your OpenEMR installation you will need to configure some basic elements of ClearOS. Most elements are accomplished via the Webconfig interface.

=== Groups ===
Prepare a group that will be used to assign security for the EMR Website. This groups should have membership limited to the administrators of the system. Navigate to <navigation>Directory >> Accounts >> Groups</navigation>. Specify //'emr-admin'// in the **Group** field and add //'OpenEMR admin group'// in the **Description** field.

=== DNS and Naming ===
It is good practice to set a DNS name for your OpenEMR application. If you are using ClearOS for your local DNS server, navigate to <navigation>Network >> Settings >> Local DNS Server</navigation>. For example (if your domain is example.com), you can set the address for the internal IP of your server and the hostname //emr.example.com//.

=== MySQL ===
Navigate to <navigation>Server >> Database >> MySQL</navigation>. Create a password for the MySQL root user. For security purposes, this password should be different than root user of the system. This password should be secure and stored securely. Start the MySQL service if it is not running and set the status to boot automatically.

OpenEMR requires special settings on the MySQL server that may preclude operation with other LAMP applications. It is recommended that you run OpenEMR only with compatible LAMP applications. Specifically, OpenEMR requires STRICT_MODE to be disabled. You can do this from the web based management modules linked from within the Webconfig MySQL module. Click **Go** in the MySQL module and enter the //'root'// username and password for the MySQL server.

In the variables tab, ensure that the variable **sql mode** does not have a mode listed that has any //Strict// values. 

== Set non-strict mode ==
If a strict mode is listed it may be that you are using a strict mode for a particular LAMP application then you will want to use a different MySQL server to avoid an incompatibilty. If you are sure that your want to change the value enter the SQL tab and enter the following into the **Run SQL query/queries on server "localhost":** field.

  SET @@global.sql_mode= '';

Click **Go**. 

=== Web Server ===
You will need to enable a virtual server. Navigate to <navigation>Server >> Web >> Web Server</navigation>. For compliance, make sure that you make your web server **SSL-Enabled**. Click **Update**. Enter the name you have previously entered above into the DNS Settings section. Click **Add**. If you want to use the file services to access your server, select **Yes** in the //Allow File Server Upload// section. Change the //Upload Access// section to use the group you specified earlier in the Groups section. Click **Update**.

Ensure that the Web Server is started and the set to start on boot.

===== Installing OpenEMR =====
For the installation of OpenEMR we will be using the command line. Use your favorite terminal program (ie. PuTTY). Log into the server as root.

Download the OpenEMR code from [[http://www.oemr.org/wiki/OpenEMR_Downloads]]. For example:

  wget http://voxel.dl.sourceforge.net/project/openemr/OpenEMR%20Current/4.0.0/openemr-4.0.0.tar.gz

Extract the files:

  tar pxvzf openemr-4.0.0.tar.gz

Move the files to the virtual server directory. For example:

  cd openemr-4.0.0
  mv * /var/www/virtual/emr.example.com/
  cd ..
  rmdir openemr-4.0.0
  cd /var/www/virtual/emr.example.com/
  chown -R flexshare:emr-admin /var/www/virtual/emr.example.com/.

===== Setting up your Site =====
Once the site is uploaded you will be able to run the setup by navigating your browser to **https://emr.example.com/setup.php*

/etc/httpd/conf.d/openemr.conf

  <Directory "/var/www/virtual/emr.example.com/sites/default/documents">
  order deny,allow
  Deny from all
  </Directory>
  <Directory "/var/www/virtual/emr.example.com/sites/default/edi">
  order deny,allow
  Deny from all
  </Directory>
  <Directory "/var/www/virtual/emr.example.com/sites/default/era">
  order deny,allow
  Deny from all
  </Directory>



  
{{keywords>clearos, clearos content, kb, howtos, clearos6, skunkworks, clearos7, categoryserver, maintainer_dloper, maintainerreview_x, keywordfix}}
