===== ClearOS Directory Model =====
Directories are amazingly powerful things. Originally they were used like a phone book or a [[http://en.wikipedia.org/wiki/Rolodex|rolodex]]. To this day, they can still be used as such but in using software to produce these directories it was soon discovered that they could be used for so much more. Today, directories are by an large used for Authentication, Authorization, Accounting and Access Control.

The goal of ClearOS is to provide powerful open source tools that allow users everywhere the ability to have proper security, filtration, and management. Directories can be leverages to provide a foundation for all three of these objectives.

===== Models for management =====
Our usage of a directory comes from the need to offer a basis for identity management across disparate services. There are multiple models for management of a directory some of which we currently use with ClearOS, some of which are on the roadmap, and some which we have decided not to pursue. I will discuss some of them here. They include:

  * Stand-Alone
  * Master/Replicate
  * Master/Slave
  * Directory-less
  * Directory-less with caching
  * Synchronization
  * Active/Active with Catalog

==== Stand-Alone ====
By far, the most deployed directory model for management of ClearOS is 'Stand-Alone'. This is ClearOS' default behavior and it is the easiest to deploy. 

==== Master/Replicate ====
This model has one server which contains all of the records of a directory and all of the replicates contain read-only methods. The replicate servers can take a snapshot of the master and run with incremental changes being applied whenever the master changes. In this model, either the entire directory structure or a partition of such will be replicated. ClearOS 5.1 and above support this mode.

==== Master/Slave ====
In this model, the master server contains the authoritative records but slave server are read/write capable. The slave servers will post their changes to the master server and the master will usually accept them. If the master does NOT approve the change the slave server will remove its record as well. Like Master/Replicate, either the entire directory structure or a partition of such will be replicated. In the Windows world, this is the 'PDC/BDC' paradigm.

==== Directory-less ====
The directory-less model means that a server will store no information about any account. Instead, when a request for a service is made, it will reference a valid directory server and leave the decision to that trusted directory controller. Directory-less systems are unaware of any directory structure that may exist on a directory controller. In the Windows world, this is the 'Domain Member' paradigm.

==== Directory-less with caching ====
This is exactly the same as above except that when an authentication is requested, the server will note and store the current credentials used. If it cannot contact the authorizing directory on an authentication request, it will validate the user based on the cached credentials. This is common in the windows 'Domain Member' method. ClearOS 6.0 supports this mode with the ability to connect to Active Directory.

This is an excellent choice of topologies to use when you are trying to integrate a simple ClearOS gateway to complement an Active Directory environment. This is best deployed when the ClearOS server is on the same subnet as a reliable domain controller.

==== Synchronization ====
Synchronization is a method whereby a directory will transfer specific records between two directories. Both directories can be master. The advantage of this method is that it can move only directory components that you desire and it doesn't require preservation of the directory structure. The disadvantage is that you must define complex rules and you usually use non-native tools to do so. Additionally, changes to the master or target structure can unknowingly break a rule. You are also reliant on an agent to either capture the changes as they occur or to poll the directory and scan for differentiation.

This method is used by many because it does not require deep integration or interoperability for it to be functional. However, we did not consider this paradigm for a number of reasons:

  * Requires additional software 
  * Doesn't work well with Active Directory
  * Security

One of the target goals for ClearOS is the ability to connect to disparate directory structures. ClearOS is intended to be a platform for connectivity rather than a solution only. This means that we build in mind that users may wish to run ClearOS against differing directory topologies. We don't want to engineer people away from choice. Creating a single technology for synchronizing directory elements is well beyond the scope of what we are trying to do. A good alternative if you still want to use this approach may be [[http://www.infoworld.com/d/security-central/identity-management-challenge-506|one of these]]:

  * Courion Enterprise Provisioning Suite
  * IBM Tivoli Identity Manager
  * Microsoft Identity Integration Server
  * Novell Identity Manager
  * Sun Java System Identity Manager
  * Thor XellerateIM

==== AD ====
One of the obvious interoperable platforms on the radar is Active Directory (AD). Using a synchronization method with AD always requires a agent to reside on each of the AD server in a domain. Let me explain.

Active Directory keeps user passwords in non-reversible encryption (UnicodePwd). In addition, they CANNOT be read! While it is possible to get access to the hashes, they are relatively useless except for services that use that exact hashing mechanism.

What is typically done is that synchronization software will include a binary which must be installed anywhere where a password change event occurs (ie. on every single domain controller). This way when a password change occurs, this software will perform a 'man-in-the-middle' hijack of the username and password and then transmit the password to the synchronization agent on the target server.

We have decided to not use synchronization as a method because it requires non-standard oriented software to be installed on the Windows servers and that software must always be running or troubles may ensue. Additionally, users are all required to change their passwords once this technology is implemented because there is no way to create a valid database of usernames and passwords without the password hijack having occurred.

==== Active/Active with Catalog ====
Active/Active means that all directory service members contain read/write methods and that they have robust controls for conflict resolution so that no member is considered inferior when it comes to change control. In this mode, one or more members will be tasked with keeping track of the log of change called a catalog. This catalog is referenced when trying to 'play back' the events of change in the directory. This is the Active Directory model and will be added to ClearOS at the completion of Samba 4.

This is a great solution for expanding your existing Active Directory to multiple sites or to create a simple backup of your AD server.
{{keywords>clearos, clearos content, kb, skunkworks, clearos5, clearos6, maintainer_dloper}}
