===== Dynamic Firewall =====
The dynamic firewall app allows an administrator to generate and implement very specific, time-based, firewall rules triggered off events. For example, rather than opening up ports for SSH or OpenVPN to the entire Internet, the Dynamic Firewall app can be configured to open these ports after a user authenticates via Webconfig (ideally, using two-factor) from the source IP of the user logging on. In short, this app allows you to reduce your network's exposure while still providing essential services to remote users.

To learn more about this app, click [[https://www.clearos.com/products/purchase/clearos-marketplace-apps/network/dynamic_firewall|here]]
===== Documentation =====
==== Version 6 ====
This app is not available for ClearOS 6.
==== Version 7 ====
Documentation for ClearOS 7 can be found [[content:en_us:7_ug_dynamic_firewall|here]].
==== Additional Notes ====