===== Download Packages from YUM Repository =====
If you want to get packages from the YUM repository you can use the yum-downloadonly option to get these.

===== Setup =====
==== ClearOS 5.x ====
Install the yum-downloadonly package onto your ClearOS 5.x server:

  wget ftp://ftp.gtlib.gatech.edu/pub/centos/5.8/os/i386/CentOS/yum-downloadonly-1.1.16-21.el5.centos.noarch.rpm
  rpm -Uvh yum-downloadonly-1.1.16-21.el5.centos.noarch.rpm

==== ClearOS 6.x ====
To install yum-downloadonly onto ClearOS 6.x server:

  yum -y install yum-plugin-downloadonly

===== Using downloadonly =====
The following command would download the ClearOS bridge-utils.i386 for your current version to the /root directory on your server

{{keywords>clearos, clearos content, kb, clearos6, clearos7, categorysystem, howtos, maintainer_dloper}}
