===== Transmission Bittorrent Client-Legacy =====
{{:howtos:transmission-bittorrent-300x300.png?150 |Transmission BitTorrent client for ClearOS}}

[[http://www.transmissionbt.com/|Transmission]] is an open-source BitTorrent client.  The software has a low memory footprint and includes a web-based version that is designed to operate in headless environments.  The software includes the features that you need in a modern BitTorrent client: encryption, peer exchange, magnet links, DHT, webseed support, watch directories, tracker editing, global and per-torrent speed limits, and more.
 
<note warning>Transmission is now available for free through the [[http://www.clearcenter.com/support/documentation/user_guide/transmission_bittorrent_client|ClearOS 6 Marketplace]].  The following document is here for historical purposes only.</note>

===== Installation =====
To install the Transmission BitTorrent client, run the following from the command line:

  yum --enablerepo=base-extras install transmission 

==== Set Admin Password ==== 
After installing the software, you will need to set an administrator password with the following command:

  transmission-password pick_a_password 
  
==== Start the Service ====
We're almost there.  The next step is to start the Transmission service:

  service transmission-daemon start
  
By default, the service will start on a reboot.

==== Connect via Web Interface ====
Using your favorite web browser, point it to your new Transmission BitTorrent client running on port 9091:

  http://192.168.1.1:9091
  
Change 192.168.1.1 to the IP address of your ClearOS system.  Enter the following credentials when prompted for a username and password:

  * **Username** - admin
  * **Password** - set using the transmission-password command 

{{:howtos:transmissionclearos.png|Transmission BitTorrent Web Interface}}

The above screenshot is from the Transmission BitTorrent web interface.

===== Configuration =====
{{ :howtos:transmissiondownload.png?120|Transmission BitTorrent - changing preferences}} Before you begin downloading, there are two important settings that you should set: i) the download directory and ii) the default port number.  To set these parameters, click on the menu button in the bottom left corner (see adjacent screenshot) and select <navigation>Preferences</navigation>.

==== Download Directory ====
On the **Preferences** page, set the download directory to whatever you desire.  Feel free to use a Flexshare directory so that you can access the files via Windows Networking, the web and even FTP.  In the  screenshot below, the download directory is set to /data/media/Downloads/Transmission.  The transmission user must have permissions to write to this directory!  Use the [[http://www.clearfoundation.com/docs/man/index.php?s=1&n=chown|chown]] command to do this, for example:

  chown transmission.allusers /data/media/Downloads/Transmission

==== Incoming TCP Port ====
You will also want to make sure the chosen **Incoming TCP Port** is open on the ClearOS firewall.  Add this port via the <navigation>Network|Firewall|Incoming</navigation> page in Webconfig.

{{:howtos:transmission_download_and_port.png|Transmission BitTorrent - setting the download directory and port number}}
 
===== Third Party Tools =====
There are some very nice third party tools for Transmission including:

  * [[http://code.google.com/p/transmission-remote-dotnet|A Windows client to remotely manage Transmission on ClearOS]]
  * [[http://code.google.com/p/transmisson-remote-gui/|A Linux, Mac and Windows client to remotely manage Transmission on ClearOS]]
  * [[http://www.transdroid.org/|An Android client to remotely manage Transmission on ClearOS]] 
  * [[http://www.transmissionbt.com/resources.php|And more...]]

===== References =====
{{keywords>clearos, clearos content, kb, howtos, maintainer_dloper, maintainerreview_x, keywordfix}}