===== Creating a Secure Gateway for SMB and Enterprise =====
/**
 * Comments can be given in this format.
 */

/* Comments can also be given in this format */
This guide will help you create a secure gateway from the standpoint of a hostile or potentially hostile LAN segment. While creating firewalls that prevent access from external Internet is common, treating your local LAN segment as hostile as well can sometimes be a daunting process.

This guide will take you through the WHOLE process from the beginning to the end. There are also points in this document that will take you to other documents that will help you deploy this at scale. At points that are for deploying at scale, you will be given a link to other documents that cover the scaling topic for that section. Otherwise, this document will help you put together a single proof of concept (PoC) box. For purposes of this demonstration, we will be using a single server with two network cards (the minimum for a gateway.) 

===== What You Will Need =====
The following list of things you will need in order to deploy this solution:
  * A prepurchased license or demo of ClearOS Business (Silver or higher recommended)
  * A prepurchased license of IDS updates (included with Silver or higher)
  * A prepurchased license of Gateway.Management for Business
  * A server with 2 or more NICs

In all cases for licensing, you can use a 30 day demo license for PoC deployments. Contact ClearCenter sales for a demo license.

===== Install ClearOS =====
For your PoC of the technology, feel free to install ClearOS directly onto the server using a USB key or DVD method. You may also have ClearOS preinstalled in some cases. For deploying ClearOS [[:content:en_us:kb_making_a_recovery_partition_in_clearos|at scale, use this]] or another method (such as imaging via PXE or USB to disk.)

{{keywords>clearos, clearos content, gateway, app-gatewaymanagement-community, clearos7, categorygateway, maintainer_dloper}}
