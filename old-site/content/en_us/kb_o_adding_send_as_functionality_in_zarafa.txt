===== Adding Send As Functionality in Zarafa =====
This howto is intended to be a guide for adding 'Send As' functionality to ClearOS' implementation of Zarafa. It is also intended as a technical guide for inclusion of 'Send As' functionality in the user interface.

===== Server configuration =====
The default Zarafa Server configuration in /etc/zarafa/ldap.cfg contains the following attribute which do NOT need to be changed:

  * ldap_sendas_attribute = zarafaSendAsPrivilege
  * ldap_sendas_attribute_type = text
  * ldap_sendas_relation_attribute =

=== ldap_sendas_attribute ===
This is the attribute in the user record which contains the resource which has access to this user ability to 'send as'.

=== ldap_sendas_attribute_type ===
Possible values here are 'text' and 'dn'. The value'dn' is required for Active Directory. The value 'text' is required for OpenLDAP.

=== ldap_sendas_relation_attribute ===
This value is blank and when left blank refers to the ldap_user_unique_attribute value in the same file which in this case is:

  * ldap_user_unique_attribute = uidNumber

So it is the uidNumber that needs to be correlated to the attribute type.

===== Manually adding =====
Sufficient testing is required to determine if a 'service zarafa-server reload' is required after an add of the zarafaSendAsPrivilege attribute to the OpenLDAP database.

==== Preparation ====
You will need the following information to proceed with an add:

  * Bind DN
  * Bind password

Both of these can be obtained by referencing the Directory Server module in Webconfig. For this example we will be using the Bind DN of 'cn=manager,ou=Internal,dc=example,dc=com' and the Bind password 'abcdefghijklmnopq'.

You will also need a list of user DNs for EACH user that will be granted 'Send As' permissions for. You will also need a list of users that will be granted 'Send As' permissions to. This can be confusing so let's imagine that I have two users; Angela Bobson (**abobson**) and Bruce Tomson (**btomson**). I need to give **abobson** the ability to send emails as though from her email account where she changes the from address to **btomson** and those emails appear to be from him.

For this exercise, I will need atomson's uidNumber and btomson's DN. You can gather all this information by performing a dump of your LDAP user database:

  slapcat -n3 > /root/ldapdump

This command will overwrite any existing file in this directory of the same name with the current contents of your user database in LDAP.

You can then edit this text file and find your information.

<note warning>Before proceeding with manual changes to your LDAP environment, we suggest a backup of your directory. This information is given without warranty. Use at your own risk.</note>

==== Making the change ====
=== Creating the LDIF file ===
To make the change to LDAP you will need to make an LDIF import file. Let us say these are the two pertinent source records we are dealing with:

<code>
dn: cn=Angela Bobson,ou=Users,ou=Accounts,dc=example,dc=com
uidNumber: 1007
uid: abobson
givenName: Angela
sn: Bobson
objectClass: top
objectClass: posixAccount
objectClass: zarafa-user
cn: Angela Bobson
mail: abobson@example.com
</code>

<code>
dn: cn=Bruce Tomson,ou=Users,ou=Accounts,dc=example,dc=com
uidNumber: 1008
uid: btomson
givenName: Bruce
sn: Tomson
objectClass: top
objectClass: posixAccount
objectClass: zarafa-user
cn: Bruce Tomson
mail: btomson@example.com
</code>

From this information we will create our ldif file by supplying the values for the appropriate DN and uidNumber. Use this information to create a file named after the user that will have their name in the from field (ie. btomson). This file will contain the DN for btomson and the uidNumber for abobson.

== btomson.ldif ==
<code>
dn: cn=Bruce Tomson,ou=Users,ou=Accounts,dc=example,dc=com
changetype: modify
add: zarafaSendAsPrivilege
zarafaSendAsPrivilege: 1007
</code>

=== Importing the attribute ===
We will use the Bind DN information gathered and the Bind password to now add this ldif change to the running OpenLDAP database:

  ldapmodify -D cn=manager,ou=Internal,dc=example,dc=com -w abcdefghijklmnopq -f btomson.ldif

==== Testing it out ====
You may need to reload the zarafa-server service to make this go into effect immediately.

  service zarafa-server reload

Test this in Outlook by adding the From field to your send message box. Then be sure to pull the name from the Global Address Book on Angela's machine and select 'Bruce Tomson'

===== Links =====

  * [[http://doc.zarafa.com/6.40/Administrator_Manual/en-US/html/_send_as_permissions_option.html|Zarafa Manual - Send as Permissions option]] Pay special note to the last paragraph about the flip.
  * [[http://doc.zarafa.com/7.1/User_Manual/en-US/html/_delegation.html|Additional Zarafa Documentation]]
{{keywords>clearos, clearos content, AppName, app_name, kb, howto, xcategory, maintainer_dloper, maintainerreview_x, keywordfix}}
