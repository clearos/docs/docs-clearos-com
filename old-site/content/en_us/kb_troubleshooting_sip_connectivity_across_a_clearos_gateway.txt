===== Troubleshooting SIP connectivity across a ClearOS Gateway =====
By default, ClearOS handles most SIP traffic without any difficulty at all. However, if you are having difficulty with SIP traffic, you may need additional support across the firewall. This guide will help you test if those kernel connectivity helpers improve your SIP connections.

===== See if it works =====
To enable additional SIP connection support in ClearOS, run the following from command line:

  modprobe ip_nat_sip

Now test your phone. It is crucial that you test the results because this is one of many tools that can be useful and in some cases it can be deleterious (i.e. there are some reports that if SIP is running on the ClearOS server itself that this helper has a negative effect). 

You can validate that the module is running by performing the following:

  lsmod|grep sip

You should see an output similar to this:

<code>
nf_nat_sip              6171  0 
nf_conntrack_sip       19359  1 nf_nat_sip
nf_nat                 22759  9 nf_nat_sip,ipt_REDIRECT,nf_nat_h323,nf_nat_pptp,nf_nat_proto_gre,nf_nat_irc,nf_nat_ftp,ipt_MASQUERADE,iptable_nat
nf_conntrack           79389  16 nf_nat_sip,nf_conntrack_sip,xt_state,nf_nat_h323,nf_conntrack_h323,nf_nat_pptp,nf_nat_irc,nf_nat_ftp,ipt_MASQUERADE,nf_conntrack_pptp,nf_conntrack_proto_gre,nf_conntrack_irc,nf_conntrack_ftp,iptable_nat,nf_nat,nf_conntrack_ipv4
</code>

===== Making it permanent =====
If that works, we can make it so this module loads on ClearOS every time you boot. Create the following file: **/etc/sysconfig/modules/ip_nat_sip.modules**

Insert the following into that file:

<code>
#!/bin/bash
test=`/sbin/lsmod|grep nf_conntrack_sip`
if [ "$test" ] ; then
      echo Module already exists
      exit 0
else
      echo Adding ip_nat_sip modules
      exec /sbin/modprobe ip_nat_sip >/dev/null 2>&1
fi
</code>

Make this file executable:

  chmod +x /etc/sysconfig/modules/ip_nat_sip.modules

Test to see if the system has the module loaded after your next reboot by running the following:

  lsmod|grep sip

You should see an output similar to this:

<code>
nf_nat_sip              6171  0 
nf_conntrack_sip       19359  1 nf_nat_sip
nf_nat                 22759  9 nf_nat_sip,ipt_REDIRECT,nf_nat_h323,nf_nat_pptp,nf_nat_proto_gre,nf_nat_irc,nf_nat_ftp,ipt_MASQUERADE,iptable_nat
nf_conntrack           79389  16 nf_nat_sip,nf_conntrack_sip,xt_state,nf_nat_h323,nf_conntrack_h323,nf_nat_pptp,nf_nat_irc,nf_nat_ftp,ipt_MASQUERADE,nf_conntrack_pptp,nf_conntrack_proto_gre,nf_conntrack_irc,nf_conntrack_ftp,iptable_nat,nf_nat,nf_conntrack_ipv4
</code>


===== Help =====
==== Navigation ====

[[:|ClearOS Documentation]] ... [[..:|Knowledgebase]] ... [[:Knowledgebase:Troubleshooting:|Troubleshooting]]
{{keywords>clearos, clearos content, AppName, app_name, versionx, xcategory, maintainer_x, maintainerreview_x, titlefix, keywordfix}}
