===== Administrators =====
The administrator manager page allows you to add, delete and manage administrators on the system.

===== Installation =====
If your system does not have this app available, you can install it via the [[:content:en_us:7_ug_Marketplace|Marketplace]].

===== Menu =====
You can find this feature in the menu system at the following location:
 
<navigation>System|Accounts|Administrators</navigation>

===== Configuration =====
In some circumstances, you may want to grant access to specific reports and configuration pages in [[:content:en_us:7_ug_webconfig|Webconfig]] to other users.  In an example scenario, an administrator could be granted access to all the read-only reports, along with the [[:content:en_us:7_ug_users|user manager]] tool.

<note important>
Best practices in this area include what is called 'least privilege access' meaning a user should only be allowed to do what is needed for their role and no more.  This provides for greater security and less opportunities for malicious acts.
</note>

==== System Administrator Access ====
Administrator access can be enabled or disabled at any time.  For example, you may only want to enable administrator access while away on vacation.  To do this, you can either remove a user from the assigned group in Group Manager or delete the app policy in the Administrators app.

==== Create a Group of Administrators ====

Before getting started in the Administrators app, be sure to create one or more groups of administrators in the [[content:en_us:7_ug_groups|Group Manager]].

==== Creating an App Policy ====

{{content:en_us:7_administrators_01.png}}

Click <button>Add</button> to create a new app policy.

{{content:en_us:7_administrators_02.png}}

Fill in 'Policy Name' and 'Description'.

<note>
'Policy Name' is a string that cannot include any spaces.
</note>

Assign a group from the dropdown.

<note important>
Before creating a new policy, you'll need to make sure a group of users is created in the [[content:en_us:7_ug_groups|Group Manager]].
</note>

{{content:en_us:7_administrators_03.png}}

Click <button>Add</button>.\\
Here, you will be shown the list of all the features available in [[:content:en_us:7_ug_webconfig|Webconfig]].  Select which apps the policy will grant access to.

{{content:en_us:7_administrators_04.png}}

Once you've made your selections, click <button>Update</button>.

==== Editing a current App Policy ====

Click <button>Configure Policy</button> to change which apps can be accessed by the group assigned to the policy.\\
Click <button>Edit</button> to change the name of the app policy, its description, or group assigned.\\
Click <button>Delete</button> to delete the App Policy.

{{content:en_us:7_administrators_05.png}}

{{keywords>clearos, clearos content, Administrators, app-administrators, clearos7, userguide, categorysystem, subcategoryaccounts, maintainer_dloper}}
