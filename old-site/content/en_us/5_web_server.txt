===== Web Server =====
ClearOS includes the Apache web server -- the same software that powers many of the world's largest web sites.

===== Installation =====
If you did not select this module to be included during the installation process, you must first [[:content:en_us:5_software_modules|install the module]].

===== Menu =====
You can find this feature in the menu system at the following location:
 
<navigation>Server|Web|Web Server</navigation>

===== Configuration =====
==== General ====
=== Server Name ===
The server name is a valid name (for example, www.example.com) for your web server.  This name is used on some infrequently used error pages, so it is not all that important.

=== SSL-Enabled - Secure Site ===
The web server comes with built-in SSL encryption for enhanced security.  If your website requires a username and password for login, then it is a good idea to use encryption.  For instance, if you have the webmail or groupware solution installed, you should access their respective login pages via the secure web server.  In your web browser, you should use the encrypted **https://your.domain.com** instead of the un-encrypted **http://your.domain.com** (https vs http).  When enabled, all communication between the web server and user's web browser is encrypted using a 128-bit security key. 

<note warning>SSL encryption requires a web site certificate.  ClearOS automatically generates a default certificate that is secure.  However, this certificate is not verified by one of the web site certificate authorities (it costs at least $50 per year to maintain a verified web site certificate). Your users will see the following warning (or similar) when connecting to the secure web server.</note>

==== Virtual Hosts ====
The web server includes support for "virtual hosts".  This means your web server can be used for hosting more than one web site.

==== Adding Dynamic Content to Your Site ====
There are many options for adding dynamic content to a website:
  * Perl and CGI
  * PHP
  * JSP
  * ASP

PHP and perl CGI are installed by default.  The set-up and configuration of other engines are beyond the scope of this help document. 

===== Uploading files to your Server =====
To upload files to your server, you can enable either FTP or File Server access to the site or you can use both. To allow file access to your default website or your virtual website, simply set the pull-down box to 'Yes' in either the 'Allow FTP Upload section' or the 'Allow File Server Upload' and click 'Update'.

{{:omedia:ss-webserver-file-access.png?550}}
==== FTP access ====
If your server is running the firewall you will need to open up ports to support access of the FTP server from the outside. The web server storage resides in a virtual FTP site. You will need to open the following ports to access the default FTP storage: 2121, 65000-65100

{{:omedia:ss-webserver-ftp-incoming.png?550}}
==== File Server access ====
Samba access of your web server is only available from networks with the LAN role in IP Settings.

=== File Server access using Windows ===
You can access the resource in Windows by running the IP address as a UNC in the Run dialog box.
  \\ip_address

=== File Server access using Mac OSX ===
From Mac OSX, type Command+K in finder and type the address using the CIFS protocol.
  cifs://ip_address

===== Troubleshooting =====
==== ISP Blocking ====
Some ISPs are known to block web (port 80) traffic to residential broadband connections in an attempt to cut down on illegal sites hosted on their network.  If you think your configuration is set-up correctly and you suspect your ISP is blocking HTTP traffic, try a remote port scan.

==== Firewall Rules ====
A web server listens to client requests coming in on port 80 (HTTP) or 443 (HTTPS/secure).  Did you remember to open the correct port(s)?

===== Links =====
  * [[:content:en_us:5_firewall_incoming|Adding incoming firewall rules]]
{{keywords>clearos, clearos content, Web Server, app-web-server, clearos5, userguide, categoryserver, subcategoryweb, maintainer_dloper}}
