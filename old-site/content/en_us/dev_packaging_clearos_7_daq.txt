===== Packaging ClearOS 7 DAQ =====
DAQ is a data acquisition library developed for the Snort intrusion detection system.  It replaces direct calls to PCAP functions with an abstraction layer that facilitates operation on a variety of hardware and software interfaces without requiring changes to Snort. 

===== Reference =====
  * [[http://www.snort.org/snort-downloads|Download page]]
{{keywords>clearos, clearos content, app-intrusion-detection, dev, packaging, categorygateway, maintainer_dloper}}
