===== System Report =====
The **System Report** provides a historical view of various computing resources:

  * System Load
  * Memory Usage
  * Processes
  * Uptime

===== Installation =====
If your system does not have this app available, you can install it via the [[:content:en_us:7_ug_marketplace|Marketplace]].

===== Menu =====
You can find this feature in the menu system at the following location:
 
<navigation>Reports|Peformance and Resources|System Report</navigation>

===== Reports =====
==== System Details ====

==== Filesystem Summary ====

{{keywords>clearos, clearos content, System Report, app-system-report, clearos7, userguide, categoryreports, subcategoryperformanceandresources, maintainer_bchambers}}
