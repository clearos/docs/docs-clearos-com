===== TConsole =====
The tconsole provides a simple text-based login screen when the full graphical console (X-Windows) is not installed or enabled. 

{{keywords>clearos, clearos content, dev, packaging, maintainer_dloper, maintainerreview_dloper, keywordfix}}
