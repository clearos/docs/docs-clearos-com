===== DHCP Server =====
The **DHCP Server** app (Dynamic Host Configuration Protocol) allows hosts on a network to request and be assigned IP addresses.  This service eliminates the need to manually configure new hosts that join your network.

===== Installation =====
If your system does not have this app available, you can install it via the [[:content:en_us:6_marketplace|Marketplace]].

===== Menu =====
You can find this feature in the menu system at the following location:
 
<navigation>Network|Infrastructure|DHCP Server</navigation>

===== Configuration =====
==== Global Settings ====
=== Status ===
You can enable and disable the DHCP server at any time.

=== Authoritative ===
Unless you are running more than one DHCP on your network, enable **Authoritative** mode.  When this is enabled, then DHCP requests on unknown leases from unknown hosts will not be ignored.  This will be the case when a foreign laptop is plugged into your network.

=== Domain Name ===
The server can auto-configure the default Internet domain name for systems using DHCP on your network.  You can either use your own Internet domain (for example: //example.com//) or you can simply make one up (for example: //lan//).  Example:

  * A desktop system on your local network has a system name //scooter// and uses DHCP.
  * The domain name specified in the DHCP server is //example.com//.
  * On startup, the desktop system appends //example.com// to its system name.  Its full hostname would become //scooter.example.com//.

==== Subnet Configuration ====
In a typical installation, the DHCP server is configured on all LAN interfaces.  To add/edit DHCP settings for a particular network interface, click on the appropriate add/edit button.

=== Network ===
The network number is automatically detected by the system.

=== IP Ranges ===
Keep a range of IP addresses available for systems and services that require static addresses.  For instance, a [[:content:en_us:6_pptp_server|PPTP VPN server]] and some types of network printers require static IP addresses.

In a typical local area network, the first 99 IP addresses are set aside for static addresses while the remaining addresses from 100 to 254 are set aside for the systems using the DHCP server.  Adjust these settings to suit your needs and your network.

=== DNS Servers ===
The server can auto-configure the DNS settings for systems using DHCP on your network.  By default, the IP address of the [[:content:en_us:6_dns_server|DNS Server]] on your ClearOS system is used.  You should change this setting if you want to use an alternate DNS server.

=== WINS Server ===
If you have a Microsoft Windows Internet Naming Service (WINS) server on your network, you can provide the IP address to all Windows computers on your network.  This will allow Windows systems to access resources via Windows Networking.  You can enter the LAN IP address of your ClearOS system here if you have enabled [[:content:en_us:6_windows_networking|Windows networking]] on your system..

=== TFTP Server ===
If you have a TFTP server on your network, you can specify the IP address in your DHCP configuration.  Among other uses, TFTP is commonly used by VoIP phones.

=== NTP Server ===
If you have an NTP (time) server on your network, you can specify the IP address in your DHCP configuration.  Though most modern desktop systems are already configured with a time server, some devices and applications require a local NTP server available.

==== Active and Static Leases ====
A list of systems that are actively using the DHCP server is shown in the **Active Leases** table.  If you would like to make a DHCP lease for a particular system permanent, you can click on the appropriate <button>Add</button> button in this list.  

===== Common Issues =====
  * You should only have one (1) DHCP server per network.
  * Enabling DHCP on your Internet connection is... not a good idea.
{{keywords>clearos, clearos content, DHCP Server, app-dhcp, clearos6, clearos6, userguide, categorynetwork, subcategoryinfrastructure, maintainer_dloper}}
