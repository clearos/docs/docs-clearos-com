{{ :userguides:antiphishing.svg?80}}
<WRAP clear></WRAP>

===== Gateway Antiphishing =====
The Antiphishing engine is designed to help prevent your users from visiting sites designed to steal personal information by dubious means. This includes preventing known malicious sites that look and feel like banks or other trusted sites which entice or create false confidence in order to entice the user to entrust personal data such as usernames, passwords, or other personal or financial data. This app requires the installation and configuration of the Web Proxy and Content Filter apps.

To learn more about this app, click [[https://www.clearos.com/products/purchase/clearos-marketplace-apps/gateway/antiphishing|here]]
===== Documentation =====
==== Version 6 ====
Documentation for ClearOS 6 can be found [[content:en_us:6_antiphishing|here]].
==== Version 7 ====
Documentation for ClearOS 7 can be found [[content:en_us:7_ug_antiphishing|here]].
==== Additional Notes ====
