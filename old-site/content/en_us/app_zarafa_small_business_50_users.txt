{{ :userguides:zarafa_small_business_50_users.svg?80}}
<WRAP clear></WRAP>

===== Zarafa Small Business =====
Additional 50 users for Zarafa Small Business for ClearOS.

To learn more about this app, click [[https://www.clearos.com/products/purchase/clearos-marketplace-apps/server/zarafa_small_business_50_users|here]]
===== Documentation =====
==== Version 6 ====
Documentation for ClearOS 6 can be found [[content:en_us:6_zarafa_small_business_50_users|here]].
==== Version 7 ====
Documentation for ClearOS 7 can be found [[content:en_us:7_ug_zarafa_small_business_50_users|here]].
==== Additional Notes ====
