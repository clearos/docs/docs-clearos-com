===== ClearOS 7.2.0 Community Final Release Information =====
**Released: 7 March 2015**

ClearOS 7.2.0 Final is here!

ClearOS version 7.2 is primarily a maintenance and bug fix release. There are some general improvements to the wizard and minor features throughout please see the changelog for more detailed information


==== New Upstream Features Include ====

  * Support for LVM caching
  * Improved VM Support

ClearOS 7 features a Community, Home, and Business version. All versions of ClearOS will install from the same install image. You will be required to select your version during the initial setup wizard after your first boot.

==== Change Log ====
The full changelog can be found here:

  * [[https://tracker.clearos.com/changelog_page.php?version_id=161|Changelog - ClearOS 7.2.0 ]]


===== Known Issues =====

  * Samba 4 Directory (Still, very much in beta)
  * [[https://tracker.clearos.com/roadmap_page.php?version_id=171|Known bugs]]

===== Feedback =====
Please post your feedback in the [[https://www.clearos.com/clearfoundation/social/community/clearos-7-2-0-final-released-discussion|ClearOS 7.2 Final Discussion Forum]]. 


===== Download =====
==== ISO Images ====
^Download^Size^SHA-256^
|[[http://mirror.clearos.com/clearos/7/iso/x86_64/ClearOS-DVD-x86_64-7.2.0.iso|64-bit Full]]|816 MB|  b4da8ffa10945e4d95478edb32f7a3dd9691f9a6c6d7820b1ffa346f6eb6a878 |
|[[http://mirror.clearos.com/clearos/7/iso/x86_64/ClearOS-netinst-x86_64-7.2.0.iso  |64-bit Net Install]]|370 MB|  a7c75eefd626d6f2a1f1886b6582552164e5b2a77e89389870c287840639734f |



==== Virtual Machine and Cloud Images ====
Virtual Machine and Cloud Images are not yet available for this release.

==== Upgrade from Earlier ClearOS Community 6.x Releases ====
There is no supported method yet from upgrading from ClearOS 6 to ClearOS 7. 

==== Upgrade from Earlier ClearOS Community 7.x Releases ====
Upgrades from 7.1.0 on properly running systems will happen automatically. To manually upgrade from ClearOS 7.1.0 or to validate that you are at the latest version run:

  yum update



{{keywords>clearos, clearos content, announcements, releases, clearos7.1, final, clearos7.2.0, maintainer_dloper}}
