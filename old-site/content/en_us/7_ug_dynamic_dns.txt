===== Dynamic DNS =====
The following document provides information on how to activate and configure the **Dynamic DNS** service for your ClearOS system.  For an overview of the features and benefits of the service, please review the [[http://www.clearcenter.com/Services/clearsdn-dynamic-dns-1.html|service information here]].  Dynamic DNS is a free service provided by ClearCenter
.

===== Installation =====
If your system does not have this app available, you can install it via the [[:content:en_us:6_marketplace|Marketplace]]

===== Marketplace Tags =====
SYSTEM NETWORK HOSTNAME IP DYNAMIC DNS

===== Menu =====
You can find this feature in the menu system at the following location:
 
<navigation>Cloud|Services|Dynamic DNS</navigation>

===== Configuration =====

Once the ClearCenter Dynamic DNS app is installed, you can use the webconfig front end to make settings changes and view update history.

When you first navigate to the webconfig page, you'll see blank entries in the form fields...the service is not enabled by default.

Click on the **Edit** button.  You will be asked to login with your ClearCenter account username to proceed to make changes.

<note tip>Why do you have to authenticate to change settings?  Many Managed Service Providers provide access to the Webconfig for their customers...but would not want certain settings changed by novice 'admins' curious about making changes to the system.</note>

Your ClearCenter authentication token will be delete after 15 minutes of inactivity or when you logout of Webconfig.

==== Dynamic IP Updates ====
Enable and/or disables the dynamic DNS service.

==== Subdomain ====
Type a subdomain that you desire and then select a domain from list.  The subdomain can be left blank if you have [[:content:en_us:sdn_domain_registration|registered your own domain]], or, you can choose from one of the available free domains.

==== Domain ====
ClearCenter provides one or more freed domains that you can use to easily remember your hostname should you need remote access to your system.  If you have [[:content:en_us:sdn_domain_registration|registered a domain]] with ClearCenter or subscribed to ClearCenter's [[:content:en_us:sdn_domain_transfer|DNS service]], your domain will be listed in the dropdown box as an alternative to the ClearCenter default domain (poweredbyclear.com).

==== IP Address ====
The dynamic DNS solution on your ClearOS performs IP address updates automatically.  However, you can also manually update the IP address by altering the current IP address.

===== Custom Domains =====
If you have already registered your own domain or are considering [[:content:en_us:sdn_domain_registration|registering a domain]] with ClearCenter, you can take advantage of our Dynamic DNS service.  The service is fully integrated into the ClearOS system.

===== Logs/Notification History =====
If you are interested in seeing how many times your IP changed, you can view the logs which display all historical updates (automatic and manual) over the last three (3) months.

If you want to hide a log entry, click on the **Acknowledge** button.

<note important>Acknowledging a log entry does nothing except hide an entry from the summary table.</note>
===== Other Dynamic DNS Services =====
There are several organizations that provide dynamic DNS service (both free and for a fee).  

For example:

  * [[http://www.no-ip.com|No-IP]]
  * [[http://dyn.com/dns/|DynDNS]]
  * [[http://www.changeip.com/|ChangeIP]]
You will need to follow their guides to installing the client software that provides IP updates automatically.

===== Troubleshooting =====

==== Log Files ====
The /var/log/messages and /var/log/secure log files can provide clues when troubleshooting.

==== Force update via command line ====
If you want to force an update via the command line, run:
    dnsupdate

==== View cached IP setting ====
Your current cached public IP setting can be found by running:
    cat /var/clearos/dynamic_dns/state

==== My IP not updating ====
  - Check to ensure your system is registered to the ClearCenter network.
  - Check to ensure the Dynamic DNS service is activated
  - Try manually forcing an update
  - View the log files for information
    
{{keywords>clearos, clearos content, Dynamic DNS, app-dynamic-dns, clearos7, userguide, categorycloud, subcategoryservices, maintainer_bchambers}}
