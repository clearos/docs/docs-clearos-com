===== Installer Troubleshooting =====
There are thousands of pieces of hardware and related drivers available for use in x86-compatible world.  The advantage: consumer choice.  The disadvantage: hardware compatibility issues are common.  There are several debug screens in the installer that can help when an installation fails.  Use the Alt-Fx or Ctl-Alt-Fx where Fx is:

  * F1: main text install screen
  * F2: command line (not always available)
  * F3: general log
  * F4: driver log
  * F5: hard disk / CD log
  * F7: main install screen
{{keywords>clearos, clearos content, clearos6, userguide, categoryinstallation, subcategoryinstaller, subsubcategoryfullinstalliso, maintainer_dloper}}
