===== Setup Fail2Ban =====
[[http://www.fail2ban.org/|Fail2ban]] scans log files (e.g. /var/log/apache/error_log) and bans IPs that show the malicious signs -- too many password failures, seeking for exploits, etc. Generally Fail2Ban is then used to update firewall rules to reject the IP addresses for a specified amount of time, although any arbitrary other action (e.g. sending an email) could also be configured. Out of the box Fail2Ban comes with filters for various services (apache, courier, ssh, etc).


<code>
yum --enablerepo=clearos-epel install fail2ban
</code>

Config files are at /etc/fail2ban

==Related links==
  * http://www.clearfoundation.com/component/option,com_kunena/Itemid,232/catid,39/func,view/id,34519/
{{keywords>clearos, clearos content, fail2ban, app-web-server, clearos6, categoryserver, howtos, maintainer_dloper}}
