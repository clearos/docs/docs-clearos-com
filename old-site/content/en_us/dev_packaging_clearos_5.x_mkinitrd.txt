===== ClearOS 5.x mkinitrd =====
The mkinitrd software creates the filesystem images required to boot the system.

===== Patches =====
==== mkinitrd-kernel-name.patch ====
{{keywords>clearos, clearos content, AppName, app_name, version5, dev, packaging xcategory, maintainer_dloper, maintainerreview_x, keywordfix}}
