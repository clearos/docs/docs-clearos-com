===== DNSMASQ =====
Dnsmasq is lightweight, easy to configure DNS forwarder and DHCP server.  It is designed to provide DNS and, optionally, DHCP, to a small network.

===== Changes =====
  * The dnsmasq package was added to the Red Hat Enterprise Linux 5.x release after it was available in ClearOS.  ClearOS uses a more recent version. 
  * Merge with upstream in ClearOS 6.0


{{keywords>clearos, clearos content, AppName, app_name, version5, dev, packaging, xcategory, maintainer_dloper, maintainerreview_x, keywordfix}}
