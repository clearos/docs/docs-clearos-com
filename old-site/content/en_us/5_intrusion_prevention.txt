===== Intrusion Prevention =====
The intrusion prevention system blocks suspected attackers from your system.

===== Installation =====
If you did not select this module to be included during the installation process, you must first [[:content:en_us:5_software_modules|install the module]].

===== Menu =====
You can find this feature in the menu system at the following location:
 
<navigation>Gateway|Intrusion Protection|Intrusion Prevention</navigation>

===== Intrusion Protection Updates and ClearSDN =====
{{:omedia:clearsdn-icon-xxs.png }} The ClearSDN [[http://www.clearcenter.com/Services/clearsdn-intrusion-protection-4.html|Intrusion Protection Updates]] service provides weekly signature updates to improve the effectiveness of the intrusion prevention system.  These signatures are compiled from third party organizations as well as internal engineering resources from ClearCenter.  With intrusion prevention, it is important to keep false positives to a minimum. We keep tabs on the latest available updates and fine tune the system so you can focus on more important things.  
===== Configuration =====
The Intrusion Prevention system displays a list of IP addresses that have been blocked due to inappropriate network traffic. 

==== Description ====
=== SID ===
The SID corresponds to the Intrusion Detection ID that triggered the block.  This is a hyper-link that can be followed to reveal more information about the specific conditions that were matched.

=== Blocked IP ===
This is the IP address that triggered the block.  If this IP address should not be blocked, you can add it to a "don't block" list by clicking on Whitelist under Action.

=== Date / Time ===
The date/time fields show when the block occurred.

=== Time Remaining ===
The remaining block time is listed last.  The IP address will be unblocked when this reaches 0.

=== Action ===
A blocked host can be added to a Whitelist so it will not be blocked in the future.  You can also remove a blocked host using Delete.

==== Whitelist ====
If there are IP addresses in your Whitelist they will be listed below the Active Block List.  You can delete an entry by choosing Delete under Action.
{{keywords>clearos, clearos content, Intrusion Prevention, app-intrusion_prevention, clearos5, userguide, categorygateway, subcategoryintrusionprotection, maintainer_dloper}}
