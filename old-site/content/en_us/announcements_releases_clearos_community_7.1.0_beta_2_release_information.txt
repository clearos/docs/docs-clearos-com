===== ClearOS 7.1.0 Beta 2 Release Information =====
**Released: June 5, 2015**

ClearOS 7.1.0 Beta 2 has arrived! This release contains major improvements and insight to the eventual release of ClearOS 7 and replaces ClearOS 7.0.0 Beta 1 as the current running beta.

ClearOS version 7.1 introduces:

  * Samba 4, Directory (Microsoft® Active Directory Replacement)
  * New Google® Apps Connector
  * Updated Microsoft Active Directory Connector
  * IPv6 Ready
  * Streamlined Theme System
  * Dynamic Dashboard
  * Updated Antispam and Antivirus Engines
  * Updated IDS and IPS Engines
  * Event and Alert Notification Framework
  * Internationalization

New Upstream Features Include:

  * XFS and BTRFS Filesystem Support
  * Improved VM Support, including Microsoft Hyper-V

ClearOS 7 will feature a Community, Home, and Business version. All versions of ClearOS will install from the same install image. You will be required to select your version at the time of installation in the wizard.

Some of the new apps in the beta are **NOT** available via the Marketplace but can be installed via **yum**. Others are still in progress or not yet feature complete. You can find installation and app details further below.

<note>If you have not already done so, please review the [[:content:en_us:announcements_releases_test_releases|introduction to test releases]].</note>

===== Changes Since Beta 1 =====
  * Account Import
  * Multi-Wan
  * OpenVPN
  * Password Policies
  * MariaDB


The full changelog can be found here:

  * [[https://tracker.clearos.com/changelog_page.php?version_id=66|Changelog - ClearOS 7.1.0 Beta 2]]

===== Known Issues =====
  * Install wizard workflow does not allow for the selection of Home or Business version


===== Feedback =====
Please post your feedback in the [[https://www.clearos.com/clearfoundation/social/community/clearos-7-1-beta-2-released-discussion|ClearOS 7.1 Beta 2 Discussion Forum]]. 


===== Download =====
==== ISO Images ====
^Download^MD5Sum^
|[[http://mirror.clearos.com/clearos/testing/7.1beta2/iso/x86_64/ClearOS-DVD-x86_64.iso|64-bit]]| 574d87819c177161e15b59d4a867b2d8 |

==== Virtual Machine and Cloud Images ====
Virtual Machine and Cloud Images are not available for this beta.

==== Upgrade from Earlier ClearOS Community 6.x Releases ====
There is no supported method yet from upgrading from ClearOS 6 to ClearOS 7. There may be support for migration of configuration backups.


{{keywords>clearos, clearos content, announcements, releases, clearos7.1, beta2, previoustesting, maintainer_dloper}}
