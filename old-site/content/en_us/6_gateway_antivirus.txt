===== Gateway Antivirus =====
The **Gateway Antivirus** app protects your network from viruses.  The engine is used by various parts of your ClearOS system:

  * [[:content:en_us:6_content_filter|Content Filter]]
  * [[:content:en_us:6_mail_antivirus|Mail Antivirus]]
  * [[:content:en_us:6_antimalware_file_scan|Antimalware File Scan]]

===== Installation =====
If your system does not have this app available, you can install it via the [[:content:en_us:6_Marketplace|Marketplace]].

===== Menu =====
You can find this feature in the menu system at the following location:
 
<navigation>Gateway|Antimalware|Gateway Antivirus</navigation>

===== ClearCenter Antimalware Updates =====
The open source [[http://www.clamav.net|ClamAV]] solution is the antivirus engine used in ClearOS.  This software automatically checks for updates several times a day for new antivirus signatures.  This is already included in ClearOS for free!

{{:omedia:clearsdn-icon-xxs.png }} In addition, the ClearCenter [[http://www.clearcenter.com/Services/clearsdn-antimalware-updates-7.html|Antimalware Updates]] service provides //additional// daily signature updates to improve the effectiveness of the antivirus system.  These signatures are compiled from third party organizations as well as internal engineering resources from ClearCenter.  We keep tabs on the latest available updates and fine tune the system so you can focus on more important things.

===== Configuration =====
==== Block Encrypted Files ====
Some file formats, including zip files, can be optionally encrypted and password protected.  The antivirus system is not able to properly scan these password protected files.  Since many virus writers use this technique to bypass virus checking, you may want set your network policy to completely block encrypted files.

==== Maximum Files in Zip Files ====
When the antivirus system unpacks a compressed archive (zip file), a limit on the number of files is recommended to protect the system from a potential denial of service attack.  For this reason, we do not recommend setting this to **unlimited**. 

==== Maximum File Size in Zip Files ====
The vast majority of viruses are delivered in small files.  In order to preserve system resources, any file over the **Maximum File Size** limit will not be scanned for viruses.  

==== Maximum Recursion in Zip Files ====
A zip file can contain a zip file, which contains a zip file, inside another zip file, within a zip file, etc.  This technique of embedding multiple layers of zip files can be used to create a denial of service attack.  Keep this setting at the default unless you have very unusual requirements.

==== Update Interval ====
The open source antivirus engine ([[http://www.clamav.net|ClamAV]]) in ClearOS will check for new virus signatures on a regular interval.  Unless you are running on a very slow Internet connection, keep the update interval at the minimum.

===== Related Links =====
  * [[:content:en_us:6_antimalware_updates|Antimalware Updates]]
  * [[:content:en_us:6_mail_antivirus|Mail Antivirus]]
  * [[:content:en_us:6_mail_antimalware_premium_powered_by_kaspersky|Mail Antimalware Premium powered by Kaspersky]]
{{keywords>clearos, clearos content, Gateway Antivirus, app-gateway-antivirus, clearos6, userguide, categorygateway, subcategoryantimalware, maintainer_dloper}}
