===== Role Based Access Control - Overview =====
Role-Based Access Control (RBAC) enables the management of user permissions across infrastructure: public and private clouds, containers, hypervisors, and bare-metal devices. Each organization can have a number of teams, each with different access policies to the infrastructure. 

Note: Each organization will require a subscription plan. 

==== Nomenclature ====
  * Account - An account can have many organizations. Each organization will require a subscription.
  * Organization - Organization can have many teams.
  * Team -  Team has members.
  * Member - Members belong to 1 or more teams. Members have a unique username and password to access the ClearGLASS interface.
  * Policy - a TeamPolicy is associated with a Team. A Policy can have many rules
  * Rule - Rules define member permissions
==== Roles ====
  * //Account owner// - this role has complete administrative control; can create organizations, teams, add and remove members, and create and edit rules
  * //Members// - the default role for everyone else; a member can’t create a team or rules or invite members to join a team

RBAC workflow for an account owner

  - Create a Team.    

{{:content:en_us:cg_createteam.jpg}}

  - Create a Team Policy.

{{:content:en_us:cg_createteampolicy.png}}

  - Invite Members to join the team.

{{:content:en_us:cg_addmembers.jpg}}

Rules will apply to all team members

Organizations, Teams, and Rules are created by an account owner. A policy is always associated with a Team. Rules are always associated with a Policy and define Member’s permissions. Example:

-  //An account owner can create Rules which reside in a policy that specifies the public clouds a team member can provision a virtual server on.//

{{:content:en_us:cg_teampolicy.png}}

Rules are always assigned to a Team rather than directly to Members. To grant permissions to a Member, you first need to assign a member to a Team. Members inherit access permissions from the Team. A Member can belong to more than one Organization and/or Team.