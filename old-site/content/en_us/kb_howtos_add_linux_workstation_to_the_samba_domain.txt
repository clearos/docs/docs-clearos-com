===== Add Linux Workstation to the Samba Domain =====
You can add a Linux workstation to your ClearOS Domain. This howto will show you the steps that you need to take to get your workstation to use domain credentials to authenticate.

===== Preparation =====
Because of the variety of Linux distributions out there I will be targeting this howto towards Ubuntu 10.04.1. There may be key differences between distros that should be listed in the Appendix of this article.

<note warning>This guide is intended for new installations. Some information contained herein could jeopardize existing installations with existing user data including but not limited to loss of authentication ability</note>

==== Server Prep ====
You should already have your ClearOS server configured as a domain controller and user accounts. For purposes of this demonstration, we will make an account called [[http://www.youtube.com/watch?v=gvmyTZEqlo8|Tom Jones]]. We will give him the username 'tjones'.

==== Workstation Prep ====
Start with a fresh install of Ubuntu. We will assume that you have already configured it with your initial user. 
{{:howtos:ubuntu-install-1stuser.png?550|}}

You will also need to ensure that you have connectivity to the Internet for this installation. Log in to the workstation and let's get started.

Also, decide if you want cached authentication for this workstation. If this is a laptop and is used off-site, you will definitely want this feature. If security is a high concern and your workstation will never leave the environment, you may specifically NOT want it.

===== Setup =====
We will need a command line for most of this stuff. Open the terminal application under <navigation> Applications >> Accesories >> Terminal </navigation>.

We will be doing a lot of configuration as root. Type the following in the terminal window:

  sudo su -

You will be prompted for a password.

==== Install Packages ====
Type the following:

  apt-get install winbind samba

//I also installed the ssh package because I wanted to copy and paste much of what is in this howto.//

==== Setup Samba ====
Add the following lines to the /etc/samba/smb.conf in the **[global]** section.

  security = domain
  netbios name = UBUNTU_01
  password server = system.clearos.lan
  workgroup = ENTERPRISE
  idmap uid = 10000000-19999999
  idmap gid = 10000000-19999999
  winbind use default domain = yes
  winbind enum users = no
  winbind enum groups = no
  winbind use default domain = yes
  template shell = /bin/bash
  template homedir = /home/%D/%U
  domain master = no
  winbind enum users = yes
  winbind enum groups = yes
  add machine script = /usr/sbin/useradd -d /var/lib/nobody -g 100 -s /bin/false -M %u

Of particular note above, make sure your **netbios name**, **password server**, and **workgroup** parameters match the local machine name which is unique on the network, the DNS name that is resolvable for the ClearOS server, and the Domain name for your ClearOS server respectively.

Find and delete the line that says:

  workgroup = WORKGROUP

Lastly, run the **testparm** command to validate your entries are in the configuration and that it looks good.


==== Home directory for domain users ====
Make a directory for your domain users that matches the name of your domain. In this example, our domain is 'ENTERPRISE' so we will run the following:

  mkdir /home/ENTERPRISE

==== Name Server Switch ====
Modify your /etc/nsswitch.conf so that it looks like this:

  passwd:         compat winbind
  group:          compat winbind
  shadow:         compat winbind
  hosts:          files dns wins 
  networks:       files
  protocols:      db files
  services:       db files
  ethers:         db files
  rpc:            db files
  netgroup:       nis

==== PAM Authentication ====
There are several changes to pam.d that will need to change from the default format. There are 4 files that need to be either replaced, edited or modified.

=== common-account ===
Blow away and replace your /etc/pam.d/common-account file away with the following to command from the shell.

  cp /etc/pam.d/common-account /etc/pam.d/common-account~
  echo "account sufficient pam_winbind.so" > /etc/pam.d/common-account
  echo "account required pam_unix.so" >> /etc/pam.d/common-account

=== common-auth ===
Blow away and replace your /etc/pam.d/common-auth file away with the following to command from the shell.

  cp /etc/pam.d/common-auth /etc/pam.d/common-auth~
  echo "auth sufficient pam_winbind.so" > /etc/pam.d/common-auth
  echo "auth required pam_unix.so nullok_secure use_first_pass" >> /etc/pam.d/common-auth

=== common-password ===
Using vi or your favorite editor, add a snippet to the end of /etc/pam.d/common-password in the line:

  password	[success=2 default=ignore]	pam_unix.so obscure sha512

So that it has **min=4 max=50**

  password	[success=2 default=ignore]	pam_unix.so obscure sha512 min=4 max=50

=== common-session ===
Add a entry to the /etc/pam.d/common-session by running the following command:

  echo "session required pam_mkhomedir.so umask=0022 skel=/etc/skel" >> /etc/pam.d/common-session

==== Reboot ====
With all of these modifications, it is time to reboot the workstation.

  reboot

Don't forget to login and switch user back to root.

  sudo su -

==== Join the workstation to the Domain ====
Run the following to join the workstation to the domain. Replace the relevant value for the domain name. Here we used 'ENTERPRISE'.

  net rpc join -U winadmin

You will be prompted for winadmin's password. Once you supply it you should get the following message:

  Joined domain ENTERPRISE.

==== Add sudoers ====
Add the domain_users group to the sudoers list by editing the /etc/sudoers

  echo "%allusers ALL=(ALL) ALL" >> /etc/sudoers

Time to reboot again.

  reboot

===== Testing things out ===== 
If everything is copacetic then your install will be done and you can now log in as a domain member. If you have difficulty or would like to see some specifics about your environment or login information you can use some of the tools below.

==== WinBind Information ====
The command wbinfo can provide lots of useful facts about your winbind connectivity.

  wbinfo -n username

Tells us the SID of a particular user.

  wbinfo --all-domains

Shows all the domains on the local segment, you should have BUILTIN, UBUNTU_01 (the workstation name), and ENTERPRISE (the domain name).

  wbinfo -i username

Shows the entry for the user as it would exist in a password file.

==== Net ====
The net command is a powerful tool for Samba administration. You will need to sudo to run many of these commands.

  net getdomainsid

Returns the SID for the domain and the local workstation.

There are lots of these, type 'net' to get a list of all the things.

==== Get Entries ====
A very useful command for giving a sum total of all the entries from the combined local and samba lists. The getent utility can show a bunch of stuff including passwd, group, hosts, services, protocols, or networks.

  getent passwd

Shows all the users for the system. It enumerates the local list and the winbind list together.

  getent group

Shows all the groups. Like the passwd version, it will show local and winbind stuff.

===== Connecting Domain Resources =====
This section contains some useful scripts and other hacks for making cool stuff happen.

==== Network resources ====
You should be able to browse for shares on the network by using the <navigation> Places >> Connect to Server... </navigation> option.

==== Add home directory script ====
Gtk-bookmarks stores information about favorite locations to connect to. The advantage is that the resource is user specific so it in more secure than attaching a user to a common mountpoint.

  #!/bin/bash
  domain="ENTERPRISE"
  server="clearos"
  share="$USER"
  directory="" 
  desktopalias="$share"
  
  gtkbookmarkfile=$HOME/.gtk-bookmarks
  bookmark="smb://$domain;$USER@$server/$share/$directory $desktopalias"
  directorybookmark=`find .gtk-bookmarks | xargs grep "$bookmark"`
  
  if [ -z "$directorybookmark" ]
  then
    echo "Bookmark added"
    echo "$bookmark" >> $gtkbookmarkfile
  else
    echo "Bookmark exists"
  fi 

===== Laptops and Cached Credentials =====
Taking this workstation or laptop on the road? You will not be able to authenticate if you cannot talk to your server. If you need this capability, do the following.

Add the following line to your /etc/samba/smb.conf file in the **[global]** section:

  winbind offline logon = yes

Run the following commands to fix the common-account file in the /etc/pam.d/ directory:

  echo "account sufficient pam_winbind.so cached_login" > /etc/pam.d/common-account
  echo "account required pam_unix.so" >> /etc/pam.d/common-account

Make a file called /etc/security/pam_winbind.conf. Add the following:

  #
  # pam_winbind configuration file
  #
  # /etc/security/pam_winbind.conf 
  #
  [global]
  # request a cached login if possible
  # (needs "winbind offline logon = yes" in smb.conf)
  cached_login = yes


Restart winbind.

  /etc/init.d/winbind restart

Log off and back in again as the user. Then reboot the workstation off the network.

===== To Do =====
  * Add script support so that network scripts get applied



===== Links =====
  * [[http://ubuntuforums.org/showthread.php?t=91510|Special thanks to TFiedler for his input on the Ubuntu forums which was quite useful]]
{{keywords>clearos, clearos content, clearos5, clearos6, clearos7, categoryserver, kb, howtos, maintainer_dloper, maintainerreview_dloper}}
