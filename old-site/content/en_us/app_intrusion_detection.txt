{{ :userguides:intrusion_detection.svg?80}}
<WRAP clear></WRAP>

===== Intrusion Detection System =====
The Intrusion Detection app is the cornerstone of security for any size network.  The app uses the highly regarded Snort engine to perform real-time traffic analysis and packet logging on Internet Protocol (IP) networks.  The app can help identify, log and stop (using the IPS plugin) external attack vectors targeting the network (fingerprinting, buffer overflows, brute force authentication etc.).  The app contains over 1000 known attack vector signatures with another 8000+ signatures available (with continuous updates) via the IDS update subscription from ClearCenter (app available in the Marketplace).

To learn more about this app, click [[https://www.clearos.com/products/purchase/clearos-marketplace-apps/gateway/intrusion_detection|here]]
===== Documentation =====
==== Version 6 ====
Documentation for ClearOS 6 can be found [[content:en_us:6_intrusion_detection|here]].
==== Version 7 ====
Documentation for ClearOS 7 can be found [[content:en_us:7_ug_intrusion_detection|here]].
==== Additional Notes ====
