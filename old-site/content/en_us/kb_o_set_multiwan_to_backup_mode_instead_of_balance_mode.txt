===== Set MultiWAN to Backup Mode instead of Balance Mode =====
By default, using multiple ISPs for internet with Multiwan will work in balanced mode. You can weight interfaces for more of less of the traffic but if you want to allocate a connection to be a backup ONLY pipe there is no option in the web interface. It is possible to set a network connection to be a backup only if the primary fails by modifying a configuration file within ClearOS

===== Preparation and configuration =====
First, remove any specific settings that you may have allocated to the backup pipe including any rules that specifically send data types or destinations down that pipe.

Next, open a command line editor and modify the following:

=== ClearOS 6.x ===
Modify **/etc/clearos/multiwan.conf** using your favorite editor. For example:

  nano /etc/clearos/multiwan.conf

Next, add the following line with the interface specified that you wish to use for backup purposes only:

  EXTIF_BACKUP="eth2"

It will look something like this:

<code>
MULTIPATH="on"
MULTIPATH_WEIGHTS=""
EXTIF_BACKUP="eth2"
</code>

Save the file.

=== ClearOS 5.x ===

Modify **/etc/firewall** using your favorite editor. For example:

  vi /etc/firewall

Next, add the following line with the interface specified that you wish to use for backup purposes only:

  EXTIF_BACKUP="eth2"

The section that you modify will look something like this:

<code>
# Multipath
#----------

MULTIPATH="off"
MULTIPATH_WEIGHTS=""
EXTIF_BACKUP="eth2"
</code>

Save the file.

===== Cleaning up =====
At this point you can either reboot the server or restart the syswatch service. To reboot the server type:

  reboot

To restart syswatch, open a second terminal window (or Ctrl+Alt+F3 if you are at the console) and watch the following log to ensure that the behavior of the multiwan is correct:

  tail -f /var/log/syswatch

Back on the first console, restart the syswatch service

  service syswatch restart

After you have observed the proper behavior from the Multiwan as view from the syswatch log, you can cancel your 'tail' of syswatch by hitting Ctrl+c.

===== Multiple backups =====
If you want to have multiple backups use spaces to separate the entries. For example:

  EXTIF_BACKUP="eth2 eth3"
{{keywords>clearos, clearos content, AppName, app_name, versionx, xcategory, maintainer_dloper, maintainerreview_x, keywordfix, howto}}
