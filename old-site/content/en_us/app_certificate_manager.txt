{{ :userguides:certificate_manager.svg?80}}
<WRAP clear></WRAP>

===== Certificate Manager =====
SSL certificates are the industry standard for encrypting information sent over a network and can also be used to provide authentication, as in the case of SMIME email signature signing.

The Certificate Manager app provides an administrator with the ability to create a Certificate Authority (CA) which can then be installed as a trusted CA on any operating system, browser or mail client to ensure secure and authenticated communications between two parties.

Creating your own CA and using it to sign certificates is termed 'self-signing'.  Self-signing of certificates is as secure as purchasing signed SSL certificates from a Trusted CA like Thawte or Verisign, where prices range from $US 50-300 per year.  Self-signing is extremely convenient and cost effective if you are providing access to known users (for example, employees, clients, vendors etc.).  It is less convenient than a Trusted CA when dealing with unknown users such as website visitors using a browser to access your online store using HTTPS (HTTP over SSL), since the user will be prompted by their browser to trust the certificate that is presented to them.

To learn more about this app, click [[https://www.clearos.com/products/purchase/clearos-marketplace-apps/system/certificate_manager|here]]
===== Documentation =====
==== Version 6 ====
Documentation for ClearOS 6 can be found [[content:en_us:6_certificate_manager|here]].
==== Version 7 ====
Documentation for ClearOS 7 can be found [[content:en_us:7_ug_certificate_manager|here]].
==== Additional Notes ====
