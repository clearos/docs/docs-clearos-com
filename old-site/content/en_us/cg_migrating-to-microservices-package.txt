===== Migrating to Microservices Package =====
==== Microservices Getting Started Bundle ====
=== Are you making the move to microservices? ===
Navigating the promise vs. reality of microservices is difficult. Without solid hands-on experience and a well-thought plan, a migration project can quickly get bogged down with technical challenges, throwing it off schedule and delaying rollout. This is one of the biggest challenges facing businesses that try to adopt these new technologies.

ClearGLASS can help you avoid the gotchas and overcome the complexities. We are specialists who can help you navigate the microservices, containers, and Kubernetes ecosystem, ensuring you make good technology decisions and avoid vendor lock-in.

=== Step 1 - Preparation ===
Duration: 1 week

Assumptions: The customer has taken some steps towards containerizing application services.

ClearGLASS will provide the following services:

  * Analyze application architecture. Identify key components that can be broken down to microservices and provide know-how based on best practises. Provide container/app examples to showcase how an application is containerized. ClearGLASS will provide support as customer proceeds to containerize the full application.
  * Provide basic, technical overview of Kubernetes. This includes an introduction to working with Kubernetes, basic concepts and operations. A presentation and open discussion will be set up with the customer’s team and documentation will be provided for future reference.
  * Create and configure a ClearGLASS account. The ClearGLASS platform will be used  to manage aspects of your Kubernetes cluster and infrastructure. ClearGLASS will be used to add machines, configure keys, tags, networks, and other necessary components.
  * Provision a Kubernetes cluster to the cloud of your choice. The cluster will be used as a lab environment and for testing. Provisioning will happen from within ClearGLASS using the Cloudify Kubernetes blueprint. ClearGLASS will help setup and configure basic pods. Plus,  basic tests to verify systems are working.
=== Step 2 - Setting up development workflows ===
Duration: 1 week

ClearGLASS will demonstrate the following:

  * How to build container images - the conversion of the application to a set of containers.
  * How to deploy first service to Kubernetes. Containers above will be deployed to the existing cluster, configure networks, and expose them to the world.
  * How to develop and test locally using a cluster on a laptop with minicube. This setup replicates a production cluster, eliminating the “it used to work in my laptop” effect. Life without “tail -f” has it’s own challenges, we’ll showcase the new methods to debug.
  * How all the above come together in CI/CD workflows and illustrate best practises for moving from dev, to test, staging and production.
=== Step 3 - Going to production ===
Duration: 1 week

ClearGLASS will demonstrate the following:

  * How to scale a cluster and how to scale the application.
  * How do perform rolling updates with zero downtime of the app, as well as the kubernetes cluster itself.
  * How to setup and perform scheduled tasks (e.g. backups, pro-active scaling etc) on Kubernetes and ClearGLASS. How to use ClearGLASS to pro-actively scale cluster and how to use Kubernetes to dynamically scale the application.
  * How to monitor the cluster and respond to events using native Kubernetes tools (Heapster) and how to stream data to ElasticSearch and visualize with Grafana. How to use ClearGLASS to monitor the nodes and react to triggered events.
  * How to manage infrastructure costs. ClearGLASS provides a cost dashboard which breaks down cost per provider and per tag.
=== Step 4 - Advanced use cases ===
Duration: 1 week

ClearGLASS will demonstrate the following:

  * How to stress test the application and cluster.
  * How to recover from a lost pod or/or node.
  * How to run Master Nodes across data centers via federated clustering for disaster recovery and application geo-serving.

Engagement Deliverables

  - A fully configured and populated ClearGLASS account for your organization.
  - Deeper knowledge of Kubernetes and the ClearGLASS platform.
  - A basic workflow for development teams.
  - A production ready cluster with some services running.

Contact ClearGLASS for more details.