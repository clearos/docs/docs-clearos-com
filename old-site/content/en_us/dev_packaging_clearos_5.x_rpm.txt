===== RPM =====
The rpm software is a command line driven package manager.

===== Patches =====
==== rpm-X-pkguninstall.patch ====
This patch disables the uninstall routine when an app-X package obsoletes a cc-X package.  This was required to overcome the //Package successor// issue described on the [[http://www.rpm.org/wiki/Problems/Upgrade|RPM web site]].

<note>There is no way to discriminate between situation, when package is replaced by a successor with a diferent function, successor with the same function, but different name, or it is obsoleted without replacement.

Why?  Only Obsoletes with Provides or Obsoletes without Provides are possible. There is no further way to specify these cases.
</note>

===== Deprecated Patches =====
==== rpm-X-nrescans.patch ====
This patch fixes an crash issue in the package dependency routine used in the installer.  This was fixed upstream and is no longer required.
{{keywords>clearos, clearos content, AppName, app_name, version5, dev, packaging, xcategory, maintainer_dloper, maintainerreview_x, keywordfix}}
