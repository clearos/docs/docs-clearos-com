===== ClearOS Community 6.5.0 Beta 3 Release Information  =====
**Released: December 4, 2013**

ClearOS Community 6.5.0 Beta 3 has arrived!  Along with the usual round of bug fixes and enhancements, version 6.5.0 introduces Marketplace enhancements, a [[http://www.clearcenter.com/support/documentation/user_guide/network_detail_report|Network Detail Report]], an [[http://www.clearcenter.com/support/documentation/user_guide/administrators|Administrators]] app,[[http://www.clearcenter.com/support/documentation/user_guide/amazon_ec2_support|Official Amazon EC2 Support]], a [[http://www.clearcenter.com/support/documentation/user_guide/raid_manager|Software RAID Manager]],
[[http://www.clearcenter.com/marketplace/server/Mail_Archive.html|Mail Archive]],[[http://www.clearcenter.com/support/documentation/user_guide/upnp|UPnP Support]], an updated **Web Server** app, as well as new reporting features.

Some of the new apps in the beta are **NOT** available via the Marketplace but can be installed via **yum**.  You can find installation and app details further below.

<note>If you have not already done so, please review the [[:content:en_us:announcements_releases_test_releases|introduction to test releases]].</note>

===== Changes Since Beta 2 =====
  * Built from source code from upstream's 6.5 release
  * Added **Cloud** category to apps and menu system
  * Moved **My Account** to new top-level menu
  * Improved usability in some areas

Full changelog: [[http://tracker.clearfoundation.com/changelog_page.php?version_id=46|Changelog - ClearOS 6.5.0 Beta 3]]

===== What's New in ClearOS Community 6.5.0 =====
The following is a list of features coming in ClearOS Community 6.5.0.

  * Updated Marketplace
  *[[http://www.clearcenter.com/support/documentation/user_guide/network_detail_report|Network Detail Report]]
  *[[http://www.clearcenter.com/support/documentation/user_guide/administrators|Administrators]] 
  *[[http://www.clearcenter.com/support/documentation/user_guide/amazon_ec2_support|Amazon EC2 Support]]
  *[[http://www.clearcenter.com/support/documentation/user_guide/raid_manager|Software RAID Manager]]
  *[[http://www.clearcenter.com/support/documentation/user_guide/mail_archive|Mail Archive]]
  *[[http://www.clearcenter.com/support/documentation/user_guide/upnp|UPnP Support]]
  * Updated Web Server app
  * Updated reporting engine

Under the hood, some changes were made to support OwnCloud as well as web-based applications ([[http://www.tiki.org|Tiki Wiki]], WordPress, Joomla, etc.).  We are hoping to include a free version of the [[http://www.clearcenter.com/support/documentation/user_guide/network_map|Network Map]] app available for the final release.

For businesses and organizations, ClearOS Professional 6.5.0 also includes:

  * [[http://www.clearcenter.com/support/documentation/user_guide/qos|QoS]]
  * [[http://www.clearcenter.com/support/documentation/user_guide/network_map|Network Map]]
  * [[http://www.clearcenter.com/support/documentation/clearos_professional_6/samba_directory_-_beta_1|Samba 4 / Samba Directory (Beta)]]

The full changelog can be found here:

  * [[http://tracker.clearfoundation.com/changelog_page.php?version_id=46|Changelog - ClearOS 6.5.0 Beta 3]]

===== Known Issues =====
  * Report tick marks show funny decimals
  * Report number formatting and units is not user friendly

===== Feedback =====
Please post your feedback in the [[http://www.clearfoundation.com/component/option,com_kunena/Itemid,232/catid,39/func,showcat/|ClearOS Development and Test Release Forum]]. 


===== Download =====
==== ISO Images ====
^Download^Size^MD5Sum^
|[[http://images.clearfoundation.com/clearos-images/community/6.5.0/images/iso/clearos-community-6.5.0-beta-3-i386.iso|32-bit]]|461 MB|9898f0469764efbab900444d912bcd5b|
|[[http://images.clearfoundation.com/clearos-images/community/6.5.0/images/iso/clearos-community-6.5.0-beta-3-x86_64.iso|64-bit]]|500 MB|3583c87ff20c684f23285f417ed4190e|

==== Virtual Machine and Cloud Images ====
Virtual Machine and Cloud Images are not available for this release.

==== Upgrade from Earlier ClearOS Community 6.x Releases ====
To upgrade an existing system to ClearOS 6.5.0 Beta 3, you can run the following command:

  yum --enablerepo=clearos-updates-testing upgrade
  
===== Beta Testing Details =====
==== Marketplace ====
The number of apps available in ClearOS can be a bit overwhelming.  The ClearOS 6.5.0 release provides some new Marketplace features to help alleviate the situation.  The **Marketplace Install Wizard** now provides 3 install options for getting through the app selection process:

  * For new users, there's a **Feature Wizard** that steps through various features in ClearOS.
  * For veteran users, the **Category Wizard** displays apps by category (Server, Network, System and Reports) for quickly selecting what's needed. 
  * For MSPs and consultants, the [[http://www.clearcenter.com/support/documentation/quick_select_files|Quick Select]] option provides one-step recipes for deploying customer solutions.

In addition, there's a new **Layout Manager** that lets you choose how you want to view Marketplace apps (see screenshot below).  We are still working through some usability issues in the entire install wizard, so feedback is certainly welcome.

{{:release_info:clearos_community_6.5.0:marketplace_650.jpg|ClearOS Marketplace Updates}}

==== Network Detail Report ====
The [[http://www.clearcenter.com/support/documentation/user_guide/network_detail_report|Network Detail Report]] provides a historical view of network traffic on a per IP/user basis.  To install the app, run:

  yum --enablerepo=clearos-updates-testing install app-network-detail-report

==== Updated Reporting Engine ====
The underlying reporting engine has been updated to support additional drill-down features as well as live charts.  Known issues:

  * Some of the charts will show strange decimals on the x or y axis (e.g. 2.000002 instead of just 2)
{{keywords>clearos, clearos content, announcements, releases, clearos6.5, beta3, previoustesting, maintainer_dloper}}
