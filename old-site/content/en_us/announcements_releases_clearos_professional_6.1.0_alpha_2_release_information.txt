===== ClearOS Professional 6.1.0 alpha 2 Release Information =====
**Released: August 4, 2011**

If you do not want to read these release notes (you should!), please at least read the following two bullet items:

  * **//Please do not run this in a production environment//**
  * **//Please do not upgrade from previous versions//**

For those wanting to run a ClearOS Enterprise 6 test build in a real world environment, please wait for the beta releases.  This alpha release is just a sneak preview intended for app developers, translators and curiosity seekers.

<note>If you have not already done so, please review the [[:content:en_us:announcements_releases_test_releases|introduction to test releases]].</note>


===== What's Coming in ClearOS Enterprise 6.1.0 =====
So what can you expect in the final release of ClearOS Enterprise 6?  Along with updating the base system built on source code from Red Hat Enterprise Linux ((Trademark is the property of the respective owner.  ClearFoundation is not affiliated with Red Hat.)) 6.1, this release includes the following major changes.  Please note: these features will be rolled out during the alpha/beta phase.

==== Base System ====
  * Marketplace
  * Base system built from Red Hat Enterprise Linux ((Trademark is the property of the respective owner.  ClearFoundation is not affiliated with Red Hat.)) 6.1 source code
  * 64-bit support
  * Graphical installer
  * Intelligent RAID configuration
  * Wireless support (ClearBOX)
  * Windows BDC support
  * App developer framework

==== New Apps ====
  * Google Apps synchronization
  * Active Directory connector
  * Master/Slave support
  * Kaspersky Gateway Antivirus
  * Kaspersky Mail Antivirus
  * Kaspersky File Scan
  * RADIUS
  * Zarafa Standard and Community (coming soon)

===== Changes =====
==== Alpha 2 ====
The Alpha 2 release is basically the old Alpha 1 snapshot, but with the following changes:

  * A full installer
  * 64-bit support

{{:release_info:clearos_enterprise_6.1.0:alpha2_installer.png?600|ClearOS Enterprise Alpha 2 -Installer}}

==== Alpha 1 ====
{{ :release_info:clearos_enterprise_6.1.0:marketplace-ss.png?300|ClearOS Marketplace by ClearCenter}}

For end users, the [[http://www.clearfoundation.com/ClearFoundation-Blog/413-clearcenter-marketplace-for-clearos-enterprise.html|Marketplace]], the fast web interface engine and usability improvements are the highlights of this release.  The Marketplace is up and running and a small set of apps can be installed.

For developers, the new [[http://www.clearfoundation.com/docs/developer/framework/start|ClearOS App Framework]] is now stable and ready for use.  Whether you want to develop a free tool or a paid app, the environment for building new features for ClearOS is much improved.

For translators, the process of [[http://translate.clearcenter.com/pootle/projects/clearos_apps/|translating ClearOS Enterprise]] can get underway.  Please contact [[developer@clearfoundation.com]] if you would like to participate!

===== Available Apps =====
==== Alpha 2 Apps ====
No changes.  The graphical installer and 64-bit architecture were the focus of the Alpha 2 release.

==== Alpha 1 Apps ====
{{ :release_info:clearos_enterprise_6.1.0:enterprise610alpha1.png?400|ClearOS Enterprise 6.1.0 Alpha 1}}

The apps in Enterprise Alpha 1 may seem like an odd mix, but these were selected to provide a broad cross section of underlying base operating system components.

=== Marketplace ===
  * Marketplace

=== System ===
  * Date and Time
  * Language
  * Organization
  * Shutdown - Restart
  * Users
  * Groups

=== Gateway ===
  * Antiphishing
  * Antivirus
  * Intrusion Detection
  * Intrusion Prevention

=== Network ===
  * IP Settings (read only)
  * DHCP Server
  * DNS Server
  * RADIUS Server (authentication incomplete)
  * Incoming Firewall
  * Port Forwarding
  * PPTP VPN (authentication incomplete)

=== Server ===
  * FTP Server (authentication incomplete)

===== Feedback =====
Please post your feedback in the [[http://www.clearfoundation.com/component/option,com_kunena/Itemid,232/catid,39/func,showcat/|ClearOS Development and Test Release Forum]]. 

===== Download =====
Please select one of the following downloads.

^Download^Size^MD5Sum^
|[[http://download.clearfoundation.com/clearos/enterprise/testing/6.1alpha2/iso/i386/clearos-enterprise-6.1alpha2-i386.iso|32-bit Download]]|480 MB |d63b15f5ac094b5ad850d08415dca576|
|[[http://download.clearfoundation.com/clearos/enterprise/testing/6.1alpha2/iso/x86_64/clearos-enterprise-6.1alpha2-x86_64.iso|64-bit Download]]|521 MB |3b712ddefbdae45221a81b29f8e3fa7d|
{{keywords>clearos, clearos content, announcements, releases, clearos6.1, alpha2, previoustesting, maintainer_dloper}}
