===== Theming Document Type =====
The document type declaration is required for your theme.  This is certainly one of the easiest steps in creating a theme!

===== Hook =====
The theme engine loads the **core/doctype.php** file in your theme directory and expects to find the **theme_page_doctype** hook defined.  Your hook should return the doctype string.

===== Examples ===== 
The following returns the old 4.01 transitional doctype.

<code php>
function theme_page_doctype()
{
    return "<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN'>\n";
}
</code>

A more modern HTML 5 example:

<code php>
function theme_page_doctype()
{
    return "<!DOCTYPE html>\n";
}
</code>




{{keywords>clearos, clearos content, dev, theming, maintainer_dloper}}
