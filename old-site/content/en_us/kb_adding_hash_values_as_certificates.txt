===== Adding Hash Values as Certificates =====
When performing certain LDAP queries from command line, you may need to specify the certificate that you are trusting in order to make a secure connection. The 'ldapsearch' command may require this if the server you are connecting to uses a self-signed certificate.

===== Implementation =====
By default, LDAP search will look in the '/etc/openldap/certs/' directory for certificates necessary for making a connection. This is revealed if you do a debug flag with your 'ldapsearch', 'ldapadd', or 'ldapmodify'. For example, use the '-d3' flag.

If you need to just temporarily override the certificate negotiation, you can simply disable this ahead of the command. For example, the following will reveal the AD version number from an Active Directory Server that has SSL turned on for port 636:

  LDAPTLS_REQCERT=never ldapsearch -x -H ldaps://ad1.example.com -D "administrator@example.com" -w PASSWORD -b "CN=Schema,CN=Configuration,DC=example,DC=com" -s sub "(objectVersion=*)" objectVersion

However, if you need to access the LDAP server securely all the time, it is best practice to grab the certificates from the server in question along with the trusted CA. Place the certificates in the '/etc/openldap/certs/'.

The LDAP tools, like ldapsearch, will only use the certs in this directory if they are represented by the hashes that they have to perform this against a cert with the name of 'clearos-ca-cert.pem', do the following:

  cd /etc/openldap/certs/
  ln -s clearos-ca-cert.pem `openssl x509 -hash -noout -in clearos-ca-cert.pem`.0

This will create a file with a '.0' extension that will be usable by the OpenLDAP tools


{{keywords>clearos, clearos content, kb, openldap, ldapsearch, ldapadd, ldapmodify, clearos6, clearos7, troubleshooting, maintainer_dloper}}
