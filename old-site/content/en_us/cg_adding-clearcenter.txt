===== Adding ClearCenter =====
This guide will instruct you on how to add machines managed under the ClearCenter Service Delivery Network (ClearSDN).

==== ClearCenter Portal ====

Login to your [[https://secure.clearcenter.com/portal/|ClearCenter Portal account]].  Once logged in, hover over the 'Account' tab at the top of your screen and select 'ClearGLASS' under the 'Asset Management' subheading.  The screen below will appear.

{{content:en_us:clearglass-01.png}}

Create a new API key by typing in a name and clicking 'Add'.  Your API key will appear, which you should copy to your clipboard immediately.  This key is only shown once.  If you need a key again, you'll need to create a new one.

==== ClearGLASS ====

On the ClearGLASS main screen, click the green Cloud button located in the bottom righthand corner.  Then select 'ClearCenter' from the grid list.

{{content:en_us:clearglass-02.png}}

Paste the API Key and click 'Add'.