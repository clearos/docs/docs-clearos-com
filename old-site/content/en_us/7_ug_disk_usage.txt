===== Disk Usage =====
The **Disk Usage** report provides a drill-down tool for examining hard disk utilization.  With this tool, you can quickly pinpoint the source of wasted disk space.

===== Installation =====
If your system does not have this app available, you can install it via the [[:content:en_us:7_ug_Marketplace|Marketplace]].

===== Menu =====
You can find this feature in the menu system at the following location:

<navigation>Reports|System|Disk Usage</navigation>

===== ClearCenter Services =====
{{:omedia:clearsdn-icon-xxs.png }} The [[http://www.clearcenter.com/Services/clearsdn-remote-system-monitor-9.html|Remote System Monitor]] checks the ports, system load, disk space and memory usage on your ClearOS installation.  As such, it can offer insight into the performance or failure of given system.

===== Configuration =====
The disk usage report displays information in a pie chart (see screenshot).  You can drill down into the file system by clicking on a particular piece of the pie.  Click on the center of the pie chart to go back to your previous view.

{{:omedia:diskusage.png|ClearOS Disk Usage Report}}

===== Links =====
  * [[http://www.clearcenter.com/Services/clearsdn-remote-system-monitor-9.html|Remote System Monitor]]
{{keywords>clearos, clearos content, Disk Usage, app-disk-usage, clearos7, userguide, categoryreports, subcategoryperformanceandresources, maintainer_bchambers}}
