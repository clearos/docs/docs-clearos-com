{{ :userguides:remote_backup_1gb.svg?80}}
<WRAP clear></WRAP>

===== Remote Data Backup =====
Additional 1GB storage space for ClearCenter's Remote Server Backup Service app.

To learn more about this app, click [[https://www.clearos.com/products/purchase/clearos-marketplace-apps/cloud/remote_backup_1gb|here]]
===== Documentation =====
==== Version 6 ====
Documentation for ClearOS 6 can be found [[content:en_us:6_remote_backup_1gb|here]].
==== Version 7 ====
Documentation for ClearOS 7 can be found [[content:en_us:7_ug_remote_backup_1gb|here]].
==== Additional Notes ====
