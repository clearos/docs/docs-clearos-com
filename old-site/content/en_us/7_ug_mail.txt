===== Mail Settings =====
The Mail Settings app is used for two purposes:

  * To set the primary mail domain and hostname for ClearOS.
  * To configure outbound mail settings to make sure system-generated e-mail alerts are delivered.

===== Installation =====
If your system does not have this app available, you can install it via the [[:content:en_us:7_ug_marketplace|Marketplace]].

===== Menu =====
You can find this feature in the menu system at the following location:
 
<navigation>System|Settings|Mail Settings</navigation>

===== Settings =====
{{7_ug_mail_settings.png}}

There are two primary mail settings needed for a ClearOS system.  The **Mail Domain** is the default domain used by mail services:

  * Outbound e-mail default domain
  * SMTP services (if installed)
  * POP/IMAP servers (if installed)

It is the "example.com" bit of the email addrss me@example.com.

The **Mail Hostname** is the default hostname used by mail services.  For example, the SMTP Server (if installed) will reply to other mail servers with this hostname.

<note>If you have a public facing mail server for receiving mail, ideally the Mail Hostname should match your MX DNS record. If you then use the same Mail Hostname on your LAN, you should have a ClearOS DNS record mapping it to your ClearOS LAN IP.</note>

<note>These two settings are the same as the Mail Domain and Mail Hostname in the [[:content:en_us:7_ug_smtp|SMTP Server]] app.</note>

===== Mail Notification =====
The Mail Notification settings is a tool to configure outbound mail for system-generated mail alerts.

{{7_ug_mail_notification_settings.png}}

==== SMTP Hostname/IP ====
The hostname of the SMTP server to connect to.

==== SMTP Port ====
The port to used to send the initial connection request on.  SMTP usually uses port 25, but the app also supports using SMTPS (SMTP with SSL) on port 465.

==== Encryption ====
Encryption protocol to use when connecting to the host server. Use TLS for SMTPS on port 465. STARTTLS on port 587 is not supported.

==== SMTP Username ====
A valid username to authenticate to the server.

==== SMTP Password ====
A valid password to authenticate to the server.

==== Sender Address ====
The default sender address to use for system generated outbound mail.

==== Test Relay ====
Once you have configured the mail settings, it is time to ensure e-mails can get through successfully:

{{7_ug_mail_test.png}}

  * Enter your e-mail address into the **Test E-mail** box
  * Click on <button>Send Test E-mail</button>

If a successful connection and authentication (if required) is made, you will receive a notification that the test was successful.  If the connection could not be made or if authentication using the settings provided failed, you need to go back and check your settings and then repeat the test.

<note warning>You should also verify that receipt of the test e-mail that is sent to the address specified, especially in the cases where you are using //localhost// as the SMTP hostname.  You may find the test is successful, but you never receive the test message.  In this case, the message could be queued on the local server and unable to deliver - usually because an ISP is blocking SMTP traffic.</note>

===== Examples =====
==== Local SMTP Server ====
If you are running a local ClearOS [SMTP Server]] on the same server, you can leave the default in place (for example, port 25 at localhost).  Keep in mind, this assumes that your local mail server is either:

  * relaying directly and your ISP does not filter/block SMTP (port 25) traffic
  * relaying through your ISP's SMTP servers
  * configured to relay through an alternative (possibly non-standard port) relay service

==== Google Mail (Gmail) ====
With a valid Gmail account, one can easily set up the this app to relay through Google's SMTP server.  Here is an example for a user with a Gmail account of "example@gmail.com".

  * SMTP Hostname: smtp.gmail.com
  * SMTP Port: 465
  * Encryption: TLS
  * SMTP Username: example@gmail.com
  * SMTP Password: the_password


{{keywords>clearos, clearos content, Mail Settings, app-mail, clearos7, userguide, categorysystem, subcategorysettings, maintainer_bchambers}}
