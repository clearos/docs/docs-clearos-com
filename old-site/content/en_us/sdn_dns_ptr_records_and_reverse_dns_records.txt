===== PTR Records =====
Reverse DNS / PTR Records are maintained by your ISP and not by ClearCenter. It is a common misperception that a DNS provider provides PTR DNS records. 

In the case of Reverse DNS (called 'pointer' records, or PTR for short) the must be maintained by the ISP and not DNS providers since they are allocated to the ISP via ARIN (for much of North America and different organizations for other areas of the world.)  Blocks of IP addresses are designated to these organizations and then to the ISPs that apply to those organizations for an IP address allocation. This is a different process than the allocation of hostnames to IPs which is handled by ICANN. Please contact your Internet service provider (ISP) to update these DNS records. 

{keywords>clearos, clearos content, sdn, SDN DNS a Records and Hosts Records, categorydns, subcategoryrecords, maintainer_dloper, userguide}}
