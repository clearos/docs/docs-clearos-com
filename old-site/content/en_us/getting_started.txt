===== Getting Started with ClearOS and ClearVM =====
This page will help you set up your computer or server system with ClearOS, and/or ClearVM. Open links in new tabs as you navigate through this page. Some beginning tasks for getting started are:

  * What is ClearOS and ClearVM? Which do I need?
  * How do I install ClearOS or ClearVM?
  * I have a system from HPE or ClearBOX system. Where do I go from here?
  * What is all this Clear-thingy stuff about? How will ClearOS or ClearVM help me?

===== Installing ClearOS and ClearVM =====
ClearOS and ClearVM comes pre-installed on some systems and on other systems you will need to install it manually. Manual install is almost ALWAYS an option on all hardware platforms but you can save some time if it came preloaded or came on a system with installation tools. 

The following can help you get installed and running using pre-loaded tools. This is useful if you already have some ClearOS elements on your system:

  * Setup ClearOS on **HPE system pre-installed** with ClearOS
  * Install **ClearOS** on HPE Server that included ClearUTILS USB key for rapid deployment
  * Launch and run **ClearVM** on HPE Server that included ClearUTILS USB key for rapid deployment
  * Use other options that came on my **HPE ClearUTILS USB key**.
  * Setup ClearOS on ClearBOX Servers
  * **Get help** from a ClearCenter Sales Engineer on selecting a version of ClearOS to use.
  
If you have a blank system and wish to install ClearOS or ClearVM, or want to try ClearOS in a virtual environment, there are several options available to you. 

  * Installing ClearVM.
  * Installing ClearOS.
  * Installing or running ClearOS in a virtual environment.

