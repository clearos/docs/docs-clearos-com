===== HTTP =====
The Apache HTTP Server is a powerful, efficient, and extensible web server.

===== Changes =====
  * Updated logos and other marks to be trademark compliant
{{keywords>clearos, clearos content, AppName, app_name, version5, dev, packaging, xcategory, maintainer_dloper, maintainerreview_x, keywordfix}}
