{{ :userguides:events.svg?80}}
<WRAP clear></WRAP>

===== Events and Notifications =====
The Events and Notifications app logs all types system events on the server.  Alerts could be informational, as in User xyz logged in at hh:mm or of a more critical nature - Disk space exceeded 95%.

Alerts are generated in real-time using a daemon that tracks log files or are generated with the ClearOS API.

The app can be configured send alerts as they occur or in a daily report.

To learn more about this app, click [[https://www.clearos.com/products/purchase/clearos-marketplace-apps/reports/events|here]]
===== Documentation =====
==== Version 6 ====
Documentation for ClearOS 6 can be found [[content:en_us:6_events|here]].
==== Version 7 ====
Documentation for ClearOS 7 can be found [[content:en_us:7_ug_events|here]].
==== Additional Notes ====
