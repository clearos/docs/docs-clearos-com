===== RackSpace =====
To add a Rackspace cloud, you'll need the username and API key. To obtain it, login to  your account on Rackspace. Click on your name on the top right corner and select 'Account Setttings' from the drop down menu. In your login details you can see or reset your API key.

[image]

Copy that key and login to  https://Clear.glass Click on the 'Add cloud' button.
Choose Rackspace, select your Rackspace region and paste your Rackspace username and API key. 

[image]

Once you click Add, ClearGLASS will try to authenticate with RackSpace. If all goes well, you should see a list of your VMs in the Machines section.

==== Related Articles ====
  * [[content:en_us:cg_setting-up-clearglass|Quick Start Guide]]
  * [[content:en_us:cg_monitoring-on-linux|Setting up monitoring on Linux]]
  * [[content:en_us:cg_manage-infrastructure|Managing Infrastructure]]