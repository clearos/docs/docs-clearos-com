===== Running Manual Updates on ClearOS =====
This guide will cover running updates from command line using the 'yum update' command from command line.

===== Reasons Updating ClearOS =====
All versions of registered ClearOS subscriptions provide critical updates via the Service Delivery network. Paid versions of ClearOS provide qualified updates for critical bug fixes. Critical updates happen automatically and include fixes for security issues that could cause your ClearOS server to be compromised by software defects. Automatic updates, however, do not include feature of bug fix updates because such fixes can require reboots, changes to configurations, or other such disruptions. As such, they are not included in the automatic updates and need to be manually installed from time to time. This is especially true when updating between minor versions (ie. Version 6.2 to version 6.3). 

Another good practice is to perform manual updates on a ClearOS server immediately following the install. This can improve your experience by having your system up to date proceeding the installation wizard.

===== Command Prompt =====
In order to perform manual updates you will need to open a command prompt. This will allow you to issue the appropriate commands.

=== ClearOS Console ===
If you have a monitor and keyboard attached to your ClearOS server, you can get to a console prompt by issuing the following keystroke combination:

  * Ctrl + Alt + F2

If you are done or want to return to the graphical menu (GUI) or the text menu (TUI) from this black console screen, use the following (only one or the other will be running):

  * Ctrl + Alt + F7
    * Text User Interface
  * Ctrl + Alt + F7
    * Graphical User Interface

Once you are at the console prompt, you will be asked for a username and password. Type **'root'** for the username and supply the password for root at the next prompt (the password input will NOT echo your password as you type it or put any characters on the screen even though it is taking input). Proceed to the section entitled **Update Commands**.

=== Windows ===
If you use a Windows workstation to manage your ClearOS server, you can use a terminal program to access the command prompt of your server. A common and free program for this is called PuTTY and it can be [[http://www.chiark.greenend.org.uk/~sgtatham/putty/download.html|downloaded here]].

You can download the full application installer (putty-0.62-installer.exe) or just the executable (putty.exe). If you download the 'exe' then just launch putty from the location you downloaded it to. If you installed, you will have an icon for the installer. When you launch PuTTY, it will ask you for the IP address and port. It will say port 22 for the port. That is the right port. You will then put in the IP address or the hostname for your server. If you are connecting from the outside (WAN) then you will need to open port 22 on the incoming firewall.

Once you connect it will prompt your to accept the key for the server and ask you for a username and password. Type root for the username and supply the password (it will not echo the characters your type for the password but it will still work).

Proceed to the section entitled **Update Commands**.

=== Mac ===
On Mac (OSX) you already have a terminal program that is capable of connecting to ClearOS. Under Applications, find the Utilities folder and open the program entitled 'Terminal'. This will open the shell for your Mac. From here type the following (replacing 192.168.1.1 for the IP address or hostname of your ClearOS server):

  ssh root@192.168.1.1

If you are connecting from the outside (WAN) then you will need to open port 22 on the incoming firewall.

Once you connect it will prompt your to accept the key for the server. Type 'yes' and hit enter. It will then ask you for root's password. Supply the password (it will not echo the characters your type for the password but it will still work).

Proceed to the section entitled **Update Commands**.

===== Update Commands =====
Once you are logged in, you can update the server as long as the server has access to the internet. It is best if the server is NOT behind a proxy. If your server can only access port 80 via a proxy, please contact ClearOS support.

You can validate that you can get to the internet and your DNS is working. The following should reply:

  ping mirror2-dallas.clearsdn.com

(Type 'Ctrl + c' to stop ping.)

Once you validate that you can get to the internet, you can update your server by issuing the following command:

  yum update

The update process will identify update servers and get a list of updates that your server can use. It will ask you if you want to proceed with the updates. Type 'y' to continue with the updates. 

Depending on your internet speed, the updates will proceed to download, install and then clean up. We recommend a reboot at the conclusion of your updates especially if the update included a 'kernel' package.

===== Help =====
==== Navigation ====

[[:|ClearOS Documentation]] ... [[..:|Knowledgebase]] ... [[:Knowledgebase:Troubleshooting:|Troubleshooting]]
{{keywords>clearos, clearos content, AppName, app_name, versionx, xcategory, maintainer_x, maintainerreview_x, titlefix, keywordfix}}
