===== Mail Archive =====
The **Mail Archive** system logs all incoming and outgoing e-mail passing through the gateway to a central database. This module can be used to meet regulatory compliance or assist an organization to enforce internal policies for e-mail use in the workplace.

===== Installation =====
If you did not select this module to be included during the installation process, you must first [[:content:en_us:5_software_modules|install the module]].

===== Configuration =====
On first configuring the mail archiver after installation, a warning will be displayed prompting the user to initialize the database. This is perfectly normal and should be done before continuing.

<Screenshot needed>

A table containing three form tabs is displayed as indicated in the screenshot above.

    * Mail Archive Settings - General configuration settings
    * Current Statistics - Data and actions relating to the current database
    * Search Statistics - Data and actions relating to the search database 

An explanation of the difference between the Current and Search databases will be explained below. 

==== E-mail Archive Settings ====

Activation and configuration of the email archive system can be done via the "Mail Archive Settings" tab. The section below explains each setting in details.

==== Archive [Enable/Disable] ====

Enables or disables the archiving of email passing through the SMTP server.

==== Policy ====

Allows an administrator to archive all email passing through the server or restrict (exempt) certain users, as required. Set this to "All messages" to archive email for every user. Select "Filter messages" to configure a filter to archive only some users email.

==== Configure (Policy) ====

A configure link will be displayed when "Filter messages" is selected as the policy. Click on this link to 'fine tune' which users' email should be archived.

==== Discard Attachments ====

The "Discard Attachments" drop down option is only available when the "Policy" is set to "All messages" - otherwise, discarding of attachments is done in the 'Configure' page.

To save on storage space (and assuming attachments are not required to be archived either by corporate policy or law), select "Always". Otherwise, select a level in which attachments should be discarded (i.e. "Never", > 1MB etc.). 

<note important>Files which are identical but attached to different e-mails as attachments only consume the size of the file, not N x the size of the file, where N is the number of emails going through the archive system with the same attachment.</note>

==== Auto Archive ====

Auto archive controls the movement of archive data from the "Current" database to an archived file. This allows the email archive to be easily moved from the server to a storage medium (for example, another server, a USB Mass Storage Device, a tape drive etc.) for safe storage. All emails that have been archived to this file can be retrieved and searched at a later date, if required.

Use this field to provide consistent archive files for a give period (i.e. weekly or monthly) or of a certain size (i.e. a DVD etc.).

==== Encrypt Archives ====

The transition of data from the database to a dump file can be encrypted to prevent unauthorized access. This can be extremely important (and may be required by law) if e-mails contain confidential information.

==== AES Encryption Password ====

The password used to encrypt the archive file if "Encrypt Archives" is set to "Yes". By default, this password must be at least 12 characters and contain both upper and lower case letters and at least 1 number. 

<note important>Twelve characters was chosen as a length to ensure the security of the encrypted file. If a smaller password is desired, you can override this setting in the /etc/archive.conf file by setting the 'encrypt-password-length' parameter.</note>

===== Searching the Database Archives =====

==== Current vs. Search Database ====

The mail archive operates using two databases. The 'Current' database is used to retrieve and store new messages arriving from the SMTP (mail) server. The 'Search' database is a transient database - its contents can be deleted and replaced with data corresponding to the search requirements and space of the drive.

The dual-database system is designed for maximum scalability. A single database could quickly become of such enormous size that an administrator would be continually adding drive storage space to accommodate the email archives. By giving the user the ability to take certain sized (or certain periods of time) snapshots from the current database and allowing one or more to be loaded to the 'Search' database, searching for past emails can be done quickly and efficiently without the overhead of hundreds of GB of disk space. 

<note important>Think of the search database as a 'sandbox', where archives can be dumped, searched and then removed (reset).</note>

==== The Current Database ====

The current database contains all archived emails since the last file archive was performed. A file archive can either be performed manually or can occur automatically if the Auto Archive setting is enabled and triggered.
Performing a Search

To view how many emails and the approximate size of the archive in the 'Current' database, click on the Current Statistics tab. 

Screenshot Needed.

Click on the Search button. A new form will be displayed allowing you to enter your search criteria. 

Using the add links you can customize your search using a maximum of five (5) criteria using either AND or OR logic (Match all vs. Match any). The results from your search will be displayed in the results table below. 

==== The Search Database ====

The Search Database will normally be empty until at least one file-based mail archive restore is performed (or if data from a prior search still in the database). Remember, the Search Database is designed to be reset often so that you can work with datasets that will scale with the ever-increasing demands of archived e-mails.

To restore a file-based archive, click on the Restore Archive button. 

Screenshot needed.

All prior restores will be listed in the Archives table. Rows with a green status mean the link is intact (archive exists on the server). Rows with a red status icon indicate the link is broken. If you need to restore from a file whose status is red (broken link), you will need to use Flexshares and the storage device where the archive was moved to in order to re-establish the link.

Simply click on the Restore button to start a restore to the Search database. Once complete, you can Search the database as normal. 

Screenshot needed.

==== Performing a Search ====

To navigate to the Search Database, go to the Mail Archive page and click on the Search Statistics tab. If there is data that you wish to search in the database (given the statistics you may find that there is data, but you do not remember which file archive it originates from - in this case, it is advised to reset the database and start again), click the Search button. A search form will be displayed - the same as occurs when you are searching the Current Database.

<note important>You can toggle between searching the Current and Search databases by selecting the appropriate radio button in the search form.</note>

Enter your search criteria and click Search. Any hits (results) will be displayed in the table below.

==== Resetting the Search Database ====

Since the Search Database is simply a MySQL database created by the import of one more archive files, it is perfectly safe to Reset the search database to reinitialize the database. You may want to reset the search database to make make searching the database faster or because searching an entire index (i.e. mail archive over several years) becomes too large a dataset for your existing hard disk.

==== Viewing/Restoring E-mails ====

Once an e-mail has been found using a search procedure, click on the View link next to the e-mail of interest. A new page will be displayed containing the email body contents.

==== Original Header ====

It is sometime of interest to view the original e-mail header. This information is stored in the archive database and can be viewed by clicking on the Original Header link (a '+' icon).

The screen capture below displays an e-mail view with the headers expanded. 

==== Sending ====

To resend the email (either to the original recipient or a separate user), click on the Resend E-mail link. A new form will appear allowing you to resend the email. 

<note warning>Resending the e-mail uses the SMTP relay module...make sure it has been configured correctly to send outgoing mail through your local mailserver or your ISP.</note>

==== Admin (root) account vs. Users Account ====

The mail archives (both current and search databases) can be searched by the system administrator (logged in under the 'root' account) or by users. To give users access to the archive, use the System Administration ACL to grant access to specific users to the Mail Archive module.

When logged in as 'root', all emails will be returned from a search query. However, when logged in as a 'user' system administrator, only email that has been sent to or by the user will be returned from a search query. In other words, users can view/restore mail that was sent or received by them, but no one else.

===== Advanced Users =====

==== Accessing the Database ====

This module makes use of the system MySQL service for the database back-end. The system MySQL server is a 'sandboxed' service running on a non-standard port. To access the database from the command line, you will need to fetch the database password:

cat /etc/system/database

password = AAAAAAAAAAAAAAA
reports.password = BBBBBBBBBBBBBB
zoneminder.password = CCCCCCCCCCCCCCC
archive.password = PASSWORD
dspam.password = DDDDDDDDDDDDD

The email archive database password is keyed on 'archive.password'.

Next, you'll need to access the MySQL console in a slightly different manner than the default MySQL server.

/usr/share/system-mysql/usr/bin/mysql DBNAME -uUSER -pPASSWORD

Where:

DBNAME = archive_current or archive_search
USER = archive
PASSWORD = the password retrieved from the /etc/system/database file
Troubleshooting
What if I forget my password?

In a word: don't. If you forget your archive password, there is absolutely no way to recover any e-mail from the encrypted mail archive file. 

===== Links =====

  * [[:content:en_us:5_flexshares|Using Flexshares]]
{{keywords>clearos, clearos content, Mail Archive, app-mail_archive, clearos5, userguide, categoryserver, subcategorymail, maintainer_dloper}}
