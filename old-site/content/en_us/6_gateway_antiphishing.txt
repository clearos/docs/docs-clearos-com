===== Gateway Antiphishing =====
The **Gateway Antiphishing** app protects users from phishing -- scam messages that attempt to extract sensitive information such as credit card numbers, passwords and personal information.  For those of you who have not seen a few samples of "important messages from your bank", you can read more about phishing [[http://en.wikipedia.org/wiki/Phishing|here]].

===== Installation =====
If your system does not have this app available, you can install it via the [[:content:en_us:6_Marketplace|Marketplace]].

===== Menu =====
You can find this feature in the menu system at the following location:
 
<navigation>Gateway|Antimalware|Gateway Antiphishing</navigation>

===== ClearCenter Antimalware Updates =====
The open source [[http://www.clamav.net|ClamAV]] solution is the antiphishing engine used in ClearOS.  This software automatically checks for updates several times a day for new antiphishing signatures.  This is already included in ClearOS for free!

{{:omedia:clearsdn-icon-xxs.png }} In addition, the ClearCenter [[http://www.clearcenter.com/Services/clearsdn-antimalware-updates-7.html|Antimalware Updates]] service provides //additional// daily signature updates to improve the effectiveness of the antiphishing system.  These signatures are compiled from third party organizations as well as internal engineering resources from ClearCenter.  We keep tabs on the latest available updates and fine tune the system so you can focus on more important things.

===== Configuration =====
Antiphishing configuration is mostly a matter of fine tuning some policies based on your network needs.  In the majority of cases, we recommend enabling all of the antiphishing features. 

==== Signature Engine ====
Antiphishing signatures (similar to antivirus signatures) are included and maintained by the ClearOS system.  The only time you should consider disabling this feature is when you find yourself with a hard to find false positive.

==== Heuristic Engine ====
The antiphishing engine uses a basic algorithm to detect phishing attempts.  Though this can occasionally trigger a false positive, we recommend leaving this option enabled.

==== Block SSL Mismatch ====
A common phishing attack will use a non-secure web address to actually perform the attack, but display the web address to the user as a secure/SSL address.  With this feature enabled, the antiphishing software will detect and reject this tactic. 

==== Block Cloaked URLs ====
Some scammers use special encoding in web addresses (URLs) in an attempt to trick end users.  Any URLs that have certain encoding characteristics will be blocked with this feature enabled.

===== Links =====
  * [[http://www.clamav.net|ClamAV]]

{{keywords>clearos, clearos content, Gateway Antiphishing, app-gateway-antiphishing, clearos6, userguide, categorygateway, subcategoryantimalware, maintainer_dloper}}
