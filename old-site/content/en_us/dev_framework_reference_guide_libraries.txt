===== Libraries =====
Libraries (Models) are the meat and potatoes of a ClearOS App.  

===== Getting Started =====
==== Header Documentation ====
The [[:content:en_us:dev_framework_coding_standards_documentation|documentation]] must be the first block of code to appear in your ClearOS App library.  Among other pieces of information, the **author** and **copyright** are documented.

==== License ====
Please make sure you have a [[:content:en_us:dev_framework_coding_standards_license|license]] notice just after the header documentation.  It is important that you heed the requirements of a license.

==== Namespace ====
ClearOS libraries use the namespace feature that was released with PHP 5.3.x.  By using namespaces, you avoid any class name conflicts with other ClearOS App developers.  The name space format for Apps is **clearos\apps\//app_name//** (see example below).

<code php>
///////////////////////////////////////////////////////////////////////////////
// N A M E S P A C E
///////////////////////////////////////////////////////////////////////////////

namespace clearos\apps\openvpn;
</code>

==== Bootstrap Code ====
The ClearOS engine needs to pull in core functions (e.g. clearos_load_library) and the framework configuration.  The location of the bootstrap file can be changed by using an environment variable -- very handy for developers working outside the default /usr/clearos directory.

<code php>
///////////////////////////////////////////////////////////////////////////////
// B O O T S T R A P
///////////////////////////////////////////////////////////////////////////////
    
$bootstrap = isset($_ENV['CLEAROS_BOOTSTRAP']) ? $_ENV['CLEAROS_BOOTSTRAP'] : '/usr/clearos/framework/shared';
require_once $bootstrap . '/bootstrap.php';
</code>

==== Translations ====
Translations for your application are stored in the **languages** directory in your application.  In order to keep the ClearOS framework lightweight, you need to explicitly pull in any translations that you need.  The **clearos_load_language()** function is used for this purpose.  This function is analogous to the $this->lang->load() call in the "View" and "Controller".

You can use translations from other base applications.  In fact, most libraries can take advantage of the **base** translations - it contains a large list of commonly used words in ClearOS, for example:

  * day, week, month, year
  * username, password
  * low, medium, high
  * etc.

<code php>
///////////////////////////////////////////////////////////////////////////////
// T R A N S L A T I O N S
///////////////////////////////////////////////////////////////////////////////

clearos_load_language('base');
clearos_load_language('date');
</code>

==== Dependencies ====
Your ClearOS library can use other libraries installed on the system.  The ClearOS **base** application (app-base-core) contains a number of useful libraries, including:

  * File: for manipulating files on the system
  * Folder: for manipulating folders on the system
  * Daemon: a generic base class for daemons 

... and others.  Use the **clearos_load_library()** function to load a library.  This function is analogous to [[http://codeigniter.com/user_guide/general/libraries.html|CodeIgniter's $this->load->library()]] call in the "View" and "Controller".

<code php>
///////////////////////////////////////////////////////////////////////////////
// D E P E N D E N C I E S
///////////////////////////////////////////////////////////////////////////////

clearos_load_library('base/Folder');
clearos_load_library('base/File');
</code>

==== Exceptions ====
If you need to create custom exceptions for your library, you can create this class exception in the same source code file.  As a rule of thumb, avoid creating a new exception unless you think a developer using your library will really need it. 

All custom exceptions must extend the **EngineException**.  The only two parameters available in the EngineException are:

  * **message** a short error message
  * **code** one of the following status codes
    * ClearOsError::CODE_ERROR
    * ClearOsError::CODE_WARNING
    * ClearOsError::CODE_INFO

<code php>
///////////////////////////////////////////////////////////////////////////////
// E X C E P T I O N  C L A S S
///////////////////////////////////////////////////////////////////////////////

/**
 * Timezone not set exception.
 *
 * @package ClearOS
 * @subpackage Exception
 * @author {@link http://www.clearfoundation.com/ ClearFoundation}
 * @license http://www.gnu.org/copyleft/lgpl.html GNU Lesser General Public License version 3 or later
 * @copyright Copyright 2003-2010 ClearFoundation
 */

class TimezoneNotSetException extends EngineException
{
    /**
     * TimezoneNotSetException constructor.
     *
     * @param string $message error message
     */

    public function __construct($message)
    {
        parent::__construct($message, ClearOsError::CODE_ERROR);
    }
}
</code>

===== Class =====
==== Class Declaration ====
All classes must extend the Engine class or other ClearOS classes.  You will also need to have the required documentation block that describes the class.

<code php>
///////////////////////////////////////////////////////////////////////////////
// C L A S S
///////////////////////////////////////////////////////////////////////////////

/** 
 * System time manager.
 *  
 * @package ClearOS
 * @subpackage API
 * @author {@link http://www.clearfoundation.com/ ClearFoundation}
 * @license http://www.gnu.org/copyleft/lgpl.html GNU Lesser General Public License version 3 or later
 * @copyright Copyright 2003-2010 ClearFoundation
 */
    
class Time extends Engine
{
...
</code>

==== Constants ====
Like all good programmers, you should avoid hard coding constant values in your library.  Class constants are commonly used for:

  * Paths to files and directories
  * Default configuration values
  * Configuration options 

<code php>
///////////////////////////////////////////////////////////////////////////////
// C O N S T A N T S
///////////////////////////////////////////////////////////////////////////////

const FILE_CONFIG = '/etc/sysconfig/clock';
const COMMAND_HWCLOCK = '/sbin/hwclock';
const DEFAULT_TIME_ZONE = 'US/Eastern';
</code>

==== Variables ====
Among other things, class variables are useful for improving the speed and efficiency of a series of method calls.  However, one of the design principles of ClearOS is keep libraries stateless.  This principle makes it possible to support remote calls to the library (via REST or SOAP for example).  For this reason, please use class variables with caution!  You can find more about stateless libraries [[:content:en_us:dev_framework_reference_guide_stateless_libraries|here]].

<code php>
///////////////////////////////////////////////////////////////////////////////
// V A R I A B L E S
///////////////////////////////////////////////////////////////////////////////

/**
 * @var configuration array
 */

protected $config = array();

/**
 * @var configuration loaded flag
 */

protected $is_loaded = FALSE;
</code>

==== Constructor ====
==== Methods ====
For the most part, a ClearOS library should always have fine grained method calls.  In other words, the preferred approach is to use distinct Get/Set methods for each distinct entity in your library.  Consider a generic user library.  A fine grained solution looks like:

  * GetLastName()
  * GetFirstName()
  * GetEmailAddress()
  * GetXyz()

While a coarse grained solution would return all the user information in one method call:

  * GetUserInfo() 

This coarse grained approach is sometimes necessary when it comes to building out efficient remote procedure calls.  If you think your library might need this type of coarse grained access, then feel free to add such methods to complement your fine grained implementation.

<code php>
///////////////////////////////////////////////////////////////////////////////
// M E T H O D S
///////////////////////////////////////////////////////////////////////////////

/**
 * Returns DNS server pushed out to clients.
 *
 * @return string DNS server IP address
 * @throws EngineException
 */

public function GetDnsServer()
{
    ClearOsLogger::Profile(__METHOD__, __LINE__);

    if (! $this->is_loaded)
        $this->_LoadConfig();

    return $this->config['push']['dhcp-option']['DNS'];
}
</code>

==== Validation ====
==== Profiling ====
All method calls should start with the following line:

<code php>   
ClearOsLogger::Profile(__METHOD__, __LINE__);
</code>

You can also add profiling inside your method.  This can be handy when you are dealing with a block of code that can take a significant amount of time.  Large parts of ClearOS depend on external calls to the operating system and these can sometimes take a significant amount of time.  When adding extra profiling, use the optional third parameter for custom messaging:

<code php>
ClearOsLogger::Profile(__METHOD__, __LINE__, 'starting Windows network probe');
// ... scan that may take a long time....
ClearOsLogger::Profile(__METHOD__, __LINE__, 'finished Windows network probe');
</code>

===== Links =====
  * [[:content:en_us:dev_framework_reference_guide_views|Views]]
  * [[:content:en_us:dev_framework_reference_guide_controllers|Controllers]]
{{keywords>clearos, clearos content, dev, framework, mvc, libraries, maintainer_dloper}}
