===== How to set up ClearOS as a FTP site with TLS Security =====
/**
 * This does not replace the userguide
 */

By default, ClearOS with the FTP Server app from the marketplace is already running in secure mode for FTP over TLS. This guide will help you implement it in that mode and it is considered best practices to run FTP with security as both the content and the username/password are transmitted over the internet in plain text. 

<note>This is called FTP/S in the computer industry and is not to be confused with SFTP.</note>

===== Preparation =====
==== Marketplace ====
Make sure that the FTP Server is installed and running. Also, make sure to install the Flexshare app.

==== Incoming Firewall ====
Add Firewall Rule. Choose the 'Standard Service' type labeled **'FTPS'**. this will open two ports (989 and 990).

{{:content:en_us:kb_ftp_secure_fw.png?550|Firewall Configuration}}

In addition to this, you will likely need to add passive FTP ports so that you can connect to the FTP server in a more dynamic way which is more amenable to modern firewalls. This is a service on the Incoming Firewall rules list.

{{:content:en_us:kb_ftp_secure_fw1.png?550|Firewall Configuration Passive}}

Be sure to remove the default port 20 and 21 ports for non-secure if you are trying to enforce secure only FTP.

===== Setting up the Flexshare =====

Go through the normal procedures of setting up a group that will have rights to an FTP Share and a user that is a member of that group. In this demonstration, the user is called 'guestftp' and the group is called 'fs-ftpshare'. Next, create a flexshare share for this group. In the example, I call it ftpshare. Make the group have access to this share.

{{:content:en_us:kb_ftp_secure_fs.png?550|Flexshare configuration}}

===== Your FTP Client =====
The heavy lifting is configuring your FTP client to work with TLS on port 990. While there are many FTP clients, we will show and example of how to configure Filezilla for use with ClearOS FTP/S. You can apply the logic here to your own client software or simply download Filezilla using the link at the bottom of this howto.

The fastest way to get connected is to normally use the Quickconnect feature. This will not work however because ClearOS uses a robust structure in order to be able to support private flexshares and home directories. You will need to make a manual connection instead of using Quickconnect. Quickconnect will connect but will fail to list the directories. This is by design. Click the site manager in the upper left hand corner:

{{:content:en_us:kb_ftp_secure_fz5.png?550|Site Manager}}

Supply the following information:

  * - Host
    * - The host IP address or hostname of the server (you can even use ClearOS Dynamic DNS to supply a hostname that will always work).
  * - Port
    * - 990
  * - Protocol
    * - Require implicit FTP over TLS
  * - Login Type
    * - Normal
  * - Username
    * - The username that is a member of the group associated to the flexshare
  * - Password
    * - The user's password

{{:content:en_us:kb_ftp_secure_fz3.png?550|Site Manager Config - General}}

In the Advanced section, supply the following:

  * - Default remote directory
    * - /**{insert the name of your Flexshare}**

{{:content:en_us:kb_ftp_secure_fz4a.png?550|Site Manager Config - Advanced}}

You will be prompted to accept a certificate. You can tick the checkbox to remember this certificate so that it doesn't prompt you again.

{{:content:en_us:kb_ftp_secure_fz2.png?550|Certificate}}

At this point you should be connected and be able to see the contents of your Flexshare.

===== Troubleshooting FTP Client =====
Because of the way that FTP works, you should be able to separate whether your problem is happening on the command channel or the data channel.

  * - Connects and shows the data: 
    * - Congratulations, it is working
  * - Connects fails to list the directory contents:
    * - The certinficate, you username and password are working but the data channel is not working
  * - Fails to connect or list the directory: 
    * - Problems with the connection settings

==== Connection issues ====
Things to check if the connection is failing:

  * - Review the firewall settings section above to make sure you are allowing FTPS connections on both 990 and 989 ports (both TCP). 
  * - Ensure that the FTP service is running. 
  * - Make sure your user is part of the group that is authorized to use the Flexshare and that the user is authorized to use FTP services
  * - Make sure that the Flexshare is enabled
  * - Make sure that ClearOS is on the public IP address or that the ports are properly forwarded for 990 and 989. [[https://www.clearos.com/resources/documentation/clearos/content:en_us:kb_troubleshooting_connectivity#telnet|You can test 990 using telnet to see if the SYN packet is happening]].

==== Data Channel Issues ====
  * - Test FTP on the inside of the firewall to determine if the problem is firewall or router related
  * - Make sure that the path to the directory is properly specified in your FTP settings for the default directory path.
  * - Try opening up passive FTP ports

===== Links =====
  * - [[:content:en_us:7_ug_ftp|ClearOS 7.x FTP Server Userguide]]
  * - [[https://filezilla-project.org/|Filezilla]]



{{keywords>clearos, clearos content, FTP, Filezilla, howto, app-ftp, clearos6, clearos7, categoryserver, subcategoryfile, maintainer_dloper}}

