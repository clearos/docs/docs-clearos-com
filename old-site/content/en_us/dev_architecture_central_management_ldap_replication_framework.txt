===== Central Management LDAP Replication Framework =====
One of the core building blocks in the [[:content:en_us:dev_architecture_central_management_start|Central Management]] architecture is LDAP replication.  The OpenLDAP software in ClearOS already provides the necessary bits for LDAP replication, so we are already half way there.

<note info>You can read more about the OpenLDAP replication settings [[:content:en_us:dev_architecture_central_management_openldap_replication_settings|here]].</note>

In order to provide a robust [[:content:en_us:dev_architecture_central_management_start|Central Management]] solution, we need to go one step further and design a framework to handle: 

  * Joining new server to a ClearOS LDAP directory as a replicate.
  * Disjoining from ClearOS LDAP directory.
  * Promoting replicate to master
  * Demoting master to to replicate
  * Synchronizing changes and detecting inconsistencies
  * Providing helpers to the application layer

This last bullet needs a little explanation.  Some applications may need to replicate non-LDAP data across all ClearOS systems.  For example, the Samba netlogon scripts need to be replicated across some ClearOS systems (specifically, those configured as a backup domain controller / BDC).  The Central Management system needs to be extended to handle this file replication as well.
{{keywords>clearos, clearos content, dev, architecture, maintainer_dloper}}
