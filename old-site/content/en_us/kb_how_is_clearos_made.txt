===== How is ClearOS Made =====
The short answer is to the question of how ClearOS comes to be is simple and complex. The simple answer is 'love and care'. This is accurate and true. Many people contribute to ClearOS including hard-working paid individuals, volunteers, community members, and even people that contribute to other open source products directly affect ClearOS.

===== Historical =====
ClearOS is a Linux distribution which means that it is an operating system based on Linux. ClearOS started under the name 'Clarkconnect' back in late 1999 - early 2000 timeframe. It was originally a project started by software engineers and programmers who came from Carnegie-Mellon university. From its inception, ClearOS has always held a concept of Internet delivered service to the operating system. This key innovation which is vital to business today has been part of ClearOS from the beginning.

{{:content:en_us:clearos_historical_snippet.png|Image Copyright A. Lundqvist, D. Rodic v. 12.10. License GNU FDL}}

===== Upstream =====
ClearOS benefits from the larger open source community and contributes as well. The key components that drive the underlying architecture are common among Fedora, Redhat, Centos, Scientific Linux, and others. 

While ClearOS benefits from this heritage, our process has varied over the years. Presently, ClearOS takes many of its packages directly from the CentOS distribution and the EPEL libraries. The current kernel of ClearOS (7.x) is based on a rebuild of the CentOS kernel and is modified. This means that most, but not all software and drivers will work equally between ClearOS and CentOS.

===== Key differences =====
ClearOS uses different mechanisms to enjoin services and features from disparate open source projects to give a solutions-based result. That result is targeted to the application of IT in the Small Business Office environment and the Distributed Enterprise. While the components of ClearOS are certainly customizable to the tasks of larger enterprise and even desktop OS, ClearOS is not designed for these specific tasks. That doesn't mean that you cannot do them it just means that you will be using the command line often if you try it (which is what you'd do in those other many of the other operating systems anyways)

Here are some services and how ClearOS improves the ease of operations for them:

  * Directory integration
    * We assume users of ClearOS will tie their installation to a directory
    * Local accounts are discouraged
    * Using ClearOS as your master directory is one way to cut costs
    * Using ClearOS with Active Directory is one way to facilitate integration
    * De facto group-based authentication for best practices and compliance
  * Firewall integration points
    * Integration between firewall and apps allows for desired outcomes
    * ClearOS API helps apps to identify access and provide exceptions
  * Gateway integration points
    * Content filter engine and proxy integration with directory and firewall
    * Antivirus engine integration with open and closed source virus definitions
  * Server integration points
    * Protocol agnostic storage mechanisms through flexshares
    * Third-party plugins for storage integration between proprietary apps and open source storage

===== Processes =====
==== Distribution Build process ====
ClearOS intakes several sources for code and houses those sources in its build system. These intake sources exist as prebuilt RPM packages, SRPMS, and git source. From these sources, ClearOS is built around the core of upstream sources with the template of what is different. After all of that, ClearOS sources, community contributions, and 3rd party sources and build what we know as ClearOS today. 

ClearOS is repository based so different component will show up in different places. Testing and QA users of ClearOS will manually subscribe to testing repositories so that they can preview code. Community members are subscribed to a different code so that they can review the code used for ClearOS released as part of the main distribution. Paid customers subscribe to even other repos so that they can get quality-tested versions of the software and avoid certain bugs and issues that are discovered in testing and community repos. From these high quality sources the installation media for ClearOS is created.

===== Getting Involved =====
==== Buy it ====
We feel that ClearOS is worth the value that it presents to IT. Buy a copy today! Also, consider helping the project by donating or if you have a specific need for an app, sponsor it on ClearOS. ClearCenter also offers partner opportunities for building your IT business around ClearOS

[[https://www.clearos.com/products/purchase/buy-it|Buy ClearOS or Clearcenter products]]

==== Use it ====
One of the best ways to get involved with ClearOS is to start using the software and benefitting from it today. Whether at your business or home, ClearOS can provide an impactful solution to your IT infrastructure. You can download your copy of ClearOS from any one of our mirrors:

[[http://mirror.clearos.com/clearos/|ClearOS Mirrors|ClearOS Mirrors]]

Whether you use the free or paid version, your experience and feedback helps ClearOS.

==== Break it ====
ClearOS is meant to be scrutinized. Get under the hood and hack away. It is through this process that you can become an expert at ClearOS. Along the way you will want to work with the ClearOS community. Solve someone else's problem or tell them yours. The forums are a productive area where improvement to ClearOS and passion about ClearOS both express itself.

[[https://www.clearos.com/clearfoundation/social/community/|ClearOS Community Forums]]

==== Fix it ====
ClearOS is a platform that begs for your ideas to become a turnkey reality. Get started today with coding on ClearOS or bringing your application to market. We can work with your software to integrate it on ClearOS and can aid you in the use of existing APIs, methods, and integration points that will take your application from its 'standalone' focus to broader IT integration. To get started, check out this link:

[[https://www.clearos.com/clearfoundation/development/clearos/content:en_us:dev_start|Getting Started with ClearOS App Development]]


{{keywords>clearos, clearos content, clearos7, blog, kb, maintainer_dloper}}
