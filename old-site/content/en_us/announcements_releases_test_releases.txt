===== Information on Test Releases of ClearOS =====
On a regular basis, we release test versions of ClearOS in preparation for an official release.  These test releases are an excellent way for users and developers to experiment with the latest features.

Please review the release notes of each test release.  Some releases are only for developers and not usable by an end user.  On the other hand, some releases will be one or two bug fixes away from completion.
	
<note warning>
Familiarity with the command-line environment is recommended is you plan on running an alpha or beta release.  If you are just installing the test release to look around, no command line experience is necessary.  During the alpha and beta releases, we will sometimes provide instructions on how to upgrade or fix a test system, but only via the command line.
</note>
	  	
===== Alpha vs. Beta vs. Release Candidate =====
The definitions of alpha, beta and release candidate depend on who you are talking to.  For ClearOS we define these terms as follows...

==== Alpha ====
  * Alphas should not be running in a production environment.
  * Many core features are incomplete or untested.
  * Upgrading to the final release is not guaranteed. 

==== Beta ====
  * Betas can be running in less critical environments (personal home server)
  * Most of the features are complete and in working order.
  * Upgrading to the final release is (usually) painless.

==== Release Candidate ====
  * Release candidates can be running in a limited production environment.
  * The software is feature complete with no known major bugs.
  * Upgrading to the final release is rarely an issue.
{{keywords>clearos, clearos content, announcements, releases, generalinformation, testreleases, maintainer_dloper}}
