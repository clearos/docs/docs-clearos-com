===== Bandwidth Manager =====
The bandwidth manager is used to shape or prioritize incoming and outgoing network traffic.  You can limit and prioritize bandwidth based on IP address, IP address ranges and ports. 

===== Installation =====
If you did not select this module to be included during the installation process, you must first [[:content:en_us:5_software_modules|install the module]].

===== Menu =====
You can find this feature in the menu system at the following location:


<navigation>Gateway|Bandwidth and QoS|Bandwidth</navigation>

===== ClearSDN Services =====
{{:omedia:clearsdn-icon-xxs.png }} The [[http://www.clearcenter.com/Services/clearsdn-remote-bandwidth-monitor-6.html|Remote Bandwidth Monitor]] service provides hourly bandwidth measurements from our remote system monitors. The service is an excellent tool for detecting unauthorized network usage and monitoring your Internet Service Provider's (ISP) quality of service. 
===== Configuration =====
Before getting started with the bandwidth configuration, it is important to know about best practices.  There are two ways to approach bandwidth management:

  * Limit low priority traffic in an effort to improve speeds for high priority traffic
  * Reserve bandwidth for high priority traffic which will shuffle low priority traffic aside

It is impossible to predetermine what types of traffic will be low priority, but typically quite easy to identify important traffic (VoIP being an obvious one).  Therefore, **reserving bandwidth for high priority traffic** is the best way to proceed with bandwidth management.  

==== External Interface Upload/Download Settings ====
The upstream and downstream rates for your external (Internet) interfaces must be specified in order to optimize the underlying bandwidth engine. If you set these values below your actual upload/download rates, then you will find your bandwidth capped by these lower values.

==== Add Bandwidth Rule ====
The basic Add Bandwidth Rule provides a simple way to specify bandwidth rules on your system. If you need more fine grained control over your bandwidth rules, see the next section: Add Advanced Rule.

=== Mode ===
There are two types of bandwidth modes available.

  * Limit - clamps the bandwidth at a maximum rate
  * Reserve - guarantees the specified bandwidth 

With reserve mode enabled, the system will guarantee the minimum bandwidth and use more if it is available. When all the bandwidth that has been reserved/limited is in use, then the system will share the bandwidth proportionately.

=== Service ===
The network service, e.g. web traffic.

=== Direction ===
You must specify the direction of the bandwidth flow.

    * Flowing to your network -- a user on your LAN downloading a file over the web. 

    * Flowing from your network -- a user on your LAN uploading a file via a peer-to-peer network. 

    * Flowing to your system -- inbound mail going to the mail server running on your system. 

    * Flowing from your system -- outbound mail from the system's mail server getting delivered to various locations on the Internet. 

=== Rate ===
The bandwidth rate to reserve/limit in kilobits per second.

=== Greed ===
The greed level tells the bandwidth manager how to handle any extra available bandwidth on your network. Consider the following example:

  * A 1000 kbps connection to the Internet
  * 200 kbps reserved for web traffic, low greed
  * 300 kbps reserved for mail traffic, high greed
  * 500 kbps unallocated 

If both mail and web traffic require 900 kbps each, mail traffic will get its full 300 kbps allotment, plus the majority (but not all) of the unallocated 500 kbps since the bandwidth rule is greedy. Web traffic will be guaranteed its 200 kbps, but will only get a small portion of the unallocated bandwidth.

==== Add Advanced Rule ====
The meaning of Source and Destination in the advanced bandwidth rules can be confusing at first. Please take a look at some of the examples in the next section for helpful hints.

=== Nickname ===
An easy to remember name to remind you of the purpose of the bandwidth rule.

=== IP Address/Range ===
The IP address parameter can contain:

    * A single IP address
    * A IP address range
    * nothing 

If this field is left blank, then the bandwidth rule will be used by all IP addresses will.

When specifying an IP address range with a starting and ending IP (for example, 192.168.1.100 to 192.168.1.200), each of the individual IP addresses will be assigned the configured rule. For example, the following bandwidth rule would clamp downloads from every workstation on 192.168.1.254 to a maximum of 100 kbps:

    * IP Address Range - Destination - 192.168.1.1 : 192.168.1.254
    * Direction - Download
    * Rate - 100 kbps
    * Ceiling - 100 kbps 

An alternative bandwidth range can be specified using [Network Notation|network/netmask]]. In this case, the range of IP addresses are treated as a single bandwidth rule. For example, the following bandwidth rule would clamp downloads for 192.168.1.x to a maximum of 500 kbps:

    * IP Address Range - Destination - 192.168.1.0/24
    * Direction - Download
    * Rate - 500 kbps
    * Ceiling - 500 kbps 

If only one person on the 192.168.1.0/24 network was downloading, the would get the 500 kbps. If two people were downloading, they would share the 500 kbps.

=== Port ===
The port parameter is used to apply a bandwidth rule to a particular service. For instance, you can limit web traffic by specifying port 80. If the port is left empty, then all ports will be affected.

=== Direction ===
The direction of the network packet flow that you desire.

=== Rate ===
The upload/download speed to reserve (guarantee) for the service.

=== Ceiling ===
The maximum upload/download speed allowed for the service. If you would like the rule to use all available bandwidth, leave this field blank. If you set rate and ceiling to the same value, then you will be clamping bandwidth uploads at the ceiling rate.

=== Greed ===
The greed level tells the bandwidth manager how to handle any extra available bandwidth on your network. Consider the following example:

    * A 1000 kbps connection to the Internet
    * 200 kbps reserved for web traffic, low greed
    * 300 kbps reserved for mail traffic, high greed
    * 500 kbps unallocated 

If both mail and web traffic require 900 kbps each, mail traffic will get its full 300 kbps allotment, plus the majority (but not all) of the unallocated 500 kbps since the bandwidth rule is greedy. Web traffic will be guaranteed its 200 kbps, but will only get a small portion of the unallocated bandwidth.

==== Web Proxy Gotchas ====
Having a web proxy configured either on a ClearOS gateway or some other local proxy server complicates matters.  As soon as a web request is made via the proxy, the source IP address for the request is lost.  In other words, configuring bandwidth rules using an IP address on your local network will not have an effect for any traffic going through the proxy.  See the examples for ways to limit bandwidth to your proxy server.

===== Examples =====
Unless otherwise specified, fields should be left blank or with defaults.

==== Limit web downloads going the proxy to 300 kbps ====
If you have the web proxy enabled for your network, you can limit how much bandwidth can be used for web downloads:
    * Nickname - Web_proxy_limit
    * Port - Source: 80
    * Direction - Download
    * Rate - 300 kbps
    * Ceiling - 300 kbps 
    * Greed - Very Low

==== Limit web downloads to workstation 192.168.1.100 to 100 kbps ====
    * Nickname - Download_to_workstation100_port80
    * IP Address - Destination: 192.168.1.100
    * Direction - Download
    * Port - Source: 80
    * Rate - 100 kbps
    * Ceiling - 100 kbps 

==== Limit uploads from workstation 192.168.1.100 to 100 kbps ====
This type of rule is useful for limiting peer-to-peer uploads for a specific user on your network.

    * Nickname - Upload_from_workstation100
    * IP Address - Source: 192.168.1.100
    * Direction - Upload
    * Rate - 100 kbps
    * Ceiling - 100 kbps 

==== Limit web downloads from Internet host 1.2.3.4 to 100 kbps ====
With this rule, the maximum web downloads from 1.2.3.4 is limited to 100 kbps (even if your Internet connection is idle).

    * Nickname - Download_from_remotehost_port80
    * IP Address - Source: 1.2.3.4
    * Port - Source: 80
    * Direction - Download
    * Rate - 100 kbps
    * Ceiling - 100 kbps 

==== Reserve uploads to Internet host 1.2.3.4 to 100 kbps ====
This type of rule is useful for limiting uploads to a specific host on the Internet. For example, 1.2.3.4 might be an offsite backup system that should not get high priority bandwidth.

    * Nickname - Upload_to_remotehost
    * IP Address - Destination: 1.2.3.4
    * Direction - Upload
    * Rate - 50 kbps
    * Greed - Very Low 

==== Reserve bandwidth to/from a VoIP/SIP Provider ====
If you have a SIP provider for your VoIP system, you will want to reserve bandwidth for this traffic.  You will need to provide two bandwidth rules -- one for traffic **from** your provider, and one for traffic **to** your provider.

=== Traffic from SIP Provider ===
  * Nickname: from_sip
  * IP Address - Source: 1.2.3.4
  * Direction - Download
  * Rate - 800 kbps

=== Traffic to SIP Provider ===
  * Nickname: to_sip
  * IP Address - Destination: 1.2.3.4
  * Direction - Upload
  * Rate - 800 kbps

===== Units - kbit/s, kbps, Mbps, and Other Confusing Notation =====
Depending on where you are and who you are talking too, there are different measurement units used for bandwidth.  Here are some tips to help with converting from one unit to another -- capitalization is important:

Conversion tips:
  * Mega is 1000 times larger than kilo
  * A byte is 8 times larger than a bit

Examples:
  * 1 Megabit per second is approximately 1000 kilobits per second 
  * 1 Megabyte per second is approximately 8000 kilobits per second

===== Links =====
  * [[http://www.lartc.org/howto/|Linux Advanced Routing and Traffic Control]]
{{keywords>clearos, clearos content, Bandwidth Manager, app-bandwidth, clearos5, userguide, categorygateway, subcategorybandwidthandqos, maintainer_dloper}}
