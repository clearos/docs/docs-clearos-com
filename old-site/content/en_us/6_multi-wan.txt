===== Multi-WAN =====
The multi-WAN feature in ClearOS allows you to connect your system to multiple Internet connections.  ClearOS multi-WAN not only provides load balancing, but also automatic failover.

===== How It Works =====
The ClearOS multi-WAN has the following features:

  * auto-failover
  * load balanced
  * round-robin based on user-defined weights
 
To give you an example of how multi-WAN works, imagine two 1 Mbit/s DSL lines with two users on the local network.  With every new connection to a server on the Internet, the multi-WAN system alternates WAN interfaces.  User A could be downloading a large file through WAN #1, while User B is making a voice-over-IP (VoIP) telephone call on WAN #2.  

With some applications, the download speed for the multi-WAN system can use the full 2 Mbit/s available.  For example, downloading a large file from a peer-to-peer network will use the bandwidth from both WAN connections simultaneously.  This is possible since the peer-to-peer technology uses many different Internet "peers" for downloading.  At the other end of the spectrum, consider the case of downloading a large file from a web site.  In this case, only a single WAN connection is used -- 1 Mbit/s maximum.

[[http://en.wikipedia.org/wiki/Link_aggregation|Bandwidth aggregation]] (combining multiple WAN interfaces to look like a single WAN interface) is not possible without help for your ISP since both ends of an Internet connection must be configured.

===== Configuration =====
==== Weights ====
Multi-WAN weights are used to load balance outbound Internet traffic.  By default, all WAN interfaces are given a weight of one.  This default configuration means the network traffic will be roughly evenly split amongst the different WAN connections.

In one of the typical multi-WAN configurations, a second broadband connection is used for backup.  This second connection is often a low-cost and low-bandwidth connection.  In this case, you would want to set the weight on your high-bandwidth connection to 3 or 4, while leaving your low-cost/low-end connection with a weight of 1.

The calculation for amount of bandwidth for a certain NIC is the weight of that NIC over the sum of all NICs. For example, if I have 3 external multi-WAN NICs eth0, eth1, and eth2 and the weight is 4, 3, and 1 respectively. This will mean that the bandwidth will be divided as such:

^NIC^Weight^Fraction^Percentage^
|eth0|4|4/8|50%|
|eth1|3|3/8|37.5%|
|eth2|1|1/8|12.5%|
|||||
|**TOTAL**|**8**|||

==== Dynamic DNS ====
While on the road, you may need to connect back to your ClearOS system.  If you have a dynamic IP, you can use the dynamic DNS hostname to make the connection.  For this reason, the built-in dynamic DNS system will //always// report an IP address that has a working connection to the Internet.  

If your ClearOS system uses real public IP addresses, you can select the preferred WAN interface to use when all your WAN connections are functioning.

==== Source Based Routes ====
In some situations, you may want a system on your local area network (LAN) to always use a particular WAN interface.  A source-based route definition makes this possible.

==== Destination Port Rules ====
In some situations, you may want to send network traffic for a specific port out a particular WAN interface.  For example, you may want to send all DNS traffic out a particular WAN network.

===== Routing Policies =====
Some Internet service providers (ISPs) will not allow traffic from source addresses they do not recognize as their own.  The following scenarios will give you a good idea of common issues faced in a multi-WAN environment.  In the examples, we assume two connections, but the same issues crop up with three or more connections.

==== DNS Servers ====
The DNS servers configured on the ClearOS system will be provided by one or both ISPs.  In our example, we are going to assume that ISP #1 provides the DNS servers.  If a DNS request from your network goes out the ISP #2 connection, it might get blocked by ISP #1.  Result: DNS requests will only succeed on ISP #1.

**Solution** -- Use DNS servers that are accessible from any network.  If your ISPs do not provide such DNS servers, then we recommend using [[http://www.opendns.com|OpenDNS]].

Note: your DHCP/DSL network configuration settings should have the **Automatic DNS Servers** checkbox unchecked.

==== DMZ Networks and 1-to-1 NAT ====
If you have a range of extra IP addresses provided by ISP #1, you may need to explicitly send traffic from these extra IPs out the ISP #1 connection.  ISP #2 may drop the packets.

**Solution** -- Use a Source Based Route for your DMZ network or 1-to-1 NAT.

{{keywords>clearos, clearos content, MultiWAN, Multi-WAN, app-multiwan, clearos6, userguide, categorynetwork, subcategorysettings, maintainer_dloper}}
