===== Web Proxy =====
The web proxy in ClearOS is a high-performance proxy caching server for web clients, supporting HTTP, FTP and some lesser known protocols. The software not only saves bandwidth and speeds up access time, but also gives administrators the ability to track web usage via web-based reports.

===== Web Site Bypass =====
In some circumstances, you may need to bypass the proxy server.  Typically, this is required for web sites that are not proxy-friendly.  You can read more about this feature in the [[http://www.clearcenter.com/support/documentation/|User Guide]].  If you find a web site or web service that requires a bypass, please add it to the list on the following page:

  * [[:content:en_us:dev_apps_web_site_bypass_-_site_list|Web Site Bypass - Site List]] 


{{keywords>clearos, clearos content, dev, apps, app-web_proxy, maintainer_dloper}}
