===== Remote Security Audit =====
The following document provides information on how to activate and configure the **Remote Security Audit** service for your ClearOS system.  For an overview of the features and benefits of the service, please review the [[http://www.clearcenter.com/Services/clearsdn-remote-security-audit-5.html|service information here]]. 

===== Installation =====
If your system does not have this app available, you can install it via the [[:content:en_us:6_Marketplace|Marketplace]].

===== Menu =====
You can find this feature in the menu system at the following location:
 
<navigation>System|Security|Remote Security Audit</navigation>

===== Configuration ======
You can enable and disable the **Remote Security Audit** service from your ClearOS system.  A report on recent updates is also provided.

===== Status Reports =====
The **Remote Security Audit** service includes a report of recent activity on your system.  When a significant event occurs, an audit report is e-mailed to either the account administrator, or notification e-mail addresses specified for the system.  You can manage these notification e-mail addresses via your online account - go to <navigation>Systems|System Settings|Alert Notification</navigation> in the menu.

===== How It Works =====
The goal of the security audit is to pick up clues that typically result from a server containing malicious data. This can be determined by:

  * Detecting changes in critical files and directories
  * Checking for a change in the number of hidden files and directories
  * Monitoring the inventory of setuid/setguids files
  * Detecting a change in the number of superuser accounts
  * Auditing the number of accounts without passwords 

On a regular interval, the security audit will:

  * Connect to your system
  * Make sure the audit tools have not been tampered with
  * Signal the system to run the audit
  * Wait for the audit to complete
  * Save the status results in your ClearSDN online account
 
The system will send an e-mail alert if any irregularities occur during this process.

{{keywords>clearos, clearos content, Remote Security Audit, sdn-remote-security-audit, clearos6, userguide, categorysystem, subcategorysecurity, maintainer_dloper}}
