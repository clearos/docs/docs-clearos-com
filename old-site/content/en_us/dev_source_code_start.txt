===== ClearOS Source Code =====
Please see and visit us at:
  * https://github.com/clearos
  * https://github.com/clearos-packages/

Please use, fork and send in some Pull Requests (PRs) :-)

==== Legacy =====
Previously, SVN was used for source code management. It was at:
  * **[[http://code.clearfoundation.com/svn/|Via the web]]**
  * Via SVN: %%svn://svn.clearfoundation.com/clearos%%

For those of you with the ability to commit source code, please review the following documents:

  * [[:content:en_us:dev_source_code_changelog_policy|Changelog Policy]] 
  * [[:content:en_us:dev_source_code_management_system|Source Code Management System]]
  * [[:content:en_us:dev_creating_and_managing_ssh_keys|Creating and Managing SSH Keys]]

===== API =====
{{ :omedia:android_monitor02.png?250}}

With the software API, you can make ClearOS sing!  An example is shown in the adjacent screenshot.  It is a [[http://www.android.com|Google Android mobile application]] using the ClearOS API.  This clever tool monitors bandwidth on a remote ClearOS system in real-time.  

Keep in mind, this section of the web site is a reference document only.  The tutorials, coding standards and "Hello World" documentation will be coming soon.

  * [[http://www.clearfoundation.com/docs/developer/api/reference|API Reference Documentation]]