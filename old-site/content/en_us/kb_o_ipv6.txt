===== IPv6 =====
This is an architecture guide for IPv6 on ClearOS 6.x. This guide discusses the format for IPv6 structures within ClearOS and is designed to be a howto for current users as well as a structure guide for future development and modules.

<note warning>This guide is still under construction</note>

===== Prerequisites =====
To enable ClearOS for IPv6 support, validate that following setting in **/etc/sysconfig/network**:

  NETWORKING_IPV6=yes

===== Configuring interfaces =====
For each IPv6 enabled interface you will update the following:

  IPV6INIT=yes
  IPV6ADDR=<IPv6-IP-Address>
  IPV6_DEFAULTGW=<IPv6-IP-Gateway-Address>

Your interface may look like this (**/etc/sysconfig/network-scripts/ifcfg-eth0**):

  DEVICE=eth0
  BOOTPROTO=static
  ONBOOT=yes
  USERCTL="no"
  HWADDR=00:11:22:33:44:55
  IPADDR=1.2.3.4
  GATEWAY=1.2.3.1
  NETMASK=255.255.255.248
  IPV6INIT=yes
  IPV6ADDR=1234:0000:1000:0011:0000:0000:0102:0304
  IPV6_DEFAULTGW=1234:0000:1000:0011:0000:0000:0102:0301

{{keywords>clearos, clearos content, kb, dev, architecture, howto, ipv6, maintainer_dloper}}
