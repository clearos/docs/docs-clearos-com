===== Re-initialize OpenLDAP Directory =====
This guide will help you re-initialize your OpenLDAP directory on ClearOS. Re-initialization WILL delete all users and reset OpenLDAP structures. Other sub-systems and apps may be adversely affected by this action. We recommend fully backing up your system before proceeding.

<note warning>Performing these actions are irreversible and can result in loss of user account data and can adversely affect existing services which may store configuration data in the OpenLDAP directory. Use at your own RISK!!!</note>

===== ClearOS 5 =====
Under ClearOS 5.x, you can use the following command to reset the OpenLDAP directory. This is useful to make a replicate or to flush all users:

  ldapsetup -f

===== ClearOS 6 =====
Under ClearOS 6, you can use the following commands to purge your OpenLDAP directory. You will then need to reset the directory structures in your Webconfig interface:

<code>
sudo rm /var/clearos/accounts/initialized  /var/clearos/accounts/config  /var/clearos/ldap/initialized  /var/clearos/openldap/config.php /var/clearos/mode/mode.conf /var/clearos/samba/init* -fv
sudo /sbin/service smb stop
sudo /sbin/service nmb stop
sudo /sbin/service winbind stop
sudo /sbin/service nscd stop
sudo /sbin/service nslcd stop
sudo /sbin/service slapd stop
</code>

===== Help =====
==== Navigation ====

[[:|ClearOS Documentation]] ... [[..:|Knowledgebase]] ... [[:Knowledgebase:Troubleshooting:|Troubleshooting]]
{{keywords>clearos, clearos content, clearos-5, troubleshooting, help, support, openldap, directory, openldap directory, reinitialize, reset, reset directory, start over}}
