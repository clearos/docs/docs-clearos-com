===== Directory Plugins =====
Directory plugins allow applications to extend the directory for user/group based services.  For example, the Squid web proxy plugin provides the necessary glue to map users and groups to the proxy server settings.  Changes to LDAP schemas and attributes are **not** required for plugins.  Instead, plugins use the schema and attributes provided in the base ClearOS implementation.  

===== List of Plugins =====
  * Apache Web Server
  * PPTP
  * ProFTPd FTP
{{keywords>clearos, clearos content, dev, architecture, maintainer_dloper}}
