===== Form View =====
There are three main styles of forms/controllers found in ClearOS.  This document provides information on the basic structure of a simple form view.  You can find more information about the other two styles of view:

  * [[:content:en_us:dev_framework_reference_guide_crud_view|CRUD Views]] 
  * [[:content:en_us:dev_framework_reference_guide_views|advanced views]] 

You can also find the complementary form controller for this view [[:content:en_us:dev_framework_reference_guide_form_controller|here]]. 

===== Copyright and License =====
Please make sure you have a [[:content:en_us:dev_framework_coding_standards_license|license]] notice at the top of your source code file.

===== Dependencies =====
Load any dependencies required by your view.  The dependency list will typically include:

  * One or more translations sets
  * Library/Helper dependencies 

<code php>
///////////////////////////////////////////////////////////////////////////////
// Load dependencies
///////////////////////////////////////////////////////////////////////////////

$this->load->language('dns');
$this->load->language('network');
</code>

===== Form Handler =====
There are a number of cases where two different controller methods can use almost identical forms.  For example, a web form for **adding** and **editing** a user only contain less than a handful o differences.  If this is the case for your view, add a //Form Handler// section for clearly defining these differences.

<code php>
///////////////////////////////////////////////////////////////////////////////
// Form handler
///////////////////////////////////////////////////////////////////////////////

if ($form_type === 'edit') {
    $read_only = TRUE;
    $form_path = '/dns/edit';
    $buttons = array(
        form_submit_update('submit'),
        anchor_cancel('/app/dns/'),
        anchor_delete('/app/dns/delete/' . $ip)
    );
} else {
    $read_only = FALSE;
    $form_path = '/dns/add';
    $buttons = array(
        form_submit_add('submit'),
        anchor_cancel('/app/dns/')
    );
}
</code>

===== Form Open / Close =====
Every form needs to have an opening and a closing - form_open and form_close.

<code php>
///////////////////////////////////////////////////////////////////////////////
// Form open
///////////////////////////////////////////////////////////////////////////////

// Example with hard-coded form path
echo form_open('dhcp/general');
echo form_header(lang('base_general_settings');

// Example where view could be an "add" or "edit" form
echo form_open($form_path . '/' . $ip);
echo form_header(lang('base_general_settings');

///////////////////////////////////////////////////////////////////////////////
// Form close
///////////////////////////////////////////////////////////////////////////////

echo form_footer();
echo form_close(); 
</code>

===== Form Fields =====
The Field Helper provides the necessary function calls to build out your web form.  This helper was developed to simplify and standardize the code.  The technical details are provided in the Field Helper reference documentation.

<code php>
///////////////////////////////////////////////////////////////////////////////
// Form fields
///////////////////////////////////////////////////////////////////////////////

echo form_fieldset(lang('dns_dns_entry'));

echo field_input('ip', $ip, lang('network_ip'), $read_only);
echo field_input('hostname', $hostname, lang('network_hostname'));
echo field_input('alias1', $aliases[0], lang('dns_alias') . " #1");
echo field_input('alias2', $aliases[1], lang('dns_alias') . " #2");
echo field_input('alias3', $aliases[2], lang('dns_alias') . " #3");

echo form_fieldset_close();
</code>

===== Buttons / Anchors =====
Like most web interfaces, ClearOS action buttons/anchors appear at the bottom of the form.  The **form_submit_X** function calls are used to create form submit buttons, while the **anchor_** function calls are used to link to CodeIgniter routed pages.  For example, the '/app/users/edit/emily' is an anchor that will display the user edit form for username "emily".

  * Most themes, including the default themes in ClearOS, will style anchors and buttons in the same way. 

  * All buttons should be wrapped in a button set.  This is a standard way to handle buttons in certain types of interfaces (notably, mobile interfaces).  Other themes can choose to ignore grouping buttons together.
 
  * All buttons and anchors need to specify their importance.  The theme developer may display common actions prominently, while less common actions diminutively. 

<code php>
///////////////////////////////////////////////////////////////////////////////
// Buttons
///////////////////////////////////////////////////////////////////////////////

// Example with hard-coded form buttons
echo button_set(
    form_submit_update('submit', 'high') .
    anchor_cancel('/app/users', 'low')
);

// Example where view could be an "add" or "edit" form, so buttons are passed in
echo button_set($buttons);

</code>

===== Links =====
  * [[:content:en_us:dev_framework_reference_guide_form_controller|Form Controller]]
  * [[:content:en_us:dev_framework_reference_guide_crud_view|CRUD View]]
  * [[:content:en_us:dev_framework_reference_guide_views|Advanced View]]
{{keywords>clearos, clearos content, dev, framework, mvc, views, maintainer_dloper}}
