===== Elasticsearch =====
Elasticsearch is a flexible and powerful free / libre / open source, distributed, real-time search and analytics engine. It is super fast, RESTful, based on Apache Lucene and has a very high activity level: www.ohloh.net/p/elasticsearch

It is part of "The Elasticsearch ELK Stack". The two other apps of the stack are:
  * Logstash helps you take logs and other time based event data from any system and store it in a single place for additional transformation and processing.
  * Kibana is Elasticsearch’s data visualization engine, allowing you to natively interact with all your data in Elasticsearch via custom dashboards.

It is also part of [[http://www.clearfoundation.com/Community/Groups/141/Viewgroup.html|Tiki Suite]]

Assumption: you want to use Elasticsearch for data on a ClearOS instance to index data on the same server (ex.: with [[http://www.elasticsearch.org/guide/en/elasticsearch/client/community/current/integrations.html|WordPress, Tiki, etc.]]). To have a more advanced setup (ex.: a cluster), please refer to the Elasticsearch documentation.

== Big picture steps ==
  - Install Java
  - Install Elasticsearch
  - Configure your apps to use Elasticsearch

  * By default ClearOS 6.x installs Java 1.6.0_30 and  [[http://www.elasticsearch.org/blog/elasticsearch-1-2-0-released/|Elasticsearch 1.2 requires requires Java 7]], so we'll get latest stable versions below.

== Step by step ==
  - Get Java 1.8 <code>yum --enablerepo=clearos-core install java-1.8.0-openjdk</code> 
  - Set Java 1.8 as default <code>alternatives --config java</code>
  - Import public key <code>rpm --import http://packages.elasticsearch.org/GPG-KEY-elasticsearch</code>
  - create /etc/yum.repos.d/elasticsearch.repo with the following snippet <code>[elasticsearch-1.4]
name=Elasticsearch repository for 1.4.x packages
baseurl=http://packages.elasticsearch.org/elasticsearch/1.4/centos
gpgcheck=1
gpgkey=http://packages.elasticsearch.org/GPG-KEY-elasticsearch
enabled=1</code>
  - Install Elasticsearch (and follow instructions) <code>yum install elasticsearch</code> 
  - Configure Elasticsearch to automatically start during bootup <code>chkconfig --add elasticsearch</code>
  - Reboot to test <code>reboot</code>
  - Check if Elasticsearch is running with the following command <code>curl localhost:9200</code>
  - Make sure example.org:9200 does not answer. If it does, you need to activate the firewall. It's important to add security (by default, it's wide open). See Elasticsearch documentation.

Here is an example of a working Elasticsearch instance.
<code>
[root@example ~]# curl localhost:9200
{
  "status" : 200,
  "name" : "Norrin Radd",
  "cluster_name" : "elasticsearch",
  "version" : {
    "number" : "1.4.4",
    "build_hash" : "c88f77ffc81301dfa9dfd81ca2232f09588bd512",
    "build_timestamp" : "2015-02-19T13:05:36Z",
    "build_snapshot" : false,
    "lucene_version" : "4.10.3"
  },
  "tagline" : "You Know, for Search"
}
</code>

== Todo ==
   * [[http://blog.lavoie.sl/2012/09/configure-elasticsearch-on-a-single-host.html|Test / document how to set up on servers with less RAM (ex.: 1 gig)]]
   * [[http://www.elasticsearch.org/guide/en/elasticsearch/client/community/current/health.html|Setup monitoring]] (Especially how much RAM it is using)
   * Add support for Elasticsearch to index [[http://www.clearcenter.com/support/documentation/user_guide/flexshare|Flexshares]] (and make available to apps such as Tiki, etc.)

== Long term ==
  * Experiment with & document [[http://www.elasticsearch.org/overview/logstash/|Logstash]] and [[http://www.elasticsearch.org/overview/kibana/|Kibana]]
  * Add support for [[https://github.com/salyh/elasticsearch-river-imap|Elasticsearch to index emails from IMAP]]
  * Make an official Elasticsearch app for ClearOS.


== Related links==
   * https://github.com/elasticsearch/elasticsearch/issues/664
   * http://stackoverflow.com/questions/15503455/elasticsearch-allow-only-local-requests
   * https://github.com/sonian/elasticsearch-jetty
   * [[http://www.clearfoundation.com/component/option,com_kunena/Itemid,232/catid,40/func,view/id,61299/]]
   * http://www.elasticsearch.org/blog/apt-and-yum-repositories/
   * https://github.com/salyh/elastic-defender
   * https://suite.tiki.org/Elasticsearch

{{keywords>clearos, kb, howtos, tiki, skunkworks, maintainer_dloper, maintainerreview_dloper}}
