===== Freeswitch with sipXecs =====

This guide will (hopefully) help you get the enterprise communication server known as sipXecs installed on your ClearOS installation.  SipXecs is a powerful VOIP server that utilizes FreeSWITCH, the awesome, scalable, open source telephony platform.  It also incorporates OpenFire, the really cool open source instant messaging server.  It used to included access to DimDim as well but DimDim was acquired by another company is no longer freely available.  All of these features make for a great unified communication system and it has a ton of other great features as well.  SipXecs has, IMHO, a very good, easy-to-use web interface also so there's no great need to edit files via the command-line.

===== Before You Get Started =====

Make sure the web server module is installed on your ClearOS server.  The web server does not have to be running but it does have to be installed.  

By default, sipXecs uses ports 80 and 443 for its web-based configuration and management interface known as sipXconfig.  If you already have Apache configured and running on ports 80 and 443, you will need to change the "Listen" ports in the following web configuration files for sipXecs:

/etc/sipxpbx/httpd.conf

/etc/sipxpbx/httpd-sipxchange-common-ssl.conf

The following steps will need to be completed after you have installed sipXecs.

Within /etc/sipxpbx/httpd.conf, change 

  Listen 80 

to 

  Listen 9080 (or some other unused port)

Within /etc/sipxpbx/httpd-sipxchange-common-ssl.conf, change

  Listen 443

to

  Listen 9443 (or some other unused port)

<note important>You will need root access to the ClearOS server via SSH.  All of the commands specified in this document were entered as the root user.</note>

 
======Let's Get Started======


First, download the sipXecs repository file:

  wget -P /etc/yum.repos.d/ http://download.sipfoundry.org/pub/sipXecs/sipxecs-4.4.0-centos.repo

It is now necessary to run "yum update" but first, we must edit the repo file we just downloaded in order to prevent an error from occurring:

  cd /etc/yum.repos.d
  vi sipxecs-4.4.0-centos.repo  (you may use nano to edit the file if you like)  :-D

Change line 3 from:

  baseurl=http://download.sipfoundry.org/pub/sipXecs/4.4.0/CentOS_$releasever/$basesearch

to

  baseurl=http://download.sipfoundry.org/pub/sipXecs/4.4.0/CentOS_5/i386/

<note important>

When running yum update without making the modifications above, the following error will occur:

  [root@system ~]# yum update
  Loading "protect-packages" plugin
  Loading "kmod" plugin
  base-supplements               |  951 B     00:00
  base-os                        | 1.1 kB     00:00
  clearcentos-updates            |  951 B     00:00
  base-console                   |  951 B     00:00
  http://download.sipfoundry.org/pub/sipXecs/4.2.1/CentOS_%24releasever.repodata/repomd.xml: [Errno 14] HTTP Error 404: Not Found
  Trying other mirror.
  Error: Cannot retrieve repository metadata (repomd.xml) for repository: sipXecs. Please verify its path and try again
  [root@system ~]#
</note>

==== Install the Necessary Files ====

  yum -y update
  yum -y install sipxecs
  

==== Set Up sipXecs ====

Run the following command:

  sipxecs-setup

You'll be presented with several screens, about 5, prompting you for information such as:
  * Whether or not this is your first sipXecs server or you're adding a server to an existing sipXecs configuration for a domain.  Choose <navigation>First Server</navigation>.
  * You may be prompted to enter an IP address and network mask.
  * You will be prompted to enter a SIP domain name.  By default, the field will be pre-populated with the domain name of your ClearOS server but you may change it if you like and you know how to get it working.  The installation will be much easier if you leave it as the default.
  * Upon completion of the setup, you'll be given the option to Start sipXecs, Exit to Prompt or Reboot.  Choose <navigation>Exit to Prompt</navigation>.

You must change the user and group IDs that the sipXecs program operates under because during the installation, some of the accounts are configured to use UIDs and GIDs that are reserved for LDAP users.

<note warning>If you do not do these steps, the sipXecs accounts will be automatically deleted by the ClearOS server when you reboot your server and sipXecs will not start upon boot.</note>

Begin by modifying the UIDs and GIDs.  The following UIDs and GIDs are just suggestions.  I tried to pick some numbers that were not typical.  You may receive an error indicating that one of more of these IDs is in use on your system.  You can examine the list of users by typing "cat /etc/passwd" to see the UIDs in use or "cat /etc/group" to see the list of GIDs in use.

  groupmod -g 400 sipxchange
  groupmod -g 401 PlcmSpIp
  groupmod -g 402 lvp2890
  usermod -u 400 -g 400 sipxchange
  usermod -u 401 -g 401 PlcmSpIp
  usermod -u 402 -g 402 lvp2890


After you change the UIDs and GIDs, many of the files that were owned by these accounts under their original UIDs and GIDs will be become orphaned, owned simply by the original UID or GID and not the account having the new UID or GID.  So, we have to fix that.

Use the following command to find all of the files that have no group assigned to them and save that output to a file:

  find / \( -fstype ext3 -o -fstype ext3 \) -nogroup -print > output_from_find_group.txt


Now, find all of the files that have no user assigned to them and save that output to a file:

  find / \( -fstype ext3 -o -fstype ext3 \) -nouser -print > output_from_find_user.txt


Now, create a shell script (using vi or nano) named change-group.sh that will change the ownership of the group on the orphaned files.  Place the following contents into the shell script:

  #! /bin/sh
  for file in $(cat output_from_find_group.txt)
      do
         chgrp sipxchange $file
      done


Run the script:

  sh change-group.sh


Now, create a shell script named change-user.sh that will change the ownership of the user on the orphaned files.  Place the following contents into the shell script:

  #! /bin/sh
  for file in $(cat output_from_find_user.txt)
      do
         chown sipxchange $file
      done


Run the script:

  sh change-user.sh


Everything related to the UIDs and GIDs should be fixed now.

If you have the web server module installed but it is not running and you don't plan on using it for any other web sites, you may now start sipXecs by typing:

  /etc/init.d/sipxecs start

If you have the web server module running and you are using it for other web sites or you plan to use it for other sites, please perform the steps outlined in the "Before You Get Started" section to change the ports on which sipXconfig listens.  After you have changed the ports, you may start sipXecs


==== Set up DNS domain record ====

Add an entry for your domain, if you have not done so already, in the ClearOS **Local DNS Server** records via Webconfig

(e.g., brownfamily.local)

{{:playground:clearos-domainname-addition-to-dns-server.jpg?550x200|}}



==== Set up SRV and MX records ====

Edit /etc/dnsmasq.conf and add the following (with information specific to your ClearOS install):

  srv-host=_sip._udp,clear.brownfamily.local,5060,100,1
  srv-host=_sip._tcp,clear.brownfamily.local,5060,200,1
  mx-host=brownfamily.local,clear.brownfamily.local
  mx-target=clear.brownfamily.local

<note>The srv-host records are in the form of _sip.udp,<hostname of clearos server running sipXecs>,5060,100,1.

The mx-host record is in the form of <domain name>,<hostname of clearos server running sipXecs>

The MX records are needed because sipXecs has the ability to e-mail your voicemail messages to you so it needs to know where to deliver them to you.
</note>


Then restart dnsmasq

  /etc/init.d/dnsmasq restart



==== Access sipXconfig - the Web Interface of sipXecs ====

Access sipXconfig by going to http://yourclearosserver if you did not have to modify any web server configuration files.  If you did modify the sipXecs httpd configuration files, navigate to http://yourclearosserver:<the port you configured for sipxconfig>.

"Yourclearosserver" can be the IP address or the hostname of your ClearOS server.  The page will automatically redirect to https://yourclearosserver:8443/sipxconfig.

You can also navigate directly https://yourclearosserver:8443/sipxconfig.

======SipXecs Configuration======

When you access the sipxconfig page, you will be prompted to create a PIN for the superadmin user.

{{:playground:sipxecs_configuration_system_superadmin_pin_setup_screenshot.jpg|}}

You will then be prompted to login as the superadmin.  Use the password/PIN you just created.  The PIN can be alpha-numeric and not just numeric.

Upon logging in, navigate to <navigation>System|Servers</navigation>.

<note warning>Be sure that the IP address of your server does not have quotation marks around it.  For example, if your ip address is 192.168.24.119, it should not be listed as "192.168.1.119" in the IP address field.  If it is, many of the sipXecs services will not start and you will need to log in to the ClearOS server as the root user and run the following command:

  grep -rl 192.168.24.119 /etc/sipxpbx/ | xargs sed -i ’s/"192.168.24.119"/192.168.24.119/g'

Then, refresh the page.

It should look similar to the following:

{{:playground:sipxecs_configuration_system-servers_screenshot.jpg?430x80|}}
"</note>

Click on the name of your server and verify that all of the services are running.

If any of the services are not running, click on the link next to the service that's not running and it will provide you with more info.

If all the services are running, it's time to set up some users!

==== SipXecs User Creation ====

Navigate to <navigation>Users|Users</navigation> and then click on <navigation>Add New User</navigation>.

By default, sipXconfig has extensions 200-299 already configured in an extension pool and users are automatically assigned extensions out of this pool.  So your first user will have the extension of 200.  You may change this as well.

Enter the following information on the Add New User page:
  * Last name
  * First name
  * PIN (this should be a numeric PIN)
  * Confirm PIN

Click <navigation>OK</navigation> or <navigation>Apply</navigation>

==== Register the SIP Phone ====

Now, it's time to register your SIP phone, be it hardware or software, with the sipXecs server.

For this example, I'll use the free softphone, Zoiper (it can be downloaded from http://www.zoiper.com/download_list.php).

Open the configuration options for Zoiper (click on the little wrench icon on the right-hand side of the app and click on <navigation>Add new SIP account</navigation>.

Enter a name and click <navigation>OK</navigation>

Enter the following:
  * Domain: (The ClearOS domain name you created earlier)
  * Username: (The extension you created in sipXecs for the first user (e.g., 200))
  * Password: (The password associated with the extension.  One was pre-configured in sipXecs for you.  If you don't remember what it was, log in to sipXconfig (the web interface for sipXecs), navigate to <navigation>Users|Users</navigation> and then click on the User ID link.  Then click on <navigation>Show Advanced Settings</navigation> and you'll be able to see the password.)
  * Caller ID Name: (The name you want to be displayed to the person you're calling.)

Click <navigation>OK</navigation>

Your account should now show as "Registered" at the bottom of the Zoiper application.

==== Make the First Call ====

Now, dial 100 or 101 from your Zoiper application and say hello to your Auto Attendant!

======Additional Information======

In order to connect sipXecs to the outside world, you'll need to set up a SIP trunk and that is beyond the scope of this document.  There are also a ton of other things you can do with sipXecs so I encourage you to look around the sipXconfig interface.  Check out the conferencing features available to each user, and check out the DimDim integration.  Log into sipXconfig as one of the users using the UserID and PIN and check out the functionality there as well!  There's some great stuff!  Here are some links to some excellent sipXecs resources:
  * http://sipx-wiki.calivia.com/
  * http://www.sipfoundry.org/
  * http://wiki.sipfoundry.org/display/sipXecs/LDAP+Integration
  * http://wiki.sipfoundry.org/display/sipXecs/LDAP+-+Openfire+Integration
  * http://wiki.sipfoundry.org/display/sipXecs/Installing+on+Fedora+and+CentOS
  * http://wiki.sipfoundry.org/display/sipXecs/ClearOS

Enjoy!


======sipXecs 4.6 on ClearOS 6.x======
The instructions above are for sipXecs 4.4.0 on ClearOS 5.x with i386. Some of the steps above are no longer necessary for the more recent versions. Let's document below for sipXecs 4.6 on ClearOS 6.x with 64 bits (Use the notes above as a reference though).

As of 2013-11-09, the instructions below are incomplete. There are conflicts between the installations (Firewall, Mails, port 80). So for now, it's better to install on different servers.

======Download and install sipXecs 4.6======
Get the most recent yum repos

  wget -P /etc/yum.repos.d/ http://download.sipfoundry.org/pub/sipXecs/sipxecs-4.6.0-centos.repo

Then edit the file

  cd /etc/yum.repos.d
  nano sipxecs-4.6.0-centos.repo 

change line 3 to: (it very most likely won't work with /i386/ )

  baseurl=http://download.sipfoundry.org/pub/sipXecs/4.6.0/CentOS_6/x86_64/

Them run an update

  yum -y update

Then, this

  yum install epel-release

Another update just to be sure :-)

  yum -y update


And now, the real install

  yum groupinstall sipxecs

And now the setup

  sipxecs-setup

After, you should visit this URL

  https://example.org/sipxconfig


If problem, try:

  yum remove 'sipx*'
  yum clean all
  
  
=====Avoiding conflict between the two firewalls=====
sipXecs has its own firewall, and it conflicts with ClearOS's firewall. The solution is to turn off the sipXecs firewall, and add those rules to ClearOS.

example.com/sipxconfig/ -> System -> Firewall -> Settings -> Configuration Control -> Unmanaged Firewall service -> "Enable this option if you don't want firewall service to be managed by unified communication system (for e.g. generating configuration, automatic restart of services)."
  
Create a file /etc/clearos/firewall.d/10-sipx with the following content:

  # Requires service for sipXecs
  iptables -t filter -I INPUT -i eth0 -p tcp --dport 80 -m state --state NEW,ESTABLISHED -j ACCEPT
  iptables -t filter -I INPUT -i eth0 -p tcp --dport 443 -m state --state NEW,ESTABLISHED -j ACCEPT
  iptables -t filter -I INPUT -i eth0 -p udp --dport 53 -m state --state NEW -j ACCEPT
  iptables -t filter -I INPUT -i eth0 -p tcp --dport 21 -m state --state NEW,ESTABLISHED -j ACCEPT
  iptables -t filter -I INPUT -i eth0 -p tcp --dport 20 -m state --state NEW,ESTABLISHED -j ACCEPT
  iptables -t filter -I INPUT -i eth0 -p tcp --dport 50000:50050 -m state --state NEW,ESTABLISHED -j ACCEPT
  iptables -t filter -I INPUT -i eth0 -p udp --dport 123 -m state --state NEW -j ACCEPT
  iptables -t filter -I INPUT -i eth0 -p udp --dport 69 -m state --state NEW -j ACCEPT

The file can have any name. The 10- prefix is to permit ordering of application of rules.

=====Putting sipXecs on a port not used by ClearOS =====
sipXecs by default puts the admin panel on port 80 (example.com/sipxconfig/). This is also where websites will live, and thus, it's better to put sipXecs on another port 


=====Other conflicts =====
NTP, DNS, Mail (SMTP, Mail Retrieval) need to be resolved
{{keywords>clearos, clearos content, kb, clearos6, skunkworks, categoryserver, howtos, maintainer_dloper}}
