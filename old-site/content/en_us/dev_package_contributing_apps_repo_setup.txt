===== Submitting Paid and Free Apps to the ClearOS Build System =====
This guide will show you how setup and submit your application to the ClearOS build system so that it can be built against ClearOS in the ClearOS Marketplace.

===== Getting Started =====
You will need a [[https://bitbucket.org/|BitBucket account]] that will allow ClearFoundation and ClearCenter to build your code against ClearOS. This is a free account. You will not need to set up a repository in BitBucket, we will be setting up the repository from which we will build your app. Any repository that you have with BitBucket or other source control provider is in your court. We will not be using your other repo for building ClearOS, even if it is on BitBucket already.

<note>This process works the same for paid and free apps. If you are making free and open source apps (FOSS) or other publicly available apps for the Marketplace, your apps will appear on public testing mirrors once you submit it for build. If you are making a private and/or paid app, your app will appear in a private, testing repo. Paid app submitters will have already made arrangements for how their applications will be made available to them on the builder.</note>

===== Creating your Permissions =====
You will need to let the ClearOS team know what your username is for BitBucket. Once we know your BitBucket account username (don't give us your password, we will never need it), we will provision a repository for you on our BitBucket account. We will then grant you permissions to that repository for everything that you will need to manipulate your own code. This will allow you to push your code to the repo that will automatically be picked up by the ClearOS Builder.

==== Your Tasks ====
==== ClearOS Team Tasks ====
=== Webhook ===
The ClearOS team will be setting the parameter on the repo settings for Webhook under the configuration gear for your repository.

  http://koji.clearos.com/atlassian-webhook

{{:content:en_us:dev_packaging_bitbucket-webhook.png|Webhook Setting}}

=== HipChat ===
The ClearOS team will be setting up HipChat notifications so that when you update packages to your repo, we are notified. This is useful to be able to monitor and intercede if your package has problems. You will not need to sign up or participate in HipChat, it is for behind the scenes voodoo (pay no attention to the man behind the curtain.)

{{:content:en_us:dev_packaging_bitbucket-hipchat.png|HipChat}}

=== Adding the Package to Koji ===
The ClearOS team will add you package to Koji. This will tell Koji where the package belongs and allow it to be built. We will tell Koji that package is paid or free. Free means that anyone can get it. Some apps that have their own pay mechanism will still show up a **free**. This would include things like Plex Media Server. The paid or free status affects the ClearOS Marketplace.

**Paid**
  koji add-pkg --owner=paid contribs7-paid app-money-maker-app-name

**Free**
  koji add-pkg --owner=contribs contribs7 app-my-free-app-name


===== Workflow =====
Once you have this repo which we provision for you, you will get an email indicating that you have permissions to that repository. You will need to accept the invitation to use the repo. You will also see the repo in your BitBucket interface.

==== Getting Your Source to BitBucket ====
Change directory to the root of your code. Then do a 'remote add' to make a directive in git for bitbucket to be a remote for your code. This will leave your existing git to your source code manager in tact. If you are using ssh-key based authentication for BitBucket, you can do the following (change your username to be correct for BitBucket):


  cd /path/to/my/app-example
  git remote add bitbucket https://MYUSERNAME@bitbucket.org/clearos/app-example.git
  git push -u bitbucket --all # pushes up the repo and its refs for the first time

BitBucket will prompt you for your BitBucket password.



You only need to push the package to the proper branch of the ClearOS repo and the builder will start to build it. For example, if you are building your package for just ClearOS 7, simply push it to the 'clear7' branch to start the build for ClearOS 7. For other versions, push the package to the appropriate branch. Current available branches are:

  * clear6
  * clear7


==== Hashing Your Data ====

  sha256sum ../../example/SOURCES/example


{{keywords>clearos, clearos content, howto, kb, dev, clearos7, packaging, buildsystem, maintainer_dloper}}
