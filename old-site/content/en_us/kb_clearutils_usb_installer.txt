===== ClearUTILS USB Installer =====
ClearUTILS is a USB key software installer designed for deploying ClearOS and ClearVM on HPE ProLiant servers.  This guide will help you burn the ClearUTILS image file to USB.

<note important>
The ClearVM client provided with ClearUTILS is version 2, which is scheduled to be released in mid-May 2018.  Until then, you will be unable to register a ClearVM client installed from ClearUTILS.  For more information, click [[https://www.clearos.com/resources/documentation/clearos/content:en_us:kb_clearvm2.0_availability|here]].
</note>
===== Requirements =====
You will need a system or program that has the ability to write the raw image to USB. For Linux and Mac, the 'dd' program is perfect for the task and is already installed on your operating system. 

Download the [[https://ftp1.clearos.com/download/ClearUTILS-HPE.zip|HPE version of the image for ClearUTILS]].

Open a terminal program. Plug in the USB device and determine the raw name for the disk.

<note warning>WARNING!!! Be advised if you select the wrong disk, you can effectively destroy data on your existing operating system. Be sure to use the RIGHT device name. I'll put more warnings throughout the document in case you didn't read this one or believe me that this will really make you hate the fact that you ever found this howto.</note>

=== Security Recommendation ===
To validate you downloaded the right thing and that it is valid and not compromised, you can and should run an SHA256 sum on the file. What that means is that the image mathematically has a fingerprint that should exactly match what is listed below.  Unzip the file and run the 'sha256sum' command (or equivalent) against the file.

Linux:

<code>
[root@server ClearUTILS]# sha256sum ClearUTILS-2.1.5-geneva-enabled.img
9ff407813d4e69195480e8cb2e1450de53dddea931c958f352caf8939cfe95b7  ClearUTILS-2.1.5-geneva-enabled.img
</code>

Mac: 

<code>
Mac:Downloads user$ shasum -a 256 ClearUTILS-2.1.5-geneva-enabled.img
9ff407813d4e69195480e8cb2e1450de53dddea931c958f352caf8939cfe95b7  ClearUTILS-2.1.5-geneva-enabled.img
</code>

Windows:

You'll need to install a checksum program.  We recommend [[https://www.slavasoft.com/fsum/|FSUM]]
<code>
C:\test>fsum.exe -sha256 ClearUTILS-2.1.5-geneva-enabled.img
9ff407813d4e69195480e8cb2e1450de53dddea931c958f352caf8939cfe95b7  ClearUTILS-2.1.5-geneva-enabled.img
</code>

==== Mac ====
=== Find the USB disk ===
To know exactly which partition is your USB, Launch the 'Terminal' program under the Utilities Folder under Applications. Run the following:

  ls /dev/disk*

This will show your your listed drives. Insert the USB drive and run it again.

  ls /dev/disk*

Here I can see that my disk is /dev/disk1. It may be /dev/disk2 or something completely different.

<code>
Mac:Downloads user$ ls /dev/disk*
/dev/disk0	/dev/disk0s1	/dev/disk0s2	/dev/disk0s3
Mac:Downloads user$ ls /dev/disk*
/dev/disk0	/dev/disk0s1	/dev/disk0s2	/dev/disk0s3	/dev/disk1
</code>

You can also validate the disk by running the following and verifying the size:

  diskutil list

Again, it is really important that you identify your correct disk. If I do the dd command against /dev/disk0, I'll destroy my Mac's main drive.

=== Unmount the disk so we can raw write to it ===
When you insert your USB drive, your Mac will try to mount it. We need it unmounted before we can format and write our image to the drive. Under Applications, open the Utilities menu. Launch the 'Disk Utility' program and keep the 'Terminal' program open.

In Disk Utilities, highlight any existing partitions under the main device and Unmount them using the button above.

Another way to unmount it from command line is to run the diskutil to unmount the name you discovered in the previous step. For example:

  diskutil unmountdisk /dev/disk2

=== Write the data to USB ===
Change directory (using the 'cd' command) to the location of your ClearUTILS image folder, for example (change 'myuser' to your user. If you need to see the list of users, type 'cd /Users/' and then hit tab twice.):

  cd ~/Downloads/

Using the 'dd' command, write the ClearOS image to the USB drive (Change the image name to the one you downloaded and change the device name to the one you found on the list (ls /dev/disk*) earlier). You will also want to put an 'r' in front of the device type because it writes faster and more complete on Mac. DON'T COPY and PASTE HERE, structure the command properly. I'll give an example:

** Writes ClearUTILS to disk2 **
  sudo dd if=ClearUTILS-2.1.5-geneva-enabled.img of=/dev/rdisk2 bs=512

<note warning>Just an additional warning, the above command can utterly ruin your day if done wrong. If you are just mindlessly copy and pasting the above, you are doing it wrong. Be sure to to specify the correct disk for the output file ('of') parameter. It might be disk1 but it can be totally different. Check again. Once more please. Yes, you can phone a friend. Is that your final answer?</note>

You will be prompted to provide your password for your username to make this change since it has the potential of damaging your computer. If all goes well, you will have put the image to your USB. If you did it wrong and pushed the image to your Mac's hard drive; I'm sorry, I tried to warn you.

You will know that it is done when it gives your command prompt back.

==== Linux ====
The install process to USB is similar to that of the Mac. You can use 'wget' or 'curl -o' to download the package and then 'dd' it to your USB drive. When you insert the drive you can use the following to determine the correct drive:

  fdisk -l | grep Disk

Once you know the drive you will use, you will reference it in the next command for the 'of' parameter.

  dd if=ClearUTILS-2.1.5-geneva-enabled.img of=/dev/rdisk2 bs=512


<note warning>As with the Mac instructions, I will be very sad but have to tell you repeatedly 'Sorry' and that there is nothing I can do to recover your system if you do this command to the wrong drive. Just sayin'.</note>

==== Windows ===
You can use a program like [[https://sourceforge.net/projects/windd/|WinDD]] or [[http://aeroquartet.com/movierepair/dd%20for%20windows|dd for Windows]] to write the file to USB.

Something like this...maybe?
 
<code>dd if=ClearUTILS-2.1.5-geneva-enabled.img of=\\.\Volume{6f41f4b2-d11a-11de-b318-001d4f88486c}</code>

=== dd for Windows ===
Download and extract the dd zip file and then place the executable in your C:\Windows\ directory. Make sure that you insert the USB drive but close any applications that might open up the volume.

Open a command prompt (Start >> Run >> cmd)

In the command prompt, run the following to get a list of your drives:

  dd --list

You should get an output such as this:

<code>
C:\Users\Username>dd --list
rawwrite dd for windows version 0.5.
Written by John Newbigin <jn@it.swin.edu.au>
This program is covered by the GPL.  See copying.txt for details
Win32 Available Volume Information
\\.\Volume{19d85149-ddf1-11e2-9904-806e6f6e6963}\
  link to \\?\Device\HarddiskVolume4
  fixed media
  Mounted on \\.\d:

\\.\Volume{4268772a-e552-11e5-868d-e89a8f45233b}\
  link to \\?\Device\HarddiskVolume5
  removeable media
  Mounted on \\.\f:

\\.\Volume{19d85148-ddf1-11e2-9904-806e6f6e6963}\
  link to \\?\Device\HarddiskVolume2
  fixed media
  Mounted on \\.\c:

\\.\Volume{19d85147-ddf1-11e2-9904-806e6f6e6963}\
  link to \\?\Device\HarddiskVolume1
  fixed media
  Not mounted

\\.\Volume{9f0e9e5a-ddea-11e2-9d3a-e89a8f45233b}\
  link to \\?\Device\SftVol
  fixed media
  Mounted on \\.\q:

\\.\Volume{92aa90c6-ddf2-11e2-8556-806e6f6e6963}\
  link to \\?\Device\CdRom0
  CD-ROM
  Mounted on \\.\e:


NT Block Device Objects
\\?\Device\CdRom0

Virtual input devices
 /dev/zero   (null data)
 /dev/random (pseudo-random data)
 -           (standard input)

Virtual output devices
 -           (standard output)
</code>

In this case, my USB device is 'F' and the volume label is:

  \\.\Volume{4268772a-e552-11e5-868d-e89a8f45233b}\

Then run the dd program to image the ClearUTILS image to the disk.

  dd if=C:\Users\Username\Downloads\ClearUTILS-2.1.5-geneva-enabled.img of=\\.\Volume{4268772a-e552-11e5-868d-e89a8f45233b} bs=1M

{{keywords>clearUTILS, clearos content, kb, howto, installcategory, maintainer_dloper}}
