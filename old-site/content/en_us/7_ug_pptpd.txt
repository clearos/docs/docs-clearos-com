===== PPTP Server =====
The **PPTP Server** app is a cost effective way to provide road warrior VPN connectivity.  The PPTP VPN client is built-in to Windows 2000, XP, 7, Vista and 10 and other non Microsoft operating systems. No extra software is required and ClearOS provides full password and data encryption.

<note warning>Since 2012, Microsoft, the creators of the PPTP VPN protocol, recommend you no longer use it because of security vulnerabilities. See {{https://docs.microsoft.com/en-us/security-updates/securityadvisories/2012/2743314|Microsoft Security Advisory 2743314}} and {{https://en.wikipedia.org/wiki/Point-to-Point_Tunneling_Protocol|Wikipedia}} among other references. The suggested alternative for roadwarriors in ClearOS is [[:content:en_us:7_ug_openvpn|OpenVPN]].</note>

===== Installation =====
If your system does not have this app available, you can install it via the [[:content:en_us:7_ug_Marketplace|Marketplace]].

===== Menu =====
You can find this feature in the menu system at the following location:
 
<navigation>Network|VPN|PPTP Server</navigation>

===== Configuration =====
==== Configuring the PPTP Server ====
=== Local IP and Remote IP ===
You must select a range of LAN IP addresses for the PPTP VPN connections. This range should be on the same network as your local area network.  By default, the [[:content:en_us:7_ug_dhcp_server]] on ClearOS only uses IP addresses from x.x.x.100 and up.  All addresses below this number are reserved for static use.  We strongly suggest you use this sub-100 static range for PPTP.

=== WINS Server ===
The Microsoft Networking WINS server used by the PPTP client.  Depending on your network configuration, you may need to specify the WINS settings in VPN client configuration.

=== DNS Server ===
The DNS server used by the PPTP client.

=== App Policies ===
PPTP users must have a valid account with the PPTP option enabled.  See the [[:content:en_us:7_ug_users|User Manager]] for more information. You can give the users access to PPTP here by using the <button>Edit Members</button> button or in the [[:content:en_us:7_ug_users|User Manager]] or [[:content:en_us:7_ug_accounts|Accounts Manger]] screens.

==== Configuring Microsoft Windows ====
Almost every OS comes with a built-in PPTP client.

=== Configuring Windows 10 ===
The PPTP client is built-in to Windows 10.

  * Click on **Windows key** then the **Gear Wheel** icon.
  * Click on **Network and Internet**.
  * Click on **VPN** on the left.
  * Click on **Add a VPN Connection** on the right.
  * Give it a Connection Name.
  * Input the VPN server Name or address
  * VPN Type is "Point to Point Tunnelling Protocol (PPTP).
  * You can optionally enter your user name and password here and have the system remember them.
  * Your connection will appear below the "Add a VPN connection" line, where you can select it to connect.

Note in Windows 10 there are a number of different ways of creating the VPN, like  through the Control Panel > Network and Sharing Center
===== PPTP Passthrough =====
PPTP requires special software when passing through firewalls.  This feature is included with ClearOS.  However, there is one important restriction for PPTP pass-through mode: **a PPTP server must not be running on the same gateway that has PPTP connections crossing it.**

If you run a PPTP server on a ClearOS gateway, you will not be able to have people from behind the same gateway make reliable outbound PPTP connections to other servers.  By default, the firewall will automatically disable PPTP pass-through when the firewall already allows connections to a PPTP server.  You will see warning messages in the web-based configuration about these configuration issues.

Depending on the circumstances, you may be able to have both PPTP pass-through and a PPTP server running at the same time.  To do this, you need to override the firewall behavior noted in the previous paragraph.  In the /etc/clearos/firewall.conf file, add the following line:

**PPTP_PASSTHROUGH_FORCE="yes"**

Then restart the firewall with the following command:

**/sbin/service firewall restart**

===== Troubleshooting =====
==== Error 619, PPTP and Firewalls ====
PPTP requires special software when passing through gateways/firewalls. If you are having trouble connecting to a PPTP server, make sure any gateways/firewalls between your desktop and the ClearOS server support PPTP passthrough mode. If you see the following in the /var/log/messages log file on the ClearOS system, then it is likely a PPTP passthrough issue on the client side of the connection:

  PTY read or GRE write failed

You can view log files via the web-based administration tool -- go to <navigation>Reports|Performance and Resources|Log Viewer</navigation> in the menu.

Another quick way to diagnose the issue is by connecting to the PPTP server while connected directly to the local network. With a direct connection to the ClearOS PPTP server, you can eliminate the potential for the PPTP passthrough issue. 

==== Two PPTP Connections to the Same Server ====
The PPTP protocol does not allow two VPN connections from the same remote IP address.  In other words, if you have two people behind a gateway (for example, ClearOS) connecting to the same PPTP server, then the connection should fail.  Note: it is fine to have two people behind a gateway connecting to **different** PPTP servers.

Some PPTP servers and gateways (including ClearOS) do make an exception for this shortcoming.  However, some PPTP servers may strictly follow the standard below:

<note info>The PPTP RFC specifies in section 3.1.3 that there may only be one control channel connection between two systems. This should mean that you can only masquerade one PPTP session at a time with a given remote server, but in practice the MS implementation of PPTP does not enforce this, at least not as of NT 4.0 Service Pack 4. If the PPTP server you're trying to connect to only permits one connection at a time, it's following the protocol rules properly. Note that this does not affect a masqueraded server, only multiple masqueraded clients attempting to contact the same remote server.</note>

==== PPTP VPN Isn't Working ====

**Step 1.** Stop and start the PPTP Server app.\\
**Step 2.** Confirm firewall rules are configured correctly.\\
**Step 3.** Ensure Samba (smb, nmb, winbind) services are running in the Services (System > Settings > Services) app.  These should all be started and enabled.  If they are not enabled, they will not auto-start.  PPTP relies on Samba to authenticate the user.

If you have completed these steps and your PPTP VPN still isn't working, you will need to contact ClearCenter Support.  If you don't have a Business Gold or Platinum subscription, you will need to purchase a Per-Incident Support Ticket or upgrade to Business Gold or Platinum.  For upgrade pricing, please contact 'sales@clearcenter.com'.
===== Links =====
  * [[:content:en_us:7_ug_openvpn|OpenVPN]]
{{keywords>clearos, clearos content, PPTP Server, app-pptp, clearos7, userguide, categorynetwork, subcategoryvpn, maintainer_dloper}}
