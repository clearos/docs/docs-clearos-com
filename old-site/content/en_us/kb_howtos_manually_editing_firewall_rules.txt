===== Manually Editing Firewall Rules in ClearOS 5.x =====
Great care must be taken when modifying firewall rules. It is advised that you have direct console access when manually editing the firewall rules. It is quite easy and possible to lock yourself out of any and all remote management tools if the firewall is mis-configured.

Also, proficiency with a command line text editor is required for manipulation of these files. By default ClearOS comes with the vim editor (vi Improved).

===== /etc/firewall =====
ClearOS uses the file in /etc/firewall for the management of the firewall rules. Moreover, the system reads from this file when displaying the firewall rules in WebConfig. A well formed file can be safely edited from command line.

===== EXAMPLE: Opening WebConfig on External Interfaces =====
Open /etc/firewall with the vim editor.

  vi /etc/firewall

Navigate to the RULES section and add the following line:

  Webconfig||0x10000001|6||81| \

On a server where Webconfig is the only incoming allowable service, your rules will look like this:

  RULES="\
  	Webconfig||0x10000001|6||81| \
  "

Save and exit the file. Restart the firewall rules:

  service firewall start

To validate that the port is open...

  [root@server ~]# service firewall status |grep hosts2-ns
   1494  226K ACCEPT     tcp  --  any    any     anywhere             192.168.0.254       tcp dpt:hosts2-ns 
   1791 1795K ACCEPT     tcp  --  any    eth0    192.168.0.254        anywhere            tcp spt:hosts2-ns 

{{keywords>clearos, clearos content, clearos5, howto, troubleshooting, categorynetwork, maintainer_dloper}}
