===== OpenVPN Connection Script =====
Someday OpenVPN connections may be added to the Managed VPN service but until then a common need is for a VPN tunnel to be restarted if the service fails. To do this we will employ a simple connection script. The connection script should be programmed to use the P-t-P address of the far end of the tunnel and reboot the service if a series of pings fails to reach the destination.

===== ClearOS 6.x Script =====
==== Script ====
For this example we will create the script in /root/support/scripts/ and give it the name of **openvpn.connection.tester.sh**.

  mkdir /root/support /root/support/scripts && vi /root/support/scripts/openvpn.connection.tester.sh

  #!/bin/bash
  
  site=$1
  if [ "$site" == "" ]; then
  	echo Usage: openvpn.connection.tester.sh [IP_address]
  	exit 0
  fi
  
  ping_test=`ping -c 4 $site | grep "100% packet loss"`
  if [ "$ping_test" ]; then
  	service openvpn restart
  else
  	echo "Connection good."
  fi

Now we will gather the information about the far side of the tunnel:

  [root@home ~]# ifconfig tun1
  tun1      Link encap:UNSPEC  HWaddr 00-00-00-00-00-00-00-00-00-00-00-00-00-00-00-00  
            inet addr:10.100.0.1  P-t-P:10.100.0.2  Mask:255.255.255.255
            UP POINTOPOINT RUNNING NOARP MULTICAST  MTU:1500  Metric:1
            RX packets:0 errors:0 dropped:0 overruns:0 frame:0
            TX packets:0 errors:0 dropped:0 overruns:0 carrier:0
            collisions:0 txqueuelen:100 
            RX bytes:0 (0.0 b)  TX bytes:0 (0.0 b)

In this scenario the address we would be pinging is 10.100.0.2. Test that now.

  ping 10.100.0.2

If that works then we can safely apply the script. To execute the script, change it so that root can run it:

  chmod u+x /root/support/scripts/openvpn.connection.tester.sh

Now run the program:
  
  /root/support/scripts/openvpn.connection.tester.sh 10.100.0.2

Not test it against something that will fail to ping:

  /root/support/scripts/openvpn.connection.tester.sh www.msnbc.com

==== Scheduling the test ====
With the test in place you can now schedule a test of the far side of the tunnel and restart the services locally (Note: you will need to use **vi** commands to edit crontab with this method).

  crontab -e

Add this line to the end of the crontab (Shift+g o):

<code>*/5	*	*	*	*	/root/support/scripts/openvpn.connection.tester.sh 10.100.0.2 &> /dev/null</code>

Save the file. (<Esc> :wq <Enter>)

===== ClearOS 7.x Script =====
==== Script ====
For this example we will create the script in /root/support/scripts/ and give it the name of **openvpn.connection.tester.sh**.

  mkdir /root/support /root/support/scripts

With your favourite text editor, create the file /root/support/scripts/openvpn.connection.tester.sh and in it put:
<code>#!/bin/bash

conffile=$(basename $1)
fullfile="/etc/openvpn/"$conffile
tunnel=$(basename $conffile .conf)

# Check if OpenVPN config file is specified
if [ " $conffile " == "  " ]; then
	echo Usage: openvpn.connection.tester.sh [OpenVPN_tunnel_conf_file]
	exit 0
fi

# Check if OpenVPN config file exists
if [ ! -f $fullfile ]; then
	echo "OpenVPN configuration file not found!"
	exit 0
fi

# Pick out the tunnel's remote IP 
remote_IP=$(grep -e "^ifconfig\s\+" $fullfile | awk '{print $3}')

# If it is not specified, config is invalid (e.g. roadwarrior config)
if [ " $remote_IP " == "  " ]; then
	echo "Configuration file is not a valid site-site configuration"
	exit 0
fi

### The next section is experimental as one routing failure has been seen in the field once ###
### You can delete or comment out the whole section if you want
### Change the target e-mail address to something suitable or remove the line

# Check routing table is not corrupt
route_test=`/usr/sbin/ip r | grep "via $remote_IP dev tun"`
if [ ! "$route_test" ]; then
	systemctl restart openvpn@$tunnel.service
	logger -t openvpn.connection.tester "$tunnel restarted due to routing failure"
	echo `/usr/sbin/ip r` | mail -s "Routing table failed to $remote_IP" yourname@your_domain.com
	sleep 60
fi

### End of experimental section

# Ping the remote tunnel
# If 100% packet loss, log message then restart tunnel. Otherwise continue
ping_test=`ping -c 4 $remote_IP | grep "100% packet loss"`
if [ "$ping_test" ]; then
	systemctl restart openvpn@$tunnel.service
	logger -t openvpn.connection.tester "$tunnel restarted"
	echo "ping failed"
else
	echo "Connection good."
fi
</code>

Make the script editable:

  chmod u+x /root/support/scripts/openvpn.connection.tester.sh

==== Scheduling the test ====
With the script in place you can now schedule a test of the far side of the tunnel and restart the tunnel locally. You need to know the name of the conf file which contains your site-site tunnel (with or without the full path) (Note: you will need to use **vi** commands to edit crontab with this method).

  crontab -e

To run the checker every 5 minutes, add this line to the end of the crontab (Shift+g o):

<code>*/5	*	*	*	*	/root/support/scripts/openvpn.connection.tester.sh OpenVPN_tunnel_conf_file &> /dev/null</code>

Save the file. (<Esc> :wq <Enter>)

<note tip>By default ClearOS will use the vi editor. If you wish to permanently use a different editor, in /root/.bash_profile add the following lines:
<code>EDITOR="nano"
export EDITOR</code>
Replace "nano" with the editor of your choice.</note>





{{keywords>clearos, clearos content, kb, howtos, maintainer_dloper, maintainer_nhowitt, maintainerreview_x, keywordfix}}