===== Overview =====
The ClearCenter Marketplace is a service that allows{{ :omedia:ss_marketplace_overview.png|Marketplace}} administrators to browse and search for apps compatible with the platform/version and install them.  Apps are applications that have been specifically developed and integrated into the ClearOS webconfig user-interface that extends or enhances the functionality and/or security of a system.

The Marketplace hosts a diverse set of apps for ClearOS, categorised into 4 groups:
  * System
  * Network
  * Gateway
  * Server
In each category, admins may find free and paid apps by various software vendors, developers and ClearFoundation community members.  Apps available in the Marketplace have gone through a stringent quality control process by ClearCenter to ensure the quality and security of each submission.
<note tip>If you've ever played with a smartphone, think of the ClearOS platform and ClearCenter's Marketplace being analogous to the [[http://www.android.com|Android smartphone]] and Google's [[https://market.android.com|Android Marketplace]] or Apple's iPhone coupled with the iStore.</note>
===== Installation =====
The Marketplace is part of the core system and installed by default.
<note>The system must be [[:content:en_us:6_system_registration|registered with ClearCenter]] to access the store.</note>

===== Configuring the Marketplace =====
Your Marketplace can be customised by clicking on the 'Settings' button{{ :omedia:ss_marketplace_settings.png|Settings}} found among the cluster of buttons/links used for paginating the Marketplace apps and beginning the install process. 
==== Number of Apps to Display ====
Determines how many apps will be displayed on the page.  More apps per page translate to longer page load times, however, all queries in Marketplace are cached for 24 hours locally on the system, making this setting more of a personal preference rather than performance setting.
==== Pseudonym for Ratings & Reviews ====
The text that will be displayed as a author reference for all ratings and reviews of apps you submit to the Marketplace.
==== Display Support Policy ====
This feature provides a color-coded bar on each configuration page which allows users to hover-over and instantly access what type of support they are eligible for based on the app.  Support varies based on scope, vendor, version (eg. Community vs. Professional) etc.

The color-coding system has a legend displayed when a user mouse-overs the policy section.  This mouse-over could become annoying/distracting when setting up a server, especially for experienced users.  Uncheck this setting to hide the support policy feature in the Right-Hand-Side app information widget.
==== Display Recommended Apps ====
On each webconfig page, Marketplace will automatically populate common apps that other users have installed along with the app in focus.  This can be very useful to new users who may not know about synergistic apps (eg. Antispam Updates as a service to the Antispam app).

While useful for some, this app might be annoying to more experienced users who consider it a form of 'advertising'.  Uncheck this setting to hide recommended apps in Webconfig.

===== Searching the Marketplace =====
Several tools exist to make searching the Marketplace quick and easy.<note tip>Command Line Guru?  User Interfaces just slow you down?  Have a look at the Advanced section to learn more about installing apps from the CLI.  Note - the CLI tools do not support payment/checkout of a paid app...you will need to use Marketplace to purchase paid apps or have previously purchased and assigned a paid app through the ClearCenter portal.</note>

==== Filters ====
Applying filters allows an admin to quickly drill down to desired apps based on common attributes.
=== Category ===
All apps are grouped together by one of four categories (Server, Network, Gateway and System).  These categories form the basis of the menu system.
=== Price ===
Filter apps based on cost - free or paid.
=== Install Status ===
Apply a filter to specifically display apps that are installed, installed by are not up-to-date or not installed.
=== Introduced to Marketplace ===
Useful if you haven't browsed the Marketplace in some time and wish to see what's new.
==== Keyword Search ====
Keyword searches scan the app name, description (in all locales available) and the tag field for a match.
===== Feature Wizard =====
===== Quick Select Files =====
Quick Select is a Marketplace feature to select and install multiple apps based on templates (QSF - Quick Select Files).  For more information on Quick Select, click [[:content:en_us:cc_doc_marketplace_quick_select|here]].
===== App Details =====
Clicking on the app logo or name in the Marketplace will display details about an app that will assist an admin in determining if it is required.
==== Description ====
A full description of the app including typical uses, features, limitations etc.
==== About the Vendor ====
Information about the vendor that developed the app and is responsible for its maintenance (bug fixes, updates etc.).
==== User Reviews ====
User reviews are a valuable tool in closing the feedback loop between developers and end-users.  It also assists administrators in making informed decisions on whether to install an app based on other users real-world experiences with the app.
=== Adding a Review ===
Whether you're a budding literary genius or not, everyone benefits when you provide a concise, honest and fair review of an app.  If you are considering posting a review, please attempt to maintain a professional atmosphere appropriate for a wide range of audiences.

To add a new review, follow the steps illustrated below.

1.  Find and select the app you wish to review from the Marketplace.  **Click on the app icon or heading**, not the //Configure// button, to display the apps details.

<note important>You can only add reviews for apps that you have installed.  Check your filter settings, since the default is to list only those apps that have not been installed.</note>

{{:omedia:ss-marketplace-review1.png|Select App}}

2.  Click on the Review tab to display past reviews.

{{:omedia:ss-marketplace-review2.png?600|Review Tab}}

3.  Click on the "Submit a Review" button to the right of the page.

{{:omedia:ss-marketplace-review3.png|Review Tab}}

4.  Enter your ClearCenter account information.
<note tip>It is 100% optional to post your review anonymously or not.  Authenticating to your ClearCenter account is *only* used in cases where inappropriate content is submitted and an account must be suspended.  See content section below.</note>

{{:omedia:ss-marketplac-review4.png|Review Tab}}

5.  Submit your review.  Click on the number of stars you wish to rate.  Star ratings are subjective, of course, but the following can be used as a guideline:

{{:omedia:ss-marketplace-review5.png?600|Submit}}

  * 0 stars - Non-usable and/or causes system instability
  * 1 star - Has major bugs or limitations that inhibit the use or functionality
  * 2 stars - Works, but has minor bugs and very basic feature set/functionality
  * 3 stars - Works as advertised...room for improvement
  * 4 stars - Works and is a great benefit to the system feature set
  * 5 stars - Stellar!  Who developed this app...Pete B.?

Comments are optional...if you just want to add a star rating, that's perfectly OK, although its always nice to get feedback...we can't fix what we don't know is a problem.

We encourage all types of feedback, so posting "This app sucks!", while not being very helpful (Why does it suck?) or encouraging ("This app currently sucks, but keep trying!") is perfectly acceptable.

=== Modding a Review ===
Don't have the time or interest in creating your own review or you've seen a review that captures what you think (or feel compelled to argue against)?  Modding a review up helps to push a review to the top of the list (where more people benefit from reading it) while modding a review down makes it less likely a new admin viewing the app reviews will see a poorly modded review.
<note tip>A word to anyone thinking about using reviews to sell or advertise their solution - please don't.  It just causes work/time for everyone and will ultimately get you banned from the Marketplace.</note>
==== Screenshots ====
A picture is worth a thousand words!  Screenshots can help you understand what an app does and what kind of features are supported.
==== Localisation Support ====
If English is not your first language, ClearOS has been translated into many different languages.  Depending on interest and when an app was added to the Marketplace, translation of an app may be complete, not started or somewhere in between.  The localisation support will give you an idea of how complete the translation is for the language setting you have selected for the core system.
==== Version History ====
Displays information on the each version release.
===== Installing or Upgrading Apps =====
Apps can be selected for installation in one of two ways - individually by clicking on //Download and Install// on the detailed information page or by selecting the checkbox (marking for install or upgrade) on the Marketplace overview pages.  If using the latter process, any number of apps can be installed at one time.  After marking the apps, click on //Install or Upgrade Selected App// from the main page.

To assist in helping you find new apps in the menu system, the menu system will mark an app with an icon ({{:omedia:icon-new-install.png|}}) to help distinguish it from the other menu items.  This flag (see screenshot below for example) will exist for 24 hours and then disappear.

The flag will re-appear if the app has recently (again, within 24 hours) been upgraded.  On an upgrade, you may see a difference on the apps webconfig page (eg. a new feature)...or you may not if it was a bug fix or some improvement to the app that did not include any changes to the UI (View in the MVC model).

{{:omedia:marketplace_new_upgrade.png|}}

==== Paid Apps - Configuring Your ClearCenter Account ====
Marketplace can be used for installing free apps with no additional information other than your ClearCenter account username - the account you used to register your ClearOS install to.

<note tip>Setting up an account to allow the purchase/installation of paid apps is a one-time process.  Any future systems registered to the same account will automatically be eligible to process paid apps from the Marketplace.</note>

If you plan to install paid apps, or if you have received a pop-up{{ :omedia:ss_marketplace_billing.png?300|Billing}} dialog from within webconfig like the example on the right indicating that your account is not yet configured to process paid apps from the Marketplace, you will need to configure the following steps from within the ClearCenter portal:

=== Step 1 - Ensure Marketplace is Enabled ===
By default, Marketplace is enabled.  If you wish to disable Marketplace, login to the [[https://secure.clearcenter.com|ClearCenter portal]] and navigate to "Systems --> Marketplace Settings" and click //Enable Marketplace//.
=== Step 2 - Provide contact information ===
Additional contact information is required for ClearCenter to create an official invoice for your purchase and comply with regulations related to tax collection.

ClearCenter protects any information you share with us. If you do not want to have customer information in our database, we will delete this information at any time.  We do not sell or give away your name, mail address, e-mail address, phone number, or any other personal information.  The full version of our privacy policy can be found [[http://www.clearcenter.com/Company/terms.html|here]].

=== Step 3 - Configure one or more payment options ===
ClearCenter Marketplace currently provides four payment options - Credit Card, PayPal, wire tranfers and Terms (eg. Purchase Order).
====Credit Card====
ClearCenter's PCI compliant credit card processing solution allows you to securely store your card information as a token on our system which can then be used to purchase or renew apps and services at any time.  ClearCenter accepts, VISA, Mastercard, American Express and Discover.
====PayPal====
With the PayPal payment method, you choose the amount to transfer to your ClearCenter account.  There are no processing fees...100% of your PayPal transfer goes towards your ClearCenter account balance (think Sony PS3 "Wallet").  Funds from your ClearCenter account can then be used to purchase or renew apps and services in the Marketplace.

To transfer funds from your PayPal account, go [[https://secure.clearcenter.com/portal/paypal_deposit.jsp|here]]
====Wire Transfer====
Similar to the PayPal option above, ClearCenter can accept wire transfers.  The minimum transfer accepted for Marketplace use is USD 350.  For account information, please contact our sales team at sales@clearcenter.com.

====Purchase Order====
ClearCenter offers net 30 terms to applicants who have demonstrated a prior purchasing history with ClearCenter dating back to at least 6 months.  If you would like to be considered for credit terms, please contact our sales team at sales@clearcenter.com.

===== Removing an App =====
Removing (or uninstalling) and app from the server is only possible from the CLI at this time.  To remove an app, login to a shell as root and type "rpm -e app-<basename>".  For example:

  rpm -e app-firewall-custom

The basename for an app can be found in a number of ways.  From the command line, you can list all ClearOS Marketplace apps:

  rpm -qa | grep "^app\-" | grep -v "\-core" | sed "s/-[0-9+].*//g"
  
Alternatively, search for your app on the [[http://www.clearcenter.com/marketplace/|ClearCenter Marketplace website]].  Once found, click on the title to bring up the app details.  In the right hand column under "App Details" find the package name.  Use this name with the "rpm -e" command above to un-install a package. 
  
For ClearOS Professional users, please submit a support ticket before removing an app.

===== Applying Paid Apps to a Re-Installed System =====
Paid apps are associated with an individual system registered to an account in the ClearCenter portal.  You can view your systems at any time from the [[https://secure.clearcenter.com/portal/device.jsp|System Overview Page]].

To view apps (both paid or free) that are associated with a given system, in the same portal click on the link "System Subscriptions".  There you will see all apps associated with your system.

{{:omedia:ss_mp_assigned.png?450|Assigned}}

In addition to any apps that are available (Available Licenses).

{{:omedia:ss_mp_available.png?450|Available}}

There may be instances where you are required to re-install your system.  Upon entering the Marketplace on your newly installed system, you may find the Marketplace forcing you to pay for an app you know you have purchased already.

<note tip>Don't worry!  Apps are transferable to newly registered systems.  Use the notes below to associate paid apps.  If you run into problems, please feel free to submit a [[https://secure.clearcenter.com/portal/ticket.jsp|Support Ticket]] to get your issue resolved.</note>

The quickest and easiest way to ensure all paid apps get automatically transferred to your new install is to register the system as a [[http://www.clearcenter.com/support/documentation/user_guide/system_registration#type|Re-Install]].  Following this procedure ensures all of the paid apps purchased in the past will be available to you at no cost.

<note tip>You will still need to install these apps from the Marketplace...but their price will be replaced with the note 'Credit Available'.</note>

In the event you have registered a system as a new install and wish to transfer an app over to the new registration, you will need to look under the 'Available Subscriptions' tab (see above screenshot).  If your previously purchased app is not listed, it is because it is assigned to another system.  Deleting that system from the portal (Systems --> Delete System) will free it up and allow you to re-assign the paid app.

Once you see your paid app listed in the "Available Licenses" section, click on the "Assign" button to associate it with your new system.  Once done, go back to Marketplace (in Webconfig), reset the Marketplace cache (via Settings) or logout and back into Webconfig (essentially resetting the cache) and search for the paid app.  It will now be listed and available to install as a paid app which costs zero because of a previous purchase.

{{:omedia:ss_mp_credit_avail.png?450|Credit Available}}

<note warning>Some users may have more than one ClearCenter account.  Make sure you are logged into the account used to purchase the original app.</note>

===== Disabling Marketplace =====
In some circumstances, you may want to lock down access to the Marketplace.  To disable Marketplace, login to the [[https://secure.clearcenter.com|ClearCenter portal]] and navigate to "Systems --> Marketplace Settings" and click //Disable Marketplace//.
===== Marketplace Access Control =====
{{ :omedia:ss-marketplace-accounts.png|Account Hierarchy}}
In most cases, a ClearOS server is registered to a ClearCenter account created by the administrator.  However, there are circumstances where a system is registered to a ClearCenter sub-account.  This usually happens with larger, distributed organizations, ClearCenter partners or IT Managed Service Providers (MSP) managing the subscriptions on behalf of another office or client.  In this case, a ClearCenter account owns or controls the subscription assets that are used to provide services and support to a system but a sub-account under the main account is used to register the system and used for organizational purposes.

<note warning>Marketplace Access Control was implemented as an update in June, 2012.  In order to use this feature, you must have Marketplace App version 1.2.1 or greater.</note>

A sub-account is functionally the same as regular account with one difference.  Assets purchased from ClearCenter (Professional subscriptions, paid services, Marketplace paid apps etc.) and transferred to a sub-account for use cannot be modified by the owner of the sub account.  In this way, a partner or MSP can purchase the required subscriptions for deployment and manage the relationship between vendor (ClearCenter) and MSP.

In cases where the administrator of the main account wishes to limit how the sub-account user can interact with the ClearCenter Marketplace, the ClearCenter portal provides an access control form to restrict permissions.  The screenshot below shows an example of the controls.

{{:omedia:ss-marketplace-acl.png?600|Marketplace ACL}}

====Marketplace Enabled====
If unchecked, disables Marketplace for all users on the server.
====Free Apps====
If checked, allows an administrator who has been granted Marketplace access in webconfig (ie. root account, sub-administrator account) to install free applications, regardless of whether they have the authentication credentials to either the sub-account or main account where the system is registered to.

If you *do not* want someone who has access to webconfig to install applications that are free, uncheck this box.

====User Account====
If checked, users with access to the Marketplace in webconfig (ie. root account, sub-administrator account) *and* having the ClearCenter authentication credentials to the account where the system is registered to, can install free apps.  In addition, if they have set-up their ClearCenter account with billing information (eg. pre-authorized credit card, PayPal deposit or payment terms with ClearCenter), they will be able to purchase and install paid apps.

If you are a partner managing a client's system or an IT administrator who wants to control what local administrators have access to in the Marketplace, you will want to disable User Account access (uncheck box).

====Owner Account====
The owner account is determined by the account in which paid subscriptions (eg. Professional, IDS Signatures and Updates, Zarafa Professional for ClearOS etc.) were purchased.

Only disable (uncheck box) the owner account if you do not wish your username to show as an option in webconfig when authentication to the ClearCenter Marketplace is required.
===== Advanced Users =====
The following instructions should only be used by advanced users familiar with ClearOS command line interface (CLI).

==== Package Listing ====
A complete listing of all packages in the yum repository can be found by using the following command:

  yum list

By piping the results of the search into a grep filter, you can easily find what you are looking for.  For example, if you wanted to find packages relating to the Postfix SMTP mail server, you could issue the following command:

  yum list | grep postfix

The response would include all packages containing the search string 'postfix'.

==== Package Installation ====
Some users will prefer to use yum via command line to install and manage packages.  The following example would install the [[:content:en_us:6_intrusion_detection|Intrusion Detection]] app:

  yum install app-intrusion-detection
  
