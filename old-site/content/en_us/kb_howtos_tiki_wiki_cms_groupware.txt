===== Tiki Wiki CMS Groupware =====
Tiki Wiki CMS Groupware is a full-featured, web-based, multilingual (40+ languages), tightly integrated, all-in-one Wiki+CMS+Groupware, Free Source Software (GNU/LGPL), using PHP, MySQL, Zend Framework, jQuery and Smarty. Tiki can be used to create all kinds of Web applications, sites, portals, knowledge base, intranets, and extranets. It is actively developed by a very large international community. 

Tiki & ClearOS are components part of the [[http://suite.tiki.org|Tiki Suite]].

Please join the [[http://www.clearfoundation.com/Community/Groups/141/Viewgroup.html|Tiki Wiki CMS Groupware & Tiki Suite group on the Clear Foundation's site]].

This page will soon have documentation on how to install Tiki on ClearOS. For now, you can check out the notes at: https://tiki.org/Tiki+on+Clearos


=== links ===
  * http://tiki.org
  * http://doc.tiki.org/Installation
  * http://suite.tiki.org/
  * http://suite.tiki.org/ClearOS
  * http://tikisuite.org/ 
  * https://github.com/clearos/app-tiki
{{keywords>clearos, clearos content, tikiwiki, skunkworks, category_server, clearos6, clearos7, howtos, maintainer_dloper}}
