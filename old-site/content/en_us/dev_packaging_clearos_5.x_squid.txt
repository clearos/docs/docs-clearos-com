===== Squid =====
The squid package provides the web proxy server for ClearOS.

===== Patches =====
==== squid-X-ldap.patch ====
This patch provides support for storing the web proxy password in a different LDAP attribute.

===== Changes =====
==== Latest Stable Release ====
To keep up with the latest web proxy features, ClearOS will often use a more recent stable version of Squid.

==== Startup Security Policy ====
The init script has been modified to automatically apply safe security settings based on the ClearOS firewall/network configuration.  Avoid calling an init script a configuration file "i.e. config(noreplace)".

==== Proxy Cache Manager ====
The proxy cache manager interface has been disabled.  

==== Build-time Features ====
  * The Ident has been disabled
  * The ARP ACL (MAC address filtering) has been enabled

{{keywords>clearos, clearos content, AppName, app_name, version5, dev, packaging, xcategory, maintainer_dloper, maintainerreview_x, keywordfix}}
