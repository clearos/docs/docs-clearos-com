===== Account Synchronization =====
The **Account Synchronization** app makes it easy to synchronize users, groups and passwords across multiple ClearOS Professional installations.  This app is ideal for managing a distributed environment, for example 10 ClearOS gateways across multiple remote offices.

Account information is managed from one master system and fully replicated to other distributed systems in your network.  With full replication, remote sites can continue to provide account services even when the master system is unavailable.

===== Installation =====
If your system does not have this app available, you can install it via the [[:content:en_us:6_marketplace|Marketplace]].

===== Getting Started =====
==== A Real World Scenario ====
The easiest way to describe Account Synchronization is to walk through a real world scenario. Let's say you are the network manager for the following organization:

  * 10 office locations: 10 in the Americas, 5 in Europe and 5 in Africa.
  * A total of 300 people are employed across the organization.
  * Each of these locations needs to have two ClearOS systems: one installed as the Internet gateway, the other installed as a local file server.
  * Each location needs a Windows Backup Domain Controller (BDC) for authentication

Managing users and groups on these 20 systems (2 systems x 10 locations) by logging into every single system is time consuming (read: expensive), tedious, error prone and - quite frankly - just not a fun job!

==== The Basics - Master and Slave ====
The first thing that this example organization needs is a **Master** node.  This node is where managing users, groups and passwords take place.  For this type of organization, we recommend dedicating a node to performing this specific role.  Yes, you can add other apps (e.g. Web Proxy), but for the master node, less is better.  

Since all other slave systems will be replicating information off of the master node, it is best to put this master node in a data center or at the very least a location with a reliable Internet connection.  Keep in mind, the slave nodes can operate without a connection to the master since the data is fully replicated!  The slaves will continue to use the existing users, groups and passwords indefinitely and wait patiently to reconnect to the master node.

===== Configuration =====
==== Initializing the Mode ====
When you visit the Account Synchronization app for the first time, you will be given  chance to configure the mode.  The **Master** and **Slave** roles are explained above.  The **Standalone** mode is for a simple standalone system - you can think of it as Master node without any slaves.

When you configure your master node, you will see two pieces of information:

  * The Device ID: a unique ID that sometimes comes in handy for troubleshooting
  * The Synchronization Key: the secret that allows slave systems to replicate off of the master node

When you configure slave node, use the **Synchronization Key** to connect the slave system to the master node.  Once the slave has been configured, you can visit the [[Users]] and [[Groups]] apps to verify the replication.  Depending on the number users and groups, it can take up to a minute for the first replication to occur, so please be patient!

<note warning>TCP ports 81 (ClearOS), 636 (secure LDAP), 8154 and 8155 (filesync) need to be accessible on the master node.</note>

==== Extensions and Plugins in the Marketplace ====
Extensions and Plugins are an important concepts in Account Synchronization.  These two types of apps let you add attributes to a user's profile.  For example, the **Contact Extension** adds:

  * E-mail address
  * Phone number
  * City
  * Region
  * Country
  * etc.

Similarly, plugins also extend user's profile but with just a simple "enable/disable" attribute.  In Master/Slave mode, plugins and extensions can be considered to be one and the same, so there's no need to worry about it much further. In [[:content:en_us:6_active_directory_connector|other modes]], the distinction is important.

The number of plugins and extensions are always changing, but here is a sample:

^Extension^Description^
|Contact Extension|Provides basic contact information|
|Samba Extension|Provides necessary attributes for Windows Networking|
|Shell Extension|Provides shell (SSH) login options|
|Zarafa Extension|Provides Zarafa attributes, notably quota settings|

^Plugins^Description^
|FTP Server Plugin|Provides FTP Server access control|
|PPTP Server Plugin|Provides PPTP Server access control|
|Print Server Administrator Plugin|Provides Print Server administrator access (for managing printers)|
|SMTP Plugin|Provides outbound SMTP Authentication access control|
|Web Proxy Plugin|Provides Web Proxy Server access control|

You will only see these apps in [[:content:en_us:6_marketplace|Marketplace]] when your system is in master mode.  Unlike all other apps, plugins and extensions do not have a separate configuration page inside the web-based administration tool.  

==== Windows Networking ====
For the most part, any app on your ClearOS system that depends on users, groups and passwords will automatically handle master/slave mode.  For example, the [[:content:en_us:6_pptp_server|PPTP Server]] on a slave node will show a read-only list of users who have access to the slave's PPTP server.

Windows Networking is a bit of a special case.  In an Account Synchronization network, the **Master** node is set as the Primary Domain Controller (PDC) while all slave nodes are set as the Backup Domain Controller (BDC).

===== Links =====

{{keywords>clearos, clearos content, Account Synchronization, app-account-sync, clearos6, userguide, categorysystem, subcategoryaccountmanager, maintainer_dloper}}