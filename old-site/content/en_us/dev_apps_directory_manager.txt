===== Dev Apps Directory Manager =====
The Directory app provides the LDAP implementation in ClearOS.  If you are looking for detailed information on the LDAP design, please take a look [[:content:en_us:dev_architecture_directory_start|architecture documentation]].

===== Bugs and Features =====
Follow the link to view bugs and features in the bug tracker.

  * [[http://tracker.clearfoundation.com/search.php?project_id=1&category=Directory&sticky_issues=1&sortby=severity&dir=DESC&hide_status_id=90|Directory]]
{{keywords>clearos, clearos content, maintainer_dloper, maintainerreview_x, keywordfix}}
