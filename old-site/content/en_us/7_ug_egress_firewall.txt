===== Egress Firewall =====
The **Firewall Outgoing** feature lets you block or allow certain kinds of  traffic from leaving your network.

<note>The Egress Firewall applies to traffic from the LAN passing through the firewall. It does **not** apply to traffic originating from ClearOS</note>
===== Installation =====
If you did not select this module to be included during the installation process, you must first [[:content:en_us:7_ug_Marketplace|install the module]].

===== Menu =====
You can find this feature in the menu system at the following location:
 
<navigation>Network|Firewall|Outgoing</navigation>

===== Configuration =====
The **Firewall Outgoing** feature is useful for blocking/allowing instant messaging, chat, certain type of downloads, and more.  You may also want to block traffic using the [[:content:en_us:6_protocol_filter|Protocol Filter]], so make sure you review that feature as well.

You have two ways to block/allow traffic:
  * by destination port/service
  * by destination IP address/domain

==== Choose an Outgoing Mode ====
There are two modes to the **Firewall Outgoing** system:

  * Allow all outbound traffic and block specific types of traffic (default)
  * Block all outbound traffic and allow specific types of traffic

==== Outgoing Traffic - By Port/Service ====
**Destination Ports** prevents/allows a connection on a particular port/service.  For instance, adding port 80 (web) disables/enables web-surfing for your entire local network.

==== Outgoing Traffic - By Host/Destination ====
**Destination Domains** allows you to block/allow certain networks and sites. For instance, if your outgoing mode is set to allow all outgoing traffic, you could block the entire island nation of [[http://en.wikipedia.org/wiki/Tuvalu|Tuvalu]] (why would you do that?) by adding the 202.2.96.0/19 network to the block list. The "Destination Domain" can be a Domain, IP address or Subnet (in CIDR or IP/netmask form).

<note>It is not advisable to block by domain name as the firewall will convert the domain name to an IP address before applying the rule. If the IP address changes because of things like load balancing, the domain will no longer be blocked. If a domain uses multiple IP's (e.g. google.com) you need to block the whole range of IP's.</note>

<note warning>If you block destinations with the firewall bear in mind that users of the proxy may not be blocked.  If you require proxy users to be blocked, your best option is to block the destinations using the [[:content:en_us:6_content_filter|Content Filter]].</note>

===== Other Blocking Methods =====
Blocking popular content such as Facebook or YouTube or appl like BitTorrent with the Egress Firewall is probably not the easiest or most effective way of blocking and there may be other apps or techniques more suitable.

[[kb_o_content_filtering_ins_and_outs#dns|DNS Poisoning]]\\
[[7_ug_content_filter|Content Filter]]\\
[[7_ug_gateway_management_business|Gateway Management Business]]\\
[[7_ug_gateway_management_community|Gateway Management Community]]\\
[[7_ug_application_filter|Application Filter]]\\
[[7_ug_protocol_filter|Protocol Filter]]\\
[[kb_o_content_filtering_ins_and_outs|Content Filter Ins and Outs]]

{{keywords>clearos, clearos content, Egress Firewall, app-egress-firewall, clearos7, userguide, categorynetwork, subcategoryfirewall, maintainer_dloper}}
