{{ :userguides:mssql.svg?80}}
<WRAP clear></WRAP>

===== Microsoft SQL Server =====
MS SQL Server is a version of Microsoft's relational database management system (RDBMS) that was ported and made available to Linux operating systems in 2016.

To learn more about this app, click [[https://www.clearos.com/products/purchase/clearos-marketplace-apps/server/mssql|here]]
===== Documentation =====
==== Version 6 ====
This app is no available for ClearOS 6
==== Version 7 ====
Documentation for ClearOS 7 can be found [[content:en_us:7_ug_mssql|here]].
==== Additional Notes ====
