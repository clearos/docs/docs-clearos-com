===== 1-to-1 NAT =====
1-to-1 NAT maps a public Internet IP to an IP on your local area network (LAN).

===== Installation =====
If your system does not have this app available, you can install it via the [[:content:en_us:6_Marketplace|Marketplace]].

===== Menu =====
You can find this feature in the menu system at the following location:

<navigation>Network|Firewall|1-to-1 NAT</navigation>

===== Configuration =====
You can map 1-to-1 NAT IPs in one of two ways:

  * With no firewall at all
  * With selective ports open

==== 1-to-1 NAT - No Firewall ====
Some protocols can be finicky behind firewalls.  In this case you want to configure 1-to-1 NAT with no firewall (make sure you secure the target LAN system some other way!). 

==== 1-to-1 NAT - Selective Ports Open ====
If you only want to map selective ports, for example the TCP 80 web server port, you can configure particular ports in your 1-to-1 NAT mapping.

==== 1-to-1 NAT - With MultiWAN ====
If you have Multi-WAN enabled, please review the topic on [[:content:en_us:6_multi-wan|source-based routes]].  Each 1-to-1 NAT rule must typically be assigned to an external MultiWAN interface as shown by example below:
{{keywords>clearos, clearos content, 1-to-1 NAT Firewall, app-nat-firewall, clearos6, userguide, categorynetwork, subcategoryfirewall, maintainer_dloper}}
