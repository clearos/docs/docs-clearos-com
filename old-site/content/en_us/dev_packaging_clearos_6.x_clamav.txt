===== Clamav =====
ClamAV is an open source (GPL) antivirus engine designed for detecting Trojans, viruses, malware and other malicious threats. It is the de facto standard for mail gateway scanning. It provides a high performance mutli-threaded scanning daemon, command line utilities for on demand file scanning, and an intelligent tool for automatic signature updates. The core ClamAV library provides numerous file format detection mechanisms, file unpacking support, archive support, and multiple signature languages for detecting threats. 

===== Links =====
{{keywords>clearos, clearos content, AppName, app_name, version6, dev, packaging, xcategory, maintainer_dloper, maintainerreview_x, keywordfix}}
