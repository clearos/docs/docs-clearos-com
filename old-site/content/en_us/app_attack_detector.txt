{{ :userguides:attack_detector.svg?80}}
<WRAP clear></WRAP>

===== Attack Detector =====
Attack Detector scans your system for authentication failures across various types of services installed on your system. If the failure threshold is reached, the app will block the attacking system.

To learn more about this app, click [[https://www.clearos.com/products/purchase/clearos-marketplace-apps/gateway|here]]
===== Documentation =====
==== Version 6 ====
This app is not available for ClearOS 6.
==== Version 7 ====
Documentation for ClearOS 7 can be found [[content:en_us:7_ug_attack_detector|here]].
==== Additional Notes ====
