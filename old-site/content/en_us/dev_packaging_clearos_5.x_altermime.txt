===== Altermime =====
A tool to alter MIME-encoded mail on the server.  Specifically, this tool is used to add a mail disclaimer to outbound mail.

===== Links =====
{{keywords>clearos, clearos content, Altermime, version5, dev, packaging maintainer_x, maintainerreview_x, keywordfix}}
