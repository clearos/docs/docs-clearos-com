===== Delete clouds =====
If you want to delete a cloud of yours, enter the main ClearGLASS dashboard and click on the cloud you want to delete (eg EC2 Ireland in this example)

{{content:en_us:cg_delete-cloud-01.png}}

Now scroll to the bottom of the page and click 'DELETE CLOUD'. This will remove the cloud credentials from ClearGLASS and also disable monitoring to VMs of this cloud that had ClearGLASS monitoring. This **WILL NOT** delete any VMs/servers on this cloud. 

{{content:en_us:cg_delete-cloud-02.png}}