===== ClearOS 5.x Cyrus IMAP =====
Cyrus IMAP is an enterprise class mail system that provides IMAP and POP services.

===== Changes =====
The Kolab groupware not only required a more recent Cyrus IMAP, but also a number of patches.

  * cyradm-annotate.patch
  * imapd-annotate.patch
  * imapd-foldernames.patch
  * imapd-groups.patch
  * imapd-logging.patch
  * imapd-uid.patch
{{keywords>clearos, clearos content, AppName, app_name, version5, dev, packaging, xcategory, maintainer_dloper, maintainerreview_x, keywordfix}}
