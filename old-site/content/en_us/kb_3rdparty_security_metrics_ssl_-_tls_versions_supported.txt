===== SSL-TLS Versions Supported =====
This entry from Security Metrics indicates that they can determine which SSL/TLS versions are used in your web services. In and of itself this is not a risk but you may consider using tighter versions. However, using tighter versions will not change the results of this entry.

===== ClearCenter response =====
==== Short response ====
The negotiation of SSL calls for offering supported protocols by design. This is not indicative of a specific risk or vulnerability.

==== Long response ====
When computers begin SSL communication, they will negotiate which version of SSL they will use to communicate. This is called for in the protocol and in itself does not pose a risk to the system or indicate a vulnerability.

==== Resolution ====
No action required.

If you want to change the behavior of your web server to ONLY allow strong encryption, perform the following steps:

First, take a look at what a weak connection looks like by running the following:

  openssl s_client -connect localhost:443 -cipher MEDIUM

This should give you a long and valid response. Second, connect to your server via command line and modify the following file using your favorite editor (i.e. nano, vi, emacs, et al). Modify the /etc/httpd/conf.d/ssl.conf file.

  vi /etc/httpd/conf.d/ssl.conf

Change these lines:

  SSLProtocol all -SSLv2
  SSLCipherSuite ALL:!ADH:!EXPORT:!SSLv2:RC4+RSA:+HIGH:+MEDIUM:+LOW

to this:

  SSLProtocol -ALL +SSLv3 +TLSv1
  SSLCipherSuite ALL:!aNULL:!ADH:!eNULL:!LOW:!EXP:RC4+RSA:!MEDIUM:+HIGH

Third, save the file and reload the httpd service.

  service httpd reload

Fourth, connect again to the httpd service using the same command as before:

  openssl s_client -connect localhost:443 -cipher MEDIUM

You should get rejected this time. Now try with only high levels of encryption:

  openssl s_client -connect localhost:443 -cipher HIGH

You should get a connection and a lot of output just like at the first, but this time it is more secure.

===== Links =====
{{keywords>clearos, clearos content, 3rd party, security metrics, non-cve, maintainer_dloper}}
