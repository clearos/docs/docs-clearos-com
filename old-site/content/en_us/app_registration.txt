{{ :userguides:registration.svg?80}}
<WRAP clear></WRAP>

===== System Registration =====
System registration provides access to the Marketplace containing hundreds of apps to customize your server and receive the latest updates.  Creating an account and registering your system is quick and easy.

To learn more about this app, click [[https://www.clearos.com/products/purchase/clearos-marketplace-apps/system/registration|here]]
===== Documentation =====
==== Version 6 ====
Documentation for ClearOS 6 can be found [[content:en_us:6_registration|here]].
==== Version 7 ====
Documentation for ClearOS 7 can be found [[content:en_us:7_ug_registration|here]].
==== Additional Notes ====
