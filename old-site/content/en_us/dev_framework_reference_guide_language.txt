===== Language =====
ClearOS includes support for multiple languages.  The framework uses the standard [[http://codeigniter.com/user_guide/libraries/language.html|CodeIgniter]] system which is described here.

A team of ClearFoundation translation volunteers are ready to help with your application!  We have also made it easier for you to import and maintain translations with the [[:content:en_us:dev_translations_start|online translation tool]].

===== File System Layout =====
Inside every [[:content:en_us:dev_framework_reference_guide_file_system_layout|ClearOS App directory]], you will the **language** directory.  Inside this directory is a subdirectory for each supported language.  The language files are stored in these subdirectories.

===== Creating Language Files =====
All language files must end with **_lang.php**.  For example, the ClearOS User Manager App (app-user) includes the translation file **user_lang.php**.  Inside this file are $lang array entries similar to what is shown below.

<code php>
$lang['user_first_name'] = 'first name';
$lang['user_last_name'] = 'last name';
$lang['user_email_address' = 'e-mail address';
...
</code>

<note info>It is good practice to prefix all language tags with the App name.  This prevents collisions with other translation files.</note>

As you are developing your ClearOS App, edit the translation file in your language as you go.  When you are ready to publish your app, you can import your translation file into the online translator.  You can find out more about this process [[:content:en_us:dev_translations_start|here]].

===== Loading Language Files =====
In order to keep ClearOS lightweight, translation files are not automatically loaded.  Instead, translations must be explicitly loaded in your library, view or controller.  In a library, loading a translation is done through clearos_load_language():

<code php>
clearos_load_language('base/base');
clearos_load_language('user/user');
</code>

In a view or controller, loading translations is done with the standard CodeIgniter call - $this->lang->load():

<code php>
$this->lang->load('base/base');
$this->lang->load('user/user');
</code>

===== Using Language Files =====
Use the **lang()** function anywhere in your code to access the translations. That's all there is to it.  

<code php>
echo lang('user_last_name') . ': ' . $lastname;
echo lang('base_help');
</code>

===== Links =====
{{keywords>clearos, clearos content, dev, framework, helpers, maintainer_dloper}}
