===== Restrict Youtube Content =====
/**
 * Ticket #545290
 */

This guide will show you how to implement Restricted YouTube control on the DNS Cache for ClearOS in accordance with this [[https://support.google.com/a/answer/6214622|Google Guide]].

===== Overview =====
This method is not exhaustive and has some limitations in operability due to the nature of the underlying DNSMasq service. You will be required to input current information for some domains into your hosts file and then create CNAME records in your DNSMasq cache. The reason for the host file entries is due to a limitation in DNSMasq. [[http://www.thekelleys.org.uk/dnsmasq/docs/dnsmasq-man.html|See the manual page for DNSMasq for more information]].

===== Host Entries =====
According to the Google Doc, you need to create CNAME records for the following host names:

  www.youtube.com
  m.youtube.com
  youtubei.googleapis.com
  youtube.googleapis.com
  www.youtube-nocookie.com

Due to a limitation in DNSMasq, you will need to create these as host files. You can do that in your DNS and Hosts app in Webconfig or you can modify the /etc/hosts file.

First, find the IP address associated with each hostname. For example:

  ping -c 1 www.youtube.com
  ping -c 1 m.youtube.com
  ping -c 1 youtubei.googleapis.com
  ping -c 1 youtube.googleapis.com
  ping -c 1 www.youtube-nocookie.com

That produces these results (at the time of this writing):

  youtube-ui.l.google.com (208.187.128.25)
  m.youtube.com (208.187.128.23)
  googleapis.l.google.com (216.58.193.74)
  youtube.googleapis.com (173.194.203.239)
  youtube-ui.l.google.com (208.187.128.25)

From this list I would make the following entries in the DNS and Hosts app or the /etc/hosts file:

  208.187.128.25           youtube-ui.l.google.com
  208.187.128.23           m.youtube.com
  216.58.193.74            googleapis.l.google.com
  173.194.203.239          youtube.googleapis.com
  208.187.128.25           youtube-ui.l.google.com

<note warning>IP to hostname assignments can change regularly. This information can easily be invalidated by Google at any time and these addresses reflect only the addresses returned for just one of many on a query at the time of the writing of this article. You will need to keep these files up to date.</note>

===== DNSMasq =====
Now that the host entries are made, DNSMasq is able to assign CNAME records. Modify your /etc/dnsmasq.conf file and add the entries for the CNAME records at the bottom of your config file:

  cname=youtube-ui.l.google.com,restrict.youtube.com
  cname=m.youtube.com,restrict.youtube.com
  cname=googleapis.l.google.com,restrict.youtube.com
  cname=youtube.googleapis.com,restrict.youtube.com
  cname=youtube-ui.l.google.com,restrict.youtube.com

Restart the dnsmasq service:

  service dnsmasq restart

Now, test your entries with 'dig':

  dig @localhost youtube-ui.l.google.com

<code>; <<>> DiG 9.8.2rc1-RedHat-9.8.2-0.37.rc1.el6_7.5 <<>> @localhost youtube-ui.l.google.com
; (1 server found)
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 31186
;; flags: qr aa rd ra; QUERY: 1, ANSWER: 2, AUTHORITY: 0, ADDITIONAL: 0

;; QUESTION SECTION:
;youtube-ui.l.google.com.		IN	A

;; ANSWER SECTION:
youtube-ui.l.google.com.	0	IN	CNAME	restrict.youtube.com.
restrict.youtube.com.	3600	IN	A	216.239.38.120

;; Query time: 1 msec
;; SERVER: 127.0.0.1#53(127.0.0.1)
;; WHEN: Mon Mar 14 13:23:49 2016
;; MSG SIZE  rcvd: 74</code>




{{keywords>clearos, clearos content, clearos6, clearos7, app-dns, kb, howto, maintainer_dloper}}
