===== Central Management App-Marketplace =====
Now that you have the ClearOS framework installed, you can install Marketplace.  You can install other apps (for example app-date), but installing Marketplace will let you install the rest of your apps via the web-based administration tool.  

===== Install =====
When you install Marketplace, you will see other required apps installed as well:

  yum install app-marketplace

The output should look something like the following:

  Installing:
  app-marketplace                    noarch              1:1.0.2-1.v6
  Installing for dependencies:
  app-clearcenter                    noarch              1:1.0.2-1.v6
  app-clearcenter-core               noarch              1:1.0.2-1.v6
  app-language-core                  noarch              1:1.0.2-1.v6
  app-marketplace-core               noarch              1:1.0.2-1.v6
  app-network                        noarch              1:1.0.2-1.v6
  app-network-core                   noarch              1:1.0.2-1.v6
  app-registration                   noarch              1:1.0.2-1.v6
  app-registration-core              noarch              1:1.0.2-1.v6
  app-suva-core                      noarch              1:1.0.2-1.v6
  suva-client                        x86_64              3.1-6.v6    

Next, it is a good idea to restart webconfig to make sure the underlying PHP modules are active.  On a normal ClearOS install, this is already done of course, but we're doing things manually here:

  service webconfig restart
  
===== Accessing the Web-based Administration Tool =====
At this point, you should be able to point your web browser to https://your.ip.address:81 and see the web-based installation wizard.

That's it... we're done.  Going through this process, we hope you can see that  ClearOS is similar to CentOS and RHEL, but with nicely integrated web-based interface.
{{keywords>clearos, clearos content, dev, architecture, maintainer_dloper}}
