{{ :userguides:radius.svg?80}}
<WRAP clear></WRAP>

===== RADIUS Server =====
The RADIUS Server app provides an implementation of the RADIUS protocol, using FreeRADIUS.  RADIUS (Remote Authentication Dial In User Service) features centralized management, authentication, authorization and accounting management for computers and network devices (smart phones, tablets etc.) to connect and use network resources.

To learn more about this app, click [[https://www.clearos.com/products/purchase/clearos-marketplace-apps/network/radius|here]]
===== Documentation =====
==== Version 6 ====
Documentation for ClearOS 6 can be found [[content:en_us:6_radius|here]].
==== Version 7 ====
Documentation for ClearOS 7 can be found [[content:en_us:7_ug_radius|here]].
==== Additional Notes ====
