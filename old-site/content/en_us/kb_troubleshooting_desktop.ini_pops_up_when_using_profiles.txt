===== Desktop.ini Pops Up when Using Roaming Profiles =====
When logging in using roaming profiles, a Windows Vista or Windows 7 machine pops up a notepad with the document notepad.ini. This is caused because a hidden file on the original profile computer was uploaded/sync'd to the profile and was not able to preserve the hidden attribute.

===== Solution =====
To solve this problem, add the following to the /etc/samba/smb.conf configuration in the Other section:

  hide files = /desktop.ini/RECYCLER/

This will hide both the desktop.ini file and any recycler folders.

===== Help =====
==== Links ====
  * [[http://dev.mysql.com/doc/refman/5.0/en/resetting-permissions.html|Details from MySQL Website]]
==== Navigation ====

[[:|ClearOS Documentation]] ... [[..:|Knowledgebase]] ... [[:Knowledgebase:Troubleshooting:|Troubleshooting]]
{{keywords>clearos, clearos content, AppName, app_name, versionx, xcategory, maintainer_x, maintainerreview_x, titlefix, keywordfix}}
