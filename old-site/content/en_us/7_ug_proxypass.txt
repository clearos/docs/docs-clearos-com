===== Proxy Pass =====

===== Menu =====
You can find this feature in the menu system at the following location:
 
<navigation>Server|Web Server|Proxy Pass</navigation>


===== Introduction =====
vss allows the configuration of a reverse proxy. This application can be used for servicing HTTP(S) requests to a background server or host multiple virtual hosts on separate SSL certificates using SNI.

===== Getting Started =====
{{7_ug_proxypass.png}}

==== Virtual host ====
This should be the FQDN that you wish to forward to another web server e.g. subdomain.example.com. This subdomain should resolve to your WAN IP on the web.

If you are accessing the subdomain on your LAN, it should resolve to your primary webserver LAN IP (probably your ClearOS gateway LAN IP)

<note>I have tried testing with with a completely different domain (my LAN machine's poweredbyclear.com subdomain) which resolves to my WAN IP, but I could not get it to work. If anyone gets this to work, please post with the details)</note>

==== Alias ====
This can be another subdomain which you wish to be redirected to the same LAN web server e.g.subdomain2.example.com. The same DNS rules apply as above.

==== Enabled ====
Enabled or Disabled

==== Protocol ====
Do you want to redirect http, https or both?

==== Redirect to HTTPS ====
Do you want to force http to https?

==== Target server ====
The LAN IP or FQDN of the server which is going to serve this subdomain e.g.172.17.2.5. if you use an FQDN it must resolves to your LAN IP of your target server). If you don't specify it, the app will automatically prepend <nowiki>http:// or https://</nowiki>

==== Target path ====
The path to the web site on your target web server e.g. /something if the site 172.17.2.5/something  is going to serve the page.

==== Certificate ====
You can choose a certificate from your primary web server that you want to use for this subdomain.
===== Help =====

===== Links =====

{{keywords>clearos, clearos content, Proxy Pass, app-proxypass, userguide, categoryserver, subcategorywebserver, maintainer_bchambers}}
