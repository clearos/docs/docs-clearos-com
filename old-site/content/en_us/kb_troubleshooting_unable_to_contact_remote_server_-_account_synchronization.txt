===== Unable to Contact Remote Server in Account Synchronization =====
When you attempt to join a slave to a master OpenLDAP directory server you get the following error:

<code>
Accounts Synchronization Status
Unable to contact remote server.
</code>

===== Possible solutions =====
Here are some possible reasons and some solutions to this issue.

=== Master server cannot contact the SDN ===
If the Master server cannot communicate with the ClearCenter Service Delivery Network (ClearSDN) then it will not be able to perform a critical validation check before allowing the slave to join.

Perform the following from the master:

  ping ns1.clearsdn.com

=== The Slave server or Master is firewalling the other node or has bad routing ===
If there are firewall rules between the servers, then they will be unable to communicate over the network to each other.

Make sure that the servers can ping each other with their hostnames. To find out the hostname type the following from the first server:

  [root@slave ~]# hostname
  slave.system.lan

Then apply that hostname in a ping from the second server:

<code>
[root@master ~]# ping -c1 slave.system.lan
PING bu (192.168.1.1) 56(84) bytes of data.
64 bytes from bu (192.168.1.1): icmp_seq=1 ttl=64 time=0.018 ms

--- slave ping statistics ---
1 packets transmitted, 1 received, 0% packet loss, time 0ms
rtt min/avg/max/mdev = 0.018/0.018/0.018/0.000 ms
</code>

Additionally, You should see the following from the slave (type 'Control + ]' to produce the '^]' needed to exit telnet):

<code>
[root@slave ~]# telnet master.system.lan 636
Trying 192.168.1.1...
Connected to master.system.lan.
Escape character is '^]'.
^]
telnet> quit
Connection closed.
</code>

Additional ports needed may include:

  * 81
  * 636
  * 8154

===== Help =====
==== Navigation ====

[[:|ClearOS Documentation]] ... [[..:|Knowledgebase]] ... [[:Knowledgebase:Troubleshooting:|Troubleshooting]]
{{keywords>clearos, troubleshooting, help, support, account sync, account syncronization, unable to contact remote server, openldap}}
