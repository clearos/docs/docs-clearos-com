[[:knowledgebase:developer|Developer]] > [[:content:en_us:dev_packaging_start|Packaging]]

===== Getting Started with Packaging Apps on ClearOS =====
The **Packaging** section of the developer site provides information on building and maintaining RPMs for ClearOS. 

===== Building Packages =====
The following three documents take you through the process of building RPMs for ClearOS.  The first document is a general primer for manually building RPMs, so those familiar with this process can skip right over it.  The next two documents go into details about the specifics of the ClearFoundation build system.

  * [[:content:en_us:dev_packaging_building_an_rpm_-_the_basics|Building an RPM - The Basics]]
  * [[:content:en_us:dev_packaging_building_an_rpm_-_build_system|Building an RPM - ClearOS Development Environment]]
  * [[:content:en_us:dev_packaging_contributing_and_patching_rpms|Building an RPM - Koji Build System]] 

The following document is also useful for those who are involved with the GitHub migration:

  * [[:content:en_us:dev_packaging_upstream_packages|Upstream Packages on GitHub]]


===== Package Information =====
{{ :omedia:rpm_logo.png|Building RPMs for ClearOS}} ClearOS starts from open source code from Red Hat Enterprise Linux.  We then use other open source software to fill in some of the gaps (for example, intrusion detection).  We are not trying to re-invent the wheel around here!  One of the goals of ClearOS is to keep the number of upstream RPM changes to a minimum.  You can find the list of packages and some of the gory technical details here:

  * [[:content:en_us:dev_packaging_clearos_5.x_start|ClearOS 5.x]]
  * [[:content:en_us:dev_packaging_clearos_6.x_start|ClearOS 6]]
  * [[:content:en_us:dev_packaging_clearos_7.x_start|ClearOS 7]]

===== Packaging Guidelines =====
For the most part, ClearOS follows the [[http://fedoraproject.org/wiki/Packaging/Guidelines|Fedora Packaging Guidelines]].  We do make some exceptions as described in [[:content:en_us:dev_packaging_clearos_packaging_guidelines|ClearOS Packaging Guidelines]].

   * [[:content:en_us:dev_packaging_workflow|Workflow]]

===== Infrastructure =====
We will have more information on the following coming soon:

  * [[:content:en_us:dev_packaging_building_an_rpm_-_build_system|Build System]]
  * [[:content:en_us:dev_packaging_clearos_7_clearos-release|Release System]]
