===== Flexshare =====
A Flexshare is a flexible and secure collaboration utility which integrates  four of the most common methods of accessing files or content:

    * Web (HTTP/HTTPS)
    * FTP (FTP/FTPS)
    * File Shares (Samba)
    * E-mail (SMTP/MIME/SMIME)

It is an extremely powerful and versatile tool that has many uses.  The example below (a hypothetical engineering consulting firm //Eng-123// and its client //OEM-XYZ//) describes a Flexshare and a typical working environment.
A Flexshare might be defined on a server owned by //Eng-123// after successfully bidding on an engineering project for //OEM-XYZ//.  CAD files (engineering drawings) associated with the project's design are centrally located on the server and should be accessed only by the users included in //Eng-12//'s engineering group.  The file-sharing (Samba) Flexshare definition is used to allow restricted access to this directory from the Local Area Network (LAN) or over Virtual Private Network (VPN) tunnels in the event engineers work remotely.

By adding Flexshare's FTPS (secure FTP) access and configured to require a username/password for read-only permission, the project manager of //OEM-XYZ// can have access to the drawings at any time from anywhere on the Internet.  The increase in productivity by allowing real-time access to the CAD drawings keeps the project on track and negates having to e-mail CAD files which are often large and not ideal for e-mail transfers.
In the event //Eng-123// and //OEM-XYZ// want to track schedule 'snapshots' of an OpenOffice Calc document or notes on the design phase in PDF format, //Eng-123//'s administrator configures Flexshare's email upload access.  Both companies can now send signed/encrypted emails to a single email address where the attachment (a .ods or .pdf file extension in this case) is automatically stripped from the email and stored on the server.  These same files can then be accessed by web, FTP or file share and provides the added benefit of having a historical view of the entire project.
Nearing the completion of the project, //OEM-XYZ//'s sales/marketing team make a request to have an assortment of images created from the CAD software's rendering engine from 3D wire-frame.  Flexshare's web access, set-up with unrestricted access, gives the sales team the images they need to begin pre-selling - with just a browser and a URL provided.

The above illustrates just one possible use of Flexshares.  Much simpler Flexshare's can be created for every-day tasks common to any small business such as hosting and updating a website, creating user-restricted file shares or using e-mail as a simple file transfer utility.

===== Installation =====
If you did not select this module to be included during the installation process, you must first [[:content:en_us:5_software_modules|install the module]].

You will also need to install one or more of the following modules to enable functionality for the following modules:

    * [[:content:en_us:5_web_server|Web Server]]
    * [[:content:en_us:5_ftp|FTP Server]]
    * [[:content:en_us:5_windows_settings|Windows File Server]]
    * [[:content:en_us:5_pop_and_imap_server|POP and IMAP Server]]

===== Configuration =====
==== Share Overview ====
When you click on the Flexshare configuration page, you will be presented with the Flexshare Overview.

The first table lists the shares you have currently defined, allowing you to quickly view which access methods are enabled in addition to overall flexshare status (either enabled or disabled).  You can //Edit//, //Delete// and //Toggle// the status of each Flexshare using the **Action** links in the right hand column.  Of course, if no Flexshares are defined, the **Action** links will not be visible.

The second table allows you to define (create) a new Flexshare.  

==== Creating a New Flexshare ====
To define a new Flexshare, fill out the //Name//, //Description// and //Owner// fields in the **Add a new Flexshare** form.  A Flexshare template will be created (with no access and disabled by default).  The //Editing a Flexshare// form will be displayed, allowing you to customize the share options and enable access options.

==== Editing a Flexshare ====
You can make edits/changes to any defined Flexshare at any time.  A newly created Flexshare will have no access points enabled, so you will want to configure at least one service (Web, FTP, Filesharing or E-mail) to take advantage of the share you have created.

To begin editing a Flexshare, you'll need to select which access point you want to modify.

Select the appropriate configuration and use the help sections below to guide you through each type of access point and the options that are available.

<note warning>Changes will take place immediately upon clicking either the **Update** or **Enable/Disable** links for the access point you are configuring.</note>

=== Web ===
Configuring Flexshare's **Web** access enables anyone (or authorized users only) to use a web-browser to navigate to a website in order to view content, interact with a dynamic web page (for example - a PHP or CGI enabled online store) or download files from an index listing.

The rest of this section will describe the different settings that will modify the behavior of a Web accessible Flexshare.

== Enabled ==
Indicates the current status of the //Web Access// for a Flexshare.  Note, even though the Web Access point is enabled, the overall Flexshare must also be //Enabled// in order to work.
Use the //Enabled/////Disabled// link at the bottom of the form to toggle the status.

== Last Modified ==
A timestamp indicating the last time a change was made to the Web Flexshare configuration.

== Server Name ==
The server name (domain name) that will be used to access this Flexshare.  If the default ports are being used (for example, 80 for HTTP or 443 for HTTPS), this parameter is locked to the //Server Name// field defined in the [[:content:en_us:5_web_server|Web Server configuration]].  If custom ports are used, you can set this parameter to take advantage of Apache's Virtual Host capability.

== Server URL ==
This field (actually a hyperlink for convenience) indicates the URL which will be used to access the share.

== Accessibility ==
Accessibility allow you to restrict which interfaces incoming requests to the share are allowed from.  Setting this field to //LAN Only// essentially makes your Flexshare accessible from your Intranet only.

<note warning>Firewall Configuration | If set to //All//, make sure you have added the appropriate incoming firewall rule if the server is the gateway, or forwarded the appropriate port on your firewall.</note>

== Show Index ==
If //Show Index// is set to **Yes**, browsers will display a listing of all files if there is no index page (for example, index.html, index.php etc.).  This is normally only desirable if using the Flexshare as a file access service (similar to FTP).  If you are running a website, this option should definitely be set to **No**.

== Require SSL (HTTPS) ==
Determines the protocol to use - HTTP or HTTPS.  If you have enabled authentication, you are advised to set this to **Yes** (use HTTPS) since users will be required to provide their username/passwords to authenticate to the server.  Using HTTPS ensures this sensitive data is encrypted.

== Override Default Port ==
In some cases (for example, an ISP that blocks port 80), you may want to run the server on a non-standard port.  In this case, set this field to **Yes** and supply a valid port for the service to bind to.

== Require Authentication ==
If set to **Yes**, upon first connecting to the server, a user (for example, web client) will be prompted with a login dialog pop-up where they will enter their username/password.  Before gaining access to the Flexshare, the username/password will be confirmed as a valid account on the server.  In addition, the user **must belong to at least one group** that has been given access to the share. 

== Web Domain (Realm) ==
Indicates to the person logging in what realm they are attempting to access.  The only time the value of this field is displayed in during the authentication process.  In the screenshot above, the text "Sales Team Secure Flexshare" is the //Web Domain (Realm)// entry.

== Enable PHP ==
Enables the execution of PHP script on the server.  Any file with a .php/php4/php5 extension will be parsed by the PHP engine rather than by Apache directly.

== Enable CGI ==
Similar to the PHP field above, but pertaining to CGI script.  CGI script, however, is isolated to the **/cgi-bin** sub-directory (for example, <nowiki>http://example.com/flexshare/sales/cgi-bin/store</nowiki>).

=== FTP ===
Configuring Flexshare's **FTP** access enables anonymous or authorized users only (or both) to use an FTP-client to connect via File Transfer Protocol in order to upload and/or download files to the server.  The FTP protocol, while outdated, is still a prominent service today and is particularly useful for handling large files.

<note info>One of the downsides of the FTP protocol is that it uses separate ports to control dataflow and transmit payload data which causes conflicts with firewalls (both server and client side).</note>

== Enabled ==
Indicates the current status of the //FTP Access// for a Flexshare.  Note, even though the FTP Access point is enabled, the overall Flexshare must also be //Enabled// in order to work.  Use the //Enabled/////Disabled// link at the bottom of the form to toggle the status.

== Last Modified ==
A timestamp indicating the last time a change was made to the FTP Flexshare configuration.

== Server URL ==
The FTP URL (or domain name) used to access the service.  This parameter is locked to the //Server Name// field defined in the [[:content:en_us:5_ftp|FTP Server]] configuration.

== Require SSL (FTPS) ==
Determines the protocol to use - FTP or FTPS.  If you have enabled authentication, you are advised to set this to **Yes** (use FTPS) since users will be required to provide their username/passwords to authenticate to the server.  Using FTPS ensures this sensitive data is encrypted.

== Override Default Port ==
Flexshare FTP/FTPS uses port 2121/2120 and 2123/2122 as the default ports (see bubble below for an explanation). You can override these standard ports by setting this parameter to **Yes** and entering the custom ports in the fields that will appear upon changing the override drop-down.
<note warning>Virtual Hosts | Unlike the Apache web-server, the ProFTP FTP-server lacks **true** virtual host capability, restricting the server domain to a single entry.  As a result, the ProFTP server default ports for FTP and FTPS have been set to 2121 and 2123 respectively to allow users/administrators to continue to the default configuration file for FTP for their own custom use (for example, users home directories etc.).</note>

== Allow Passive (PASV) ==
Allowing passive connections can improve the experience/usability of FTP access to clients accessing the service outside the local network.  However, care must be taken to open or forward appropriate ports to your network for the port range you designate for passive exchange.  For more information on Active vs. Passive connections, see below.

== Require Authentication ==
If set to **Yes**, non-anonymous authentication is required.  Before gaining access to the FTP Flexshare, the username/password will be confirmed as a valid account on the server.  In addition, the user **must belong to at least one group** that has been given access to the share. 

== Group Greeting ==
A greeting that is displayed once when a user authenticates and has access to the FTP Flexshare.

== Allow Anonymous ==
Allows anonymous FTP access.  Users only have to provide the username //anonymous// and (usually) their e-mail address to gain access to the share.   Use anonymous when you are not providing access to restricted files and you do not want/need to create individual accounts on your server to authenticate against.

== Anonymous Greeting ==
Same as [[:content:en_us:5_flexshares#Group_Greeting|Group Greeting]] except applied to the anonymous login.

== Anonymous Permissions ==
Same as [[:content:en_us:5_flexshares#Group_Permissions|Group Permissions]] except applied to the anonymous login.

== Anonymous Upload Attributes ==
Same as [[:content:en_us:5_flexshares#Group_Upload_Attributes|Group Upload Attributes]] except applied to the anonymous login.

=== File ===
Configuring Flexshare's **File** access (SAMBA) enables public or authorized users only (or both) to connect via file sharing in order to move files from desktop to the server and vice-versa.

== Enabled ==
Indicates the current status of the //File Access// for a Flexshare.  Note, even though the File Access point is enabled, the overall Flexshare must also be //Enabled// in order to work.
Use the //Enabled/////Disabled// link at the bottom of the form to toggle the status..

== Last Modified ==
A timestamp indicating the last time a change was made to the File Flexshare configuration.

== Comment ==
Allows a comment or description of the fileshare to be displayed to other computer clients accessing the share.

== Public Access ==
Set //Public Access// field to **Yes** if you want to allow anyone on the Local Area Network (LAN) access to the Flexshare.

== Permissions ==
The //Permissions// field determines what type of access group members (or public if set) they have to files on the share.

== File Write Attributes ==
If users have write permission to this flexshare, setting this field will set all files copied to the server with the appropriate permissions.  See [[:content:en_us:5_flexshares#Group_Upload_Attributes|Group Upload Attributes]] for information on these settings.

=== E-mail ===
Configuring Flexshare's **E-mail** access allows the uploading of files to the server.  This is accomplished by simply attaching one or more files to the an e-mail and sending it to the corresponding Flexshare e-mail address.  To place restrictions on who can upload files, mandatory digital signatures combined with group lists and a separate Access Control List (ACL) are imposed.

== Enabled ==
Indicates the current status of the //E-Mail Access// for a Flexshare.  Note, even though the E-Mail Access point is enabled, the overall Flexshare must also be //Enabled// in order to work.
Use the //Enabled/////Disabled// link at the bottom of the form to toggle the status..

<note warning>If disabled, all email sent to the Flexshare will automatically be deleted, regardless of the //Save Attachments// setting.</note>

== Last Modified ==
A timestamp indicating the last time a change was made to the E-mail Flexshare configuration.

== Email Address ==
The e-mail address that users will use to upload files to the Flexshare.

== Save Attachment Path ==
Possible options are:

    * **Root Directory** - files will be saved to ///var/flexshare/shares/FLEXSHARE_NAME//
    * **Mail Sub-Directory** - files will be saved to the ///mail// sub-directory off the root directory
    * **Specify in Subject Heading** - A user can specify the path they would like the file(s) uploaded to by using the format //Dir = PATH// in their subject, where PATH is the directory path to use

== Write Policy ==
Allows you to control overwrites if a file already exists.

== Save Attachments ==
Setting this field to //Require Confirmation// keeps messages (and their attachments) in the queue.  Any file attachments will only be saved when confirmed.

Set this field to //Automatically poll at 5 minute intervals// to have the server initiate a check for new messages and save the attachments automatically to the server.  These files will then be immediately accessible by the other Flexshare access methods.

== Notify on Receive (e-mail) ==
If the //Save Attachments// field is set to //Require Confirmation//, use the //Notify on Receive (e-mail)// field to enter a valid e-mail address to send an alert upon receiving new e-mails contains file attachments.

== Restrict Access ==
Set this to //Yes// to match an address to a system user or the ACL.

<note warning>It is highly recommended that the //Restrict Access// feature is enabled to prevent anonymous file uploads from occurring.</note>

== E-mail ACL ==
Add e-mails to the //E-mail ACL// (Access Control List) to allow non-system accounts access to upload files to the server via e-mail.

== Require Signature ==
Signing e-mail using digital signatures is the only way to verify e-mail is originating from the address it claims to be sent from.  Enabling this feature will discard any e-mails and the associated attachments which are //not signed//.

<note warning>Spoofing Return Address | It is a trivial task to //spoof// the **From Address** contained in an e-mail header.  Take advantage of 4.0's SSL Certificate Manager and use signed certificates to validate the sender's address.</note>

== File Write Attributes ==
Saved files to the server originating from e-mail attachments will use the permissions set in this field.  See [[:content:en_us:5_flexshares#Group_Upload_Attributes|Group Upload Attributes]] for information on these settings.
==== Deleting a Flexshare ====
Deleting a Flexshare that is currently defined can be done from the **Overview** page.  Click on the //Delete// link next to the share you wish to delete.  A form similar to the one shown below will be displayed requesting you to confirm your intention to delete the share.

Checking the //Delete all files and remove share directory// will do exactly that - make sure you no longer need **any** files in the share directory and all sub-directories or have backups located elsewhere.

<note info>Use the //Disable// share function instead of //Delete// in the event you want to remove share access temporarily but not lose all your configuration settings.</note>

===== Troubleshooting =====
==== Firewall ====
Remember to open up appropriate ports on your firewall if your intention is to allow access from outside your network.  Some common ports for Flexshare access services are listed below.

==== FTP Access Going to Home Directory Instead of Flexshare ====
If you have enabled FTP access and require authentication and you find that users are being sent to their home directories instead of the defined Flexshare, the solution is quite simple - the cause quite complex.
The problem stems from the fact that FTP does not support virtual domains and is attempting to resolve the system hostname in order to determine which configuration to use.  If you have an entry in your /etc/hosts file mapping your system hostname to your internal IP, users logging in from outside the network will experience the problem described above.  To fix the problem, use Webconfig and navigate to <navigation>Network|Settings|Local DNS Server</navigation>.  Remove the entry that maps your server hostname to the internal address (for example, 127.x.x.x or 192.168.x.x or 10.x.x.x).  Once you have done this, go to the FTP configuration and stop and then restart the service.

==== Out of Memory Error ====
When copying files from a Windows system to a Flexshare with audit logging enabled, a dialog box with the following may appear: //Cannot copy X: There is not enough free memory.//  This warning is caused by a Windows IRPStackSize issue when some types of software have been installed on the Windows system.  A solution is provided in the [[http://tracker.clearfoundation.com/view.php?id=24|software tracker]].
 
===== Links =====
  * [[http://www.proftpd.org/docs/directives/linked/by-name.html|ProFTP - List of Directives]]
  * [[http://slacksite.com/other/ftp.html|FTP - Active vs. Passive]]
{{keywords>clearos, clearos content, AppName, app_name, clearos5, userguide, categoryserver, subcategoryfileandprint, maintainer_dloper}}
