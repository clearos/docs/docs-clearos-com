===== IP Settings =====
This is place to learn how to configure your network, hostname and DNS servers.
===== Menu =====
You can find this feature in the menu system at the following location:

Network|Settings|IP Settings
===== Configuration =====
==== Settings ====
{{7_ip_settings_settings1.png}}

=== Network Mode ===
The ClearOS system can run in one of four different modes:
  * **Standalone Mode - No firewall** - for a standalone server without a firewall (for example, a file server) 
  * **Standalone Mode** - for a standalone server with a firewall (for example, a public web server)
  * **Gateway** - for connecting your LAN, DMZ, and/or HotLAN  to the Internet
  * **Trusted Gateway** This is an undocumented mode which allows ClearOS to act as a transparent in-line bridge. To get it set <code>MODE="trustedgateway"</code> in /etc/clearos/network.conf.

=== Hostname ===
A hostname is the full name of your system.  If you have your own domain, you can use  a hostname like //gateway.example.com//, //mail.example.com//, etc.  The hostname does require at least one period (.)
=== Internet Hostname ===
This is how you are known from the internet. It may be your poweredbyclear.com DDNS name or another name. It is used, for example, by OpenVPN when creating its client configs to say which FQDN the OpenVPN client should contact to make a connection.
=== Default Domain ===
Specify the Internet domain of this server. This is the domain name (e.g. example.com) of your organization and not the hostname of this server but it could also be the internal domain name that you want to use on your LAN.  If you do not have your own domain then you can use one of the free dynamic DNS hostnames provided by the [[http://www.clearcenter.com/Services/services.html|ClearSDN]].  Alternatively, you can also make one up:  //gateway.lan//, //mail.lan// and use it internally only.
==== DNS ====
{{7_ip_settings_DNS.png}}

On DHCP and DSL/PPPoE connections, the DNS servers will be configured automatically for your IP Settings.  Users with static IP addresses should use the DNS servers provided by your Internet Service Provider (ISP).\\
If you use automatic DNS servers they can be temporarily overridden until the next reboot. Or, If you un-check the Automatic DNS Servers box in the External interface, you can manually specify your DNS servers here.\\
If you are using Multi-WAN, please review the [[:content:en_us:7_ug_multiwan|MultiWAN User Guide]] on the topic of DNS servers.

==== Network Interfaces ====
{{7_ip_settings_interfaces.png}}

This is broken down into five normal sections, Ethernet, Virtual, VLAN, xDSL and Wireless. Some of the configuration is common between the sections. Sections will only show if interfaces of those types exist. Other sections such as Bonded will appear with [[kb_howtos_network_bonding|manual configuration]].

From this screen you can configure or delete interfaces or run speed tests on external interfaces.

<note>If you delete a PPPoE interface it will revert to its underlying Ethernet Interface for reconfiguration.</note>

==== Configuring Ethernet Interfaces ====
{{7_ip_settings_interface.png}}

=== Roles ===
When configuring a network interface, the first thing you need to consider is the network role in IP Settings.  Will this network card be used to connect to the Internet, for a local network, for a network with just server systems?  The following network roles in IP Settings are supported in ClearOS and are described in further detail in the next sections:

  * External - network interface with direct or indirect access to the Internet
  * LAN - local area network
  * Hot LAN - local area network for untrusted systems
  * DMZ - de-militarized zone for a public network

<note warning>On a standalone system, your network card should be configured with an external role, not a LAN role</note>

== External ==
The external role provides a connection to the Internet.  On a ClearOS system configured as a gateway, the external role is for your Internet connection.  On a system configured in standalone mode, the external role is for connecting to your local area network.

On ClearOS, you can have more than one external interface configured for load balancing and automatic failover.  See the [[:content:en_us:7_ug_multiwan|Multi-WAN user guide]] for details.

== LAN ==
The LAN (local area network) role provides network connectivity for your desktops, laptops and other network devices.  LANs should be configured with an IP address range of 192.168.x.x, 172.16.x.x-172.31.x.x or 10.x.x.x.  For example, you can configure your ClearOS LAN interface with the following settings:

  * IP: 192.168.2.1
  * Netmask: 255.255.255.0

In this example, all systems on your LAN would have IP addresses in the range of 192.168.2.2 to 192.168.2.254.

By default, all LAN's (including VLAN's) can communicate with each other. A LAN can also access a HotLAN but not vice-versa.

<note tip>To avoid problems with VPN's and obscure networking issues, it is recommended to avoid the 192.168.0.0/24 and 192.168.1.0/24 subnets. Also avoid 10.8.0.0/24 and 10.8.10.0/24 as they clash with the default OpenVPN subnets.</note>

== Hot LAN ==
Hot LAN (or "Hotspot Mode") allows you to create a separate LAN network for untrusted systems.  Typically, a Hot LAN is used for:

  * Servers open to the Internet (web server, mail server)
  * Guest networks 
  * Wireless networks

A Hot LAN is able to access the Internet, but is **not** able to access any systems on a LAN or ClearOS itself. As an example, a Hot LAN can be configured in an office meeting room used by non-employees. Users in the meeting room could access the Internet and each other, but not the LAN used by company employees.

The [[:content:en_us:7_ug_port_forwarding|firewall port forwarding]] page in webconfig is used to forward ports to both LANs and Hot LANs.

<note warning>Only one Hot LAN is permitted.</note>

== DMZ ==
In ClearOS, a DMZ interface is for managing a block of //public// Internet IP addresses. If you do not have a block of public IP addresses, then use the Hot LAN role of your IP Settings. A typical DMZ setup looks like:

  * WAN: An IP addresses for connecting to the Internet
  * LAN: A private network on 192.168.x.x
  * DMZ: A block of Internet IPs (e.g from 216.138.245.17 to 216.138.245.31)

Webconfig has a [[:content:en_us:7_ug_dmz|DMZ firewall configuration page]] to manage firewall policies on the DMZ network.

<note tip>For a further explanation of Network Roles please see the [[:content:en_us:kb_o_network_types_-_external_lan_hotlan_dmz|Network Types]] KB article</note>

=== Connection Type ===
==DHCP==
For most cable and Ethernet networks, DHCP is used to connect to the Internet. In addition, your system will have the DNS servers automatically configured by your ISP when the **Automatic DNS Servers** checkbox is set.
If you have an upstream proxy on your external interface you can configure it here.\\
You will rarely want to have an LAN interface configured as DHCP.

== PPPoE (for xDSL) ==
For PPPoE xDSL connections, you will need the **username** and **password** provided by your ISP.

<note tip>If you have an ADSL external connection, an MTU of 1492 is often to avoid issues speed and connectivity issues. 1500 is probably fine for VDSL.</note>
== Static ==
If you have a static IP, you will need to set the following parameters:
  * IP (typically ends in 1 or 254 for a LAN interface)
  * Netmask (e.g. 255.255.255.0)
  * Gateway (for external connections only - typically ends in 1 or 254)
For a LAN interface you will almost always want it to be static.

=== Automatic DNS Servers ===
For interface types DHCP and PPPoE the **Automatic DNS Servers** checkbox is set. If you would like to configure your own DNS servers (often required for [[:content:en_us:7_ug_multiwan|Multi-WAN]]) then leave this setting unchecked.\\

=== Upstream and Downstream Bandwidths ===
You can set them manually here or on the previous scree you can run a speed test on the interface by clicking on the speedtest icon:
{{7_ip_settings_speedtest.png}}

=== Upstream Proxy ===
If you have an upstream proxy on your external interface you can configure it here.
==== Configuring Virtual Interfaces ====
ClearOS supports virtual IPs.  To add a virtual IP address, click on the link to configure a Virtual Interface and add specify the **interface you want the IP address to be associated with**, the **IP Address** and **Netmask**.  ClearOS will determine the interface Role from the interface it is associated with. You will also need to create [[:content:en_us:7_ug_firewall_custom|custom firewall rules]] if the virtual IP is on the Internet.


<note info>Though you can access this virtual IP address on the ClearOS system, it is not possible to use a virtual IP as a LAN gateway.</note>

==== Configuring VLANs ====
Can be internal or external. You need to decide the NIC which the VLAN belongs to and allocate it's VLAN ID. Other settings are the normal Connection Type, IP Address, Netmask, Gateway, Enable DHCP Server, Automatic DNS Servers.

==== Configuring a Wireless Interface ====
The wireless interface has been removed from the IP settings for the moment. Any Wireless configuration needs to be done manually, although the app-wireless (from the command line) and DHCP Server still work.

<del>The Wireless section allows you to set specific Wireless options in addition to the normal ones. The interface can only be used to configure 802.11g networks. Many settings are similar to the Ethernet settings

<note>If you have a wireless NIC attached and you can see it here and you don't see any wireless options you may need to install the wireless options manually with:<code>yum install app-wireless-core</code>If you do not see the Wireless interface at all, you will need to troubleshoot the NIC drivers.</note>

<note tip>If you want to configure 802.11n or 802.11ac you will need to edit /etc/hostapd/hostapd.conf manually.</note>

== Mode ==
Choose between WEP (do not use in AP mode), WPA/PSK and WPA infrastructure (Radius).

<note>External interfaces must be WEP</note>

== SSID ==
This is the ID that will show as the network available when people search for wireless access points from their computer. Users will need to know this information as well in order to connect. You can use any UTF-8 characters for this field. 

== Passphrase/RADIUS Shared Secret ==
Depending on which mode you select, this will be the key used to authenticate users to the wireless or the wireless to the RADIUS server. You should only use ASCII characters for this field.
<note warning>If you switch to WPA Infrastructure mode then any Passphrase you had will be used for your Rasius Shared Secret, but the setting will disappear from the webconfig!</note>

== Channel ==
Either specify the 802.11g channel to use. Automatic does not appear to do anything in AP mode and used the previously selected channel
</del>
===== Configuration Extras =====
==== Isolating LAN's and VLan's ====
By default all LAN's and VLAN's can communicate with each other. It you want to isolate them, create a file /etc/clearos/firewall.d/05-isolatedlans and in it put:
<code>#!/bin/bash

# Source networking configuration.
. /etc/clearos/network.conf

# Bail if not ipv4
if [ "$FW_PROTO" != 'ipv4' ]; then
    return 0
fi

if [ -n "$LANIF" ]; then

    # Add isolation rules
    for iLAN in $LANIF; do
        for oLAN in $LANIF; do
            if [ "$iLAN" != "$oLAN" ]; then
                $IPTABLES -I FORWARD -i $iLAN -o $oLAN -j DROP
            fi
        done
    done

fi

# Really all the above should be inserted below the existing "RELATED,ESTABLISHED" rule
#  but this is a dirty fix. Delete the existing and add a new one.
$IPTABLES -D FORWARD -m state --state RELATED,ESTABLISHED -j ACCEPT
$IPTABLES -I FORWARD -m state --state RELATED,ESTABLISHED -j ACCEPT</code>Then restart the firewall with a:<code>systemctl restart firewall.service</code>

<note>LAN's will still be able to communicate with HotLAN's. This script would need to be extended further to cover communication to HotLAN's. You would need to repeat the inner "for" loop but iterate over "oLAN in $HOTIF" rather than "oLAN in $LANIF"</note>

==== Multiple HotLAN's ====
It is possible to have multiple HotLAN's but you would need add a firewall script /etc/clearos/firewall.d/04-hotlans and in it put:
<code>#!/bin/bash

# Source networking configuration.
. /etc/clearos/network.conf

# Bail if not ipv4
if [ "$FW_PROTO" != 'ipv4' ]; then
    return 0
fi

if [ -n "$HOTIF" ]; then

    # Clear all HotLAN rules
    for HotLAN in $HOTIF; do
        RULE_IDS=$($IPTABLES -nv --line-numbers -L FORWARD |\
            grep " $HotLAN *0\.0\.0\.0\/0 *0\.0\.0\.0\/0" | awk '{ print $1 }' | sort -rn)
        if [ -n "$RULE_IDS" ]; then
            for rule_id in $RULE_IDS; do
                $IPTABLES -D FORWARD ${rule_id}
            done
	    fi
    done
	
    # Add replacement HotLAN rules
    for HotLAN in $HOTIF; do
        if [ -n "$EXTIF" ]; then
            for WANIF in $EXTIF; do
                $IPTABLES -A FORWARD -i $HotLAN -o $WANIF -j ACCEPT
            done
        fi
    done
fi</code>Then restart the firewall with a:<code>systemctl restart firewall.service</code>

==== PPPoE with VLAN tag ====
If you want a PPPoE connection and need to specify a VLAN ID (e.g. 101 in the UK for VDSL), this is generally set in the modem so is nothing to do with ClearOS. However there is a modemless PPPoE set up used by some ISP's in some countries (Canada?) where the network cable from the ISP goes straight into ClearOS and there is a requirement to set a VLAN ID.

In theory, to do this, create a VLAN interface attached to your external interface, set the VLAN ID, Role=External, Connection Type=PPPoE and set your PPPoE username and password.

There is currently a bug in the Network Interfaces where you are not given the option to set the Connection Type to PPPoE. The workaround is to set the connection type to DHCP and save the interface. Then go back and edit the interface you've just created. You will then be able to set the Connection Type to PPPoE and input your username and password.
===== Troubleshooting =====
<note warning>
The two network cables coming from your box may need to be swapped. If you are having a hard time connecting to the Internet, make sure you try swapping the cables.</note>

In most installs, the network cards and IP settings will work straight out of the box.  However, getting the network up the first time can be an exercise in frustration in some circumstances.  Issues include;

  * Network card compatibility
  * Invalid networks settings (username, password, default gateway)
  * Finicky cable/DSL modems that cache network card hardware information

Here are some helpful advanced tools and tips to diagnose a network issue from the command line:

  * **[[http://www.clearfoundation.com/docs/man/index.php?s=8&n=mii-tool|mii-tool]]** displays link status and speed
  * **[[http://www.clearfoundation.com/docs/man/index.php?s=8&n=ethtool|ethtool eth0]]** displays links status, speed, and many other stats - not all cards support this tool
  * **[[http://www.clearfoundation.com/docs/man/index.php?s=8&n=ifconfig|ifconfig eth0]]** displays IP settings on eth0

{{keywords>clearos, clearos content, IP Settings, app-network, clearos7, userguide, categorynetwork, subcategorysettings, maintainer_dloper}}
