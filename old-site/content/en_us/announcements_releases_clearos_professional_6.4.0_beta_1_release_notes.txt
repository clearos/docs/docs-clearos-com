===== ClearOS Professional 6.4.0 Beta 1 Release Notes =====
**Released: Not yet released**

ClearOS Professional 6.4.0 Beta 1 has arrived!  Along with the usual round of bug fixes and enhancements, this release introduces a new reports engine, a storage manager, an antimalware file scanner.  A number of new reports are available with the release of the reports engine:

  * [[:content:en_us:6_filter_and_proxy_report|Filter and Proxy Report]]
  * [[:content:en_us:6_resource_report|Resource Report]]
  * [[:content:en_us:6_network_report|Network Report]]

The new apps in the alpha are not available via the Marketplace.  Instead, you can use **yum** to install.  You can find installation and app details further below.

===== What's New in ClearOS Professional 6.4.0 =====
The following is a list of features coming in ClearOS Community 6.4.0.

  * [[:content:en_us:6_filter_and_proxy_report|Filter and Proxy Report]]
  * Intrusion Protection Report
  * [[:content:en_us:6_resource_report|Resource Report]]
  * [[:content:en_us:6_network_report|Network Report]]
  * [[:content:en_us:6_storage_manager|Storage Manager]]
  * [[:content:en_us:6_imap_and_pop_server|Basic IMAP/POP Server]]
  * [[:content:en_us:6_upstream_proxy|Upstream Proxy Support]]
  * [[:content:en_us:6_antimalware_file_scan|Antimalware File Scan]]
  * Amazon EC2 Images
  * Samba 4 Directory (Beta)

===== Download =====
Not yet available.

{{keywords>clearos, clearos content, announcements, releases, clearos6.4, beta1, previoustesting, maintainer_dloper}}

