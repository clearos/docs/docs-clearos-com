===== Access Control =====
Time-based Access Control allows an administer to enforce time-of-day web access to users or computers (IP or MAC address) using the web proxy. 

===== Installation =====
If you did not select this module to be included during the installation process, you must first [[:content:en_us:5_software_modules|install the module]].

===== Menu =====
You can find this feature in the menu system at the following location:


<navigation>Gateway|Proxy and Filtering|Access Control</navigation>

===== Configuration =====
==== Adding Time Periods ====
Time periods define the day of week (for example, Monday, Tuesday ...) and the time of day (for example, 12:00 - 13:00) that an access control rule should apply. Select <button>Add/Edit Time Period</button> from the webconfig tab menu to:

  * display and/or edit a currently defined time period
  * add a new time period definition
  * delete an existing time period definition 

<note warning>Deleting a time period will delete any access control rule that depends on the time period definition being deleted.</note>

In the sample screenshot below, we have created two time period definitions. The first defines a lunch break on weekdays between 12:00pm and 1:00pm (13:00). The second covers the entire day over a weekend (Saturday and Sunday).

{{:omedia:ss_squid_acl_time.png|}}

==== Adding Access Control Lists ====
An unlimited number of access control list definitions can be created to customize precisely how users or machines on the LAN should be given access to the web via the proxy server. In the example below, a rule to allow all machines on the LAN to have access to the web during the weekend is being created.  By specifying an internal IP range of 192.168.1.100 to 192.168.1.255, the IP based identification will apply this rule to all computers on the LAN receiving a DHCP lease in this IP range.

{{:omedia:ss_squid_acl_add_rule.png|}}

=== Name ===
A unique name identifying the access control.

=== ACL Type ===
Sets the ACL rule type - allow or deny. Allow provides web access to the user/computer...Deny forbids web access.

=== Time-of-Day ACL ===
References a unique time of day rule. The drop down menu will be empty and a link to create a new time period will be displayed if no time definitions have been created.

=== Restriction ===
Determines whether the ACL rule will apply to the time period defined or all time outside of the time period defined. For example, if you defined a time period name Lunchtime that was defined as 12:00 - 13:00 from Monday to Friday and you wanted a specific rule to apply during the lunch hour, select Within. Conversely, if you wanted a rule to be applied for all hours outside of the lunch period, you would select Outside.

=== Method of Identification ===
Depending on your proxy configuration, up to three different methods of user/machine identification are possible - username, IP address and MAC address.

**Username** - Username-based authentication is only available if you have User Authentication enabled. Users must provide login credentials 'and' have access to the proxy server (as defined by the user account configuration. Once logged into a proxy session, ACL rules based on username Will apply.

**IP Address** - To restrict web access to a particular computer or multiple computers (for example, a computer lab), IP address based identification can be used. A single IP address or a range of IP addresses (separated by a dash) can be added. Valid entry examples include:

  * 192.168.1.100
  * 10.0.0.121
  * 192.168.1.100-192.168.1.150 

The IP address represents the address of the machine connecting to the proxy. Since the computer is located on the LAN segment of the network, any IP address or range listed here should be restricted to an | internal IP address or range.

***MAC Address*** - A MAC address is a unique identifier originating from a computer's network card. MAC addresses can be a good alternative to IP addresses if an administrator does not lock down the network settings of a machine which might allow a savvy user to get around an IP address-based access control rule. A MAC address must be obtained from the machine and is dependent on the OS.  See //Tips and Tricks// for information on how to determine a MAC address.

=== ACL Priority ===
New ACL rules are added to the bottom of the list... that is to say, new rules begin with the lowest priority.

The proxy server analyzes each rule in successive order... starting from the top and working through each rule. The first rule to match a true condition stops the processing and allows (or denies, depending on the rule type) access to the web.

In the example below, there are three rules...AllEmployees has the highest priority, followed by LunchHourStaff and finally (lowest priority) HourlyEmployees.

{{:omedia:ss_squid_acl_summary.png|}}

To understand priorities, it is probably easiest to follow through a few scenarios.

Saturday - since it is a weekend, and through the creation of the AllEmployees rules, all IP address on the LAN have be defined in the creation of the ACL, all computers on the LAN will have access to the web, regardless of MAC or username based ACL's and regardless of whether it is lunch hour (for example, 12pm - 1pm) or not. In this case, the first rule (All Employees) applies (returns true) and processing of further rules is not performed.

Monday @ 12:15pm - All users who are using computers whose IP's have been added to the LunchHourlyEmployees IP list will have access to the web.

Monday @ 1:15pm - All users who are using computers whose IP's have been added to the HourlyEmployees IP list will be denied access to the web. This is because the third rule is applied since the first two rules did not return a true statement. Any user who is using a computer whose IP is not listed in the HourlyEmployees rule will be allowed access to the web.
	  	
{{:omedia:ss_squid_acl_priority.png |}}
By default, if no ACL rules return true (for example, are executed as allow/deny) a user is allowed access to the web. To apply a blanket block rule, create an IP range ACL using the deny type along with a time definition from 00:00 - 24:00. 	 

Use the up and down arrows on the ACL Summary page to bump the priority of any ACL rule you create in order to enforce time of day web access.

===== Tips and Tricks =====
==== Finding a MAC Address ====
=== Linux ===
On Linux, you can usually find the MAC address using the network configuration tools.  From the command line, you can also use the //ifconfig// command, for example:

  ifconfig eth0

Where eth0 represents the network (Ethernet) card. 

=== Windows ====
To obtain the MAC address under Windows, click on the <button>Start</button> button and select the //Run// menu option. Enter //cmd// in the run field. Once you are at the Windows command prompt, type:

  ipconfig /all

and click enter.  You can find the MAC address next to the //Physical Address// field.  Make sure you get the MAC address of the correct device... there may be more than one if you have both a network card and wireless networking card.

===== Links =====
{{keywords>clearos, clearos content, AppName, app-web_access_control, clearos5, userguide, categorygateway, subcategoryproxyandcontentfilter, maintainer_dloper, maintainerreview_x, keywordfix}}
