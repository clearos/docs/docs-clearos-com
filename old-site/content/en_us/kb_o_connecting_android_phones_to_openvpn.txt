===== Connecting Android Phones to OpenVPN =====
This guide covers connecting Android phones to ClearOS using OpenVPN. This guide is a generalized how to and not a specific howto as Android versions and phone types are diverse as are the many OpenVPN clients. This howto version covers the "OpenVPN for Android" by Arne Schwabe.

===== Prerequisites =====
Ensure that OpenVPN is installed and running. Also validate that you are allowing port 1194 to come into your server in the Incoming firewall rules.

From a workstation, log in as the user that will be connecting to the OpenVPN at the Webconfig interface using that user's name and password. If you have not already set a password for their certificate, do so now. Download the following to a regular workstation:

  * Certificate
  * Certificate Authority
  * Private Key

Once you have these files, you will need to download them to your Android device. For my test, I copied the files to my user's home directory and used "ES File Explorer" to copy and paste them on my phone in a folder called 'openvpn-certs'.

Once you have the keys, download and install the VPN client. As stated previously, we used "OpenVPN for Android" by Arne Schwabe.

===== Configuring OpenVPN for Android =====
Launch the application and choose 'Add' to add a profile. Give it a name. Navigate to the settings section of that profile. Under 'Basic', enter your server IP address or hostname.

Under 'Type', select **User/PW + Certificates**.

For the CA Certificate, Client Certificate, and the Client Certificate Key choose the **ca-cert.pem**, **client-username-cert.pem**, and ** client-username-key.pem** files respectively.

In the **Username** field, type the user's name. You may specify the password or for even better security, leave it blank and require it each time the user connects.

{{keywords>clearos, clearos content, AppName, app_name, kb, howto, xcategory, maintainer_dloper, maintainerreview_x, keywordfix}}
