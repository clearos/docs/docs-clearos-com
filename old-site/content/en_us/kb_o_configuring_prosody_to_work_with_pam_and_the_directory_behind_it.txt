===== Configuring Prosody to Work with PAM and the Directory Behind it. =====
<note>This document is a work in progress. Please check back regularly. It works as is, and we want to make it even easier to install and to eventually become a package.</note>

This guide will walk through the process of converting the typical build of prosody into one that relies on the centralized PAM architecture of ClearOS. While there are several ways to authenticate users (through RADIUS, direct LDAP queries, and others) this guide will focus on the use of SASL as the authentication broker. Using SASL allows us to keep the authentication encrypted within the subsystem so that we can broker the authentication more securely against PAM. Additionally, by using PAM instead of directed queries to a directory structure, we are able to apply this solution to a broad range of backends without adding complexity to the model.

===== Setup =====
This assumes that you have set up Prosody already according [[:content:en_us:kb_o_setting_up_prosody_on_clearos|to this guide.]]

<note>Your Directory services should be running on ClearOS before proceeding. Ensure that you can see the users in your directory and at the minimum, you have users listed in your user list.</note>

Once you have completed this, you need to ensure that the **saslauthd** service is running.

  service saslauthd status

If it is not running, start it:

  service saslauthd start

Also set it to automatically start:

  chkconfig saslauthd on && chkconfig --list saslauthd

Should return:

  saslauthd      	0:off	1:off	2:on	3:on	4:on	5:on	6:off

==== cyrussasl.so ====
You will need a copy of **cyrussasl.so** in your /usr/lib64/lua/5.1/ directory. Currently you have to either build or copy this file as there is no RPM yet for it.

==== cyrus-sasl-plain ====
You will also need to install the cyrus-sasl-plain module:

  yum install cyrus-sasl-plain

Restart saslauthd

  service saslauthd restart

===== Configuration =====
==== SASL ====

You will need to create a configuration file for Prosody in SASL. Create the file using your favorite editor (vi, nano, et al):

  vi /etc/sasl2/prosody.conf 

Insert the following text:

  pwcheck_method: saslauthd
  mech_list: plain 

Restart saslauthd:

  service saslauthd restart

==== PAM ====
You will need to create a PAM plugin for the XMPP processes. To do that, modify and create the following using your favorite editor (vi, nano, et al):

  vi /etc/pam.d/xmpp

Insert the following text:

<code>
#%PAM-1.0
auth       include  password-auth
account    required pam_listfile.so item=group sense=allow file=/etc/clearos/xmpp.d/authorize onerr=fail
account    include  password-auth
</code>

To test this, identify a user in your directory that you can log in with ('aschmoe' in my case) and their password ('asdfasdfasdf' is what I used in test.)

  sudo -u prosody testsaslauthd -s xmpp -u aschmoe -p asdfasdfasdf

This should return

  0: OK "Success."


==== prosody.cfg.lua ====
Most of what makes Prosody work with the existing Directory Services and PAM is located in the /etc/prosody/prosody.cfg.lua configuration file. If you have followed the other guide, you should have already made changes to enable logging and to start your virtual domain.

In addition you this, you will need to change the authentication to use the local Cyrus/PAM infrastructure.

Change the following parameters in the /etc/prosody/prosody.cfg.lua configuration file:

<code>
authentication = "internal_plain"
</code>

To:

<code>
-- authentication = "internal_plain"
authentication = "cyrus"
cyrus_application_name = "prosody"
cyrus_service_name = "xmpp"
</code>

==== Setting up group-based authentication ====
Similar to the SMTP PAM module you will need to create an authorization file and a group for XMPP.

Make the following:

  mkdir /etc/clearos/xmpp.d
  echo xmpp_plugin > /etc/clearos/xmpp.d/authorize

Make a group in ClearOS called '**xmpp_plugin**' and add your users to this group.

{{keywords>clearos, clearos content, AppName, app_name, kb, howto, xcategory, maintainer_dloper, maintainerreview_x, keywordfix}}
