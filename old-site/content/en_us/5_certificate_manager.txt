===== Certificate Manager =====
SSL certificates are the de-facto standard for encrypting information sent over a network and can also be used to provide authentication, as in the case of SMIME email signature signing.

This module provides an administrator with the ability to create a Certificate Authority (CA) which can then be installed as a trusted CA on any operating system, browser or mail client in order to encrypt/decrypt (and/or sign emails) communications between two computers.  Creating your own CA and using it to sign certificates is termed "self-signing".

Self-signing of certificates is as secure as purchasing signed SSL certificates from a //Trusted CA// like Thawte or Verisign, where prices range from $US 50-300 per year.  Self-signing is extremely convenient (and cost effective!) if you are providing access to known users (for example, employees, clients, vendors etc.).  It is less convenient than a //Trusted CA// when dealing with unknown users such as website visitors using a browser to access your online store using HTTPS (HTTP over SSL), since the user will be prompted by their browser to trust the certificate that is presented to them.

The SSL Certificate Manager module can also create Certificate Signing Request (CSR) certificates.

A CSR is an unsigned copy of your certificate which can then be sent to a //Trusted CA// to be signed.  The CSR will be used by the //Trusted CA// to generate your signed x509 SSL certificate (CRT).  The //Trusted CA// sends back the signed certificate which may look similar to the CSR, but is not.

Whether your CRT was self-signed or signed by a //Trusted CA//, it now represents the public part of a public/private key (certificate) pair.  The private half of the key (usually ending in .key or -key.pem) was generated automatically during the CSR creation and //should never be sent across an untrusted network// (for example, the Internet).  Unless this key was intended to secure another server, it should not be moved from its directory of origin (/etc/ssl/private).

===== Installation =====
This module is installed by default and should not be un-installed.  SSL certificates are used by the webconfig User Interface.

===== Configuration =====
==== Creating a Certificate Authority ====
A Certificate Authority (or CA) is a trusted entity which issues digital certificates for use in cryptography and/or authentication.  When dealing with unknown persons, you will probably want to use a commercial CA which is in business to provide a service - verifying an individual or organization is who they say they are, usually by way of a domain name or email address.

The SSL Certificate Manager module allows you to create your own CA that one can then use to sign and validate certificates.  You can have users download and import this CA to validate certificates presented to them.  A common and cost-effective use of a self-signed certificate is the SSL certificate that encrypts communications in the webconfig User Interface.

The module will force you to create a CA prior to allowing the creation of certificates requests, signed certificates or PKCS12 files.  The form to create the CA is presented when no CA is found on the server (in the /etc/ssl directory).  A brief description and suggested defaults is provided in the following sections.

=== Key Size ===
This is the RSA key length.  1024b (default) is a good compromise between security and speed.  Anything below 1024b can theoretically be cracked by brute force techniques.  Note, this is the RSA key size and will not impact, for example, the encryption strength of a web browsing session (typically 128b, but could be 40b or 256b) that is dictated by the capabilities/settings of both the client web-browser and server.

=== Common Name ===
The common name in the certificate authority can be anything.  Generally speaking, you will want this to be descriptive of the purpose of the certificate as a trusted root certificate.  An example might be //Point Clark Networks Root Certificate Authority//.
=== Organization Name ===
Typically the company name or person responsible for the CA.  Example - //Point Clark Networks Ltd//.
=== Organization Unit ===
In larger organizations, the organization unit might be a department within the company, such as //IT Department//.
=== City ===
The organization's city - for example, //Toronto//.
=== State/Province ===
The organization's state or province - for example, //Ontario or ON//.  Leave blank if this does not apply.
=== Country ===
The organization's country - for example, //Canada//.  The module will automatically convert the country to the 2-letter ISO-3166 country code.
=== E-mail ===
The e-mail address of the person responsible for the CA within the organization - for example, //certificates@pointclark.net//.

==== Creating a Certificate Request or Signed Certificate ====
Once a Certificate Authority has been created on your server, you will see a summary of the CA and any certificates you have created.  If you have only just created your CA, you obviously won't have any signed certificates or PKCS12 files.

Use the form below the three summary tables as illustrated above to create either a certificate request or signed certificate.  For those new to SSL and encryption, it may not be immediately obvious as to the difference.
=== Certificate Request ===
The certificate request is a pre-cursor to creating a signed certificate.  It represents the public half of the private/public key pair used in RSA encryption.  **All** signed certificates originate from a certificate request.  A certificate request does not have an expiry date associated with it, but does have all the other fields associated with a signed certificate (common name, organization name etc.).  A certificate request is cannot be used in cryptography and must be signed (usually from a trusted CA for an annual fee) in order to be useful.
=== Signed Certificate ===
As the name implies, this is a public certificate (the public half of the RSA private/key pair) that has been signed (verified) by a Certificate Authority (CA).  The CA's service to the certificate holder and to anyone viewing the certificate is as a 3rd party validator as to the authenticity of the certificate owner.  For example, if the certificate is to be used on an encrypted website (HTTPS), the CA will take measures to verify the owner of the domain against the certificate request being presented to be signed.  A signed certificate has both a not-valid before and non-valid after timestamps that was //attached// to the certificate when the CA signed the request.
==== Creating a Certificate Request ====
If you have determined a need for a trusted CA to sign a certificate request, you can use the webconfig UI to generate the key.  Select the purpose for the certificate (web/FTP encryption or e-mail signing/encryption) and your RSA key size (1024b recommended) and select //Use Trusted CA (fees may apply)// option from the **Signing Authority** field.  Complete the other fields as they apply (see [[#Troubleshooting|troubleshooting section]] below) and click **//Create//**.

Notice how the //Term// field disappears when you selected //Use a Trusted CA// option - this is by design, since certificate requests do not store expiry dates.
==== Creating a Signed Certificate ====
Selecting the //Self-Sign// option will use the CA you created during the initializing of the SSL module to sign a certificate request that is temporarily created during the creation process.

Two differences to note from the creation of a certificate request.  First, there is an additional //Term// field - this field indicates the expiry date from the date of creation.  For convenience, some users may want to set this to 25 years (essentially no expiry), but lesser terms may be desired  for some applications.  Second, additional fields named //Import Password for PKCS12// and //Verify Password for PKCS12// are visible.  The Personal Information Exchange Syntax Standard (also called PKCS12) file is a convenient format to install certificates onto client machines for use in validating e-mail signatures.  The file is protected with a password since the PKCS12 file contains both the private and public keys associated with the SSL signed certificate.

==== Importing a Signed Certificate from a Trusted CA ====
In order to import a signed certificate from a trusted CA, you first need a Certificate Request.  If you haven't made one already follow the steps [#Creating_a_Certificate_Request here].  Certificate requests (also known as unsigned certificates) will be listed in the //Unsigned Certificates//.

This request needs to be downloaded and sent (typically via e-mail or a web form) to a //Trusted CA//.  Click on the **View** link to view the contents of the certificate, including the part a //Trusted CA// requires.

At this point, you have two options to download the certificate request.  First, use the **Download** link to save the entire PEM file to your local machine.  The second option is to simply select the //PEM Contents// text starting from //-----BEGIN CERTIFICATE REQUEST-----// and ending (and including) the //-----END CERTIFICATE REQUEST-----// tag with your mouse, and "cut-and-paste" this into an e-mail to be sent to a //Trusted CA// or a web form for submission.
Once you receive the signed certificate back from the //Trusted CA// (a process that make take up to 48 hours), return to the SSL webconfig page, click on **View** again, and this time, select **Import Signed Certificate** from the available //Actions//.  A web form will be displayed allowing you to "paste" the certificate contents.

Once "copied-and-pasted" into the form, click **Save**.  Your certificate is now imported and ready for use.

==== Creating, Importing & Installing a Personal Information Exchange Syntax Standard File (PKCS12) ====
The Personal Information Exchange Syntax Standard (or PKCS12) file is an industry standard format for storing or transporting a user's private keys, certificates or other //secret// information.  The PKCS12 file format is used with the SSL module in ClearOS' webconfig to password-protect and relate a private key tied to an e-mail address with a certificate authority in order to sign and/or encrypt e-mail.
=== Creating a PKCS12 File ===
A PCKS12 file is created automatically when a self-signed certificate is created with the //Purpose/Use// is set to **Sign/Encrypt E-mail**.  See section [[#Creating a Signed Certificate|Creating a Signed Certificate]] for information related to the fields/settings to create the PKCS12 in parallel with a self-signed certificate.
To create a PKCS12 file, you should already have a signed certificate under management with the appropriate e-mail that will match the user's signature (for example, e-mail address).  
To start the PKCS12 creation, click on the **View** link next to the certificate.  Details of the certificate along with several actions which can be executed on the signed certificate will be displayed.

If you **do not see the Create PKCS12** option, it is because it already exists on the system.  Return to the main menu and look under the **PKCS12 Files** table.
Since the certificate already exists, you only need to provide the password and verification that will be used to secure the PKCS12 file.

Clicking on the "Create" button will create the PKCS12 file using the password supplied and list it for download under the PKCS12 section.  See the next sub-section for information on downloading and installing the file to your computer.

=== Importing a PKCS12 File ===
Provided you have been successful in creating a PKCS12 file, you should see thees files listed under the **PKCS12 Files** table.  You can delete these files at any time, with the knowledge that the file can be re-created with a new password, if necessary, at any time.  Since the PKCS12 file is specific to a user, once provided to the user, there is no need to keep the file on the server, except for purposes of backup.  The screenshot below shows the PKCS12 summary, containing one file for //Joe Developer//.  Assuming we are //Joe Developer// or Joe's IT administrator, we will now go through the steps to import (download) the PKCS12 file and install it.

Click on the //Download// link next to the PKCS12 you wish to download to your local machine (computer).  Depending on your OS and browser, you will see a dialog box.

If access is from the machine where the file will be installed, you can choose the "Open With" which uses the PFXFile binary in Windows.  If you will be e-mailing or making the file available to download via alternative ways (for example, FTP), you'll need to "Save to Disk" to save a copy of the PKCS12 file locally.
<note warning>Installing on Thunderbird | If you use Mozilla's Thunderbird e-mail client, you need to use the "Save to File" option and import into the client in a separate step (see below).</note>

=== Installing a PKCS12 File ===
Examples have been provided for installing PKCS12 files into two of the more popular mail clients, Thunderbird and Outlook/Outlook Express.
== Thunderbird ==
Before starting, make sure you have downloaded or received your PKCS12 file and saved it to your local machine.  If you have not yet done this, see instructions provided in the above sections.
Open the Thunderbird mail client and click on **//Tools --> Account Settings//**.  Click on the //Security// summary under your account.
Click on //View Certificates// under the **Certificates** section.  Under the **Your Certificates** tab, click on //Import//.  Use the filemanager dialog pop-up to select the PKCS12 file you saved to your computer earlier.  At this point, you may be prompted to created a master password for the security device.  Choose a password you can remember but also difficult for anyone to guess.  You will need to use this password each time you close and re-open Thunderbird to send a signed or encrypted e-mail.
You will then be prompted for the password for the PKCS12 file you are about to import.  This is the password that was used during the creation of the PKCS12 using the ClearOS SSL Manager module.  You should now see your certificate installed under **Your Certificates**.

You're not quite done - note how the //Purposes// field indicates //Issuer Not Trusted//.  What you did not see happen transparently when installing the PKCS12 file is the import of a trusted CA under the **Authorities** section.  You need to explicitly confirm what purpose **Your Certificate** can be used for.  Click on the **Authorities** tab and scroll down until you find the Certificate Authority that was used to sign the certificate used to create the PKCS12 file.  When you find your CA in the list, click once to highlight it and then click on the //Edit// button.  A pop-up dialog box will be displayed.

Place a checkmark in each checkbox, and click **OK**.  Go back to the **Your Certificates** - you should now see the message //Issuer Not Trusted// has been replaced with //Client, Server, Sign, Encrypt//.  Close the **Certificate Manager** dialog window and click on either of the //Select// buttons in the //Digital Signing// or //Encryption// sections.  You will be prompted to select a certificate from a dropdown box which will likely just have the one certificate you installed.  Select it, and click **OK**.  Close the **Account Settings** dialog window by clicking **OK**.
Congratulations - you can now sign e-mail and receive encrypted e-mail if senders use your public key to encrypt the message.

== Outlook/Outlook Express ==
Outlook and Outlook Express uses the Windows OS certificate manager to perform message signing and encryption/decryption.  The following help section describes how to install a PKCS12 file onto Microsoft's XP platform.
Click on **Start --> Control Panel** and select **Internet Options** from the menu system.  Select the **Content**.

Working in the **Certificate** dialog box pop-up, select the **Personal** tab and click on the //Import// button.  An //Import Wizard// will start up, taking you the process in steps.  Click //Next// to continue.  Click on the //Browse// button and find the PKCS12 file that you saved to your system.  Note, you may have to the default filetype from //X509// to //Personal Information Exchange// to see the proper extensions.  Click //Next// to continue.  The wizard will then ask you for the password.  Enter the password you used in the ClearOS SSL Manager module when creating the PKCS12 file.  It's also a good idea to check off both checkboxes for additional security.

Keep the default location to store the certificate - Personal Store.  Click //Next// to continue.  Click //Finish// to complete the PKCS12 install.  Unlike Thunderbird, Microsoft automatically enabled the uses for the certificate.

Congratulations - you can now sign e-mail with Outlook and receive encrypted communications from people using your public key.
==== Renewing a Certificate ====
Certificates that have been self-signed by the locally created Certificate Authority can be renewed at any time.  Click on the **View** link, followed by the **Renew** button under the action options.  A form similar to the one below will allow you to select the term to extend the original certificate in addition to re-issuing a new PKCS12 file with password.

When renewing a certificate that was not self-signed, a new certificate request will be created which can then be sent to a //Trusted CA// for signing and subsequent import.
===== Troubleshooting =====
There are really only two fields in the certificate generation process that can get you into trouble - Common Name and E-mail.  These fields are explained below in relation to the two typical applications of SSL certificates (web and email).
==== Web/FTP ====
=== Common Name Field ===
For websites or FTP, the **Common Name** field //must match exactly// the domain name of the site.
=== E-mail Field ===
Typically, this field would be the e-mail address of the web master or some alias referring back to support.
=== Example ===
Website URL:  https://secure.clearcenter.com/portal/
Common Name = secure.clearcenter.com
E-mail = admin@clearcenter.com
==== E-mail Signing/Encryption ====
=== Common Name ===
The common name is typically the full name of the individual.
=== E-mail Field ===
This field //must match exactly// the e-mail address of the sender who intends to include a signed signature and/or receive encrypted communications.

=== Example ===
E-mail Address of Sender:  joe.developer@clearfoundation.com
Common Name = Joe Developer
E-mail = joe.developer@clearfoundation.com

===== Links =====
  * [[http://www.openssl.org|OpenSSL]]
  * [[http://en.wikipedia.org/wiki/Public_key|Public Key Cryptography]]
{{keywords>clearos, clearos content, Certificate Manager, app-certificate_manager, clearos5, userguide, categorysystem, subcategorysecurity, maintainer_dloper}}
